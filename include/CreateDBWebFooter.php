<?php
// ------------------------------------------------------------------------------
//     Create DB System for MySQL
//		createDBColFooter.php
//    	charset=UTF-8
//
//      共通フッター
//
// Copyright (C) 2014 dbu@mac.com All Rights Reserved.
// ------------------------------------------------------------------------------
	//現在の日付を取得
	$today = getdate();
	$THIS_YEAR = sprintf( "%04d",$today["year"]);

	if ( $THIS_YEAR == '2013'){
		$cp_year = '2013';
	}else{
		$cp_year = '2013-'.$THIS_YEAR;
	}

print <<<END_OF_HTML

	</div>
	<!-- Contents end -->
	<!-- areaFooter begin -->
	<div class="footer_area">
	<!-- areaFooter begin -->
	<hr>
		<div class="fGlay09100" align="center">Copyright(C) {$cp_year} dbu@mac.com All Rights Reserved.</div>
	</div>
	</body>
</html>
END_OF_HTML;

?>
