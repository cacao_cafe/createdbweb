<?php
// ------------------------------------------------------------------------------
//     Create DB System for MySQL
//		createDBColHeader.php
//    	charset=UTF-8
//
//		HEADER
//
// Copyright (C) 2014 dbu@mac.com All Rights Reserved.
// ------------------------------------------------------------------------------

print <<<END_OF_HTML
<html>
	<head>
		<meta http-equiv="Content-type" content="text/html; charset=utf-8">
		<meta name="robots" content="noindex,nofollow">
		<meta http-equiv="X-FRAME-OPTIONS" content="SAMEORIGIN" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />

		<title>$HtmlTitle</title>
		<link rel="shortcut icon" href="./favicon.ico" type="image/vnd.microsoft.icon">
		<!--  css3Stylesheet -->
 		<link rel="stylesheet" href="./css/CreateDBWeb.css">
 		<script type="text/javascript" src="./css/respond.js"></script>

        <!-- jQuery -->
		<link rel="stylesheet" href="./js/jquery-ui.min.css">
		<script src="./js/jquery.js"></script>
		<script src="./js/jquery-ui.min.js"></script>

        <!-- SyntaxHighlighter -->
		<script type="text/javascript" src="./js/lib/SyntaxHighlighter/scripts/shCore.js"></script>
		<script type="text/javascript" src="./js/lib/SyntaxHighlighter/scripts/shBrushPhp.js"></script>
		<script type="text/javascript" src="./js/lib/SyntaxHighlighter/scripts/shBrushJScript.js"></script>
		<script type="text/javascript" src="./js/lib/SyntaxHighlighter/scripts/shBrushSql.js"></script>
		<link type="text/css" rel="stylesheet" href="./js/lib/SyntaxHighlighter/styles/shCore.css">
		<link type="text/css" rel="stylesheet" href="./js/lib/SyntaxHighlighter/styles/shCoreDefault.css">

		<script type="text/javascript" src="./js/fixed_midashi.js"></script>
        <!-- JavaScript -->
        <script src="./js/CreateDBWeb.js"></script>

	</head>
	<body onLoad="FixedMidashi.create();">

END_OF_HTML;

?>
