<?php
//------------------------------------------------------------------------------
//
//	MySQLを使ったDBI/Oｼｽﾃﾑ自動生成 Create DB Web System for MySQL with PHP&JAVASCRIPT
//			Copyright (C) 2015 ScenarioWriterCafe All Rights Reserved.
//
//		MySQL のTABLE構文を生成する
//
//		fncMakeCreateTableSyntax()
//
//   	charset=UTF-8
//------------------------------------------------------------------------------
function fncMakeCreateTableSyntax(){

    global  $ThisPHP,$DefaultDirPath;
    global	$SubmitMode;
	global	$areaRows;
	global	$cdbDbName,$cdbTableName,$cdbTableNameComment,$cdbSelectTable,$cdbDefaultCharSet;
	global	$cdbNameArray,$cdbCommentArray,$cdbTypeArray;
	global	$cdbColSizeArray,$cdbKeyArray,$cdbDefaultValueArray;
	global	$cdbObjTypeArray,$cdbObjSizeArray,$cdbListArray,$cdbReqArray;
	global  $maxColNameLength;
	global	$CreateTableSyntax;
    
	//現在の日付を取得
	$today = getdate();
	$THIS_YEAR = sprintf( "%04d",$today["year"]);

	//ﾃｰﾌﾞﾙ名からclass名を編集
	$cdbTableName = strtoupper($cdbTableName);

	$phpName = 'createTable_'.CreateDBWebFuncEditName($cdbTableName).'.php';
	
	$str = <<<END_OF_PHP
<?php
// -----------------------------------------------------------
//
// Copyright (C) $THIS_YEAR ScenarioWriterCafe All Rights Reserved.
// 
//     CREATE TABLE $cdbTableName
//
//     $phpName
// -----------------------------------------------------------

END_OF_PHP;

	$CreateTableSyntax .= htmlentities($str,ENT_COMPAT,"UTF-8");

	$cdbTableName = strtoupper($cdbTableName);
	//ﾍｯﾀﾞ表示
	//$CreateTableSyntax .= CreateDBWebFuncEditHeader("TABLE CREATE $cdbTableName","../include/cdbHeader.php");
	
	$CreateTableSyntax .= "\$Result = fncCreateTable();\n";
	$CreateTableSyntax .= "echo \$Result;\n";
	$CreateTableSyntax .= "exit;\n\n";
	//ﾌｯﾀｰ
	//$CreateTableSyntax .= CreateDBWebFuncEditFooter("../include/cdbFooter.php");
	
	$str = <<<END_OF_PHP

// -----------------------------------------------------------
//     fncCreateTable()
// -----------------------------------------------------------

END_OF_PHP;
	$CreateTableSyntax .= htmlentities($str,ENT_COMPAT,"UTF-8");
	
	$CreateTableSyntax .= "function fncCreateTable(){\n";
	
	//共通定数を読み込む(DBDから相対パス)
	$CreateTableSyntax .= CreateDBWebFuncEditIncludeConstant("../../sw_config/swConstant.php");
	//MySQL接続PHPを編集(DBDから相対パス)
	$CreateTableSyntax .= CreateDBWebFuncEditMySQLConnect("../include/ConnectMySQL.php");	


	//SQLを組み立てる
	$CreateTableSyntax .= CreateDBWebFuncEditSQLStart("DROP TABLE SQLを編集",1);
	//drop table syntax
	$CreateTableSyntax .= "DROP TABLE IF EXISTS ".$cdbTableName.";\n\n";
	//SQL END
	$CreateTableSyntax .= CreateDBWebFuncEditSQLEnd();
	//ExecuteSQL
	$CreateTableSyntax .= CreateDBWebFuncEditExecuteSQL("\$stmt","DROP TABLE EXECUTE",1);


	//SQLを組み立てる
	$CreateTableSyntax .= CreateDBWebFuncEditSQLStart("CREATE TABLE SQLを編集",1);
	$CreateTableSyntax .= "CREATE TABLE ".$cdbTableName."(\n";
	$primaryKeyArray = array();
    //index syntax
	$CreateIndexSyntax = '';
	
	for ( $rowNo = 0; $rowNo < $areaRows; $rowNo++ ){
		//ﾃﾞｰﾀを配列から取得
		$cdbName = strtoupper($cdbNameArray[$rowNo]);
		$cdbComment = $cdbCommentArray[$rowNo];
		$cdbType = $cdbTypeArray[$rowNo];
        if ( $cdbType == 'DATENOW'){
            $cdbType = 'DATE';
        }
		$cdbColSize = $cdbColSizeArray[$rowNo];
		$cdbKey = $cdbKeyArray[$rowNo];
		
		//column size編集
		if ( $cdbType == 'VARCHAR' or $cdbType == 'TEXT' ){ 
			$cdbColSize = "(".$cdbColSize.")";
		}else{
			$cdbColSize = '';
		}
		//key 項目編集
		if ( $cdbKey == 'PRIMARY KEY' ){
            array_push($primaryKeyArray,$cdbName);
            $increment = 'NOT NULL';
		}elseif($cdbKey == 'AUTO_INCREMENT'){
            array_push($primaryKeyArray,$cdbName);
			$increment = 'NOT NULL AUTO_INCREMENT';
		}else{
			$increment = '';
		}
		//index 編集
		if ( $cdbKey == 'INDEX' ){
			//SQLを組み立てる
			$CreateIndexSyntax .= CreateDBWebFuncEditSQLStart("INDEX のSQLを編集",1);
			//$CreateIndexSyntax .= "//INDEX のSQLを編集 \n";
			//$CreateIndexSyntax .= "\$strSQL = "."&lt;&lt;&lt;"."END_OF_SQL\n\n";
			$CreateIndexSyntax .= "\tCREATE INDEX ".$cdbTableName."_KEY_".$cdbName." ON ".$cdbTableName."(".$cdbName.");\n";
			//SQL END
			$CreateIndexSyntax .= CreateDBWebFuncEditSQLEnd();
			//ExecuteSQL
			$CreateIndexSyntax .= CreateDBWebFuncEditExecuteSQL("\$stmt","CREATE INDEX",1);
		}
		//列を揃えるための空白
		$sp = CreateDBWebFuncMakeSpaceWidth($cdbName,$maxColNameLength);
		if( $rowNo > 0 ){
			$CreateTableSyntax .= "\t, ".$cdbName.$sp." $cdbType"."$cdbColSize"." $increment"." COMMENT '".$cdbComment."'\n";
		}else{
			$CreateTableSyntax .= "\t  ".$cdbName.$sp." $cdbType"."$cdbColSize"." $increment"." COMMENT '".$cdbComment."'\n";
		}
	}//end for

	//UNIQUE INDEX
    $KeyColumnName = '';
    for ( $i = 0; $i < count($primaryKeyArray); $i++ ){
        if ( $i == 0 ){
            $KeyColumnName = $primaryKeyArray[$i];
        }else{
            $KeyColumnName .= ",".$primaryKeyArray[$i];
        }
    }
	$CreateTableSyntax .= "\t, PRIMARY KEY (".$KeyColumnName.")\n";
	$CreateTableSyntax .= "\t)DEFAULT CHARACTER SET '".$cdbDefaultCharSet."'\n";
	$CreateTableSyntax .= "\tCOMMENT '".$cdbTableNameComment."';\n\n";

	//SQLを実行する
	$CreateTableSyntax .= CreateDBWebFuncEditSQLEnd();
	$CreateTableSyntax .= CreateDBWebFuncEditExecuteSQL("\$stmt","CREATE TABLE EXECUTE",1);

	//SQLを組み立てる
	$CreateTableSyntax .= CreateDBWebFuncEditSQLStart("UNIQUE INDEX のSQLを編集",1);
	if (isset($primaryKeyArray[0])){
		$PrimaryKeyStr = $primaryKeyArray[0];
	}else{
		$PrimaryKeyStr = '';
	}
    $CreateTableSyntax .= "\tCREATE UNIQUE INDEX ".$cdbTableName."_KEY_".$PrimaryKeyStr." ON ".$cdbTableName."(".$KeyColumnName.");\n";
 	//SQLを実行する
	$CreateTableSyntax .= CreateDBWebFuncEditSQLEnd();
	$CreateTableSyntax .= CreateDBWebFuncEditExecuteSQL("\$stmt","CREATE INDEX EXECUTE",1);

	$CreateTableSyntax .= "\n".$CreateIndexSyntax;

	//DB切断(DBDから相対パス)
	$CreateTableSyntax .= CreateDBWebFuncEditMySQLDisConnect("../include/DisConnectMySQL.php");
	
	
//PHPの終わり
	$str = <<<END_OF_PHP
	
	
	return	'CREATE TABLE $cdbTableName Success...';
}//end function
// -----------------------------------------------------------
?>
END_OF_PHP;

	//HTML エンティティに変換する
	$CreateTableSyntax .= htmlentities($str,ENT_COMPAT,"UTF-8");

    return	$CreateTableSyntax;
}
//------------------------------------------------------------------------------
?>