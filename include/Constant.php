<?php
// ------------------------------------------------------------------------------
//
//		System Constant
//				DBConstant.php
//
//		Copyright(C) 2014 dbu@mac.com All Rights Reserved.
//
// ------------------------------------------------------------------------------
	//ﾃﾞﾊﾞｯｸﾞ
	define("DEBUG_MODE",FALSE);

	//内部文字コードを変更
	mb_language("uni");
	mb_internal_encoding("utf-8");
	mb_http_input("auto");
	mb_http_output("utf-8");

	//Timezone
	date_default_timezone_set('Asia/Tokyo');
	//server root path
	$_ROOT_URL_ = "http://localhost/scenariowritercafe/public_html/CreateDBWeb//";
    //ﾃﾞｰﾀ保存ﾊﾟｽ
	$DefaultDataDirPath = "./swdata";
    //ﾃﾞｰﾀﾃﾝﾌﾟﾚｰﾄﾊﾟｽ
	$DefaultTempDirPath = "./TEMP";
	//Max Culumn name length (MySqlのcolumn名の最大長は64文字)
	$maxColNameLength = 32;

// ------------------------------------------------------------------------------
?>
