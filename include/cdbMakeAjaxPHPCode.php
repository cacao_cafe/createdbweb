<?php
//------------------------------------------------------------------------------
//
//	MySQLを使ったDBI/Oｼｽﾃﾑ自動生成 Create DB Web System for MySQL with PHP&JAVASCRIPT
//			Copyright (C) 2014 ScenarioWriterCafe All Rights Reserved.
//
//		jQuery ajax のPHP を生成する
//
//		cdbMakeAjaxPHPCode.php
//
//   	charset=UTF-8
//------------------------------------------------------------------------------
function fncMakeAjaxPHPCode(){

    global  $ThisPHP,$DefaultDirPath;
    global	$SubMode;
	global	$areaRows;
	global	$cdbDbName,$cdbTableName,$cdbTableNameComment,$cdbSelectTable,$cdbDefaultCharSet;
	global	$cdbNameArray,$cdbCommentArray,$cdbTypeArray;
	global	$cdbColSizeArray,$cdbKeyArray,$cdbDefaultValueArray;
	global	$cdbObjTypeArray,$cdbObjSizeArray,$cdbListArray,$cdbReqArray;
	global  $maxColNameLength;
	
	global	$CreateAjaxPHPCode;
	global	$formName;
    
	//現在の日付を取得
	$today = getdate();
	$THIS_YEAR = sprintf( "%04d",$today["year"]);

	//ﾃｰﾌﾞﾙ名からphp名を編集
	$ajaxPHPName = 'ajax'.CreateDBWebFuncEditName($cdbTableName).'.php';
	//ﾃｰﾌﾞﾙ名からclass名を編集
	$className = 'cls'.CreateDBWebFuncEditName($cdbTableName);
	//ﾃｰﾌﾞﾙ名
	$tableName = CreateDBWebFuncEditName($cdbTableName);
	//form名
	$formName = 'frm'.CreateDBWebFuncEditName($cdbTableName);
	//TABLE名 大文字
	$cdbTableNameU = strtoupper($cdbTableName);
	
	$str = <<<END_OF_PHP
<?php
// -----------------------------------------------------------
//
// Copyright (C) $THIS_YEAR ScenarioWriterCafe All Rights Reserved.
// 
//     $cdbTableNameU I/O ｼｽﾃﾑ
//
//     $ajaxPHPName
// -----------------------------------------------------------

END_OF_PHP;
	$CreateAjaxPHPCode = htmlentities($str,ENT_COMPAT,"UTF-8");


	$CreateAjaxPHPCode .= "// ------------------------------------------------------------------------------\n";
	$CreateAjaxPHPCode .= "\t//定数読み込み\n";
	$CreateAjaxPHPCode .= "\tinclude_once(\"../../sw_config/swConstant.php\");\n";
	$CreateAjaxPHPCode .= "\t//DB接続ｸﾗｽの初期化\n";
	$CreateAjaxPHPCode .= "\tinclude_once(\"../include/ConnectMySQL.php\");\n";
	$CreateAjaxPHPCode .= "// ------------------------------------------------------------------------------\n";

    $CreateAjaxPHPCode .= "\t//共通関数をｲﾝｸﾙｰﾄﾞ\n";
    $CreateAjaxPHPCode .= "\tinclude_once(\"../include/swFunc.php\");\n";

    $CreateAjaxPHPCode .= "\t//ﾌｫｰﾑのﾃﾞｰﾀを読み込む\n";
    $CreateAjaxPHPCode .= "\tfncGetPostItems();\n\n";

    $CreateAjaxPHPCode .= "\t//MainProcedure\n";
    $CreateAjaxPHPCode .= "\tfncMainProc(\$mySqlConnObj);\n\n";

    $CreateAjaxPHPCode .= "\texit();\n\n";

	$str = <<<END_OF_PHP
// ------------------------------------------------------------------------------
//      POSTされた要素を取得
//          fncGetPostItems()
// ------------------------------------------------------------------------------
function fncGetPostItems(){

END_OF_PHP;
	$CreateAjaxPHPCode .= htmlentities($str,ENT_COMPAT,"UTF-8");
 
    $CreateAjaxPHPCode .= "\t//共通global変数\n";
    $CreateAjaxPHPCode .= "\tglobal\t\$SubMode;\n";
	$CreateAjaxPHPCode .= "\t//ﾌｫｰﾑ name要素をglobal変数にする\n";
	//global 変数を編集する
	$GlobalVar = '';
	for ( $rowNo = 0; $rowNo < $areaRows; $rowNo++ ){
		//変数名
		$varName = CreateDBWebFuncEditName($cdbNameArray[$rowNo]);
        $GlobalVar .= "\tglobal\t\$fdt".$varName.";\n";
	}//end for
	//global 変数
	$CreateAjaxPHPCode .= $GlobalVar;
	
	$CreateAjaxPHPCode .= "\n";
	$CreateAjaxPHPCode .= "\t//処理ﾓｰﾄが空白ならreturnするﾞ\n";
	$CreateAjaxPHPCode .= "\tif(!isset(\$_POST['SubMode'])){return;}\n";
	$CreateAjaxPHPCode .= "\t//処理ﾓｰﾄﾞ\n";
	$CreateAjaxPHPCode .= "\t\$SubMode = swFunc_GetPostData('SubMode');\n";
	$CreateAjaxPHPCode .= "\tif(\$SubMode == ''){return;}\n";
	$CreateAjaxPHPCode .= "\t//POSTされた要素を取得\n";
	//uniqueｲﾝﾃﾞｯｸｽ
	$UniqueIndexCols = '';
	//POSTされた要素を取得
	for ( $rowNo = 0; $rowNo < $areaRows; $rowNo++ ){
		//変数名
		$varName = CreateDBWebFuncEditName($cdbNameArray[$rowNo]);
		//ｺﾒﾝﾄ
		$varComment = $cdbCommentArray[$rowNo];
		//Key
		$cdbKey = $cdbKeyArray[$rowNo];
		//uniqueｲﾝﾃﾞｯｸｽ項目編集(class引数)
		if ( $cdbKey == 'PRIMARY KEY' or $cdbKey == 'AUTO_INCREMENT' ){
			//$UniqueIndexCols .= ',$fdt'.$varName;
			$UniqueIndexCols .= ','.$cdbNameArray[$rowNo];
		}
		//列を揃えるための空白
		$sp = CreateDBWebFuncMakeSpaceWidth($varName.$varName,$maxColNameLength);

        //ｵﾌﾞｼﾞｪｸﾄﾀｲﾌﾟ
        $varObjType = $cdbObjTypeArray[$rowNo];
		//ｵﾌﾞｼﾞｪｸﾄﾀｲﾌﾟで処理を分岐
		switch($varObjType ){
			case 'CALENDAR':
				$CreateAjaxPHPCode .= "\t\$fdt".$varName." = swFunc_GetPostData('fdt".$varName."');"."$sp//".$varComment."\n";
	        	$CreateAjaxPHPCode .= "\n\t//ｶﾚﾝﾀﾞｰ日付要素\n";
	        	//YY,MM,DDに分割する
	        	$CreateAjaxPHPCode .= "\t\$fdt".$varName."YY = swFunc_GetPostData('fdt".$varName."YY');"."$sp//".$varComment."年\n";
	        	$CreateAjaxPHPCode .= "\t\$fdt".$varName."MM = swFunc_GetPostData('fdt".$varName."MM');"."$sp//".$varComment."月\n";
	        	$CreateAjaxPHPCode .= "\t\$fdt".$varName."DD = swFunc_GetPostData('fdt".$varName."DD');"."$sp//".$varComment."日\n\n";
				break;
			default:
				$CreateAjaxPHPCode .= "\t\$fdt".$varName." = swFunc_GetPostData('fdt".$varName."');"."$sp//".$varComment."\n";
				break;
		}
	}//end for

	$CreateAjaxPHPCode .= "}//end function\n\n";
	
	
	//uniqueｲﾝﾃﾞｯｸｽ項目
	$UniqueIndexCols = ltrim($UniqueIndexCols, ',');
	

	$str = <<<END_OF_PHP
// ------------------------------------------------------------------------------
//      MAIN PROCEDURE
//          fncMainProc(\$mySqlConnObj)
// ------------------------------------------------------------------------------
function fncMainProc(\$mySqlConnObj){

END_OF_PHP;
	$CreateAjaxPHPCode .= htmlentities($str,ENT_COMPAT,"UTF-8");


	$CreateAjaxPHPCode .= "\tglobal\t\$SubMode;\n";
	$CreateAjaxPHPCode .= "\t//global 変数\n";
	$CreateAjaxPHPCode .= $GlobalVar;


	$CreateAjaxPHPCode .= "\n\t//SubModeで処理を制御\n";
    $CreateAjaxPHPCode .= "\tif(\$SubMode == '".$tableName."List'){\n";
    $CreateAjaxPHPCode .= "\t\t\$resultHtml = fncMake".$tableName."List(\$mySqlConnObj);\n";
    $CreateAjaxPHPCode .= "\t}\n\n";
    $CreateAjaxPHPCode .= "\t// 出力charsetをUTF-8に指定\n";
    $CreateAjaxPHPCode .= "\tmb_http_output ( 'UTF-8' );\n";
    $CreateAjaxPHPCode .= "\t// 出力\n";
    $CreateAjaxPHPCode .= "\techo(\$resultHtml);\n";
	$CreateAjaxPHPCode .= "}//end function\n\n";

    $str = <<<END_OF_PHP
//--------------------------------------------------------------------------------
// $cdbTableNameU ALL LIST
//--------------------------------------------------------------------------------

END_OF_PHP;
	$CreateAjaxPHPCode .= htmlentities($str,ENT_COMPAT,"UTF-8");

	$CreateAjaxPHPCode .= "function fncMake".$tableName."List(\$mySqlConnObj){\n";
	$CreateAjaxPHPCode .= "\t//ﾘｽﾄのﾍｯﾀﾞｰ\n";
	
	$str = "\t\$retHtml = <<<END_OF_HTML\n";
	$str .= <<<END_OF_PHP
	
		<div class="scroll_div">
		<table class="table" _fixedhead="rows:1;div-full-mode: no;">
END_OF_PHP;



//LIST指定項目
	$ListWidth = '';
	$ListHeader = '';
	$SelectItems = '';
	$ListData = '';
	for ( $rowNo = 0; $rowNo < $areaRows; $rowNo++ ){
		//column名
		$varColName = $cdbNameArray[$rowNo];
		//変数名
		$varName = CreateDBWebFuncEditName($cdbNameArray[$rowNo]);
		$varComment = $cdbCommentArray[$rowNo];
		$varObjSize = $cdbObjSizeArray[$rowNo];
		//LIST指定
		$varList = $cdbListArray[$rowNo];
		if($varList == '1'){
			//$ListWidth .= "\n\t\t\t<colgroup span=\"1\" style=\"width: ".$varObjSize."px;\"></colgroup>";
			$ListHeader .= "\n\t\t\t\t<th>".$varComment."</th>";
			
			$SelectItems .= "\t\t\$val".$varName." = \$myRow['".strtoupper($varColName)."'];\n";
			
			$ListData .= "\n\t\t\t\t<td>\$val".$varName."</td>";
		}
	}//end for
	
	$str .= <<<END_OF_PHP
$ListWidth
			<tr>$ListHeader
			</tr>
END_OF_PHP;

	$str .= "\nEND_OF_HTML;\n\n";
	$CreateAjaxPHPCode .= htmlentities($str,ENT_COMPAT,"UTF-8");

	
	$CreateAjaxPHPCode .= "\t//DB から ﾃﾞｰﾀを取得しﾌﾟﾛﾊﾟﾃｨにｾｯﾄする\n";
	
	
	//keyを取得するSELECT文
	$SelectSyntax = '*,';
	for ( $rowNo = 0; $rowNo < $areaRows; $rowNo++ ){
		//column名
		$columnName = $cdbNameArray[$rowNo];
		//Key
		$cdbKey = $cdbKeyArray[$rowNo];
		//uniqueｲﾝﾃﾞｯｸｽ項目編集
		if ( $cdbKey == 'PRIMARY KEY' or $cdbKey == 'AUTO_INCREMENT' ){
			$SelectSyntax .= strtoupper($columnName)." as KEY_ITEM";
		}
	}
	
	$ORDER_COLUMNS = strtoupper($UniqueIndexCols);
	
	$str =  "\t\$strSQL = <<<END_OF_SQL\n";
	$str .= <<<END_OF_STR
		SELECT $SelectSyntax
			FROM $cdbTableNameU
					ORDER BY $ORDER_COLUMNS;
END_OF_STR;
	$str .= "\nEND_OF_SQL;\n";
	$CreateAjaxPHPCode .= htmlentities($str,ENT_COMPAT,"UTF-8");
	$CreateAjaxPHPCode .= "\t//SQLを実行\n";
	$CreateAjaxPHPCode .= "\t\$myResult = \$mySqlConnObj->query(\$strSQL);\n";
	$CreateAjaxPHPCode .= "\twhile(\$myRow = \$myResult->fetch_assoc()){\n";
	
	$CreateAjaxPHPCode .= $SelectItems;
	$CreateAjaxPHPCode .= "\t\t\$valKeyItem = \$myRow['KEY_ITEM'];\n";
	
	$CreateAjaxPHPCode .= "\t\t//ﾘｽﾄ\n";
	
	$str = "\t\t\$retHtml .= <<<END_OF_HTML\n";
	$str .= <<<END_OF_PHP
		
			<tr onclick="fncSelect$tableName('\$valKeyItem');">$ListData
			</tr>
END_OF_PHP;
	$str .= "\nEND_OF_HTML;\n";
	$CreateAjaxPHPCode .= htmlentities($str,ENT_COMPAT,"UTF-8");
	
	$CreateAjaxPHPCode .= "\t\t}//end while\n\n";
	
	$str = "\t\t\$retHtml .= <<<END_OF_HTML\n";
	$str .= <<<END_OF_PHP
		
		</table>
		</div>
END_OF_PHP;
	$str .= "\nEND_OF_HTML;\n";
	$CreateAjaxPHPCode .= htmlentities($str,ENT_COMPAT,"UTF-8");
	
    $CreateAjaxPHPCode .= "\t//結果セットを開放\n";
    $CreateAjaxPHPCode .= "\t\$myResult->free();\n";
	$CreateAjaxPHPCode .= "\t//HTMLを返す\n";
	$CreateAjaxPHPCode .= "\treturn \$retHtml;\n";
	$CreateAjaxPHPCode .= "}\n";

    
    return	$CreateAjaxPHPCode;
}






//------------------------------------------------------------------------------
?>