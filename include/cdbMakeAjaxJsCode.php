<?php
//------------------------------------------------------------------------------
//
//	MySQLを使ったDBI/Oｼｽﾃﾑ自動生成 Create DB Web System for MySQL with PHP&JAVASCRIPT
//			Copyright (C) 2015 ScenarioWriterCafe All Rights Reserved.
//
//		jQuery ajax のjavascript を生成する
//
//		cdbMakeAjaxJsCode.php
//
//   	charset=UTF-8
//------------------------------------------------------------------------------
function fncMakeAjaxJsCode(){

    global  $ThisPHP,$DefaultDirPath;
    global	$SubmitMode;
	global	$areaRows;
	global	$cdbDbName,$cdbTableName,$cdbTableNameComment,$cdbSelectTable,$cdbDefaultCharSet;
	global	$cdbNameArray,$cdbCommentArray,$cdbTypeArray;
	global	$cdbColSizeArray,$cdbKeyArray,$cdbDefaultValueArray;
	global	$cdbObjTypeArray,$cdbObjSizeArray,$cdbListArray,$cdbReqArray;
	global  $maxColNameLength;
	
	global	$CreateAjaxJsCode;
	global	$formName;
    
	//現在の日付を取得
	$today = getdate();
	$THIS_YEAR = sprintf( "%04d",$today["year"]);

	//ﾃｰﾌﾞﾙ名からjs名を編集
	$jsName = 'ajax'.CreateDBWebFuncEditName($cdbTableName).'.js';
	//ﾃｰﾌﾞﾙ名
	$tableName = CreateDBWebFuncEditName($cdbTableName);
	//form名
	$formName = 'frm'.CreateDBWebFuncEditName($cdbTableName);
	//TABLE名 大文字
	$cdbTableNameU = strtoupper($cdbTableName);
	
	
	
	$CreateAjaxJsCode = '';
	
	$str = <<<END_OF_JS
// ------------------------------------------------------------------------------
//
// Copyright (C) $THIS_YEAR ScenarioWriterCafe All Rights Reserved.
//
//		$cdbTableNameComment
//		$jsName
//		
//    	charset=UTF-8
// ------------------------------------------------------------------------------
// ------------------------------------------------------------------------------
//	form 制御
// ------------------------------------------------------------------------------
jQuery(document).ready(function() {
	//selectbox onchange　ｲﾍﾞﾝﾄ

END_OF_JS;
	$CreateAjaxJsCode .= htmlentities($str,ENT_COMPAT,"UTF-8");

	//Selectbox
	for ( $rowNo = 0; $rowNo < $areaRows; $rowNo++ ){
		//変数名
		$varName = 'fdt'.CreateDBWebFuncEditName($cdbNameArray[$rowNo]);
		$varComment = $cdbCommentArray[$rowNo];
		//ObjectType
		$varObjType = $cdbObjTypeArray[$rowNo];
        if($varObjType == 'SELECT'){
        	$CreateAjaxJsCode .= "\t\t//--------------------------------------------\n";
        	$CreateAjaxJsCode .= "\t\t//".$varComment."が変更されたときの制御\n";
        	$CreateAjaxJsCode .= "\t\t//--------------------------------------------\n";

			$CreateAjaxJsCode .= "\t\t\$(\"#".$varName."\").change(function () {\n\n";
			$CreateAjaxJsCode .= "\t\t});\n";

        }
	}//end for
$CreateAjaxJsCode .= "});\n\n";

	$str = <<<END_OF_JS
// ------------------------------------------------------------------------------
//		ﾃﾞｰﾀﾘｽﾄ	取得 jQuery ajax 
// ------------------------------------------------------------------------------

END_OF_JS;
	$CreateAjaxJsCode .= htmlentities($str,ENT_COMPAT,"UTF-8");


	$CreateAjaxJsCode .= "\tfunction fncMake".$tableName."List(frm){\n";
	$CreateAjaxJsCode .= "\t\tjQuery(function(\$){\n";
	$CreateAjaxJsCode .= "\t\t\tvar \$form = \$(frm);\n";
	$CreateAjaxJsCode .= "\t\t\t\$phppath = \"./ajax/ajax".$tableName.".php\";\n";
	$CreateAjaxJsCode .= "\t\t\t\$.ajax({\n";
	$CreateAjaxJsCode .= "\t\t\t\tasync:false,\n";
	$CreateAjaxJsCode .= "\t\t\t\ttype: \"POST\",\n";
	$CreateAjaxJsCode .= "\t\t\t\turl: \$phppath,\n";
	$CreateAjaxJsCode .= "\t\t\t\tdata: \$form.serialize()\n";
	$CreateAjaxJsCode .= "\t\t\t\t\t+ \"&SubMode=".$tableName."List\",\n";
	$CreateAjaxJsCode .= "\t\t\t\tsuccess: function(result){\n";
	$CreateAjaxJsCode .= "\t\t\t\t\t\$(\"#side_list_area\").html(result);\n";
	$CreateAjaxJsCode .= "\t\t\t\t},\n";
	$CreateAjaxJsCode .= "\t\t\t\terror:function(result){\n";
	$CreateAjaxJsCode .= "\t\t\t\t\talert(result);\n";
	$CreateAjaxJsCode .= "\t\t\t\t},\n";
	$CreateAjaxJsCode .= "\t\t\t});\n";
	$CreateAjaxJsCode .= "\t\t});\n";
	$CreateAjaxJsCode .= "\t}\n";

$str = <<<END_OF_JS
// ------------------------------------------------------------------------------
//		ﾃﾞｰﾀﾘｽﾄ	click ｲﾍﾞﾝﾄ
// ------------------------------------------------------------------------------

END_OF_JS;
$CreateAjaxJsCode .= htmlentities($str,ENT_COMPAT,"UTF-8");

	//keyを取得
	for ( $rowNo = 0; $rowNo < $areaRows; $rowNo++ ){
		//column名
		$columnName = $cdbNameArray[$rowNo];
		//Key
		$cdbKey = $cdbKeyArray[$rowNo];
		//uniqueｲﾝﾃﾞｯｸｽ項目編集
		if ( $cdbKey == 'PRIMARY KEY' or $cdbKey == 'AUTO_INCREMENT' ){
			$KeyName = 'fdt'.CreateDBWebFuncEditName($cdbNameArray[$rowNo]);
			$KeyVal = 'val'.CreateDBWebFuncEditName($cdbNameArray[$rowNo]);
		}
	}

$CreateAjaxJsCode .="function fncSelect".$tableName."($KeyVal){\n";
$CreateAjaxJsCode .="\t\$('#".$KeyName."').val($KeyVal);\n";
$CreateAjaxJsCode .="\t\$('#SubmitMode').val('SELECT');\n";
$CreateAjaxJsCode .="\t\$('#".$formName."').submit();\n";
$CreateAjaxJsCode .="}\n";

$str = <<<END_OF_JS
// ------------------------------------------------------------------------------
//		ﾎﾞﾀﾝが押されたときの制御
// ------------------------------------------------------------------------------

END_OF_JS;
$CreateAjaxJsCode .= htmlentities($str,ENT_COMPAT,"UTF-8");

$CreateAjaxJsCode .="function fnc".$tableName."Submit(frm,valSubmitMode,valMode,valMsg){\n";

$CreateAjaxJsCode .="\tvar strMessage = valMsg + '　実行しますか？';\n";
$CreateAjaxJsCode .="\tif(valMode == 'TRUE'){\n";
$CreateAjaxJsCode .="\t\t\t//ﾀﾞｲｱﾛｸﾞを表示する\n";
$CreateAjaxJsCode .="\t\tbootbox.dialog({\n";
$CreateAjaxJsCode .="\t\t\tmessage: strMessage,\n";
$CreateAjaxJsCode .="\t\t\ttitle: \"".$cdbTableNameComment."\",\n";
$CreateAjaxJsCode .="\t\t\tbuttons: {\n";
$CreateAjaxJsCode .="\t\t\t\tsuccess: {\n";
$CreateAjaxJsCode .="\t\t\t\t\tlabel: \"Cancel\",\n";
$CreateAjaxJsCode .="\t\t\t\t\tclassName: \"btn-success\",\n";
$CreateAjaxJsCode .="\t\t\t\t\tcallback: function() {\n";
$CreateAjaxJsCode .="\t\t\t\t\t\treturn;\n";
$CreateAjaxJsCode .="\t\t\t\t\t}\n";
$CreateAjaxJsCode .="\t\t\t\t},\n";
$CreateAjaxJsCode .="\t\t\t\tdanger: {\n";
$CreateAjaxJsCode .="\t\t\t\t\tlabel: valMsg,\n";
$CreateAjaxJsCode .="\t\t\t\t\tclassName: \"btn-danger\",\n";
$CreateAjaxJsCode .="\t\t\t\t\tcallback: function() {\n";
$CreateAjaxJsCode .="\t\t\t\t\t\tfrm.SubmitMode.value = valSubmitMode;\n";
$CreateAjaxJsCode .="\t\t\t\t\t\tfrm.submit();\n";
$CreateAjaxJsCode .="\t\t\t\t\t\treturn true;\n";
$CreateAjaxJsCode .="\t\t\t\t\t}\n";
$CreateAjaxJsCode .="\t\t\t\t},\n";
$CreateAjaxJsCode .="\t\t\t}\n";
$CreateAjaxJsCode .="\t\t});\n";
$CreateAjaxJsCode .="\t}else{\n";
$CreateAjaxJsCode .="\t\t//何も聞かずにsubmitする\n";
$CreateAjaxJsCode .="\t\tfrm.SubmitMode.value = valSubmitMode;\n";
$CreateAjaxJsCode .="\t\tfrm.submit();\n";
$CreateAjaxJsCode .="\t\treturn true;\n";
$CreateAjaxJsCode .="\t}\n";
$CreateAjaxJsCode .="}\n";



    return	$CreateAjaxJsCode;
}



//------------------------------------------------------------------------------
?>