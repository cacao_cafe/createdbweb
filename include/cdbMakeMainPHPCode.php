<?php
//------------------------------------------------------------------------------
//
//	MySQLを使ったDBI/Oｼｽﾃﾑ自動生成 Create DB Web System for MySQL with PHP&JAVASCRIPT
//			Copyright (C) 2015 ScenarioWriterCafe All Rights Reserved.
//
//		Main のPHP を生成する
//
//		cdbMakeMainPHPCode.php
//
//   	charset=UTF-8
//------------------------------------------------------------------------------
function fncMakeMainPHPCode(){

    global  $ThisPHP,$DefaultDirPath;
    global	$SubmitMode;
	global	$areaRows;
	global	$cdbDbName,$cdbTableName,$cdbTableNameComment,$cdbSelectTable,$cdbDefaultCharSet;
	global	$cdbNameArray,$cdbCommentArray,$cdbTypeArray;
	global	$cdbColSizeArray,$cdbKeyArray,$cdbDefaultValueArray;
	global	$cdbObjTypeArray,$cdbObjSizeArray,$cdbListArray,$cdbReqArray;
	global  $maxColNameLength;
	
	global	$CreateMainPHPCode;
	global	$formName;
    
	//現在の日付を取得
	$today = getdate();
	$THIS_YEAR = sprintf( "%04d",$today["year"]);

	//ﾃｰﾌﾞﾙ名からphp名を編集
	$phpName = CreateDBWebFuncEditName($cdbTableName).'.php';
	//ﾃｰﾌﾞﾙ名からclass名を編集
	$className = 'cls'.CreateDBWebFuncEditName($cdbTableName);
	//ﾃｰﾌﾞﾙ名
	$tableName = CreateDBWebFuncEditName($cdbTableName);
	//form名
	$formName = 'frm'.CreateDBWebFuncEditName($cdbTableName);
	//TABLE名 大文字
	$cdbTableNameU = strtoupper($cdbTableName);
	
	$str = <<<END_OF_PHP
<?php
// -----------------------------------------------------------
//
// Copyright (C) $THIS_YEAR ScenarioWriterCafe All Rights Reserved.
// 
//     $cdbTableNameU I/O ｼｽﾃﾑ
//
//     $phpName
// -----------------------------------------------------------

END_OF_PHP;
	$CreateMainPHPCode = htmlentities($str,ENT_COMPAT,"UTF-8");


	$CreateMainPHPCode .= "// ------------------------------------------------------------------------------\n";
	$CreateMainPHPCode .= "\t//定数読み込み\n";
	$CreateMainPHPCode .= "\tinclude_once(\"../sw_config/swConstant.php\");\n";
	$CreateMainPHPCode .= "\t//DB接続ｸﾗｽの初期化\n";
	$CreateMainPHPCode .= "\tinclude_once(\"./include/ConnectMySQL.php\");\n";
	$CreateMainPHPCode .= "// ------------------------------------------------------------------------------\n";
    $CreateMainPHPCode .= "\t//ﾃﾞﾌｫﾙﾄｱｸｼｮﾝ\n";
    $CreateMainPHPCode .= "\t\$ThisPHP = '".$phpName."';\n";

    $CreateMainPHPCode .= "\t//ﾃﾞｰﾀ管理ｸﾗｽ\n";
    $CreateMainPHPCode .= "\tinclude_once(\"./class/".$className.".php\");\n";
    $CreateMainPHPCode .= "\t$".$className." = new ".$className."();\n";
    $CreateMainPHPCode .= "\t//共通関数をｲﾝｸﾙｰﾄﾞ\n";
    $CreateMainPHPCode .= "\tinclude_once(\"./include/swFunc.php\");\n";

	//ﾍｯﾀﾞ表示
	$CreateMainPHPCode .= CreateDBWebFuncEditHeader("$cdbTableNameComment","./include/swHeader.php");

    $CreateMainPHPCode .= "\t//ﾌｫｰﾑのﾃﾞｰﾀを読み込む\n";
    $CreateMainPHPCode .= "\tfncGetPostItems();\n\n";

    $CreateMainPHPCode .= "\t//MainProcedure\n";
    $CreateMainPHPCode .= "\tfncMainProc(\$mySqlConnObj);\n\n";

	//ﾌｯﾀｰ
	//$CreateMainPHPCode .= CreateDBWebFuncEditFooter("./include/swFooter.php");
    
    $CreateMainPHPCode .= "\texit();\n\n";

	$str = <<<END_OF_PHP
// ------------------------------------------------------------------------------
//      POSTされた要素を取得
//          fncGetPostItems()
// ------------------------------------------------------------------------------
function fncGetPostItems(){

END_OF_PHP;
	$CreateMainPHPCode .= htmlentities($str,ENT_COMPAT,"UTF-8");
 
    $CreateMainPHPCode .= "\t//共通global変数\n";
    $CreateMainPHPCode .= "\tglobal\t\$SubmitMode;\n";
	$CreateMainPHPCode .= "\t//ﾌｫｰﾑ name要素をglobal変数にする\n";
	//global 変数を編集する
	$GlobalVar = '';
	for ( $rowNo = 0; $rowNo < $areaRows; $rowNo++ ){
		//変数名
		$varName = CreateDBWebFuncEditName($cdbNameArray[$rowNo]);
        $GlobalVar .= "\tglobal\t\$fdt".$varName.";\n";
        //ｶﾚﾝﾀﾞｰ要素
        $varObjType = $cdbObjTypeArray[$rowNo];
        if($varObjType == 'CALENDAR'){
        	$GlobalVar .= "\tglobal\t\$fdt".$varName."YY,\$fdt".$varName."MM,\$fdt".$varName."DD;\n";
        }
	}//end for
	//global 変数
	$CreateMainPHPCode .= $GlobalVar;
	
	$CreateMainPHPCode .= "\n";
	$CreateMainPHPCode .= "\t//処理ﾓｰﾄが空白ならreturnするﾞ\n";
	$CreateMainPHPCode .= "\tif(!isset(\$_POST['SubmitMode'])){return;}\n";
	$CreateMainPHPCode .= "\t//処理ﾓｰﾄﾞ\n";
	$CreateMainPHPCode .= "\t\$SubmitMode = swFunc_GetPostData('SubmitMode');\n";
	$CreateMainPHPCode .= "\tif(\$SubmitMode == ''){return;}\n";
	$CreateMainPHPCode .= "\t//POSTされた要素を取得\n";
	//uniqueｲﾝﾃﾞｯｸｽ
	$UniqueIndexCols = '';
	//POSTされた要素を取得
	for ( $rowNo = 0; $rowNo < $areaRows; $rowNo++ ){
		//変数名
		$varName = CreateDBWebFuncEditName($cdbNameArray[$rowNo]);
		//ｺﾒﾝﾄ
		$varComment = $cdbCommentArray[$rowNo];
		//Key
		$cdbKey = $cdbKeyArray[$rowNo];
		//uniqueｲﾝﾃﾞｯｸｽ項目編集(class引数)
		if ( $cdbKey == 'PRIMARY KEY' or $cdbKey == 'AUTO_INCREMENT' ){
			$UniqueIndexCols .= ',$fdt'.$varName;
		}
		//列を揃えるための空白
		$sp = CreateDBWebFuncMakeSpaceWidth($varName.$varName,$maxColNameLength);

        //ｵﾌﾞｼﾞｪｸﾄﾀｲﾌﾟ
        $varObjType = $cdbObjTypeArray[$rowNo];
		//ｵﾌﾞｼﾞｪｸﾄﾀｲﾌﾟで処理を分岐
		switch($varObjType ){
			case 'CALENDAR':
				$CreateMainPHPCode .= "\t\$fdt".$varName." = swFunc_GetPostData('fdt".$varName."');"."$sp//".$varComment."\n";
	        	$CreateMainPHPCode .= "\n\t//ｶﾚﾝﾀﾞｰ日付要素\n";
	        	//YY,MM,DDに分割する
	        	$CreateMainPHPCode .= "\t\$fdt".$varName."YY = swFunc_GetPostData('fdt".$varName."YY');"."$sp//".$varComment."年\n";
	        	$CreateMainPHPCode .= "\t\$fdt".$varName."MM = swFunc_GetPostData('fdt".$varName."MM');"."$sp//".$varComment."月\n";
	        	$CreateMainPHPCode .= "\t\$fdt".$varName."DD = swFunc_GetPostData('fdt".$varName."DD');"."$sp//".$varComment."日\n\n";
				break;
			default:
				$CreateMainPHPCode .= "\t\$fdt".$varName." = swFunc_GetPostData('fdt".$varName."');"."$sp//".$varComment."\n";
				break;
		}
	}//end for
	
	$CreateMainPHPCode .= "}//end function\n\n";

	$str = <<<END_OF_PHP
// ------------------------------------------------------------------------------
//      MAIN PROCEDURE
//          fncMainProc(\$mySqlConnObj)
// ------------------------------------------------------------------------------
function fncMainProc(\$mySqlConnObj){

END_OF_PHP;
	$CreateMainPHPCode .= htmlentities($str,ENT_COMPAT,"UTF-8");


	$CreateMainPHPCode .= "\tglobal\t\$SubmitMode;\n";
	$CreateMainPHPCode .= "\t//class\n";
	$CreateMainPHPCode .= "\tglobal\t\$".$className.";\n\n";

	//global 変数
	$CreateMainPHPCode .= $GlobalVar;


	$CreateMainPHPCode .= "\n\t//SubmitModeで処理を分岐\n";
    $CreateMainPHPCode .= "\tswitch(\$SubmitMode ){\n";
    $CreateMainPHPCode .= "\t\tcase 'SELECT':\n";
    $CreateMainPHPCode .= "\t\t\t//ﾃﾞｰﾀ管理ｸﾗｽの初期化\n";
    $CreateMainPHPCode .= "\t\t\t\$".$className."->".$className."Init(\$mySqlConnObj".$UniqueIndexCols.");\n";
    $CreateMainPHPCode .= "\t\t\t//ﾌﾟﾛﾊﾟﾃｨGet\n";
    $CreateMainPHPCode .= "\t\t\tfnc".$tableName."GetProperty(\$".$className.");\n";
    $CreateMainPHPCode .= "\t\t\tbreak;\n";
    $CreateMainPHPCode .= "\t\tcase 'INSERT':\n";
	$CreateMainPHPCode .= "\t\t\t//登録処理\n";
	$CreateMainPHPCode .= "\t\t\tfnc".$tableName."DBInsert(\$mySqlConnObj,\$".$className.");\n";
	$CreateMainPHPCode .= "\t\t\t//選択状態にする\n";
	$CreateMainPHPCode .= "\t\t\t\$SubmitMode = 'SELECT';\n";
	$CreateMainPHPCode .= "\t\t\t//ﾃﾞｰﾀ管理ｸﾗｽの初期化\n";
	$CreateMainPHPCode .= "\t\t\t\$".$className."->".$className."Init(\$mySqlConnObj".$UniqueIndexCols.");\n";
	$CreateMainPHPCode .= "\t\t\t//ﾌﾟﾛﾊﾟﾃｨGet\n";
	$CreateMainPHPCode .= "\t\t\tfnc".$tableName."GetProperty(\$".$className.");\n";
	$CreateMainPHPCode .= "\t\t\tbreak;\n";
	$CreateMainPHPCode .= "\t\tcase 'UPDATE':\n";
	$CreateMainPHPCode .= "\t\t\t//更新処理\n";
	$CreateMainPHPCode .= "\t\t\tfnc".$tableName."DBUpdate(\$mySqlConnObj,\$".$className.");\n";
	$CreateMainPHPCode .= "\t\t\t//選択状態にする\n";
	$CreateMainPHPCode .= "\t\t\t\$SubmitMode = 'SELECT';\n";
	$CreateMainPHPCode .= "\t\t\t//ﾃﾞｰﾀ管理ｸﾗｽの初期化\n";
	$CreateMainPHPCode .= "\t\t\t\$".$className."->".$className."Init(\$mySqlConnObj".$UniqueIndexCols.");\n";
	$CreateMainPHPCode .= "\t\t\t//ﾌﾟﾛﾊﾟﾃｨGet\n";
	$CreateMainPHPCode .= "\t\t\tfnc".$tableName."GetProperty(\$".$className.");\n";
	$CreateMainPHPCode .= "\t\t\tbreak;\n";
	$CreateMainPHPCode .= "\t\tcase 'DELETE':\n";
	$CreateMainPHPCode .= "\t\t\t//削除処理\n";
	$CreateMainPHPCode .= "\t\t\tfnc".$tableName."DBDelete(\$mySqlConnObj,\$".$className.");\n";
	$CreateMainPHPCode .= "\t\t\t//ﾌﾟﾛﾊﾟﾃｨReset\n";
	$CreateMainPHPCode .= "\t\t\tfnc".$tableName."ResetProperty(\$".$className.");\n";
	$CreateMainPHPCode .= "\t\t\t//初期状態にする\n";
	$CreateMainPHPCode .= "\t\t\t\$SubmitMode = 'RESET';\n";
	$CreateMainPHPCode .= "\t\t\tbreak;\n";
	$CreateMainPHPCode .= "\t\tcase 'RESET':\n";
	$CreateMainPHPCode .= "\t\t\t//ﾌﾟﾛﾊﾟﾃｨReset\n";
	$CreateMainPHPCode .= "\t\t\tfnc".$tableName."ResetProperty(\$".$className.");\n";
	$CreateMainPHPCode .= "\t\t\t//初期状態にする\n";
	$CreateMainPHPCode .= "\t\t\t\$SubmitMode = 'RESET';\n";
	$CreateMainPHPCode .= "\t\t\tbreak;\n";
	$CreateMainPHPCode .= "\t\tdefault:\n";
	$CreateMainPHPCode .= "\t\t\tbreak;\n";
	$CreateMainPHPCode .= "\t}\n";
    $CreateMainPHPCode .= "\t//ﾌｫｰﾑの表示\n";
    $CreateMainPHPCode .= "\tfncMainForm(\$mySqlConnObj);\n";
	
	$CreateMainPHPCode .= "}//end function\n\n";



	// ﾒｲﾝﾌｫｰﾑ ｻﾌﾞﾙｰﾁﾝ 


	$str = <<<END_OF_PHP
// ------------------------------------------------------------------------------
//      MAIN FORM
//          fncMainForm(\$mySqlConnObj)
// ------------------------------------------------------------------------------
function fncMainForm(\$mySqlConnObj){

END_OF_PHP;
	$CreateMainPHPCode .= htmlentities($str,ENT_COMPAT,"UTF-8");

	//global変数
	$CreateMainPHPCode .= "\tglobal\t\$ThisPHP,\$SubmitMode;\n";

	//global 変数
	$CreateMainPHPCode .= $GlobalVar;
	$CreateMainPHPCode .= "\n";

	$str = '';
	$str .= "\t//css powerd by Bootstrap ver3\n";
	
	$str .= "\tprint <<<END_OF_HTML\n";

	$str .= <<<END_OF_PHP

	<div class="wrap" >
	
		<div class="container-fluid">
		
		<!-- ﾒﾆｭｰ 制御ﾎﾞﾀﾝ 表示ROW -->
			<div class="row">
				<div class="col-sm-12">
				<legend class="form-inline submenu">
				<ul class="list-inline">
				  <li>
END_OF_PHP;

	$str .= "\nEND_OF_HTML;\n\n";

	$str .= "\t//管理メニュー ﾄﾞﾛｯﾌﾟﾀﾞｳﾝﾒﾆｭｰ\n";
	$str .= "\t\$TargetForm = '{$formName}';\n";
	$str .= "\tinclude_once(\"./include/swMgrDropDownMenu.php\");\n\n";
	
	$switchVal = "\$SubmitMode";
	$str .= <<<END_OF_PHP
	
    //SubmitModeで表示するボタンを制御する
	switch($switchVal){
		case 'SELECT':
END_OF_PHP;

	$str .= "\n\t\t\tprint <<<END_OF_HTML\n";
	
	$str .= <<<END_OF_PHP
			 	</li>
			 	<li>
			        <input type="button" value="RESET"
			            id="btnReset"
			            onclick="fnc{$tableName}Submit({$formName},'RESET','FALSE','');"
			            class="btn btn-default btn-sm">
			        <span style="width: 120px;"></span>
			        <input type="button" value="INSERT"
			            id="btnInsert"
			            onclick="fnc{$tableName}Submit({$formName},'INSERT','TRUE','新規登録');"
			            class="btn btn-warning btn-sm">
			        <input type="button" value="UPDATE"
			            id="btnUpdate"
			            onclick="fnc{$tableName}Submit({$formName},'UPDATE','TRUE','更新');"
			            class="btn btn-warning btn-sm">
			        <input type="button" value="DELETE"
			            id="btnDelete"
			            onclick="fnc{$tableName}Submit({$formName},'DELETE','TRUE','削除');"
			            class="btn btn-danger btn-sm">
			
				</li>
END_OF_PHP;

	$str .= "\nEND_OF_HTML;\n\n";
	$str .= <<<END_OF_PHP
	
			break;
        default:
END_OF_PHP;
	$str .= "\n\t\t\tprint <<<END_OF_HTML\n";
	$str .= <<<END_OF_PHP
			    </li>
			 	<li>
			        <input type="button" value="RESET"
			            id="btnReset"
			            onclick="fnc{$tableName}Submit({$formName},'RESET','FALSE','');"
			            class="btn btn-default btn-sm">
			        <input type="button" value="INSERT"
			            id="btnInsert"
			            onclick="fnc{$tableName}Submit({$formName},'INSERT','TRUE','新規登録');"
			            class="btn btn-warning btn-sm">
			
				</li>
END_OF_PHP;
	$str .= "\nEND_OF_HTML;\n";
	$str .= <<<END_OF_PHP
            break;
    }
END_OF_PHP;

	$str .= "\n\tprint <<<END_OF_HTML\n\n";
	$str .= <<<END_OF_PHP
					</ul>
					</legend>
    			</div><!-- col-sm-12 end -->
    		</div><!-- row end -->
    		<!-- ﾒﾆｭｰ 制御ﾎﾞﾀﾝ 表示ROW ここまで-->

    		<!-- ﾌｫｰﾑ と ﾘｽﾄ -->
    		<div class="row row-0">
				<div class="col-sm-7 row-0">
END_OF_PHP;
    $str .= "\n\t\t\t\t\t<form class=\"form-horizontal\" role=\"form\" name=\"".$formName."\" id=\"".$formName."\" method=\"POST\" action=\"\$ThisPHP\">\n";
    $str .= "\t\t\t\t\t\t<input type=\"hidden\" name=\"SubmitMode\" id=\"SubmitMode\" value=\"\$SubmitMode\">\n";

	$str .= "\nEND_OF_HTML;\n\n";
	
	//input 要素を編集
	for ( $rowNo = 0; $rowNo < $areaRows; $rowNo++ ){
		//変数名
		$varName = 'fdt'.CreateDBWebFuncEditName($cdbNameArray[$rowNo]);
		$varComment = $cdbCommentArray[$rowNo];
		$varType = $cdbTypeArray[$rowNo];
		$varKey = $cdbKeyArray[$rowNo];
		$varDefaultValue = '$fdt'.CreateDBWebFuncEditName($cdbNameArray[$rowNo]);
		$varObjType = $cdbObjTypeArray[$rowNo];
		$varObjSize = $cdbObjSizeArray[$rowNo];
		$varReq = $cdbReqArray[$rowNo];
		
		$frm_label = $varComment;
		$frm_class = "NoCheck";
		if($varReq == '1'){
			$frm_class = "NotEmpty";
		}
		$frm_name = $varName;
		$frm_size = $varObjSize;
		$frm_req = $varReq;
		$frm_value = $varDefaultValue;

		//ｵﾌﾞｼﾞｪｸﾄﾀｲﾌﾟで処理を分岐
		switch($varObjType ){
			case 'NUMBER':
				$str .= fncEditNumberHtml($frm_label,$frm_class,$frm_name,$frm_size,$frm_req,$frm_value);
				break;
			case 'TEXTAREA':
				$str .= fncEditTextAreaHtml($frm_label,$frm_class,$frm_name,$frm_size,$frm_req,$frm_value);
				break;
			case 'SELECT':
				$str .= fncEditSelectBoxHtml($frm_label,$frm_class,$frm_name,$frm_size,$frm_req,$frm_value);
				break;
			case 'CALENDAR':
				$str .= fncEditCalendarHtml($frm_label,$frm_class,$frm_name,$frm_size,$frm_req,$frm_value);
				break;
			default:
				$str .= fncEditTextBoxHtml($frm_label,$frm_class,$frm_name,$frm_size,$frm_req,$frm_value);
				break;
		}
	}//end for


	$str .= "\tprint <<<END_OF_HTML\n\n";

	//ﾃｰﾌﾞﾙ名からajax js名を編集
	$ajaxJsName = 'ajax'.$tableName.'.js';

	$str .= <<<END_OF_PHP
						</form>
					</div>
					<div class="col-sm-5" >
	                	<!-- リストの表示位置 -->
	                    <span id="side_list_area"></span>
					</div>
				</div>
			</div>
 
		</div><!--container-->
	</div><!--wrap-->


    <script type="text/javascript" src="./ajax/{$ajaxJsName}"></script>
    <script type="text/javascript">
        fncMake{$tableName}List({$formName});
    </script>


	<!--footer表示位置--><span class="sw-footer"></span>
	<script language="JavaScript">
		AjaxFunc_FooterLoad();
	</script>
	
  </body>
</html>

END_OF_PHP;
	$str .= "\nEND_OF_HTML;\n\n";

	
	$str .= "}//end function\n\n";
	$CreateMainPHPCode .= htmlentities($str,ENT_COMPAT,"UTF-8");



	// ﾌﾟﾛﾊﾟﾃｨReset ｻﾌﾞﾙｰﾁﾝ

    $fncName = "fnc".$tableName."ResetProperty(\$".$className.")";
	$str = <<<END_OF_PHP
// ------------------------------------------------------------------------------
//      ﾌﾟﾛﾊﾟﾃｨReset
//          $fncName
// ------------------------------------------------------------------------------
function $fncName{

END_OF_PHP;
	$CreateMainPHPCode .= htmlentities($str,ENT_COMPAT,"UTF-8");
	
	//global 変数
	$CreateMainPHPCode .= $GlobalVar;
	$CreateMainPHPCode .= "\n";
	//ﾌﾟﾛﾊﾟﾃｨReset
	$CreateMainPHPCode .= "\t//ﾌﾟﾛﾊﾟﾃｨReset\n";
	for ( $rowNo = 0; $rowNo < $areaRows; $rowNo++ ){
		//変数名
		$varName = CreateDBWebFuncEditName($cdbNameArray[$rowNo]);
		//ｺﾒﾝﾄ
		$varComment = $cdbCommentArray[$rowNo];
		//列を揃えるための空白
		$sp = CreateDBWebFuncMakeSpaceWidth($varName,$maxColNameLength);
		//初期値
		$DefaultValue = $cdbDefaultValueArray[$rowNo];
		$CreateMainPHPCode .= "\t\$fdt".$varName." = \"".$DefaultValue."\";".$sp."//$varComment\n";
	}//end for

	$CreateMainPHPCode .= "}//end function\n\n";



	// ﾌﾟﾛﾊﾟﾃｨGet ｻﾌﾞﾙｰﾁﾝ 

    $fncName = "fnc".$tableName."GetProperty(\$".$className.")";
	$str = <<<END_OF_PHP
// ------------------------------------------------------------------------------
//      ﾌﾟﾛﾊﾟﾃｨGet
//          $fncName
// ------------------------------------------------------------------------------
function $fncName{

END_OF_PHP;
	$CreateMainPHPCode .= htmlentities($str,ENT_COMPAT,"UTF-8");
	
	//global 変数
	$CreateMainPHPCode .= $GlobalVar;
	$CreateMainPHPCode .= "\n";
	//ﾌﾟﾛﾊﾟﾃｨGet
	$CreateMainPHPCode .= "\t//ﾌﾟﾛﾊﾟﾃｨGet\n";
	for ( $rowNo = 0; $rowNo < $areaRows; $rowNo++ ){
		//変数名
		$varName = CreateDBWebFuncEditName($cdbNameArray[$rowNo]);
		//ｺﾒﾝﾄ
		$varComment = $cdbCommentArray[$rowNo];
		//列を揃えるための空白
		$sp = CreateDBWebFuncMakeSpaceWidth($varName.$varName,$maxColNameLength);
		
        //ｵﾌﾞｼﾞｪｸﾄﾀｲﾌﾟ
        $varObjType = $cdbObjTypeArray[$rowNo];
		//ｵﾌﾞｼﾞｪｸﾄﾀｲﾌﾟで処理を分岐
		switch($varObjType ){
			case 'TEXTAREA':
	        	$CreateMainPHPCode .= "\n\t//{$varComment}は<br>を\\nに変換する\n";
	        	$CreateMainPHPCode .= "\t\$fdt".$varName." = \$".$className."->".$className."Get".$varName."();".$sp."//$varComment\n";
	        	$CreateMainPHPCode .= "\t\$fdt".$varName." = str_replace(\"<br>\",\"\\n\",\$fdt".$varName.");\n\n";
				break;
			default:
				$CreateMainPHPCode .= "\t\$fdt".$varName." = \$".$className."->".$className."Get".$varName."();".$sp."//$varComment\n";
				break;
		}

	}//end for

	$CreateMainPHPCode .= "}//end function\n\n";


    $fncName = "fnc".$tableName."SetProperty(\$".$className.")";

	$str = <<<END_OF_PHP
// ------------------------------------------------------------------------------
//      ﾌﾟﾛﾊﾟﾃｨSet
//          $fncName
// ------------------------------------------------------------------------------
function $fncName{

END_OF_PHP;
	$CreateMainPHPCode .= htmlentities($str,ENT_COMPAT,"UTF-8");
	
	//global 変数
	$CreateMainPHPCode .= $GlobalVar;
	$CreateMainPHPCode .= "\n";
	//ﾌﾟﾛﾊﾟﾃｨSet
	$CreateMainPHPCode .= "\t//ﾌﾟﾛﾊﾟﾃｨSet\n";
	for ( $rowNo = 0; $rowNo < $areaRows; $rowNo++ ){
		//変数名
		$varName = CreateDBWebFuncEditName($cdbNameArray[$rowNo]);
		//ｺﾒﾝﾄ
		$varComment = $cdbCommentArray[$rowNo];
		//列を揃えるための空白
		$sp = CreateDBWebFuncMakeSpaceWidth($varName.$varName,$maxColNameLength);

        //ｵﾌﾞｼﾞｪｸﾄﾀｲﾌﾟ
        $varObjType = $cdbObjTypeArray[$rowNo];
		//ｵﾌﾞｼﾞｪｸﾄﾀｲﾌﾟで処理を分岐
		switch($varObjType ){
			case 'NUMBER':
				$CreateMainPHPCode .= "\n\t//{$varComment}はｶﾝﾏを取り除いてからSetする\n";
				$CreateMainPHPCode .= "\t\$fdt".$varName." = str_replace(\",\",\"\",\$fdt".$varName.");\n";
				$CreateMainPHPCode .= "\t\$".$className."->".$className."Set".$varName."(\$fdt".$varName.");".$sp."//$varComment\n\n";
				break;
			case 'TEXTAREA':
	        	$CreateMainPHPCode .= "\n\t//{$varComment}は改行を<br>にしてからSetする\n";
	        	$CreateMainPHPCode .= "\t\$fdt".$varName." = str_replace(\"\\r\\n\",\"<br>\",\$fdt".$varName.");\n";
	        	$CreateMainPHPCode .= "\t\$fdt".$varName." = str_replace(\"\\r\",\"<br>\",\$fdt".$varName.");\n";
	        	$CreateMainPHPCode .= "\t\$fdt".$varName." = str_replace(\"\\n\",\"<br>\",\$fdt".$varName.");\n";
				$CreateMainPHPCode .= "\t\$".$className."->".$className."Set".$varName."(\$fdt".$varName.");".$sp."//$varComment\n\n";
				break;
			case 'CALENDAR':
	        	$CreateMainPHPCode .= "\n\t//{$varComment}は日付要素を結合してからSetする\n";
	        	$CreateMainPHPCode .= "\t\$fdt".$varName." = \$fdt".$varName."YY.'/'."."\$fdt".$varName."MM.'/'."."\$fdt".$varName."DD;\n";
			    $CreateMainPHPCode .= "\tif (\$fdt".$varName." == \"//\"){\$fdt".$varName." = \"\";}\n";
			    $CreateMainPHPCode .= "\tif (\$fdt".$varName." == \"0000/00/00\"){\$fdt".$varName." = \"\";}\n";
				$CreateMainPHPCode .= "\t\$".$className."->".$className."Set".$varName."(\$fdt".$varName.");".$sp."//$varComment\n\n";
				break;
			default:
				$CreateMainPHPCode .= "\t\$".$className."->".$className."Set".$varName."(\$fdt".$varName.");".$sp."//$varComment\n";
				break;
		}

	}//end for
	$CreateMainPHPCode .= "}//end function\n\n";


    $fncName = "fnc".$tableName."DBInsert(\$mySqlConnObj,\$".$className.")";
	
	$str = <<<END_OF_PHP
// ------------------------------------------------------------------------------
//      DB INSERT
//          $fncName
// ------------------------------------------------------------------------------
function $fncName{

END_OF_PHP;
	$CreateMainPHPCode .= htmlentities($str,ENT_COMPAT,"UTF-8");
	
	//global 変数 AUTO INCRIMENT
	//uniqueｲﾝﾃﾞｯｸｽ
	$autoInc = '';
	//POSTされた要素を取得
	for ( $rowNo = 0; $rowNo < $areaRows; $rowNo++ ){
		//変数名
		$varName = CreateDBWebFuncEditName($cdbNameArray[$rowNo]);
		//Key
		$cdbKey = $cdbKeyArray[$rowNo];
		//uniqueｲﾝﾃﾞｯｸｽ項目編集
		if ( $cdbKey == 'AUTO_INCREMENT' ){
			$autoInc .= '$fdt'.$varName;
			$CreateMainPHPCode .= "\tglobal\t\$fdt".$varName.";\n";
		}
	}//end for
	
	$CreateMainPHPCode .= "\n";
	$CreateMainPHPCode .= "\t//ﾌﾟﾛﾊﾟﾃｨｾｯﾄ\n";
	$CreateMainPHPCode .= "\tfnc".$tableName."SetProperty(\$".$className.");\n";
	$CreateMainPHPCode .= "\t//ﾃﾞｰﾀ管理ｸﾗｽ DbInsert\n";
	if ( $autoInc  == '' ){
		$CreateMainPHPCode .= "\t\$".$className."->".$className."DbInsert(\$mySqlConnObj);\n";
	}else{
		$CreateMainPHPCode .= "\t".$autoInc." = \$".$className."->".$className."DbInsert(\$mySqlConnObj);\n";
	}

	$CreateMainPHPCode .= "}//end function\n\n";


    $fncName = "fnc".$tableName."DBUpdate(\$mySqlConnObj,\$".$className.")";
	
	$str = <<<END_OF_PHP
// ------------------------------------------------------------------------------
//      DB UPDATE
//          $fncName
// ------------------------------------------------------------------------------
function $fncName{

END_OF_PHP;
	$CreateMainPHPCode .= htmlentities($str,ENT_COMPAT,"UTF-8");

	$CreateMainPHPCode .= "\t//ﾌﾟﾛﾊﾟﾃｨｾｯﾄ\n";
	$CreateMainPHPCode .= "\tfnc".$tableName."SetProperty(\$".$className.");\n";
	$CreateMainPHPCode .= "\t//ﾃﾞｰﾀ管理ｸﾗｽ DbUpdate\n";
	$CreateMainPHPCode .= "\t\$".$className."->".$className."DbUpdate(\$mySqlConnObj);\n";
	
	$CreateMainPHPCode .= "}//end function\n\n";

    $fncName = "fnc".$tableName."DBDelete(\$mySqlConnObj,\$".$className.")";
	
	$str = <<<END_OF_PHP
// ------------------------------------------------------------------------------
//      DB DELETE
//          $fncName
// ------------------------------------------------------------------------------
function $fncName{

END_OF_PHP;
	$CreateMainPHPCode .= htmlentities($str,ENT_COMPAT,"UTF-8");

	$CreateMainPHPCode .= "\t//ﾌﾟﾛﾊﾟﾃｨｾｯﾄ\n";
	$CreateMainPHPCode .= "\tfnc".$tableName."SetProperty(\$".$className.");\n";
	$CreateMainPHPCode .= "\t//ﾃﾞｰﾀ管理ｸﾗｽ DbDelete\n";
	$CreateMainPHPCode .= "\t\$".$className."->".$className."DbDelete(\$mySqlConnObj);\n";

	$CreateMainPHPCode .= "}//end function\n\n";





//PHPの終わり
	$str = <<<END_OF_PHP

// -----------------------------------------------------------
?>
END_OF_PHP;

	$CreateMainPHPCode .= htmlentities($str,ENT_COMPAT,"UTF-8");
    
    
    return	$CreateMainPHPCode;
}



//------------------------------------------------------------------------------
//	input 部品ｻﾌﾞﾙｰﾁﾝ  textbox
//		$frm_label		ﾗﾍﾞﾙ
//		$frm_class		css class
//		$frm_name		input name&id
//		$frm_size		width
//		$frm_req		必須項目
//		$frm_value		初期値
//------------------------------------------------------------------------------
function fncEditTextBoxHtml($frm_label,$frm_class,$frm_name,$frm_size="20",$frm_req,$frm_value){

	$tmpHtml = "\tprint <<<END_OF_HTML\n\n";
	$tmpHtml .= <<<END_OF_HTML
							<div class="form-group row-0">
								<label for="$frm_name" class="control-label col-xs-3">$frm_label</label>
								<div class="col-xs-8">
									<input type="text" 
										class="form-control input-sm"
										id="$frm_name" name="$frm_name"
										value="$frm_value"
										placeholder="$frm_label">
								</div>
							</div>
END_OF_HTML;
	$tmpHtml .= "\nEND_OF_HTML;\n";

	return	$tmpHtml;
}
//------------------------------------------------------------------------------
//	input 部品ｻﾌﾞﾙｰﾁﾝ  Number
//		$frm_label		ﾗﾍﾞﾙ
//		$frm_class		css class
//		$frm_name		input name&id
//		$frm_size		width
//		$frm_req		必須項目
//		$frm_value		初期値
//------------------------------------------------------------------------------
function fncEditNumberHtml($frm_label,$frm_class,$frm_name,$frm_size="20",$frm_req,$frm_value){
	global	$formName;

	$tmpHtml = "\tprint <<<END_OF_HTML\n\n";
	$tmpHtml .= <<<END_OF_HTML
							<div class="form-group row-0">
								<label for="$frm_name" class="control-label col-xs-3">$frm_label</label>
								<div class="col-xs-8">
									<input type="number"
										class="form-control input-sm text-right"
										id="$frm_name" name="$frm_name"
										value="$frm_value"
										onkeyup="AjaxFunc_AddFigure($formName,'$frm_name');"
										placeholder="$frm_label">
								</div>
							</div>
END_OF_HTML;
	$tmpHtml .= "\nEND_OF_HTML;\n";

	return	$tmpHtml;
}
//------------------------------------------------------------------------------
//	input 部品ｻﾌﾞﾙｰﾁﾝ  textarea
//		$frm_label		ﾗﾍﾞﾙ
//		$frm_class		css class
//		$frm_name		input name&id
//		$frm_size		width
//		$frm_req		必須項目
//		$frm_value		初期値
//------------------------------------------------------------------------------
function fncEditTextAreaHtml($frm_label,$frm_class,$frm_name,$frm_size="20",$frm_req,$frm_value){

	$tmpHtml = "\tprint <<<END_OF_HTML\n\n";
	$tmpHtml .= <<<END_OF_HTML
							<div class="form-group row-0">
								<label for="$frm_name" class="control-label col-xs-3">$frm_label</label>
								<div class="col-xs-8">
									<textarea placeholder="$frm_label" 
										class="form-control input-sm"
										id="$frm_name" name="$frm_name"
										rows="3"
										id="InputTextarea">$frm_value</textarea>
								</div>
							</div>
END_OF_HTML;
	$tmpHtml .= "\nEND_OF_HTML;\n";
	
	return	$tmpHtml;
}
//------------------------------------------------------------------------------
//	input 部品ｻﾌﾞﾙｰﾁﾝ  selectbox
//		$frm_label		ﾗﾍﾞﾙ
//		$frm_class		css class
//		$frm_name		input name&id
//		$frm_size		width
//		$frm_req		必須項目
//		$frm_value		初期値
//------------------------------------------------------------------------------
function fncEditSelectBoxHtml($frm_label,$frm_class,$frm_name,$frm_size="100",$frm_req,$frm_value){

	$tmpHtml = "\tprint <<<END_OF_HTML\n\n";
	$tmpHtml .= <<<END_OF_HTML
							<div class="form-group row-0">
								<label for="$frm_name" class="control-label col-xs-3">$frm_label</label>
								<div class="col-xs-8">
								
END_OF_HTML;
	$tmpHtml .= "\nEND_OF_HTML;\n\n";
	$tmpHtml .= "\t//------------------------------------------------------------\n";
	$tmpHtml .= "\t//CSVﾃﾞｰﾀからselect box を作成する\n";
	$tmpHtml .= "\t\$ObjName = \"".$frm_name."\";		//select box の名称.ID\n";
	$tmpHtml .= "\t//ここでfunctionからCSVﾃﾞｰﾀを取得\n";
	$tmpHtml .= "\t\$csvArray = swFunc_MakeSelectItemsCsvSample();	//CSVﾃﾞｰﾀ\n";
	$tmpHtml .= "\t\$default = \"".$frm_value."\";		//ﾃﾞﾌｫﾙﾄ値\n";
	$tmpHtml .= "\t\$onChange = '';					//onChange で起動する javascript or jQuery\n";
	$tmpHtml .= "\t\$ViewCode = FALSE;				//ｺｰﾄﾞを表示する場合はTRUE\n";
	$tmpHtml .= "\t//SelectBoxHtml出力\n";
	$tmpHtml .= "\tprint swFunc_MakeSelectBox(\$ObjName,\$csvArray,\$default,\$onChange,\$ViewCode);\n";
	$tmpHtml .= "//------------------------------------------------------------\n\n";

	$tmpHtml .= "\tprint <<<END_OF_HTML\n\n";
	$tmpHtml .= <<<END_OF_HTML
								</div>
							</div>
END_OF_HTML;
	$tmpHtml .= "\nEND_OF_HTML;\n";
	
	return	$tmpHtml;

}

//------------------------------------------------------------------------------
//	input 部品ｻﾌﾞﾙｰﾁﾝ  calendar
//		$frm_label		ﾗﾍﾞﾙ
//		$frm_class		css class
//		$frm_name		input name&id
//		$frm_size		width
//		$frm_req		必須項目
//		$frm_value		初期値
//------------------------------------------------------------------------------
function fncEditCalendarHtml($frm_label,$frm_class,$frm_name,$frm_size="100",$frm_req,$frm_value){
	global	$formName;

	$tmpHtml = "\tprint <<<END_OF_HTML\n\n";
	$tmpHtml .= <<<END_OF_HTML
							<div class="form-group row-0">
								<label for="$frm_name" class="control-label col-xs-3">$frm_label</label>
								<div class="form-inline col-xs-8"><!-- calendar size inlileにする-->
								
END_OF_HTML;
	$tmpHtml .= "\nEND_OF_HTML;\n\n";
	$tmpHtml .= "\t//------------------------------------------------------------\n";
	$tmpHtml .= "\t//".$frm_label."を ｶﾚﾝﾀﾞｰ 付日付選択で作成\n";
	$tmpHtml .= "\t\$ObjName = \"".$frm_name."\";		//ObjName名 **YY,**MM,**DD で作成する\n";
	$tmpHtml .= "\t\$default = \"".$frm_value."\";		//ﾃﾞﾌｫﾙﾄ値\n";
	$tmpHtml .= "\t\$OnChange = '';					//onChange で起動する javascript or jQuery\n";
	$tmpHtml .= "\tprint swFunc_MakeCalendarBox(\$mySqlConnObj,'".$formName."',\$ObjName,\$default,TRUE,\$OnChange);\n";
	$tmpHtml .= "//------------------------------------------------------------\n\n";

	$tmpHtml .= "\tprint <<<END_OF_HTML\n\n";
	$tmpHtml .= <<<END_OF_HTML
								</div>
							</div>
END_OF_HTML;
	$tmpHtml .= "\nEND_OF_HTML;\n";
	
	return	$tmpHtml;

}




//------------------------------------------------------------------------------
?>