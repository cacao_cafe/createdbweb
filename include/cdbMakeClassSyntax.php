<?php
//------------------------------------------------------------------------------
//
//	MySQLを使ったDBI/Oｼｽﾃﾑ自動生成 Create DB Web System for MySQL with PHP&JAVASCRIPT
//			Copyright (C) 2014 ScenarioWriterCafe All Rights Reserved.
//
//		MySQL I/O Class PHPを組み立てる
//
//		fncMakeClassSyntax()
//
//   	charset=UTF-8
//------------------------------------------------------------------------------
function fncMakeClassSyntax(){
  global  $ThisPHP,$DefaultDirPath;
  global	$SubmitMode;
	global	$areaRows;
	global	$cdbDbName,$cdbTableName,$cdbTableNameComment,$cdbSelectTable,$cdbDefaultCharSet;
	global	$cdbNameArray,$cdbCommentArray,$cdbTypeArray;
	global	$cdbColSizeArray,$cdbKeyArray,$cdbDefaultValueArray;
	global	$cdbObjTypeArray,$cdbObjSizeArray,$cdbListArray,$cdbReqArray;
	global	$maxColNameLength;
	global	$CreateClassSyntax;

	global	$d;
	global	$TO_END_OF_SQL;
	
	$d = '$';
	$TO_END_OF_SQL = '<<<END_OF_SQL';
	//現在の日付を取得
	$today = getdate();
	$THIS_YEAR = sprintf( "%04d",$today["year"]);

	//ﾃｰﾌﾞﾙ名からclass名を編集
	$className = 'cls'.CreateDBWebFuncEditName($cdbTableName);
	$classfncName = 'clsfnc'.CreateDBWebFuncEditName($cdbTableName);

//-----------------------------------------------------------------------------------------------------------------
	$str = <<<END_OF_CLASS
<?php
// -----------------------------------------------------------
//
// Copyright (C) $THIS_YEAR ScenarioWriterCafe All Rights Reserved.
// 
//     $cdbTableNameComment ﾃﾞｰﾀ管理ｸﾗｽ
//     $className
// -----------------------------------------------------------
class $className{

END_OF_CLASS;


	$CreateClassSyntax = htmlentities($str,ENT_COMPAT,"UTF-8");
//-----------------------------------------------------------------------------------------------------------------
	
	//key 項目編集
	$UniqueIndexCols = '';
	$keyCnt = 0;
	//WHERE句
	//$WherePhrase = "\t\t\t\tWHERE ";
	$WherePhrase = <<<END_OF_DATA
				WHERE 
END_OF_DATA;
	$WherePhrase = '';
	$SelectParam = "\n\t\t//パラメータのセット";
	//変数宣言
	for ( $rowNo = 0; $rowNo < $areaRows; $rowNo++ ){
		//Column名　大文字
		$ColNameU = strtoupper($cdbNameArray[$rowNo]);
        if( strlen($ColNameU) > $maxColNameLength ){
            $maxColNameLength = strlen($ColNameU);
        }
		//変数名
		$ColName = CreateDBWebFuncEditName($cdbNameArray[$rowNo]);
        $cdbComment = $cdbCommentArray[$rowNo];
		//Key
		$cdbKey = $cdbKeyArray[$rowNo];
		//key 項目編集
		if ( $cdbKey == 'PRIMARY KEY' or $cdbKey == 'AUTO_INCREMENT' ){
			$UniqueIndexCols .= ',$val'.$ColName;
			//WHERE句
			$keyCnt++;
			if ($keyCnt == 1 ){
				$WherePhrase .=<<<END_OF_DATA
		WHERE $ColNameU = :{$ColName}
END_OF_DATA;
			//bindParam
				$SelectParam .=<<<END_OF_DATA

		{$d}stmt->bindParam(':{$ColName}', {$d}val{$ColName}, PDO::PARAM_INT);
END_OF_DATA;
			}else{
				$WherePhrase .= <<<END_OF_DATA
				
			AND $ColNameU = :{$ColName}
END_OF_DATA;
			//bindParam
				$SelectParam .=<<<END_OF_DATA

		{$d}stmt->bindParam(':{$ColName}', {$d}val{$ColName}, PDO::PARAM_INT);
END_OF_DATA;
			}
		}
		//列を揃えるための空白
		$sp = CreateDBWebFuncMakeSpaceWidth($ColName,$maxColNameLength);
        $CreateClassSyntax .= "\tvar \$".$ColName.";".$sp."//".$cdbComment."\n";
	}//end for
	
// ---------------------------------------------------------------------------------------------
	$CreateClassSyntax .= <<<END_OF_CLASS
// ｺﾝｽﾄﾗｸﾀ
    function $classfncName(){
    }
// ﾌﾟﾛﾊﾟﾃｨ

END_OF_CLASS;

// ---------------------------------------------------------------------------------------------

	for ( $rowNo = 0; $rowNo < $areaRows; $rowNo++ ){
		//Column名　大文字
		$ColNameU = strtoupper($cdbNameArray[$rowNo]);
		//変数名
		$ColName = CreateDBWebFuncEditName($cdbNameArray[$rowNo]);
		$cdbComment = $cdbCommentArray[$rowNo];
		$cdbType = $cdbTypeArray[$rowNo];
		$cdbColSize = $cdbColSizeArray[$rowNo];
		$cdbKey = $cdbKeyArray[$rowNo];
    $cdbDefaultValue = $cdbDefaultValueArray[$rowNo];
		
// ---------------------------------------------------------------------------------------------
		$CreateClassSyntax .= <<<END_OF_CLASS
// --------------------------------------
// ﾌﾟﾛﾊﾟﾃｨGet $ColName
//    $cdbComment を返す
// --------------------------------------
	function {$className}Get{$ColName}(){
		return {$d}this->{$ColName};
	}
// --------------------------------------
// ﾌﾟﾛﾊﾟﾃｨSet $ColName
//    $cdbComment を設定する
// --------------------------------------
	function {$className}Set{$ColName}({$d}val{$ColName}){

END_OF_CLASS;
// ---------------------------------------------------------------------------------------------
	 	//空白の時はDefaultValueにする
	    if($cdbDefaultValue != ''){
	    	$CreateClassSyntax .= <<<END_OF_DATA
		if({$d}val{$ColName} == ''){
			{$d}val{$ColName} = '{$cdbDefaultValue}';
		}

END_OF_DATA;
	    }
	    $CreateClassSyntax .= <<<END_OF_DATA
		{$d}this->{$ColName} = {$d}val{$ColName};
	}

END_OF_DATA;
	}//end for


// ---------------------------------------------------------------------------------------------
$CreateClassSyntax .= <<<END_OF_DATA

// -------------------------------------------------------------
//    ｸﾗｽ初期化
// -------------------------------------------------------------
	function {$className}Init({$d}mySqlConnObj{$UniqueIndexCols}){
		//ﾌﾟﾛﾊﾟﾃｨの初期化

END_OF_DATA;
	//ﾌﾟﾛﾊﾟﾃｨの初期化
	for ( $rowNo = 0; $rowNo < $areaRows; $rowNo++ ){
		//Column名　大文字
		$ColNameU = strtoupper($cdbNameArray[$rowNo]);
		//変数名
		$ColName = CreateDBWebFuncEditName($cdbNameArray[$rowNo]);
		$cdbComment = $cdbCommentArray[$rowNo];
		$cdbDefaultValue = $cdbDefaultValueArray[$rowNo];
		//列を揃えるための空白
		$sp = CreateDBWebFuncMakeSpaceWidth($ColName.$cdbDefaultValue,$maxColNameLength);
		$CreateClassSyntax .= <<<END_OF_DATA
		{$d}this->{$ColName} = '{$cdbDefaultValue}';{$sp}//{$cdbComment}

END_OF_DATA;
	}//end for
   $CreateClassSyntax .= CreateDBWebFuncEditSQLStart("DB から ﾃﾞｰﾀを取得しﾌﾟﾛﾊﾟﾃｨにｾｯﾄする",2);
	//SELECT SQLを組み立てる
    $CreateClassSyntax .= "\t\tSELECT * FROM ".strtoupper($cdbTableName)."\n";
    $CreateClassSyntax .= "\t".$WherePhrase.";\n";
	//SQL END
	$CreateClassSyntax .= CreateDBWebFuncEditSQLEnd();

    //SELECT SQL実行文
	$CreateClassSyntax .=<<<END_OF_DATA
		{$d}stmt = {$d}mySqlConnObj->prepare({$d}strSQL);
		{$d}stmt->setFetchMode(PDO::FETCH_ASSOC);
END_OF_DATA;
	//パラメータ
	$CreateClassSyntax .= $SelectParam;
	$CreateClassSyntax .=<<<END_OF_DATA
	
		{$d}stmt->execute();
		while({$d}myRow = {$d}stmt -> fetch(PDO::FETCH_ASSOC)) {
END_OF_DATA;
	//変数に代入
	for ( $rowNo = 0; $rowNo < $areaRows; $rowNo++ ){
		//Column名　大文字
		$ColNameU = strtoupper($cdbNameArray[$rowNo]);
		//変数名
		$ColName = CreateDBWebFuncEditName($cdbNameArray[$rowNo]);
		$cdbComment = $cdbCommentArray[$rowNo];
		$CreateClassSyntax .=<<<END_OF_DATA

			{$d}this->{$ColName} = {$d}myRow['{$ColNameU}'];
END_OF_DATA;
	}//end for


// ---------------------------------------------------------------------------------------------
$CreateClassSyntax .= <<<END_OF_CLASS

		}//end while
    }//end function

// -------------------------------------------------------------
//    DB更新　INSERT
// -------------------------------------------------------------
	function {$className}DbInsert({$d}mySqlConnObj){
		//ﾌﾟﾛﾊﾟﾃｨ取得

END_OF_CLASS;
	//ﾌﾟﾛﾊﾟﾃｨ取得
	$PropertySyntax = '';
	$updateParam = '';
	for ( $rowNo = 0; $rowNo < $areaRows; $rowNo++ ){
		//Column名　大文字
		$ColNameU = strtoupper($cdbNameArray[$rowNo]);
		//変数名
		$ColName = CreateDBWebFuncEditName($cdbNameArray[$rowNo]);
		$cdbComment = $cdbCommentArray[$rowNo];
		$cdbKey = $cdbKeyArray[$rowNo];
		if ( $cdbKey != 'AUTO_INCREMENT' ){
			//bindParam
			$updateParam .=<<<END_OF_DATA

		{$d}stmt->bindParam(':{$ColName}', {$d}val{$ColName}, PDO::PARAM_INT);
END_OF_DATA;
		}

		$PropertySyntax .=<<<END_OF_DATA
		{$d}val{$ColName} = {$d}this->{$ColName};

END_OF_DATA;
	}//end for
	$CreateClassSyntax .= $PropertySyntax;
// ---------------------------------------------------------------------------------------------
    //SQL文を編集する
    $NoAutoIncSQL   = CreateDBWebFuncEditSQLStart("登録確認",2);
	//SQLを組み立てる
    $NoAutoIncSQL .= "\t\tSELECT * FROM ".strtoupper($cdbTableName)."\n";
    $NoAutoIncSQL .= $WherePhrase.";\n";
    $NoAutoIncSQL .= CreateDBWebFuncEditSQLEnd();

	//パラメータ
	$NoAutoIncSQL .= $updateParam;
	$NoAutoIncSQL .=<<<END_OF_DATA
	
		{$d}stmt->execute();
END_OF_DATA;
	$NoAutoIncSQL .=<<<END_OF_DATA

		//件数取得
		{$d}myRowCnt = {$d}stmt->rowCount();
		if({$d}myRowCnt == 0 ){
            echo {$d}this->fncAlert("{$className} ->> NOT ENTRY!");
        }else{
        }//end if
END_OF_DATA;
    //SQL文を編集する
    $InsertSQL = CreateDBWebFuncEditSQLStart("INSERT SQL",2);
	//SQLを組み立てる
	$InsertSQL .= "\t\tINSERT INTO ".strtoupper($cdbTableName)."(\n";

	$col_cnt = 0;
	$InsertColumnName = '';
	$InsertDataName = '';
	$GetIdSQL = '';
	$ReturnSQL = '';
	for ( $rowNo = 0; $rowNo < $areaRows; $rowNo++ ){
		
		//Column名　大文字
		$ColNameU = strtoupper($cdbNameArray[$rowNo]);
		//変数名
		$ColName = CreateDBWebFuncEditName($cdbNameArray[$rowNo]);
		$cdbComment = $cdbCommentArray[$rowNo];
		$cdbType = $cdbTypeArray[$rowNo];
		$cdbColSize = $cdbColSizeArray[$rowNo];
		$cdbKey = $cdbKeyArray[$rowNo];
        
        $updateColName = ":".$ColName;
        //date now()
        if( $cdbType == 'DATENOW'){
            $updateColName = 'now()';
        }
        //timestamp
        if( $cdbType == 'TIMESTAMP'){
            $updateColName = 'current_timestamp';
        }
		$col_cnt++;
		if ( $col_cnt == 1 ){
			if ( $cdbKey != 'AUTO_INCREMENT' ){
				$InsertColumnName .= "\t\t\t  ".$ColNameU."\n";
				$InsertDataName .=  "\t\t\t  ".$updateColName."\n";
			}else{
				$col_cnt = $col_cnt - 1;
	       		$GetIdSQL = "\t\t//AUTO_INCREMENT値取得\n";
				$GetIdSQL .= "\t\t\$LastInsertId = \$mySqlConnObj->lastInsertId();\n";
	        	$ReturnSQL = "\t\t//AUTO_INCREMENT値を返す\n";
				$ReturnSQL .= "\t\treturn \$LastInsertId;\n";
			}
		}else{
			if ( $cdbKey != 'AUTO_INCREMENT' ){
				$InsertColumnName .= "\t\t\t, ".$ColNameU."\n";
				$InsertDataName .=  "\t\t\t, ".$updateColName."\n";
				$NoAutoIncSQL = '';
				$NoAutoIncSQLEnd = '';
			}
		}
	}//end for
	
	$CreateClassSyntax .= $NoAutoIncSQL;
	$CreateClassSyntax .= $InsertSQL;
	
	$CreateClassSyntax .= $InsertColumnName;
	$CreateClassSyntax .= "\t\t\t) values (\n";
	$CreateClassSyntax .= $InsertDataName;
	$CreateClassSyntax .= "\t\t\t);\n";
	//SQLを実行する
	$CreateClassSyntax .= CreateDBWebFuncEditSQLEnd();
	
	$CreateClassSyntax .=<<<END_OF_DATA

		{$d}stmt = {$d}mySqlConnObj->prepare({$d}strSQL);
END_OF_DATA;
	//パラメータ
	$CreateClassSyntax .= $updateParam;
	$CreateClassSyntax .=<<<END_OF_DATA
	
		{$d}stmt->execute();

END_OF_DATA;
	$CreateClassSyntax .= $GetIdSQL;
	$CreateClassSyntax .= $ReturnSQL;
    $CreateClassSyntax .= $NoAutoIncSQLEnd;
	$CreateClassSyntax .= "\t}//end function\n";
// ---------------------------------------------------------------------------------------------
    $CreateClassSyntax .= <<<END_OF_CLASS

// -------------------------------------------------------------
//    DB更新　UPDATE
// -------------------------------------------------------------
	function {$className}DbUpdate({$d}mySqlConnObj){
		//プロパティ取得

END_OF_CLASS;
    $CreateClassSyntax .= $PropertySyntax;
    
    //SQL文を編集する
    $CreateClassSyntax .= CreateDBWebFuncEditSQLStart("登録確認",2);
	//SQLを組み立てる
    $CreateClassSyntax .= "\t\tSELECT * FROM ".strtoupper($cdbTableName)."\n";
    $CreateClassSyntax .= $WherePhrase.";\n";
    $CreateClassSyntax .= CreateDBWebFuncEditSQLEnd();

	$CreateClassSyntax .=<<<END_OF_DATA

		{$d}stmt = {$d}mySqlConnObj->prepare({$d}strSQL);
END_OF_DATA;
	//パラメータ
	$CreateClassSyntax .= $SelectParam;
	$CreateClassSyntax .=<<<END_OF_DATA
	
		{$d}stmt->execute();
		//件数取得
		{$d}myRowCnt = {$d}stmt->rowCount();
		if({$d}myRowCnt == 0 ){
            echo {$d}this->fncAlert("{$className} ->> NOT ENTRY!");
        }else{

END_OF_DATA;
    //SQL文を編集する
    $CreateClassSyntax .= CreateDBWebFuncEditSQLStart("UPDATE SQL",3);
    //SQLを組み立てる
    $CreateClassSyntax .= "\t\t\tUPDATE ".strtoupper($cdbTableName)." SET \n"; 
    //UPDATE SQL
    $col_cnt = 0;
	for ( $rowNo = 0; $rowNo < $areaRows; $rowNo++ ){
		//Column名　大文字
		$ColNameU = strtoupper($cdbNameArray[$rowNo]);
		//変数名
		$ColName = CreateDBWebFuncEditName($cdbNameArray[$rowNo]);
		$cdbComment = $cdbCommentArray[$rowNo];
        $cdbType = $cdbTypeArray[$rowNo];
		$cdbKey = $cdbKeyArray[$rowNo];
		$col_cnt++;
        //PRIMARY KEY は更新しない
		if ( $cdbKey == 'PRIMARY KEY' or $cdbKey == 'AUTO_INCREMENT' ){
			$col_cnt = $col_cnt - 1;
		}else{
            if ( $col_cnt == 1 ){
		        $leftSp = "\t\t\t\t  "; 
            }else{
                $leftSp = "\t\t\t\t, ";
            }
            $updateColName = "'\$val".$ColName."'";
            //date now()
            if( $cdbType == 'DATENOW'){
                $updateColName = 'now()';
            }
            //timestamp
            if( $cdbType == 'TIMESTAMP'){
                $updateColName = 'current_timestamp';
            }
            //列を揃えるための空白
	        $sp = CreateDBWebFuncMakeSpaceWidth($ColNameU.$updateColName,$maxColNameLength);
            $CreateClassSyntax .= $leftSp.$ColNameU." = ".$updateColName."\n";
        }
	}//end for
    //WHERE句
    $CreateClassSyntax .= $WherePhrase.";\n";
    $CreateClassSyntax .= CreateDBWebFuncEditSQLEnd();

	$CreateClassSyntax .=<<<END_OF_DATA

			{$d}stmt = {$d}mySqlConnObj->prepare({$d}strSQL);
END_OF_DATA;
	//パラメータ
	$CreateClassSyntax .= "\t".$SelectParam;
	$CreateClassSyntax .=<<<END_OF_DATA
	
			{$d}stmt->execute();
		}//end if
	}//end function

// -------------------------------------------------------------
//    DB更新　DELETE
// -------------------------------------------------------------
	function {$className}DbDelete({$d}mySqlConnObj){
		//ﾌﾟﾛﾊﾟﾃｨ取得

END_OF_DATA;
	//KEY部のみﾌﾟﾛﾊﾟﾃｨ取得
	for ( $rowNo = 0; $rowNo < $areaRows; $rowNo++ ){
		if(isset($cdbKeyArray[$rowNo])){
			$cdbKey = $cdbKeyArray[$rowNo];
        }
        //PRIMARY KEY
		if ( $cdbKey == 'PRIMARY KEY' or $cdbKey == 'AUTO_INCREMENT' ){
    		//Column名　大文字
		    $ColNameU = strtoupper($cdbNameArray[$rowNo]);
		    //変数名
		    $ColName = CreateDBWebFuncEditName($cdbNameArray[$rowNo]);
		    $cdbComment = $cdbCommentArray[$rowNo];
            //ﾌﾟﾛﾊﾟﾃｨ取得
		    $CreateClassSyntax .= "\t\t\$val".$ColName." = \$this->".$ColName.";\n";
        }
	}//end for
    //SQL文を編集する
    $CreateClassSyntax .= CreateDBWebFuncEditSQLStart("登録確認",2);
	//SQLを組み立てる
    $CreateClassSyntax .= "\t\tSELECT * FROM ".strtoupper($cdbTableName)."\n";
    $CreateClassSyntax .= "\t".$WherePhrase.";\n";
    $CreateClassSyntax .= CreateDBWebFuncEditSQLEnd();

	$CreateClassSyntax .=<<<END_OF_DATA
		{$d}stmt = {$d}mySqlConnObj->prepare({$d}strSQL);
END_OF_DATA;
	//パラメータ
	$CreateClassSyntax .= $SelectParam;
	$CreateClassSyntax .=<<<END_OF_DATA
	
		{$d}stmt->execute();
		//件数取得
		{$d}myRowCnt = {$d}myResult->num_rows;
		if({$d}myRowCnt == 0 ){
            echo {$d}this->fncAlert("{$className} ->> NOT ENTRY!");
        }else{

END_OF_DATA;
    //SQL文を編集する
    $CreateClassSyntax .= CreateDBWebFuncEditSQLStart("DELETE SQL",3);
	//SQLを組み立てる
    $CreateClassSyntax .= "\t\t\tDELETE FROM ".strtoupper($cdbTableName)."\n"; 
    $CreateClassSyntax .= "\t".$WherePhrase.";\n";
    $CreateClassSyntax .= CreateDBWebFuncEditSQLEnd();
	$CreateClassSyntax .=<<<END_OF_DATA

			{$d}stmt = {$d}mySqlConnObj->prepare({$d}strSQL);
END_OF_DATA;
	//パラメータ
	$CreateClassSyntax .= "\t".$SelectParam;
	$CreateClassSyntax .=<<<END_OF_DATA
	
			{$d}stmt->execute();
		}//end if
	}//end function

END_OF_DATA;
	//JAVA SCRIPTダイアログ
	$CreateClassSyntax .= CreateDBWebFuncMakeAlertJava();
//PHPの終わり
	$str = <<<END_OF_PHP
	
} // end class
// -----------------------------------------------------------
?>
END_OF_PHP;

	$CreateClassSyntax .= htmlentities($str,ENT_COMPAT,"UTF-8");
	
    return	$CreateClassSyntax;
    
    
}//end fncMakeClassSyntax()

//------------------------------------------------------------------------------
?>