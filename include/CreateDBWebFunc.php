<?php
// ------------------------------------------------------------------------------
//
//	MySQLを使ったﾃﾞｰﾀI/Oｼｽﾃﾑ自動生成共通サブルーチン
//		createDBColFunc.php
//		
//    	charset=UTF-8
// Copyright (C) 2015 dbu@mac.com All Rights Reserved.
// ------------------------------------------------------------------------------
// ------------------------------------------------------------------------------
//	CreateDBWebFuncGetPostData($form_name)
//	POSTﾃﾞｰﾀから内容を取得(ｻﾆﾀｲｽﾞ)
// ------------------------------------------------------------------------------
function CreateDBWebFuncGetPostData($form_name){
	$postData = '';
	if (isset($_POST[$form_name])) {
		//ｻﾆﾀｲｽﾞ
		$postData = htmlspecialchars($_POST[$form_name], ENT_QUOTES, 'UTF-8');
	}else{
		$postData = '';
	}
	return	$postData;
}

// ------------------------------------------------------------------------------
// //ﾍｯﾀﾞ表示PHPを編集
//		$str = CreateDBWebFuncEditHeader($valHeaderTitle,$HeaderPath);
// ------------------------------------------------------------------------------
function CreateDBWebFuncEditHeader($valHeaderTitle,$HeaderPath){
	$str = '';
	if ( $valHeaderTitle == '' ){$valHeaderTitle = 'Create DB Web System';}
    // ﾍｯﾀﾞ表示
    $str .= "\t// ﾍｯﾀﾞ表示\n";
    $str .= "\t\$HtmlTitle = \"".$valHeaderTitle."\";\n";
    $str .= "\tinclude_once(\"".$HeaderPath."\");\n\n";

	return	htmlentities($str,ENT_COMPAT,"UTF-8");
}
// ------------------------------------------------------------------------------
// //共通定数読み込みを編集
//		$str = CreateDBWebFuncEditIncludeConstant($Path)
// ------------------------------------------------------------------------------
function CreateDBWebFuncEditIncludeConstant($Path){
	$str = '';
	$str .= "\t//共通定数\n";
    $str .= "\tinclude_once(\"".$Path."\");\n\n";

	return	htmlentities($str,ENT_COMPAT,"UTF-8");
}

// ------------------------------------------------------------------------------
// //MySQL接続PHPを編集
//		$str = CreateDBWebFuncEditMySQLConnect($dbConPath)
// ------------------------------------------------------------------------------
function CreateDBWebFuncEditMySQLConnect($dbConPath){
	$str = '';
	$str .= "\t//MySQLに接続\n";
    $str .= "\tinclude_once(\"".$dbConPath."\");\n\n";

	return	htmlentities($str,ENT_COMPAT,"UTF-8");
}

// ------------------------------------------------------------------------------
// //ﾌｯﾀPHPを編集
//		$str = CreateDBWebFuncEditFooter($FooterPath);
// ------------------------------------------------------------------------------
function CreateDBWebFuncEditFooter($FooterPath){
	$str = '';
    $str .= "\t//ﾌｯﾀ\n";
    $str .= "\tinclude_once(\"".$FooterPath."\");\n";

	return	htmlentities($str,ENT_COMPAT,"UTF-8");
}

// ------------------------------------------------------------------------------
// //MySQL切断PHPを編集
//		$str = CreateDBWebFuncEditMySQLDisConnect($dbDisConPath);
// ------------------------------------------------------------------------------
function CreateDBWebFuncEditMySQLDisConnect($dbDisConPath){
	$str = '';
    $str .= "\t//DB切断\n";
    $str .= "\tinclude_once(\"".$dbDisConPath."\");\n";

	return	htmlentities($str,ENT_COMPAT,"UTF-8");
}

// ------------------------------------------------------------------------------
// $strSQL = <<<END_OF_SQL
//
//		$str = CreateDBWebFuncEditSQLStart($valComment,$valLength);
// ------------------------------------------------------------------------------
function CreateDBWebFuncEditSQLStart($valComment,$valtabLength=0){
	//\t編集
	$tab = CreateDBWebFuncMakeTabWidth($valtabLength);

	$str = $tab."//$valComment\n";
	$str .= $tab."\$strSQL = <<<END_OF_SQL\n\n";
	
	return	htmlentities($str,ENT_COMPAT,"UTF-8");
}

// ------------------------------------------------------------------------------
// END_OF_SQL;
//
//		$str = CreateDBWebFuncEditSQLEnd();
// ------------------------------------------------------------------------------
function CreateDBWebFuncEditSQLEnd(){
	$str = "END_OF_SQL;\n\n";
	
	return	htmlentities($str,ENT_COMPAT,"UTF-8");
}
// ------------------------------------------------------------------------------
// ExecuteSQL
//
//		$str = CreateDBWebFuncEditExecuteSQL($valResult,$valComment,$valtabLength=0);
// ------------------------------------------------------------------------------
function CreateDBWebFuncEditExecuteSQL($valResult,$valComment,$valtabLength=0){
	//\t編集
	$tab = CreateDBWebFuncMakeTabWidth($valtabLength);

	$str = $tab."//$valComment \n";
	
	//$str .= $tab.$valResult." = \$mySqlConnObj->query(\$strSQL) or die(\$this->fncAlert(\$mySqlConnObj->error));\n\n";

	$str .= $tab.$valResult." = \$mySqlConnObj->prepare(\$strSQL);\n";
	//EXECUTE
	$str .= $tab."\$stmt->execute();\n\n";


	return	htmlentities($str,ENT_COMPAT,"UTF-8");
}
// ------------------------------------------------------------------------------
// Execute後にWhileで取得するときのSQL
//
//		$str = CreateDBWebFuncEditExecuteWhileSQL($valResult,$valtabLength=0);
// ------------------------------------------------------------------------------
function CreateDBWebFuncEditExecuteWhileSQL($valResult,$valtabLength=0){

	//\t編集
	$tab = CreateDBWebFuncMakeTabWidth($valtabLength);
	
	$str = '';
	//$str = $tab."\$myResult = \$mySqlConnObj->query(\$strSQL) or die(\$this->fncAlert(\$mySqlConnObj->error));\n";
    //$str .= $tab."while(\$myRow = \$myResult->fetch_assoc( )){\n";
	$d = '$';
	$str =<<<END_OF_DATA
	
		{$d}stmt = {$d}mySqlConnObj->prepare({$d}strSQL);
		{$d}stmt->setFetchMode(PDO::FETCH_ASSOC);
		//パラメータのセット
		{$d}stmt->bindParam(':AdId', {$d}valAdId, PDO::PARAM_INT);
		{$d}stmt->execute();
		while({$d}myRow = {$d}stmt -> fetch(PDO::FETCH_ASSOC)) {
END_OF_DATA;

	return	htmlentities($str,ENT_COMPAT,"UTF-8");
}

// ------------------------------------------------------------------------------
// \tを編集
//		$sp = CreateDBWebFuncMakeTabWidth($valtabLength);
// ------------------------------------------------------------------------------
function CreateDBWebFuncMakeTabWidth($valtabLength){
	$tab = '';
	for ( $i = 0; $i < $valtabLength; $i++ ){
		$tab .= "\t";
	}

	return	$tab;
}

// ------------------------------------------------------------------------------
// ﾃﾞｰﾀ名編集
// '_'を削除し先頭を大文字にする
//
//	ex.  abc_def_gh --> AbcDefGh
//
//		$nm = CreateDBWebFuncEditName($valName);
// ------------------------------------------------------------------------------
function CreateDBWebFuncEditName($valName){
	//小文字にする
    $NameArray = explode('_',strtolower($valName));
    $nm = '';
	// 配列数分ループして値を取り出す
	foreach ($NameArray as $tmp) {
		//最初の文字を大文字にする
		$nm .= ucfirst($tmp);
	}
	return	$nm;
}

// ------------------------------------------------------------------------------
// 列を揃えるための空白を編集
//		$sp = CreateDBWebFuncMakeSpaceWidth($valStr,$DefaultMaxWidth=64)
// ------------------------------------------------------------------------------
function CreateDBWebFuncMakeSpaceWidth($valStr,$DefaultMaxWidth=64){
	$spcnt = $DefaultMaxWidth - strlen($valStr);
	$sp = '';
	for ( $i = 0; $i < $spcnt; $i++ ){
		$sp .= ' ';
	}
	return	$sp;
}

// ------------------------------------------------------------------------------
// JAVA SCRIPT でダイアログを表示する
// ------------------------------------------------------------------------------
function CreateDBWebFuncMakeAlertJava(){
	$str = <<<END_OF_HTML
	// ------------------------------------------------------------------------------
	// java script alert
	// ------------------------------------------------------------------------------

END_OF_HTML;
    $str .= "\tfunction fncAlert(\$AlertMsg){\n";

	$str .= "\t\t\$strAlert = <<<END_OF_TEXT\n\n";
	$str .= "\t\t\t<script language=\"JavaScript\">\n";
    $str .= "\t\t\t\tbootbox.alert(\"\$AlertMsg\",function() {\n";
    $str .= "\t\t\t});\n";
    $str .= "\t\t\t</script>\n";
	$str .= "END_OF_TEXT;\n\n";
    $str .= "\t\treturn \$strAlert;\n";
    $str .= "\t}\n";
	
	return	htmlentities($str,ENT_COMPAT,"UTF-8");

}

// ------------------------------------------------------------------------------
// JAVA SCRIPT でダイアログを表示する
// ------------------------------------------------------------------------------
function CreateDBWebFuncAlert($AlertMsg){
    $strAlert = <<<END_OF_TEXT
 
        <script language="javascript">
            alert("$AlertMsg");
        </script>
END_OF_TEXT;
 
    return $strAlert;

}

// ------------------------------------------------------------------------------
//  FORM 部品に関するサブルーチン
// ------------------------------------------------------------------------------
// ﾃﾞｰﾀﾃﾞｨﾚｸﾄﾘのﾃﾞｨﾚｸﾄﾘ名のCSVﾃﾞｰﾀ を 取得する
//		$csvArray = CreateDBWebFuncMakeSaveDirCsv($cdbDir)
// ------------------------------------------------------------------------------
function CreateDBWebFuncMakeSaveDirCsv($cdbDirPath){
	$csvArray = array();
    
	if ($dir = opendir($cdbDirPath)) {
	    while (($file = readdir($dir)) !== false) {
	        if ($file != "." && $file != "..") {
	        	//ﾌﾙﾊﾟｽでDIR判定
	        	if (is_dir($cdbDirPath.'/'.$file)){
	        		//TEMP以外をﾘｽﾄｱｯﾌﾟ
		        	if ($file != "TEMP" and $file != "acct_config"){
		            	array_push($csvArray,"$file:$file");
					}
		        }
	        }
	    }
	    closedir($dir);
	}
	//配列をSORT
	sort($csvArray);
	//先頭にnew tableを追加
	array_unshift($csvArray,"new table:new table");
	
	return	$csvArray;
}

// ------------------------------------------------------------------------------
// DB NAMEのCSVﾃﾞｰﾀ を 取得する
//		$csvArray = CreateDBWebFuncMakeCdbDbNameSetCsv()
// ------------------------------------------------------------------------------
function CreateDBWebFuncMakeCdbDbNameSetCsv(){
	$csvArray = array();
	array_push($csvArray,"dbu_swdb:dbu_swdb");
	array_push($csvArray,"dbu_cacaocmsdb:dbu_cacaocmsdb");
	return	$csvArray;
}
// ------------------------------------------------------------------------------
// cdbDefaultCharSetのCSVﾃﾞｰﾀ を 取得する
//		$csvArray = CreateDBWebFuncMakeCdbDefaultCharSetCsv()
// ------------------------------------------------------------------------------
function CreateDBWebFuncMakeCdbDefaultCharSetCsv(){
	$csvArray = array();
	array_push($csvArray,"UTF8:UTF8");
	array_push($csvArray,"UJIS:UJIS");
	array_push($csvArray,"SJIS:SJIS");
	return	$csvArray;
}
// ------------------------------------------------------------------------------
// cdbTypeのCSVﾃﾞｰﾀ を 取得する
//		$csvArray = CreateDBWebFuncMakeCdbTypeCsv()
// ------------------------------------------------------------------------------
function CreateDBWebFuncMakeCdbTypeCsv(){
	$csvArray = array();
	array_push($csvArray,"TINYINT:TINYINT");
	array_push($csvArray,"SMALLINT:SMALLINT");
	array_push($csvArray,"MEDIUMINT:MEDIUMINT");
	array_push($csvArray,"INT:INT");
	array_push($csvArray,"BIGINT:BIGINT");
	array_push($csvArray,"FLOAT:FLOAT");
	array_push($csvArray,"DOUBLE:DOUBLE");
	array_push($csvArray,"DECIMAL:DECIMAL");
	array_push($csvArray,"-------:-------");
	array_push($csvArray,"CHAR:CHAR");
	array_push($csvArray,"VARCHAR:VARCHAR");
	array_push($csvArray,"TINYBLOB:TINYBLOB");
	array_push($csvArray,"MEDIUMBLOB:MEDIUMBLOB");
	array_push($csvArray,"LONGBLOB:LONGBLOB");
	array_push($csvArray,"TINYTEXT:TINYTEXT");
	array_push($csvArray,"TEXT:TEXT");
	array_push($csvArray,"MEDIUMTEXT:MEDIUMTEXT");
	array_push($csvArray,"LONGTEXT:LONGTEXT");
	array_push($csvArray,"-------:-------");
	array_push($csvArray,"DATE:DATE");
	array_push($csvArray,"DATENOW:DATE NOW");
	array_push($csvArray,"TIMESTAMP:TIMESTAMP");
	return	$csvArray;
}

// ------------------------------------------------------------------------------
// cdbKeyのCSVﾃﾞｰﾀ を 取得する
//		$csvArray = CreateDBWebFuncMakeCdbKeyCsv()
// ------------------------------------------------------------------------------
function CreateDBWebFuncMakeCdbKeyCsv(){
	$csvArray = array();
	array_push($csvArray,"0:");
	array_push($csvArray,"PRIMARY KEY:PRIMARY KEY");
	array_push($csvArray,"AUTO_INCREMENT:AUTO_INC");
	array_push($csvArray,"INDEX:INDEX");
	return	$csvArray;
}
// ------------------------------------------------------------------------------
// cdbObjTypeのCSVﾃﾞｰﾀ を 取得する
//		$csvArray = CreateDBWebFuncMakeCdbObjTypeCsv()
// ------------------------------------------------------------------------------
function CreateDBWebFuncMakeCdbObjTypeCsv(){
	$csvArray = array();
	array_push($csvArray,"TEXT:TEXT");
	array_push($csvArray,"NUMBER:NUMBER");
	array_push($csvArray,"TEXTAREA:TEXTAREA");
	array_push($csvArray,"SELECT:SELECT");
	array_push($csvArray,"CHECK:CHECK");
	array_push($csvArray,"DATE:DATE");
	array_push($csvArray,"CALENDAR:CALENDAR");
	return	$csvArray;
}
// ------------------------------------------------------------------------------
// YesNoのCSVﾃﾞｰﾀ を 取得する
//		$csvArray = fncMakeYesNoCsv()
// ------------------------------------------------------------------------------
function CreateDBWebFuncMakeYesNoCsv(){
	$csvArray = array();
	array_push($csvArray,"0:");
	array_push($csvArray,"1:✔");
	return	$csvArray;
}

// ------------------------------------------------------------------------------
// CSVﾃﾞｰﾀからselect box を作成する
//		$ObjName		: select box の名称.ID
//		$ObjClass			:class
//		$csvArray	: CSVﾃﾞｰﾀ
//		$default	: ﾃﾞﾌｫﾙﾄ値
//		$width		: selectboxの幅
//		$onChange	: onChange で起動する java script関数名
//		$ViewCode	: ｺｰﾄﾞを表示する
//		$retHTML = CreateDBWebFuncMakeSelectBox($ObjName,$ObjClass,$csvArray,$default,$width,$onChange,$ViewCode)
// ------------------------------------------------------------------------------
function CreateDBWebFuncMakeSelectBox($ObjName,$ObjClass,$csvArray,$default,$width,$onChange,$ViewCode){


	// onChangeを使わない場合
	if ( $onChange == ''){
		$retHTML = "<select";
		$retHTML .= " name=\"$ObjName\" id=\"$ObjName\"";
		if ( $ObjClass <> ''){
			$retHTML .= " class= \"$ObjClass\"";
		}
		if ( $width <> '' ){
			$retHTML .= " style=\"width: ".$width.";\"";
		}
		$retHTML .= ">\n";
	}else{
		$retHTML = "<select";
		$retHTML .= " name=\"$ObjName\" id=\"$ObjName\"";
		if ( $ObjClass <> ''){
			$retHTML .= " class= \"$ObjClass\"";
		}
		if ( $width <> '' ){
			$retHTML .= " style=\"width: ".$width.";\"";
		}
		
		$retHTML .= " onChange=\"$onChange\"";
		$retHTML .= ">\n";
	}
	//CSVの要素を取り出す
	$retOption = "";
	//1件以下はselectedにする
	if ( count($csvArray) <= 1 ){
		$i = 0;
		//valueとitemに分ける
		$arr = explode(":",$csvArray[$i]);
		//Option生成
		//ｺｰﾄﾞを表示するか
		if ( $ViewCode == TRUE ){
			$csvArray[$i] = str_replace('-1:','',$csvArray[$i]);
			$retOption .= "<option value=\"" .$arr[0]."\" selected>".$csvArray[$i]."</option>\n";
		}else{
			$retOption .= "<option value=\"" .$arr[0]."\" selected>".$arr[1]."</option>\n";
		}
	}else{
		//ﾏｯﾁしたかをﾁｪｯｸする
		$retOption = "";
		$matchCode = FALSE;
		for($i = 0; $i < count($csvArray); $i++){
			//valueとitemに分ける
			$arr = explode(":",$csvArray[$i]);
			//Option生成
			//ｺｰﾄﾞを表示するか
			if ( $ViewCode == TRUE ){
				$csvArray[$i] = str_replace('-1:','',$csvArray[$i]);
				
				if ($default == $arr[0]) {
					$matchCode = TRUE;
					$retOption .= "<option value=\"" .$arr[0]."\" selected>".$csvArray[$i]."</option>\n";
				}
				else {
					$retOption .= "<option value=\"" .$arr[0]."\">".$csvArray[$i]."</option>\n";
				}
			}else{
				if ($default == $arr[0]) {
					$matchCode = TRUE;
					$retOption .= "<option value=\"" .$arr[0]."\" selected>".$arr[1]."</option>\n";
				}
				else {
					$retOption .= "<option value=\"" .$arr[0]."\">".$arr[1]."</option>\n";
				}
			}
	    }
	    //ﾏｯﾁしていない場合，最初の要素をselectedにする
	    if ( !$matchCode ){
	    	$retOption = "";
			for($i = 0; $i < count($csvArray); $i++){
				//valueとitemに分ける
				$arr = explode(":",$csvArray[$i]);
				//Option生成
				if ( $ViewCode == TRUE ){
					if ($i == 0) {
						$retOption .= "<option value=\"" .$arr[0]."\" selected>".$csvArray[$i]."</option>\n";
					}
					else {
						$retOption .= "<option value=\"" .$arr[0]."\">".$csvArray[$i]."</option>\n";
					}
				}else{
					if ($i == 0) {
						$retOption .= "<option value=\"" .$arr[0]."\" selected>".$arr[1]."</option>\n";
					}
					else {
						$retOption .= "<option value=\"" .$arr[0]."\">".$arr[1]."</option>\n";
					}
				}
		    }
	    }
    }
	$retHTML .= $retOption."</select>\n";
	
	return	$retHTML;
}

// ------------------------------------------------------------------------------
// DEBUG ECHO
// ------------------------------------------------------------------------------
function CreateDBWebFuncDebug($ObjName){

	if(DEBUG_MODE){
		echo "$ObjName<br>";
	}

}

// ------------------------------------------------------------------------------
?>