<?php
// ------------------------------------------------------------------------------
//
//		System Constant
//				swConstant.php
//
//		Copyright(C) 2015 dbu@mac.com All Rights Reserved.
//
// ------------------------------------------------------------------------------
	//ﾃﾞﾊﾞｯｸﾞ
	define("DEBUG_MODE",FALSE);

	//内部文字コードを変更
	mb_language("uni");
	mb_internal_encoding("utf-8");
	mb_http_input("auto");
	mb_http_output("utf-8");

	//Timezone
	date_default_timezone_set('Asia/Tokyo');
	//server root path
	$_ROOT_URL_ = "http://scenariowritercafe.com/";

	//MySQL DB Connect
	$dbHost = "mysql906.xserver.jp";
	$dbUser = "dbu_swuser";
	$dbPass = "swuser000";
	$dbName = "dbu_swdb";

	$dbHost = "localhost";
	$dbUser = "dbu_swuser";
	$dbPass = "swuser000";
	$dbName = "dbu_swdb";

	//MySQL DB Connect
	//$dbHost = "localhost";
	//$dbUser = "root";
	//$dbPass = "root000";
	//$dbName = "dbu_swdb";


	$DefaultDB = $dbName;

    //ﾃﾞｰﾀ保存ﾊﾟｽ
	$DefaultDataDirPath = "./swdata";
    //ﾃﾞｰﾀﾃﾝﾌﾟﾚｰﾄﾊﾟｽ
	$DefaultTempDirPath = "./TEMP";
	//Max Culumn name length (MySqlのcolumn名の最大長は64文字)
	$maxColNameLength = 32;

	//server root path
	define("_ROOT_URL_","http://http://localhost/scenariowriter/public_html/");
	//define("_ROOT_URL_","http://scenariowritercafe.com/");
	//日付SelectBox 開始年と終了年
	define("_STNEN_","5");//5年前
	define("_EDNEN_","2");//2年後

	//管理者
	define("_ADMIN_","1");

	//LOGIN ID継承 MAX秒
	define("_sw_access_max_","3600");

// ------------------------------------------------------------------------------
?>
