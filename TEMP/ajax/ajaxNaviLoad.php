<?php
//	------------------------------------------------------------------------------
//
//	Copyright (C) 2015 dbu@mac.com All Rights Reserved.
//
//			WebSystemAjax
//
//			AjaxNaviLoad.php
//
//	------------------------------------------------------------------------------

	//MAIN PROC
	fncMainProc();
exit;

//--------------------------------------------------------------------------------
// MAIN PROCEDUR
//	fncMainProc($mySqlConnObj)
//--------------------------------------------------------------------------------
function fncMainProc(){

	$retNaviHtml = fncMakeNavi();

	// 出力charsetをEUC-JPに指定
	//mb_http_output( 'UTF-8' );
	// 出力
	//header ("Content-Type: text/html; charset=UTF-8");
	echo($retNaviHtml);

}


// ------------------------------------------------------------------------------
// MakeNavi
// ------------------------------------------------------------------------------
function fncMakeNavi(){
	
	$myNaviHtml = <<<END_OF_HTML
	
    <nav class="navbar navbar-default navbar-fixed-top" role="banner">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#acct-navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#page-top">ScenariWriterCafe</a>
            </div>
            <div class="collapse navbar-collapse" id="sw-navbar">
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a href="./index.html">ScenariWriterCafeについて</a>
                    </li>
                    <li>
                        <a href="./swUserRequest.html">利用申請</a>
                    </li>
                    <li>
                        <a href="./swUserLogin.html">ログイン</a>
                    </li>
                    <li>
                        <a href="./swContact.html">お問い合わせ</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>


END_OF_HTML;

	return	$myNaviHtml;



}//end function

?>
