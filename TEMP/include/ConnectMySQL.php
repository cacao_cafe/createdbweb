<?php
// ------------------------------------------------------------------------------
//
//		MySQL DB接続
//
//		Copyright(C) 2015 dbu@mac.com All Rights Reserved.
//
// ------------------------------------------------------------------------------
	
	//接続(DB選択含む)
	$mySqlConnObj = new mysqli($dbHost, $dbUser, $dbPass, $dbName);
	//接続状況ﾁｪｯｸ
	if (mysqli_connect_errno()) {
		printf("Connect failed: %s\n", mysqli_connect_error());
		exit();
	}
	
	//文字設定をUTF8にする
	$mySqlConnObj ->set_charset("utf8");

?>