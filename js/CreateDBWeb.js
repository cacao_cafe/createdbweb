/* ------------------------------------
 * CreateDBWeb.js
 -------------------------------------- */

//JQuery初期設定
jQuery(document).ready(function(jQuery) {
	$(function() {
		var row_cnt = 0;
		$("#cdbCol-tbody").find(".cdbCol_var").each(function(idx, obj) {
			//エリア数ｶｳﾝﾄ
			row_cnt++;
		});
		//エリア数を保存
		$('#areaRows').val(row_cnt);
		//１行のときremoveアイコンを非表示にする
		if ( row_cnt == 1 ){
			$("#cdbCol-tbody").find(".removeRow").eq(0).hide();
		}
	});

	// 行を追加する
	$(document).on("click", ".addRow", function(ev) {
		//１行のときremoveアイコンを表示する
		$("#cdbCol-tbody").find(".removeRow").eq(0).show();
		//とりあえず１行目のクローンを挿入する
		$("#cdbCol-tbody > tr").eq(0).clone(true).insertAfter(
			$(this).parent().parent()
		);
		//name,id に連番を振る
		fncSetItemNo();
		//indexを取得
		var idx = $(".cdbCol_var").find('.addRow').index(ev.target);
		//値を初期化
		fncSetItemVal(idx + 1);
		
	});
	
	// 行を削除する
	$(document).on("click", ".removeRow", function() {
		$(this).parent().parent().remove();
		//name,id に連番を振る
		fncSetItemNo();
	});
	
	// 行を一つ上に移動させる
	$(document).on("click", "#cdbCol-tbody > tr:gt(0) .upRow", function() {
		var t = $(this).parent().parent();
		if($(t).prev("tr")) {
			$(t).insertBefore($(t).prev("tr")[0]);
		}
		//name,id に連番を振る
		fncSetItemNo();
	});
	
	// 行を一つ下に移動させる
	$(document).on("click", ".downRow", function() {
		var t = $(this).parent().parent();
		if($(t).next("tr")) {
			$(t).insertAfter($(t).next("tr")[0]);
		}
		//name,id に連番を振る
		fncSetItemNo();
	});
	
// ---------------------------------------------------
//	値を初期化
// ---------------------------------------------------
	function fncSetItemVal(idx){
		//$(function() {
		$("#cdbCol-tbody > tr").eq(idx).find('[name]').each(function(i) {
			//値を初期化
			$(this).val('');
		}).end()
		return this;
		//})
	}

// ---------------------------------------------------
//	name,id に連番を振る
// ---------------------------------------------------
	function fncSetItemNo(){
		//$(function() {
			var row_cnt = 0;
			$("#cdbCol-tbody").find(".cdbCol_var").each(function(idx, obj) {
				$(obj).find('[name]').each(function(i) {
					//名前を取得
					var valname = $(this).attr("name");
					//_区切り
					var str = valname.split("_");
					//---------------------------------
					// id,nameを変更
					//---------------------------------
					//名前を0から始まる連番にする
					$(this).attr('name',str[0] + '_' + idx);
					//IDを0から始まる連番にする
					$(this).attr('id',str[0] + '_' + idx);
				}).end()
				//エリア数ｶｳﾝﾄ
				row_cnt++;
			});
			//エリア数を保存
			$('#areaRows').val(row_cnt);
			//１行のときremoveアイコンを非表示にする
			if ( row_cnt == 1 ){
				$("#cdbCol-tbody").find(".removeRow").eq(0).hide()
			}
			return this;
		//})
	}


  //select box が変更されたとき
	$(document).on("change", ".sbxType", function(ev){
		//index取得
		var idx = $(".cdbCol_var").find('.sbxType').index(ev.target);
		//idを編集
		var targetName = '#cdbColSize_'+idx;
		if( $(this).val() == 'VARCHAR' ){$(targetName).val(256);}
		if( $(this).val() == 'TEXT' ){$(targetName).val(50);}
		if( $(this).val() == 'INT' ){$(targetName).val(4);}
		if( $(this).val() == 'BIGINT' ){$(targetName).val(8);}
		if( $(this).val() == 'DATE' ){$(targetName).val(8);}
		if( $(this).val() == 'DATENOW' ){$(targetName).val(14);}
		if( $(this).val() == 'TIMESTAMP' ){$(targetName).val(14);}
	});
  //select box が変更されたとき
	$(document).on("change", ".sbxObjType", function(ev){
		//index取得
		var idx = $(".cdbCol_var").find('.sbxObjType').index(ev.target);
		//idを編集
		var targetName = '#cdbObjSize_'+idx;
		if( $(this).val() == 'TEXT' ){$(targetName).val(50);}
		if( $(this).val() == 'NUMBER' ){$(targetName).val(20);}
		if( $(this).val() == 'TEXTAREA' ){$(targetName).val(40);}
		if( $(this).val() == 'SELECT' ){$(targetName).val(120);}
		if( $(this).val() == 'DATE' ){$(targetName).val(20);}
	});





	//SelectTable ボタンの制御　--------------------------------------
	$('#btnSelectTable').click(function() {
		//SubmitModeのvalueを設定する
		$('#SubmitMode').attr('value', 'SELECT_TABLE');
		//Form id=frmCdbCol のsubmit
		$('#frmCdbCol').submit();
	});
	//NewTable ボタンの制御　--------------------------------------
	$('#btnNewTable').click(function() {
		//SubmitModeのvalueを設定する
		$('#SubmitMode').attr('value', 'NEW_TABLE');
		//Form id=frmCdbCol のsubmit
		$('#frmCdbCol').submit();
	});
	//Save ボタンの制御　--------------------------------------
	$('#btnSave').click(function() {
		//必須項目ﾁｪｯｸ
		if ( fncCheckEmpty() ){
			//SubmitModeのvalueを設定する
			$('#SubmitMode').attr('value', 'SAVE');
			//Form id=frmCdbCol のsubmit
			$('#frmCdbCol').submit();
		}
	});
	//Delete ボタンの制御　--------------------------------------
	$('#btnDelete').click(function() {
		if(confirm('削除しますか？')){
			//SubmitModeのvalueを設定する
			$('#SubmitMode').attr('value', 'DELETE');
			//Form id=frmCdbCol のsubmit
			$('#frmCdbCol').submit();
		}
	});
	//Make ボタンの制御　--------------------------------------
	$('#btnMake').click(function() {
		//必須項目ﾁｪｯｸ
		if ( fncCheckEmpty() ){
			//SubmitModeのvalueを設定する
			$('#SubmitMode').attr('value', 'MAKE');
			//Form id=frmCdbCol のsubmit
			$('#frmCdbCol').submit();
		}
	});
	//CreateTable ボタンの制御　--------------------------------------
	$('#btnCreateTable').click(function() {
		//phpを実行
		$phppath = $("#CreateTablePath").val();
		$.ajax({
			type: "POST",
			url: $phppath, //PHPを呼び出す
			success: function(result){
				alert(result);
			},
			error:function(result){
				alert(result);
			}
		});
	});
	//TestPHP ボタンの制御　--------------------------------------
	$('#btnTestPHP').click(function() {
		$this_action = $("#frmCdbCol").attr('action');
		//phpのPath
		$phppath = $("#MainPHPPath").val();
		$('#frmCdbCol').attr('action', $phppath);
		$("#frmCdbCol").attr('target','_blank');

		$('#frmCdbCol').submit();
		
		$("#frmCdbCol").attr('action',$this_action);
		$("#frmCdbCol").attr('target','_self');
	});
	
	//テーブル選択SELECTの制御　--------------------------------------
	$("#cdbSelectTable").change(function () {
		//SelectTable ボタンclickを呼び出す
		//$("#btnSelectTable").trigger("click");
		//SubmitModeのvalueを設定する
		$('#SubmitMode').attr('value', 'SELECT_TABLE');
		//Form id=frmCdbCol のsubmit
		$('#frmCdbCol').submit();
	});




	//tabs　--------------------------------------
	$( "#tabs" ).tabs();
	
});

//SyntaxHighlighterを初期化
	SyntaxHighlighter.all();


// ---------------------------------------------------
//	culumn type selectbox が変更された時の制御
// ---------------------------------------------------
function fncChangeType(frm){

	alert($(this).val());

}


// ---------------------------------------------------
//	必須項目をチェックする
// ---------------------------------------------------
	function fncCheckEmpty(){
		//id=cdbCol-tbodyからclass=cdbCol_varを探してループする
		$("#cdbtable_body").find(".NotEmpty").each(function(idx, obj) {
			$(obj).css('background-color', '#ffffff');
			//titleを取得
			var valTitle = $(obj).attr("title");
			//値を取得
			var valData = $(obj).val();
			if (valData == '' ){
				$(obj).focus();
				$(obj).css('background-color', '#ffcccc');
				alert(valTitle + 'は必須項目のため省略できません');
				exit;
			}
		});
		//id=cdbCol-tbodyからclass=cdbCol_varを探してループする
		$("#cdbCol-tbody").find(".NotEmpty").each(function(idx, obj) {
			$(obj).css('background-color', '#ffffff');
			//titleを取得
			var valTitle = $(obj).attr("title");
			//値を取得
			var valData = $(obj).val();
			if (valData == '' ){
				$(obj).focus();
				$(obj).css('background-color', '#ffcccc');
				alert(valTitle + 'は必須項目のため省略できません');
				exit;
			}
		});
		return true;
	}

