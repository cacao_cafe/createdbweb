<?php
//------------------------------------------------------------------------------
//
//	MySQLを使ったDBI/Oｼｽﾃﾑ自動生成 Create DB Web System for MySQL with PHP&JAVASCRIPT
//			Copyright (C) 2014 dbu@mac.com All Rights Reserved.
//
//		CreateDBWeb.php
//
//	【自動生成するﾌｧｲﾙ】
//		MySQL CAREATE TABLE 			: ctbl@TABLE_NAME@.sql
//		MySQL DB I/O CLASS SQL  		: cls@TABLE_NAME@.php
//		MySQL DB I/O FORM PHP 			: frm@TABLE_NAME@.php
//		MySQL DB I/O AJAX JAVASCRIPT 	: ajax@TABLE_NAME@.js
//		MySQL DB I/O AJAX PHP 			: ajax@TABLE_NAME@.php
//		
//   	charset=UTF-8
//------------------------------------------------------------------------------
  //共通定数
  include_once("./sw_config/swConstant.php");
  //DB接続
  include_once("./include/ConnectMySQL.php");

  $DefaultDB = 'dbu_swdb';

  //ﾃﾞﾌｫﾙﾄｱｸｼｮﾝ
  $ThisPHP = 'CreateDBWeb.php';
  //共通ｻﾌﾞﾙｰﾁﾝ
  include_once("./include/CreateDBWebFunc.php");

  //ﾍｯﾀﾞﾀｲﾄﾙ
  $HtmlTitle = 'Create ScenarioWriterCafe System for MySQL';
  //ﾍｯﾀﾞ表示
  include_once("./include/CreateDBWebHeader.php");

  //Main Procedure
  fncMainProc($mySqlConnObj);

  //ﾌｯﾀｰ表示
  include_once("./include/CreateDBWebFooter.php");

  exit;

//--------------------------------------------------------------------------------
//	fncMainProc
//--------------------------------------------------------------------------------
function fncMainProc($mySqlConnObj){
    //global変数
    global	$SubmitMode;
    global	$cdbTableName,$cdbSelectTable;
    global	$CreateTablePath,$MainPHPPath;
    
    //処理モード
    $SubmitMode = CreateDBWebFuncGetPostData('SubmitMode');
    //読み込むテーブル定義
    $cdbSelectTable = CreateDBWebFuncGetPostData('cdbSelectTable');
    switch ($SubmitMode ){
        case 'NEW_TABLE':
          //ﾌｫｰﾑの入力項目を読み込む
          fncGetPostItems();
          break;
        case 'SAVE':
          //ﾌｫｰﾑの入力項目を読み込む
          fncGetPostItems();
          //POSTﾃﾞｰﾀ保存
		      fncPostDataSave($cdbTableName);
          $SubmitMode = 'SELECT_TABLE';
          break;
        case 'SELECT_TABLE':
          //保存していたTableﾃﾞｰﾀを$_POSTとして読み込む
        	fncReadTableData($cdbSelectTable);
        	//新しい$POSTを読み込む
        	fncGetPostItems();
          break;
        case 'DELETE':
        	//保存していたTableﾃﾞｰﾀを削除する
        	fncRemoveTableDir($mySqlConnObj,$cdbSelectTable);
        	//新しい$POSTを読み込む
        	fncGetPostItems();
           break;
        case 'MAKE':
          //ﾌｫｰﾑの入力項目を読み込む
          fncGetPostItems();
          //構文組み立て
          fncMakeCode();
          //CreateTableSyntax保存
          $CreateTablePath = fncCreateTableSyntaxSave($cdbTableName);
          //保存
          $MainPHPPath = fncCodeSave($cdbTableName);
          break;
        case 'CREATE_TABLE':
          //ajaxで保存したphpを実行する
          break;
       default:
          break;
    }

    //FORMの表示
    fncMainForm();
}


//--------------------------------------------------------------------------------
//	fncMainForm()
//--------------------------------------------------------------------------------
function fncMainForm(){
  global  $ThisPHP,$DefaultDataDirPath;
  global	$CreateTablePath,$MainPHPPath;

  global	$SubmitMode;
  global	$areaRows;
  global	$cdbDbName,$cdbTableName,$cdbTableNameComment,$cdbSelectTable,$cdbDefaultCharSet;
  global	$cdbNameArray,$cdbCommentArray,$cdbTypeArray;
  global	$cdbColSizeArray,$cdbKeyArray,$cdbDefaultValueArray;
  global	$cdbObjTypeArray,$cdbObjSizeArray,$cdbListArray,$cdbReqArray;
	
  global	$CreateTableSyntax;
  global  $CreateClassSyntax;
  global  $CreateMainPHPCode;
  global  $CreateAjaxJsCode;
  global  $CreateAjaxPHPCode;
    
  if ( $areaRows == '' ){$areaRows = 1;}
    
    print <<<END_OF_HTML
    
    <div id="container">
      <p class="title">
        <span>Create DB Web System for MySQL  PHP Javascript Ajax</span>
      </p>
      <form name="frmCdbCol" id="frmCdbCol" action="$ThisPHP" method="POST">
        <input type="hidden" name="SubmitMode" id="SubmitMode" value="">
        <input type="hidden" name="areaRows" id="areaRows" value="$areaRows">
        <div class="menu_area">
		<table id="cdbtable_body">
			<tr>
				<td align="right"><span class="fBlack10100">DB NAME&nbsp;</span></td>
				<td>
END_OF_HTML;
  
		//------------------------------------------------------------
		//CSVﾃﾞｰﾀからselect box を作成する
			$ObjName = "cdbDbName";			//select box の名称.ID
			$ObjClass = "NotEmpty";	//class
			$csvArray = CreateDBWebFuncMakeCdbDbNameSetCsv();	//CSVﾃﾞｰﾀ
			$default = "$cdbDbName";			//ﾃﾞﾌｫﾙﾄ値
			$width = '250';							//selectboxの幅
			$onChange = '';							//onChange で起動する java script関数名
			$ViewCode = FALSE;						//ｺｰﾄﾞを表示する
			//html追加
			print CreateDBWebFuncMakeSelectBox($ObjName,$ObjClass,$csvArray,$default,$width,$onChange,$ViewCode);
		//------------------------------------------------------------
            
print <<<END_OF_HTML
        
        </td>
				<td>
					<span class="fBlack10100">&nbsp;DEFAULT CHARSET</span>
				</td>
				<td>
END_OF_HTML;
  
		//------------------------------------------------------------
		//CSVﾃﾞｰﾀからselect box を作成する
			$ObjName = "cdbDefaultCharSet";			//select box の名称.ID
			$ObjClass = "sbxDefaultCharSet";	//class
			$csvArray = CreateDBWebFuncMakeCdbDefaultCharSetCsv();	//CSVﾃﾞｰﾀ
			$default = "$cdbDefaultCharSet";			//ﾃﾞﾌｫﾙﾄ値
			$width = '80';							//selectboxの幅
			$onChange = '';							//onChange で起動する java script関数名
			$ViewCode = FALSE;						//ｺｰﾄﾞを表示する
			//html追加
			print CreateDBWebFuncMakeSelectBox($ObjName,$ObjClass,$csvArray,$default,$width,$onChange,$ViewCode);
		//------------------------------------------------------------
    print <<<END_OF_HTML
            
            	</td>
            	<td rowspan="3" valign="top">
            		<p>
                    <input type="button" value="Save"
                        id="btnSave"
                            class="buttonGreen">
END_OF_HTML;
	if ( $SubmitMode == 'SAVE' or $SubmitMode == 'SELECT_TABLE' or $SubmitMode == 'MAKE' ){
		print <<<END_OF_HTML
					
                    <input type="button" value="Delete"
                        id="btnDelete"
                            class="buttonRed">
					</p>
					<p>
                    <input type="button" value="Make"
                        id="btnMake"
                            class="buttonOrange">
END_OF_HTML;
	}
	if ( $SubmitMode == 'MAKE' ){
		print <<<END_OF_HTML
		
                    <input type="button" value="CreateTable"
                        id="btnCreateTable"
                            class="buttonOrange">
                    <input type="button" value="Test PHP"
                        id="btnTestPHP"
                            class="buttonGreen">
                    <input type="hidden" name="CreateTablePath" id="CreateTablePath" value="$CreateTablePath">
                    <input type="hidden" name="MainPHPPath" id="MainPHPPath" value="$MainPHPPath">
                    <span id="CreateTableResult"></span>
END_OF_HTML;
	}
		print <<<END_OF_HTML
		
					</p>
            	</td>
			</tr>
			<tr>
				<td align="right"><span class="fBlack10100">TABLE NAME&nbsp;</span></td>
				<td>
                    <input type="text" size="37" 
                    	class="NotEmpty" title="TABLE NAME"
                    	name="cdbTableName" id="cdbTableName" 
                    	value="$cdbTableName">
                </td>
                <td>
                    <span class="fBlack10100">&nbsp;TABLE COMMENT</span>
                </td>
                <td>
                    <input type="text" size="22"
                    	name="cdbTableNameComment" id="cdbTableNameComment" 
                    	value="$cdbTableNameComment">
				</td>
			</tr>
			<tr>
                <td>
                	<span class="fBlack10100">SELECT SAVED TABLE&nbsp;</span>
                </td>
                <td colspan="3">
END_OF_HTML;

		//------------------------------------------------------------
		//CSVﾃﾞｰﾀからselect box を作成する
			$ObjName = "cdbSelectTable";			//select box の名称.ID
			$ObjClass = "sbxSelectTable";	//class
			$csvArray = CreateDBWebFuncMakeSaveDirCsv("$DefaultDataDirPath");	//CSVﾃﾞｰﾀ
			$default = "$cdbSelectTable";			//ﾃﾞﾌｫﾙﾄ値
			$width = '250';							//selectboxの幅
			$onChange = '';							//onChange で起動する java script関数名
			$ViewCode = FALSE;						//ｺｰﾄﾞを表示する
			//html追加
			print CreateDBWebFuncMakeSelectBox($ObjName,$ObjClass,$csvArray,$default,$width,$onChange,$ViewCode);
		//------------------------------------------------------------

		print <<<END_OF_HTML
		
                    <input type="button" value="NewTable"
                        id="btnNewTable"
                            class="buttonOrange">
                </td>
            </tr>
		</table>
        </div>
        <div class="scroll_div">
		<table id="cdbColtable" class="table">
			<thead>
				<tr>
					<th>ORDER</th>
					<th>COLUMN NAME</th>
					<th>COLUMN COMMENT</th>
					<th>TYPE</th>
					<th>COL SIZE</th>
					<th>KEY</th>
          <th>DEFAULT VALUE</th>
					<th>OBJ TYPE</th>
					<th>OBJ SIZE</th>
					<th>REP.</th>
					<th>LIST</th>
					<th>&nbsp;</th>
				</tr>
			</thead>
			<tbody id="cdbCol-tbody">
END_OF_HTML;
		//配列からcolumn項目を編集する
		for ( $rowNo = 0; $rowNo < $areaRows; $rowNo++ ){
		
			//追加エリアHTMLを組み立てる
			$cdbColRow = fncMakeRowHtml($rowNo);
			
			print $cdbColRow;
		}

	print <<<END_OF_HTML
	
			</tbody>
		</table>
		</div>
	</form>

<div id="tabs">
  <ul>
    <li class="tab"><a href="#create_table">CREATE TABLE</a></li>
    <li class="tab"><a href="#class_table">CLASS</a></li>
    <li class="tab"><a href="#main_php">MAIN PHP</a></li>
    <li class="tab"><a href="#ajax_js">AJAX JS</a></li>
    <li class="tab"><a href="#ajax_php">AJAX PHP</a></li>
  </ul>
END_OF_HTML;
	
	$SystemName = CreateDBWebFuncEditName($cdbTableName);
	
	//CREATE TABLE 構文 -----------------------------------------------------
	if ( $CreateTableSyntax != '' ){
		$SQLName = 'createTable'.$SystemName.'.php';
		print <<<END_OF_HTML
		
		<div id="create_table">
	    <pre class="brush: php;" title="■ MySQL CREATE TABLE    ./DBD/$SQLName">
$CreateTableSyntax
		 </pre>
		 </div>
END_OF_HTML;
	}
    
	//Class 構文-------------------------------------------------------------
	if ( $CreateClassSyntax != '' ){
		$ClassName = 'cls'.$SystemName.'.php';
		print <<<END_OF_HTML
		
		<div id="class_table">
	    <pre class="brush: php;" title="■ MySQL I/O CLASS PHP   ./class/$ClassName">
$CreateClassSyntax
		 </pre>
		 </div>
END_OF_HTML;
	}
    
	//Main PHP -------------------------------------------------------------
	if ( $CreateMainPHPCode != '' ){
		$phpName = $SystemName.'.php';
		print <<<END_OF_HTML
		
		<div id="main_php">
	    <pre class="brush: php;" title="■ MySQL I/O MAIN PHP   ./$phpName">
$CreateMainPHPCode
		 </pre>
		 </div>
END_OF_HTML;
	}
    
	//Ajax Js -------------------------------------------------------------
	if ( $CreateAjaxJsCode != '' ){
		$ajaxJsName = 'ajax'.$SystemName.'.js';
		print <<<END_OF_HTML
		
		<div id="ajax_js">
	    <pre class="brush: javascript;" title="■ jQuery ajax javascript   ./js/$ajaxJsName">
$CreateAjaxJsCode
		 </pre>
		 </div>
END_OF_HTML;
	}

	//Ajax PHP -------------------------------------------------------------
	if ( $CreateAjaxPHPCode != '' ){
		$ajaxPhpName = 'ajax'.$SystemName.'.php';
		print <<<END_OF_HTML
		
		<div id="ajax_php">
	    <pre class="brush: php;" title="■ jQuery ajax php   ./js/$ajaxPhpName">
$CreateAjaxPHPCode
		 </pre>
		 </div>
END_OF_HTML;
	}
    
		print <<<END_OF_HTML
		
		</div><!- end tabs div ->
	</div><!- end container ->
END_OF_HTML;
	
}


//--------------------------------------------------------------------------------
//	fncMakeRowHtml($RowNo)
//
//		追加エリアHTMLを組み立てる
//--------------------------------------------------------------------------------
function fncMakeRowHtml($RowNo){
	global	$cdbDbName,$cdbTableName,$cdbTableNameComment,$cdbSelectTable,$cdbDefaultCharSet;
	global	$cdbNameArray,$cdbCommentArray,$cdbTypeArray;
	global	$cdbColSizeArray,$cdbKeyArray,$cdbDefaultValueArray;
	global	$cdbObjTypeArray,$cdbObjSizeArray,$cdbListArray,$cdbReqArray;

	$RowHtml = <<<END_OF_HTML
	
			<tr class="cdbCol_var">
				<td style="white-space: nowrap;">
				    <img src="./css/img/up.png"  height="18" alt="up" class="upRow" style="cursor: pointer;">
				    <img src="./css/img/down.png"  height="18" alt="down" class="downRow" style="cursor: pointer;">
				</td>
				<td>
					<input type="text" size="30" maxlength="64"
						class="NotEmpty" title="COLUMN NAME"
						name="cdbName_$RowNo" id="cdbName_$RowNo" 
						value="$cdbNameArray[$RowNo]">
				</td>
				<td>
					<input type="text" size="30" 
						name="cdbComment_$RowNo" id="cdbComment_$RowNo" 
						value="$cdbCommentArray[$RowNo]">
				</td>
				<td>
END_OF_HTML;

		//------------------------------------------------------------
		//CSVﾃﾞｰﾀからselect box を作成する
			$ObjName = "cdbType_$RowNo";			//select box の名称.ID
			$ObjClass = "sbxType";					//class
			$csvArray = CreateDBWebFuncMakeCdbTypeCsv();		//CSVﾃﾞｰﾀ
			$default = $cdbTypeArray[$RowNo];		//ﾃﾞﾌｫﾙﾄ値
			$width = '105';							//selectboxの幅
			$onChange = '';						//onChange で起動する java script関数名
			$ViewCode = FALSE;						//ｺｰﾄﾞを表示する
			//html追加
			$RowHtml .= CreateDBWebFuncMakeSelectBox($ObjName,$ObjClass,$csvArray,$default,$width,$onChange,$ViewCode);
		//------------------------------------------------------------

		$RowHtml .= <<<END_OF_HTML
		
				</td>
				<td>
					<input type="text" size="3"
						class="NotEmpty" title="COLUMN SIZE"
						name="cdbColSize_$RowNo" id="cdbColSize_$RowNo" 
						value="$cdbColSizeArray[$RowNo]"
						style="text-align: right;">
				</td>
				<td>
END_OF_HTML;
		//------------------------------------------------------------
		//CSVﾃﾞｰﾀからselect box を作成する
			$ObjName = "cdbKey_$RowNo";				//select box の名称.ID
			$ObjClass = "sbxKey";					//class
			$csvArray = CreateDBWebFuncMakeCdbKeyCsv();			//CSVﾃﾞｰﾀ
			$default = "$cdbKeyArray[$RowNo]";		//ﾃﾞﾌｫﾙﾄ値
			$width = '120';							//selectboxの幅
			$onChange = '';							//onChange で起動する java script関数名
			$ViewCode = FALSE;						//ｺｰﾄﾞを表示する
			//html追加
			$RowHtml .= CreateDBWebFuncMakeSelectBox($ObjName,$ObjClass,$csvArray,$default,$width,$onChange,$ViewCode);
		//------------------------------------------------------------

		$RowHtml .= <<<END_OF_HTML
		
				</td>
				<td>
					<input type="text" size="10"
						name="cdbDefaultValue_$RowNo" id="cdbDefaultValue_$RowNo"
						value="$cdbDefaultValueArray[$RowNo]"
						style="text-align: left;">
				</td>
				<td>
END_OF_HTML;
		//------------------------------------------------------------
		//CSVﾃﾞｰﾀからselect box を作成する
			$ObjName = "cdbObjType_$RowNo";		//select box の名称.ID
			$ObjClass = "sbxObjType";					//class
			$csvArray = CreateDBWebFuncMakeCdbObjTypeCsv();	//CSVﾃﾞｰﾀ
			$default = "$cdbObjTypeArray[$RowNo]";//ﾃﾞﾌｫﾙﾄ値
			$width = '97';							//selectboxの幅
			$onChange = '';							//onChange で起動する java script関数名
			$ViewCode = FALSE;						//ｺｰﾄﾞを表示する
			//html追加
			$RowHtml .= CreateDBWebFuncMakeSelectBox($ObjName,$ObjClass,$csvArray,$default,$width,$onChange,$ViewCode);
		//------------------------------------------------------------

		$RowHtml .= <<<END_OF_HTML
		
				</td>
				<td>
					<input type="text" size="3"
						name="cdbObjSize_$RowNo" id="cdbObjSize_$RowNo"
						value="$cdbObjSizeArray[$RowNo]"
						style="text-align: right;">
					
				</td>
				<td>
END_OF_HTML;
		$checked = '';
		if($cdbReqArray[$RowNo] == '1'){
			$checked = 'checked';
		}
		$RowHtml .= <<<END_OF_HTML
		
					<input type="checkbox"
						name="cdbReq_$RowNo" id="cdbReq_$RowNo"
						value="1" $checked>
				</td>
				<td>
END_OF_HTML;
		$checked = '';
		if($cdbListArray[$RowNo] == '1'){
			$checked = 'checked';
		}
		$RowHtml .= <<<END_OF_HTML
		
					<input type="checkbox"
						name="cdbList_$RowNo" id="cdbList_$RowNo"
						value="1" $checked>
END_OF_HTML;

		$RowHtml .= <<<END_OF_HTML
		
				</td>
				<td style="white-space: nowrap;">
				    <img src="./css/img/add.png"  height="16" alt="追加" class="addRow" style="cursor: pointer;">
				    &nbsp;&nbsp;
				    <img src="./css/img/remove.png"  height="16" alt="削除" class="removeRow" style="cursor: pointer;">
				</td>
			</tr>
END_OF_HTML;

	return	$RowHtml;
	
}




//------------------------------------------------------------------------------
//	fncGetPostItems()
//
//     POSTされたﾌｫｰﾑのﾃﾞｰﾀを読み込む
//
//------------------------------------------------------------------------------
function fncGetPostItems(){
  global  $ThisPHP,$DefaultDataDirPath;
  global	$SubmitMode;
  global	$areaRows;
  global	$cdbDbName,$cdbTableName,$cdbTableNameComment,$cdbSelectTable,$cdbDefaultCharSet;
  global	$cdbNameArray,$cdbCommentArray,$cdbTypeArray;
  global	$cdbColSizeArray,$cdbKeyArray,$cdbDefaultValueArray;
  global	$cdbObjTypeArray,$cdbObjSizeArray,$cdbListArray,$cdbReqArray;

  global  $maxColNameLength;
  global	$DefaultDB;
    
 	//columnを配列化
	$cdbNameArray = array();
	$cdbCommentArray = array();
	$cdbTypeArray = array();
	$cdbColSizeArray = array();
	$cdbKeyArray = array();
  $cdbDefaultValueArray = array();
	$cdbObjTypeArray = array();
	$cdbObjSizeArray = array();
	$cdbListArray = array();
	$cdbReqArray = array();

	//POSTされた要素を取得
	//SubmitMode PHPでの処理を制御する
    $SubmitMode = CreateDBWebFuncGetPostData('SubmitMode');
    if ($SubmitMode == 'NEW_TABLE'){
    	$cdbDbName = "$DefaultDB";
    	$cdbTableName = 'new_table';
    	$cdbSelectTable = 'new table';
    	$areaRows = 5;
    	$rowNo = 0;
		//配列初期化
		array_push($cdbNameArray, 'id');
		array_push($cdbCommentArray, '');
		array_push($cdbTypeArray, 'INT');
		array_push($cdbColSizeArray, '4');
		array_push($cdbKeyArray, 'AUTO_INCREMENT');
    array_push($cdbDefaultValueArray, '');
		array_push($cdbObjTypeArray, 'NUMBER');
		array_push($cdbObjSizeArray, '50');
		array_push($cdbReqArray, '');
		array_push($cdbListArray, '');
		for ( $rowNo = 1; $rowNo < $areaRows; $rowNo++ ){
			//配列初期化
			array_push($cdbNameArray, '');
			array_push($cdbCommentArray, '');
			array_push($cdbTypeArray, 'INT');
			array_push($cdbColSizeArray, '');
			array_push($cdbKeyArray, '');
	        array_push($cdbDefaultValueArray, '');
			array_push($cdbObjTypeArray, 'NUMBER');
			array_push($cdbObjSizeArray, '');
			array_push($cdbReqArray, '');
			array_push($cdbListArray, '');
		}
    	return;
    }
	//column数
    $areaRows = CreateDBWebFuncGetPostData('areaRows');
    $cdbDbName = CreateDBWebFuncGetPostData('cdbDbName');
    $cdbTableName = CreateDBWebFuncGetPostData('cdbTableName');
    $cdbTableNameComment = CreateDBWebFuncGetPostData('cdbTableNameComment');
    $cdbDefaultCharSet = CreateDBWebFuncGetPostData('cdbDefaultCharSet');
    if ( $cdbTableName == ''){
    	$cdbTableName = 'newtable_name';
    }
    if ( $SubmitMode == 'SAVE' ){
    	$cdbSelectTable = $cdbTableName;
    }
    
	for ( $rowNo = 0; $rowNo < $areaRows; $rowNo++ ){
		//POSTされたﾃﾞｰﾀをﾁｪｯｸ
		if(!isset($_POST['cdbName_'.$rowNo])){$_POST['cdbName_'.$rowNo] = '@@';}
		if(!isset($_POST['cdbComment_'.$rowNo])){$_POST['cdbComment_'.$rowNo] = '@@';}
		if(!isset($_POST['cdbType_'.$rowNo])){$_POST['cdbType_'.$rowNo] = '-1';}
		if(!isset($_POST['cdbColSize_'.$rowNo])){$_POST['cdbColSize_'.$rowNo] = '-1';}
		if(!isset($_POST['cdbKey_'.$rowNo])){$_POST['cdbKey_'.$rowNo] = '0';}
    if(!isset($_POST['cdbDefaultValue_'.$rowNo])){$_POST['cdbDefaultValue_'.$rowNo] = '@@';}
		if(!isset($_POST['cdbObjType_'.$rowNo])){$_POST['cdbObjType_'.$rowNo] = '0';}
		if(!isset($_POST['cdbObjSize_'.$rowNo])){$_POST['cdbObjSize_'.$rowNo] = '-1';}
		if(!isset($_POST['cdbReq_'.$rowNo])){$_POST['cdbReq_'.$rowNo] = '0';}
		if(!isset($_POST['cdbList_'.$rowNo])){$_POST['cdbList_'.$rowNo] = '0';}
		//POSTされたﾃﾞｰﾀを配列に追加
		array_push($cdbNameArray, CreateDBWebFuncGetPostData('cdbName_'.$rowNo));
		array_push($cdbCommentArray, CreateDBWebFuncGetPostData('cdbComment_'.$rowNo));
		array_push($cdbTypeArray, CreateDBWebFuncGetPostData('cdbType_'.$rowNo));
		array_push($cdbColSizeArray,CreateDBWebFuncGetPostData('cdbColSize_'.$rowNo));
		array_push($cdbKeyArray, CreateDBWebFuncGetPostData('cdbKey_'.$rowNo));
    array_push($cdbDefaultValueArray, CreateDBWebFuncGetPostData('cdbDefaultValue_'.$rowNo));
		array_push($cdbObjTypeArray, CreateDBWebFuncGetPostData('cdbObjType_'.$rowNo));
		array_push($cdbObjSizeArray, CreateDBWebFuncGetPostData('cdbObjSize_'.$rowNo));
		array_push($cdbReqArray, CreateDBWebFuncGetPostData('cdbReq_'.$rowNo));
		array_push($cdbListArray, CreateDBWebFuncGetPostData('cdbList_'.$rowNo));
	}
}

//--------------------------------------------------------------------------------
//		fncRecursiveCopyDir($dir)
//
//		$dir内の全てを上書きｺﾋﾟｰ
//		http://wakichoko.doorblog.jp/archives/13474955.html
//--------------------------------------------------------------------------------
function fncRecursiveCopyDir($src, $dst){
	//ｺﾋﾟｰ先があれば削除する
	if (file_exists($dst)) {
		fncRecursiveRemoveDir($dst);
	}
	
	//ｺﾋﾟｰ元があればｺﾋﾟｰを始める
	if (is_dir($src)){
		//ｺﾋﾟｰ先を作る
		mkdir($dst);
		//ｺﾋﾟｰ元をﾘｽﾄする
		$files = scandir($src);
		foreach ($files as $file) {
			if (($file != ".") && ($file != "..")){
				//回帰的にｺﾋﾟｰする
				fncRecursiveCopyDir("$src/$file", "$dst/$file");
			}
		}
	}else if (file_exists($src)) {
		//通常にｺﾋﾟｰする
		copy($src, $dst);
	}
}


//--------------------------------------------------------------------------------
//		fncPostDataSave($valTableName)
//
//		POSTされたﾃﾞｰﾀを保存する
//
//		./TABLE_NAME/TABLE_NAME_CDB.php
//
//--------------------------------------------------------------------------------
function fncPostDataSave($valTableName){
	global	$DefaultDataDirPath;
   //ﾃﾞｰﾀﾃﾝﾌﾟﾚｰﾄﾊﾟｽ
	global	$DefaultTempDirPath;

	//OS 判定
	if (DIRECTORY_SEPARATOR == '\\') {
		$osInfo = 'Windows';
	} else {
		$osInfo = 'NotWindows';
	}
	if ($osInfo == 'NotWindows'){
		chmod($DefaultDataDirPath, 0777);
	}

	//ﾃﾞｨﾚｸﾄﾘ
	$dirName = strtoupper("$valTableName");
	$dirPath = "$DefaultDataDirPath"."/$dirName";

	//ﾃﾞｰﾀﾃﾝﾌﾟﾚｰﾄをｺﾋﾟｰする
	fncRecursiveCopyDir($DefaultTempDirPath, $dirPath);


	
	//ﾃﾞｨﾚｸﾄﾘの存在をﾁｪｯｸ
	//ﾃﾞｨﾚｸﾄﾘがあるとき
	if(file_exists($dirPath)){
		//Windows以外は属性変更
		if ($osInfo == 'NotWindows'){
			chmod($dirPath, 0755);
			chmod($dirPath."/ajax", 0755);
			chmod($dirPath."/css", 0755);
			chmod($dirPath."/class", 0755);
			chmod($dirPath."/include", 0755);
			chmod($dirPath."/js", 0755);
		}
	}
	
	//作成するﾌｧｲﾙ名の指定
	$file_name = $dirPath.'/DBD/'.CreateDBWebFuncEditName("$valTableName".'.syntax');

	//ﾌｧｲﾙの存在確認
	//ﾌｧｲﾙが存在しないとき
	if( !file_exists($file_name) ){
		//ﾌｧｲﾙ作成
		touch( $file_name );
		//Windows以外は属性変更
		if ($osInfo == 'NotWindows'){
			//ﾌｧｲﾙの属性変更
			chmod( $file_name, 0755 );
		}
	}
	//$POSTを配列として保存
	file_put_contents($file_name, serialize($_POST));

	if ($osInfo == 'NotWindows'){
		chmod($DefaultDataDirPath, 0755);
	}

}



//--------------------------------------------------------------------------------
//		fncReadTableData($valTableName)
//
//		保存していたTableﾃﾞｰﾀを読み込む
//
//		./TABLE_NAME/TABLE_NAME_CDB.php
//
//--------------------------------------------------------------------------------
function fncReadTableData($valTableName){
	global	$DefaultDataDirPath;
	//ﾃﾞｨﾚｸﾄﾘ
	$dirPath = "$DefaultDataDirPath"."/$valTableName";
	
	//ﾃﾞﾊﾞｯｸﾞ
	CreateDBWebFuncDebug($dirPath);
	
	//読み込むﾌｧｲﾙ名の指定
	$file_name = $dirPath.'/DBD/'.CreateDBWebFuncEditName("$valTableName".'.syntax');
	
	//ﾌｧｲﾙの存在確認
	//ﾌｧｲﾙが存在するとき
	if( file_exists($file_name) ){
		//配列をファイルから読み込み$_POSTに代入する
		$_POST = unserialize(file_get_contents("$file_name"));
		$_POST['SubmitMode'] = 'SELECT_TABLE';
	}else{
		//配列を初期化
		$_POST = array();
		$_POST['SubmitMode'] = 'NEW_TABLE';
    }
}



//--------------------------------------------------------------------------------
//		fncRecursiveRemoveDir($dir)
//
//		$dir内の全てを削除
//
//		元ネタは、回帰的な削除
//		http://www.php.net/manual/ja/function.unlink.php#87045
//												by Jon Hassall
//--------------------------------------------------------------------------------
function fncRecursiveRemoveDir($dir) {
	//$dir が存在しないときは何もしない
	if(!$dh = @opendir($dir)){
		return;
	}
	//無くなるまで繰り返す
	while (false !== ($obj = readdir($dh))) {
		if($obj == '.' || $obj == '..') {
			continue;
		}
		//$dir内のﾃﾞｨﾚｸﾄﾘ削除（回帰的な削除）
		if (!@unlink($dir . '/' . $obj)) {
			fncRecursiveRemoveDir($dir.'/'.$obj);
		}
	}
	//一旦閉じる
	closedir($dh);
	//$dirを削除
	@rmdir($dir);
	
	return;
}

//--------------------------------------------------------------------------------
//		fncRemoveTableDir($valTableName)
//
//		保存していたTableﾃﾞｰﾀを一括削除する
//
//		./TABLE_NAME/TABLE_NAME_CDB.php
//
//--------------------------------------------------------------------------------
function fncRemoveTableDir($mySqlConnObj,$valTableName){
	global	$DefaultDataDirPath;
	
	//DBがCREATEされていればDROPする
	//table drop
	$strSQL = <<<END_OF_SQL
	
		DROP TABLE IF EXISTS $valTableName;
END_OF_SQL;

	$mySqlConnObj->query($strSQL) or die($mySqlConnObj->error);


	//ﾃﾞｨﾚｸﾄﾘ
	$dirPath = "$DefaultDataDirPath"."/$valTableName";
	fncRecursiveRemoveDir($dirPath);

    //配列を初期化
    $_POST = array();
    $_POST['SubmitMode'] = 'NEW_TABLE';
}




//--------------------------------------------------------------------------------
//fncMakeCode()
//
//		構文生成
//
//--------------------------------------------------------------------------------
function fncMakeCode(){
    global  $ThisPHP,$DefaultDataDirPath;
    global	$SubmitMode;
	global	$areaRows;
	global	$cdbDbName,$cdbTableName,$cdbTableNameComment,$cdbSelectTable,$cdbDefaultCharSet;
	global	$cdbNameArray,$cdbCommentArray,$cdbTypeArray;
	global	$cdbColSizeArray,$cdbKeyArray,$cdbDefaultValueArray;
	global	$cdbObjTypeArray,$cdbObjSizeArray,$cdbListArray,$cdbReqArray;
	global  $maxColNameLength;
	
	global	$CreateTableSyntax;
	global	$CreateClassSyntax;
	global	$CreateMainPHPCode;
	global	$CreateAjaxJsCode;
	global	$CreateAjaxPHPCode;
    

	//MySQL のTABLE構文 編集
    include_once("./include/cdbMakeCreateTableSyntax.php");
	$CreateTableSyntax = fncMakeCreateTableSyntax();

	//MySQL I/O Class PHP 編集
    include_once("./include/cdbMakeClassSyntax.php");
	$CreateClassSyntax = fncMakeClassSyntax();

	//MainPHP 編集
    include_once("./include/cdbMakeMainPHPCode.php");
	$CreateMainPHPCode = fncMakeMainPHPCode();

	//AJAX JS 編集
    include_once("./include/cdbMakeAjaxJsCode.php");
	$CreateAjaxJsCode = fncMakeAjaxJsCode();

	//AJAX PHP 編集
    include_once("./include/cdbMakeAjaxPHPCode.php");
	$CreateAjaxPHPCode = fncMakeAjaxPHPCode();

}

//--------------------------------------------------------------------------------
//fncCreateTableSyntaxSave($valTableName)
//
//		ﾃｰﾌﾞﾙ定義ﾌｧｲﾙ保存
//
//--------------------------------------------------------------------------------
function fncCreateTableSyntaxSave($valTableName){
	global	$dirPath;
	global	$DefaultDataDirPath;
	
	global	$CreateTableSyntax;
	
	//OS 判定
	if (DIRECTORY_SEPARATOR == '\\') {
		$osInfo = 'Windows';
	} else {
		$osInfo = 'NotWindows';
	}
	
	//CREATE SQL を保存
	//作成するﾌｧｲﾙ名の指定
	$file_name = $DefaultDataDirPath."/$valTableName".'/DBD/'.CreateDBWebFuncEditName("$valTableName".'.php');

	//ﾌｧｲﾙの存在確認
	//ﾌｧｲﾙが存在しないとき
	if( !file_exists($file_name) ){
		//ﾌｧｲﾙ作成
		touch( $file_name );
		//Windows以外は属性変更
		if ($osInfo == 'NotWindows'){
			//ﾌｧｲﾙの属性変更
			chmod( $file_name, 0755 );
		}
	}
	//保存
	file_put_contents($file_name, html_entity_decode($CreateTableSyntax));
	
	return	$file_name;
}
//--------------------------------------------------------------------------------
//fncCodeSave($valTableName)
//
//		ﾌｧｲﾙ保存
//
//--------------------------------------------------------------------------------
function fncCodeSave($valTableName){
	global	$dirPath;
	global	$DefaultDataDirPath,$MainPHPPath;
	
	global	$CreateTableSyntax;
    global  $CreateClassSyntax;
    global  $CreateMainPHPCode;
    global  $CreateAjaxJsCode;
    global  $CreateAjaxPHPCode;
    
	
	//OS 判定
	if (DIRECTORY_SEPARATOR == '\\') {
		$osInfo = 'Windows';
	} else {
		$osInfo = 'NotWindows';
	}
	
	//MainPHP
	//作成するﾌｧｲﾙ名の指定
	$MainPHPPath = $DefaultDataDirPath."/$valTableName".'/'.CreateDBWebFuncEditName("$valTableName".'.php');

	//ﾌｧｲﾙの存在確認
	//ﾌｧｲﾙが存在しないとき
	if( !file_exists($MainPHPPath) ){
		//ﾌｧｲﾙ作成
		touch( $MainPHPPath );
		//Windows以外は属性変更
		if ($osInfo == 'NotWindows'){
			//ﾌｧｲﾙの属性変更
			chmod( $MainPHPPath, 0755 );
		}
	}
	//保存
	file_put_contents($MainPHPPath, html_entity_decode($CreateMainPHPCode));

	//class
	//作成するﾌｧｲﾙ名の指定
	$classPath = $DefaultDataDirPath."/$valTableName".'/class/cls'.CreateDBWebFuncEditName("$valTableName".'.php');

	//ﾌｧｲﾙの存在確認
	//ﾌｧｲﾙが存在しないとき
	if( !file_exists($classPath) ){
		//ﾌｧｲﾙ作成
		touch( $classPath );
		//Windows以外は属性変更
		if ($osInfo == 'NotWindows'){
			//ﾌｧｲﾙの属性変更
			chmod( $classPath, 0755 );
		}
	}
	//保存
	file_put_contents($classPath, html_entity_decode($CreateClassSyntax));

	//ajaxJs
	//作成するﾌｧｲﾙ名の指定
	$ajaxJsPath = $DefaultDataDirPath."/$valTableName".'/ajax/ajax'.CreateDBWebFuncEditName("$valTableName".'.js');

	//ﾌｧｲﾙの存在確認
	//ﾌｧｲﾙが存在しないとき
	if( !file_exists($ajaxJsPath) ){
		//ﾌｧｲﾙ作成
		touch( $ajaxJsPath );
		//Windows以外は属性変更
		if ($osInfo == 'NotWindows'){
			//ﾌｧｲﾙの属性変更
			chmod( $ajaxJsPath, 0755 );
		}
	}
	//保存
	file_put_contents($ajaxJsPath, html_entity_decode($CreateAjaxJsCode));

	//ajaxPHP
	//作成するﾌｧｲﾙ名の指定
	$ajaxPHPPath = $DefaultDataDirPath."/$valTableName".'/ajax/ajax'.CreateDBWebFuncEditName("$valTableName".'.php');

	//ﾌｧｲﾙの存在確認
	//ﾌｧｲﾙが存在しないとき
	if( !file_exists($ajaxPHPPath) ){
		//ﾌｧｲﾙ作成
		touch( $ajaxPHPPath );
		//Windows以外は属性変更
		if ($osInfo == 'NotWindows'){
			//ﾌｧｲﾙの属性変更
			chmod( $ajaxPHPPath, 0755 );
		}
	}
	//保存
	file_put_contents($ajaxPHPPath, html_entity_decode($CreateAjaxPHPCode));

	return	$MainPHPPath;
}
//------------------------------------------------------------------------------
?>