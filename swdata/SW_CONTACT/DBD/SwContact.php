<?php
// -----------------------------------------------------------
//
// Copyright (C) 2015 ScenarioWriterCafe All Rights Reserved.
// 
//     CREATE TABLE SW_CONTACT
//
//     createTable_SwContact.php
// -----------------------------------------------------------
$Result = fncCreateTable();
echo $Result;
exit;


// -----------------------------------------------------------
//     fncCreateTable()
// -----------------------------------------------------------
function fncCreateTable(){
	//共通定数
	include_once("../../sw_config/swConstant.php");

	//MySQLに接続
	include_once("../include/ConnectMySQL.php");

	//DROP TABLE SQLを編集
	$strSQL = <<<END_OF_SQL

DROP TABLE IF EXISTS SW_CONTACT;

END_OF_SQL;

	//DROP TABLE EXECUTE 
	$myResult = $mySqlConnObj->query($strSQL) or die($this->fncAlert($mySqlConnObj->error));

	//CREATE TABLE SQLを編集
	$strSQL = <<<END_OF_SQL

CREATE TABLE SW_CONTACT(
	  CONTACT_ID                       INT NOT NULL AUTO_INCREMENT COMMENT 'ID'
	, CONTACT_MAILAD                   VARCHAR(256)  COMMENT 'MAILAD'
	, CONTACT_NAME                     VARCHAR(256)  COMMENT 'NAME'
	, CONTACT_SUBJECT                  VARCHAR(256)  COMMENT 'SUBJECT'
	, CONTACT_BODY                     MEDIUMTEXT  COMMENT 'BODY'
	, CONTACT_REPLY                    MEDIUMTEXT  COMMENT 'REPLY'
	, CONTACT_DATE                     TIMESTAMP  COMMENT 'DATE'
	, PRIMARY KEY (CONTACT_ID)
	)DEFAULT CHARACTER SET 'UTF8'
	COMMENT '';

END_OF_SQL;

	//CREATE TABLE EXECUTE 
	$myResult = $mySqlConnObj->query($strSQL) or die($this->fncAlert($mySqlConnObj->error));

	//UNIQUE INDEX のSQLを編集
	$strSQL = <<<END_OF_SQL

	CREATE UNIQUE INDEX SW_CONTACT_KEY_CONTACT_ID ON SW_CONTACT(CONTACT_ID);
END_OF_SQL;

	//CREATE INDEX EXECUTE 
	$myResult = $mySqlConnObj->query($strSQL) or die($this->fncAlert($mySqlConnObj->error));


	//DB切断
	include_once("../include/DisConnectMySQL.php");
	
	
	return	'CREATE TABLE SW_CONTACT Success...';
}//end function
// -----------------------------------------------------------
?>