<?php
// -----------------------------------------------------------
//
// Copyright (C) 2015 ScenarioWriterCafe All Rights Reserved.
// 
//     SW_CONTACT I/O ｼｽﾃﾑ
//
//     SwContact.php
// -----------------------------------------------------------
// ------------------------------------------------------------------------------
	//定数読み込み
	include_once("../sw_config/swConstant.php");
	//DB接続ｸﾗｽの初期化
	include_once("./include/ConnectMySQL.php");
// ------------------------------------------------------------------------------
	//ﾃﾞﾌｫﾙﾄｱｸｼｮﾝ
	$ThisPHP = 'SwContact.php';
	//ﾃﾞｰﾀ管理ｸﾗｽ
	include_once("./class/clsSwContact.php");
	$clsSwContact = new clsSwContact();
	//共通関数をｲﾝｸﾙｰﾄﾞ
	include_once("./include/swFunc.php");
	// ﾍｯﾀﾞ表示
	$HtmlTitle = "Create DB Web System";
	include_once("./include/swHeader.php");

	//ﾌｫｰﾑのﾃﾞｰﾀを読み込む
	fncGetPostItems();

	//MainProcedure
	fncMainProc($mySqlConnObj);

	exit();

// ------------------------------------------------------------------------------
//      POSTされた要素を取得
//          fncGetPostItems()
// ------------------------------------------------------------------------------
function fncGetPostItems(){
	//共通global変数
	global	$SubmitMode;
	//ﾌｫｰﾑ name要素をglobal変数にする
	global	$fdtContactId;
	global	$fdtContactMailad;
	global	$fdtContactName;
	global	$fdtContactSubject;
	global	$fdtContactBody;
	global	$fdtContactReply;
	global	$fdtContactDate;

	//処理ﾓｰﾄが空白ならreturnするﾞ
	if(!isset($_POST['SubmitMode'])){return;}
	//処理ﾓｰﾄﾞ
	$SubmitMode = swFunc_GetPostData('SubmitMode');
	if($SubmitMode == ''){return;}
	//POSTされた要素を取得
	$fdtContactId = swFunc_GetPostData('fdtContactId');              //ID
	$fdtContactMailad = swFunc_GetPostData('fdtContactMailad');      //MAILAD
	$fdtContactName = swFunc_GetPostData('fdtContactName');          //NAME
	$fdtContactSubject = swFunc_GetPostData('fdtContactSubject');    //SUBJECT
	$fdtContactBody = swFunc_GetPostData('fdtContactBody');          //BODY
	$fdtContactReply = swFunc_GetPostData('fdtContactReply');        //REPLY
	$fdtContactDate = swFunc_GetPostData('fdtContactDate');          //DATE
}//end function

// ------------------------------------------------------------------------------
//      MAIN PROCEDURE
//          fncMainProc($mySqlConnObj)
// ------------------------------------------------------------------------------
function fncMainProc($mySqlConnObj){
	global	$SubmitMode;
	//class
	global	$clsSwContact;

	global	$fdtContactId;
	global	$fdtContactMailad;
	global	$fdtContactName;
	global	$fdtContactSubject;
	global	$fdtContactBody;
	global	$fdtContactReply;
	global	$fdtContactDate;

	//SubmitModeで処理を分岐
	switch($SubmitMode ){
		case 'SELECT':
			//ﾃﾞｰﾀ管理ｸﾗｽの初期化
			$clsSwContact->clsSwContactInit($mySqlConnObj,$fdtContactId);
			//ﾌﾟﾛﾊﾟﾃｨGet
			fncSwContactGetProperty($clsSwContact);
			break;
		case 'INSERT':
			//登録処理
			fncSwContactDBInsert($mySqlConnObj,$clsSwContact);
			//選択状態にする
			$SubmitMode = 'SELECT';
			//ﾃﾞｰﾀ管理ｸﾗｽの初期化
			$clsSwContact->clsSwContactInit($mySqlConnObj,$fdtContactId);
			//ﾌﾟﾛﾊﾟﾃｨGet
			fncSwContactGetProperty($clsSwContact);
			break;
		case 'UPDATE':
			//更新処理
			fncSwContactDBUpdate($mySqlConnObj,$clsSwContact);
			//選択状態にする
			$SubmitMode = 'SELECT';
			//ﾃﾞｰﾀ管理ｸﾗｽの初期化
			$clsSwContact->clsSwContactInit($mySqlConnObj,$fdtContactId);
			//ﾌﾟﾛﾊﾟﾃｨGet
			fncSwContactGetProperty($clsSwContact);
			break;
		case 'DELETE':
			//削除処理
			fncSwContactDBDelete($mySqlConnObj,$clsSwContact);
			//ﾌﾟﾛﾊﾟﾃｨReset
			fncSwContactResetProperty($clsSwContact);
			//初期状態にする
			$SubmitMode = 'RESET';
			break;
		case 'RESET':
			//ﾌﾟﾛﾊﾟﾃｨReset
			fncSwContactResetProperty($clsSwContact);
			//初期状態にする
			$SubmitMode = 'RESET';
			break;
		default:
			break;
	}
	//ﾌｫｰﾑの表示
	fncMainForm($mySqlConnObj);
}//end function

// ------------------------------------------------------------------------------
//      MAIN FORM
//          fncMainForm($mySqlConnObj)
// ------------------------------------------------------------------------------
function fncMainForm($mySqlConnObj){
	global	$ThisPHP,$SubmitMode;
	global	$fdtContactId;
	global	$fdtContactMailad;
	global	$fdtContactName;
	global	$fdtContactSubject;
	global	$fdtContactBody;
	global	$fdtContactReply;
	global	$fdtContactDate;

	//css powerd by Bootstrap ver3
	print <<<END_OF_HTML

	<div class="wrap" >
	
		<div class="container-fluid">
		
		<!-- ﾒﾆｭｰ 制御ﾎﾞﾀﾝ 表示ROW -->
			<div class="row">
				<div class="col-sm-12">
				<legend class="form-inline submenu">
				<ul class="list-inline">
				  <li>
END_OF_HTML;

	//管理メニュー ﾄﾞﾛｯﾌﾟﾀﾞｳﾝﾒﾆｭｰ
	$TargetForm = 'frmSwContact';
	include_once("./include/swMgrDropDownMenu.php");

	
    //SubmitModeで表示するボタンを制御する
	switch($SubmitMode){
		case 'SELECT':
			print <<<END_OF_HTML
			 	</li>
			 	<li>
			        <input type="button" value="RESET"
			            id="btnReset"
			            onclick="fncSwContactSubmit(frmSwContact,'RESET','FALSE','');"
			            class="btn btn-default btn-sm">
			        <span style="width: 120px;"></span>
			        <input type="button" value="INSERT"
			            id="btnInsert"
			            onclick="fncSwContactSubmit(frmSwContact,'INSERT','TRUE','新規登録');"
			            class="btn btn-warning btn-sm">
			        <input type="button" value="UPDATE"
			            id="btnUpdate"
			            onclick="fncSwContactSubmit(frmSwContact,'UPDATE','TRUE','更新');"
			            class="btn btn-warning btn-sm">
			        <input type="button" value="DELETE"
			            id="btnDelete"
			            onclick="fncSwContactSubmit(frmSwContact,'DELETE','TRUE','削除');"
			            class="btn btn-danger btn-sm">
			
				</li>
END_OF_HTML;

	
			break;
        default:
			print <<<END_OF_HTML
			    </li>
			 	<li>
			        <input type="button" value="RESET"
			            id="btnReset"
			            onclick="fncSwContactSubmit(frmSwContact,'RESET','FALSE','');"
			            class="btn btn-default btn-sm">
			        <input type="button" value="INSERT"
			            id="btnInsert"
			            onclick="fncSwContactSubmit(frmSwContact,'INSERT','TRUE','新規登録');"
			            class="btn btn-warning btn-sm">
			
				</li>
END_OF_HTML;
            break;
    }
	print <<<END_OF_HTML

					</ul>
					</legend>
    			</div><!-- col-sm-12 end -->
    		</div><!-- row end -->
    		<!-- ﾒﾆｭｰ 制御ﾎﾞﾀﾝ 表示ROW ここまで-->

    		<!-- ﾌｫｰﾑ と ﾘｽﾄ -->
    		<div class="row row-0">
				<div class="col-sm-7 row-0">
					<form class="form-horizontal" role="form" name="frmSwContact" id="frmSwContact" method="POST" action="$ThisPHP">
						<input type="hidden" name="SubmitMode" id="SubmitMode" value="$SubmitMode">

END_OF_HTML;

	print <<<END_OF_HTML

							<div class="form-group row-0">
								<label for="fdtContactId" class="control-label col-xs-3">ID</label>
								<div class="col-xs-8">
									<input type="text" 
										class="form-control input-sm"
										id="fdtContactId" name="fdtContactId"
										value="$fdtContactId"
										placeholder="ID">
								</div>
							</div>
END_OF_HTML;
	print <<<END_OF_HTML

							<div class="form-group row-0">
								<label for="fdtContactMailad" class="control-label col-xs-3">MAILAD</label>
								<div class="col-xs-8">
									<input type="text" 
										class="form-control input-sm"
										id="fdtContactMailad" name="fdtContactMailad"
										value="$fdtContactMailad"
										placeholder="MAILAD">
								</div>
							</div>
END_OF_HTML;
	print <<<END_OF_HTML

							<div class="form-group row-0">
								<label for="fdtContactName" class="control-label col-xs-3">NAME</label>
								<div class="col-xs-8">
									<input type="text" 
										class="form-control input-sm"
										id="fdtContactName" name="fdtContactName"
										value="$fdtContactName"
										placeholder="NAME">
								</div>
							</div>
END_OF_HTML;
	print <<<END_OF_HTML

							<div class="form-group row-0">
								<label for="fdtContactSubject" class="control-label col-xs-3">SUBJECT</label>
								<div class="col-xs-8">
									<input type="text" 
										class="form-control input-sm"
										id="fdtContactSubject" name="fdtContactSubject"
										value="$fdtContactSubject"
										placeholder="SUBJECT">
								</div>
							</div>
END_OF_HTML;
	print <<<END_OF_HTML

							<div class="form-group row-0">
								<label for="fdtContactBody" class="control-label col-xs-3">BODY</label>
								<div class="col-xs-8">
									<textarea placeholder="BODY" 
										class="form-control input-sm"
										id="fdtContactBody" name="fdtContactBody"
										rows="3"
										id="InputTextarea">$fdtContactBody</textarea>
								</div>
							</div>
END_OF_HTML;
	print <<<END_OF_HTML

							<div class="form-group row-0">
								<label for="fdtContactReply" class="control-label col-xs-3">REPLY</label>
								<div class="col-xs-8">
									<textarea placeholder="REPLY" 
										class="form-control input-sm"
										id="fdtContactReply" name="fdtContactReply"
										rows="3"
										id="InputTextarea">$fdtContactReply</textarea>
								</div>
							</div>
END_OF_HTML;
	print <<<END_OF_HTML

							<div class="form-group row-0">
								<label for="fdtContactDate" class="control-label col-xs-3">DATE</label>
								<div class="col-xs-8">
									<input type="text" 
										class="form-control input-sm"
										id="fdtContactDate" name="fdtContactDate"
										value="$fdtContactDate"
										placeholder="DATE">
								</div>
							</div>
END_OF_HTML;
	print <<<END_OF_HTML

						</form>
					</div>
					<div class="col-sm-5" >
	                	<!-- リストの表示位置 -->
	                    <span id="side_list_area"></span>
					</div>
				</div>
			</div>
 
		</div><!--container-->
	</div><!--wrap-->


    <script type="text/javascript" src="./ajax/ajaxSwContact.js"></script>
    <script type="text/javascript">
        fncMakeSwContactList(frmSwContact);
    </script>


	<!--footer表示位置--><span class="sw-footer"></span>
	<script language="JavaScript">
		AjaxFunc_FooterLoad();
	</script>
	
  </body>
</html>

END_OF_HTML;

}//end function

// ------------------------------------------------------------------------------
//      ﾌﾟﾛﾊﾟﾃｨReset
//          fncSwContactResetProperty($clsSwContact)
// ------------------------------------------------------------------------------
function fncSwContactResetProperty($clsSwContact){
	global	$fdtContactId;
	global	$fdtContactMailad;
	global	$fdtContactName;
	global	$fdtContactSubject;
	global	$fdtContactBody;
	global	$fdtContactReply;
	global	$fdtContactDate;

	//ﾌﾟﾛﾊﾟﾃｨReset
	$fdtContactId = "";                       //ID
	$fdtContactMailad = "";                   //MAILAD
	$fdtContactName = "";                     //NAME
	$fdtContactSubject = "";                  //SUBJECT
	$fdtContactBody = "";                     //BODY
	$fdtContactReply = "";                    //REPLY
	$fdtContactDate = "";                     //DATE
}//end function

// ------------------------------------------------------------------------------
//      ﾌﾟﾛﾊﾟﾃｨGet
//          fncSwContactGetProperty($clsSwContact)
// ------------------------------------------------------------------------------
function fncSwContactGetProperty($clsSwContact){
	global	$fdtContactId;
	global	$fdtContactMailad;
	global	$fdtContactName;
	global	$fdtContactSubject;
	global	$fdtContactBody;
	global	$fdtContactReply;
	global	$fdtContactDate;

	//ﾌﾟﾛﾊﾟﾃｨGet
	$fdtContactId = $clsSwContact->clsSwContactGetContactId();              //ID
	$fdtContactMailad = $clsSwContact->clsSwContactGetContactMailad();      //MAILAD
	$fdtContactName = $clsSwContact->clsSwContactGetContactName();          //NAME
	$fdtContactSubject = $clsSwContact->clsSwContactGetContactSubject();    //SUBJECT

	//BODYは<br>を\nに変換する
	$fdtContactBody = $clsSwContact->clsSwContactGetContactBody();          //BODY
	$fdtContactBody = str_replace("<br>","\n",$fdtContactBody);


	//REPLYは<br>を\nに変換する
	$fdtContactReply = $clsSwContact->clsSwContactGetContactReply();        //REPLY
	$fdtContactReply = str_replace("<br>","\n",$fdtContactReply);

	$fdtContactDate = $clsSwContact->clsSwContactGetContactDate();          //DATE
}//end function

// ------------------------------------------------------------------------------
//      ﾌﾟﾛﾊﾟﾃｨSet
//          fncSwContactSetProperty($clsSwContact)
// ------------------------------------------------------------------------------
function fncSwContactSetProperty($clsSwContact){
	global	$fdtContactId;
	global	$fdtContactMailad;
	global	$fdtContactName;
	global	$fdtContactSubject;
	global	$fdtContactBody;
	global	$fdtContactReply;
	global	$fdtContactDate;

	//ﾌﾟﾛﾊﾟﾃｨSet
	$clsSwContact->clsSwContactSetContactId($fdtContactId);              //ID
	$clsSwContact->clsSwContactSetContactMailad($fdtContactMailad);      //MAILAD
	$clsSwContact->clsSwContactSetContactName($fdtContactName);          //NAME
	$clsSwContact->clsSwContactSetContactSubject($fdtContactSubject);    //SUBJECT

	//BODYは改行を<br>にしてからSetする
	$fdtContactBody = str_replace("\r\n","<br>",$fdtContactBody);
	$fdtContactBody = str_replace("\r","<br>",$fdtContactBody);
	$fdtContactBody = str_replace("\n","<br>",$fdtContactBody);
	$clsSwContact->clsSwContactSetContactBody($fdtContactBody);          //BODY


	//REPLYは改行を<br>にしてからSetする
	$fdtContactReply = str_replace("\r\n","<br>",$fdtContactReply);
	$fdtContactReply = str_replace("\r","<br>",$fdtContactReply);
	$fdtContactReply = str_replace("\n","<br>",$fdtContactReply);
	$clsSwContact->clsSwContactSetContactReply($fdtContactReply);        //REPLY

	$clsSwContact->clsSwContactSetContactDate($fdtContactDate);          //DATE
}//end function

// ------------------------------------------------------------------------------
//      DB INSERT
//          fncSwContactDBInsert($mySqlConnObj,$clsSwContact)
// ------------------------------------------------------------------------------
function fncSwContactDBInsert($mySqlConnObj,$clsSwContact){
	global	$fdtContactId;

	//ﾌﾟﾛﾊﾟﾃｨｾｯﾄ
	fncSwContactSetProperty($clsSwContact);
	//ﾃﾞｰﾀ管理ｸﾗｽ DbInsert
	$fdtContactId = $clsSwContact->clsSwContactDbInsert($mySqlConnObj);
}//end function

// ------------------------------------------------------------------------------
//      DB UPDATE
//          fncSwContactDBUpdate($mySqlConnObj,$clsSwContact)
// ------------------------------------------------------------------------------
function fncSwContactDBUpdate($mySqlConnObj,$clsSwContact){
	//ﾌﾟﾛﾊﾟﾃｨｾｯﾄ
	fncSwContactSetProperty($clsSwContact);
	//ﾃﾞｰﾀ管理ｸﾗｽ DbUpdate
	$clsSwContact->clsSwContactDbUpdate($mySqlConnObj);
}//end function

// ------------------------------------------------------------------------------
//      DB DELETE
//          fncSwContactDBDelete($mySqlConnObj,$clsSwContact)
// ------------------------------------------------------------------------------
function fncSwContactDBDelete($mySqlConnObj,$clsSwContact){
	//ﾌﾟﾛﾊﾟﾃｨｾｯﾄ
	fncSwContactSetProperty($clsSwContact);
	//ﾃﾞｰﾀ管理ｸﾗｽ DbDelete
	$clsSwContact->clsSwContactDbDelete($mySqlConnObj);
}//end function


// -----------------------------------------------------------
?>