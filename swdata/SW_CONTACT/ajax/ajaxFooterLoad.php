<?php
//	------------------------------------------------------------------------------
//
//	Copyright (C) 2015 dbu@mac.com All Rights Reserved.
//
//			WebSystemAjax
//
//			AjaxFooterLoad.php
//
//	------------------------------------------------------------------------------

	//MAIN PROC
	fncMainProc();
exit;

//--------------------------------------------------------------------------------
// MAIN PROCEDUR
//	fncMainProc($mySqlConnObj)
//--------------------------------------------------------------------------------
function fncMainProc(){

	$retNaviHtml = fncMakeFooter();

	// 出力charsetをEUC-JPに指定
	//mb_http_output( 'UTF-8' );
	// 出力
	//header ("Content-Type: text/html; charset=UTF-8");
	echo($retNaviHtml);

}


// ------------------------------------------------------------------------------
// MakeNavi
// ------------------------------------------------------------------------------
function fncMakeFooter(){

	$myNaviHtml = <<<END_OF_HTML

	<div class="footer">
		<div class="container">
      		<div class="text-center">Copyright &copy; ScenarioWriterCafe All Rights Reserved.</div>
		</div>
    </div>

END_OF_HTML;

	return	$myNaviHtml;



}//end function

?>
