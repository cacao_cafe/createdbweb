<?php
// -----------------------------------------------------------
//
// Copyright (C) 2015 ScenarioWriterCafe All Rights Reserved.
// 
//     SW_CONTACT I/O ｼｽﾃﾑ
//
//     ajaxSwContact.php
// -----------------------------------------------------------
// ------------------------------------------------------------------------------
	//定数読み込み
	include_once("../../sw_config/swConstant.php");
	//DB接続ｸﾗｽの初期化
	include_once("../include/ConnectMySQL.php");
// ------------------------------------------------------------------------------
	//共通関数をｲﾝｸﾙｰﾄﾞ
	include_once("../include/swFunc.php");
	//ﾌｫｰﾑのﾃﾞｰﾀを読み込む
	fncGetPostItems();

	//MainProcedure
	fncMainProc($mySqlConnObj);

	exit();

// ------------------------------------------------------------------------------
//      POSTされた要素を取得
//          fncGetPostItems()
// ------------------------------------------------------------------------------
function fncGetPostItems(){
	//共通global変数
	global	$SubMode;
	//ﾌｫｰﾑ name要素をglobal変数にする
	global	$fdtContactId;
	global	$fdtContactMailad;
	global	$fdtContactName;
	global	$fdtContactSubject;
	global	$fdtContactBody;
	global	$fdtContactReply;
	global	$fdtContactDate;

	//処理ﾓｰﾄが空白ならreturnするﾞ
	if(!isset($_POST['SubMode'])){return;}
	//処理ﾓｰﾄﾞ
	$SubMode = swFunc_GetPostData('SubMode');
	if($SubMode == ''){return;}
	//POSTされた要素を取得
	$fdtContactId = swFunc_GetPostData('fdtContactId');              //ID
	$fdtContactMailad = swFunc_GetPostData('fdtContactMailad');      //MAILAD
	$fdtContactName = swFunc_GetPostData('fdtContactName');          //NAME
	$fdtContactSubject = swFunc_GetPostData('fdtContactSubject');    //SUBJECT
	$fdtContactBody = swFunc_GetPostData('fdtContactBody');          //BODY
	$fdtContactReply = swFunc_GetPostData('fdtContactReply');        //REPLY
	$fdtContactDate = swFunc_GetPostData('fdtContactDate');          //DATE
}//end function

// ------------------------------------------------------------------------------
//      MAIN PROCEDURE
//          fncMainProc($mySqlConnObj)
// ------------------------------------------------------------------------------
function fncMainProc($mySqlConnObj){
	global	$SubMode;
	//global 変数
	global	$fdtContactId;
	global	$fdtContactMailad;
	global	$fdtContactName;
	global	$fdtContactSubject;
	global	$fdtContactBody;
	global	$fdtContactReply;
	global	$fdtContactDate;

	//SubModeで処理を制御
	if($SubMode == 'SwContactList'){
		$resultHtml = fncMakeSwContactList($mySqlConnObj);
	}

	// 出力charsetをUTF-8に指定
	mb_http_output ( 'UTF-8' );
	// 出力
	echo($resultHtml);
}//end function

//--------------------------------------------------------------------------------
// SW_CONTACT ALL LIST
//--------------------------------------------------------------------------------
function fncMakeSwContactList($mySqlConnObj){
	//ﾘｽﾄのﾍｯﾀﾞｰ
	$retHtml = <<<END_OF_HTML
	
		<div class="scroll_div">
		<table class="table" _fixedhead="rows:1;div-full-mode: no;">
			<tr>
				<th>ID</th>
				<th>MAILAD</th>
				<th>NAME</th>
				<th>SUBJECT</th>
			</tr>
END_OF_HTML;

	//DB から ﾃﾞｰﾀを取得しﾌﾟﾛﾊﾟﾃｨにｾｯﾄする
	$strSQL = <<<END_OF_SQL
		SELECT *,CONTACT_ID as KEY_ITEM
			FROM SW_CONTACT
					ORDER BY CONTACT_ID;
END_OF_SQL;
	//SQLを実行
	$myResult = $mySqlConnObj->query($strSQL);
	while($myRow = $myResult->fetch_assoc()){
		$valContactId = $myRow['CONTACT_ID'];
		$valContactMailad = $myRow['CONTACT_MAILAD'];
		$valContactName = $myRow['CONTACT_NAME'];
		$valContactSubject = $myRow['CONTACT_SUBJECT'];
		$valKeyItem = $myRow['KEY_ITEM'];
		//ﾘｽﾄ
		$retHtml .= <<<END_OF_HTML
		
			<tr onclick="fncSelectSwContact('$valKeyItem');">
				<td>$valContactId</td>
				<td>$valContactMailad</td>
				<td>$valContactName</td>
				<td>$valContactSubject</td>
			</tr>
END_OF_HTML;
		}//end while

		$retHtml .= <<<END_OF_HTML
		
		</table>
		</div>
END_OF_HTML;
	//結果セットを開放
	$myResult->free();
	//HTMLを返す
	return $retHtml;
}
