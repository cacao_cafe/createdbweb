<?php
// -----------------------------------------------------------
//
// Copyright (C) 2015 ScenarioWriterCafe All Rights Reserved.
// 
//     USERログイン情報 ﾃﾞｰﾀ管理ｸﾗｽ
//     clsSwUserLoginInfo
// -----------------------------------------------------------
class clsSwUserLoginInfo{
	var $UserLoginId;                     //USERログインID
	var $UserId;                          //USER_ID
	var $UserLoginDate;                   //USERログイン日時
// ｺﾝｽﾄﾗｸﾀ
    function clsSwUserLoginInfo(){
    }
// ﾌﾟﾛﾊﾟﾃｨ
// --------------------------------------
// ﾌﾟﾛﾊﾟﾃｨ Get UserLoginId
//    USERログインID を返す
// --------------------------------------
	function clsSwUserLoginInfoGetUserLoginId(){
		return $this->UserLoginId;
	}
// --------------------------------------
// ﾌﾟﾛﾊﾟﾃｨ Set UserLoginId
//    USERログインID を設定する
// --------------------------------------
	function clsSwUserLoginInfoSetUserLoginId($valUserLoginId){
		$this->UserLoginId = $valUserLoginId;
	}
// --------------------------------------
// ﾌﾟﾛﾊﾟﾃｨ Get UserId
//    USER_ID を返す
// --------------------------------------
	function clsSwUserLoginInfoGetUserId(){
		return $this->UserId;
	}
// --------------------------------------
// ﾌﾟﾛﾊﾟﾃｨ Set UserId
//    USER_ID を設定する
// --------------------------------------
	function clsSwUserLoginInfoSetUserId($valUserId){
		$this->UserId = $valUserId;
	}
// --------------------------------------
// ﾌﾟﾛﾊﾟﾃｨ Get UserLoginDate
//    USERログイン日時 を返す
// --------------------------------------
	function clsSwUserLoginInfoGetUserLoginDate(){
		return $this->UserLoginDate;
	}
// --------------------------------------
// ﾌﾟﾛﾊﾟﾃｨ Set UserLoginDate
//    USERログイン日時 を設定する
// --------------------------------------
	function clsSwUserLoginInfoSetUserLoginDate($valUserLoginDate){
		$this->UserLoginDate = $valUserLoginDate;
	}
// -------------------------------------------------------------
//    ｸﾗｽ初期化
// -------------------------------------------------------------
	function clsSwUserLoginInfoInit($mySqlConnObj,$valUserLoginId){
		//ﾌﾟﾛﾊﾟﾃｨの初期化
		$this->UserLoginId = '';                     //USERログインID
		$this->UserId = '';                          //USER_ID
		$this->UserLoginDate = '';                   //USERログイン日時
		//DB から ﾃﾞｰﾀを取得しﾌﾟﾛﾊﾟﾃｨにｾｯﾄする
		$strSQL = <<<END_OF_SQL

		SELECT * FROM SW_USER_LOGIN_INFO
				WHERE USER_LOGIN_ID = '$valUserLoginId';
END_OF_SQL;

		$myResult = $mySqlConnObj->query($strSQL) or die($this->fncAlert($mySqlConnObj->error));
		while($myRow = $myResult->fetch_assoc( )){
			$this->UserLoginId = $myRow['USER_LOGIN_ID'];
			$this->UserId = $myRow['USER_ID'];
			$this->UserLoginDate = $myRow['USER_LOGIN_DATE'];
		}//end while
    }//end function

// -------------------------------------------------------------
//    DB更新　INSERT
// -------------------------------------------------------------
	function clsSwUserLoginInfoDbInsert($mySqlConnObj){
		//ﾌﾟﾛﾊﾟﾃｨ取得
		$valUserLoginId = $this->UserLoginId;
		$valUserId = $this->UserId;
		$valUserLoginDate = $this->UserLoginDate;
			//INSERT SQL
			$strSQL = <<<END_OF_SQL

			INSERT INTO SW_USER_LOGIN_INFO(
				  USER_LOGIN_ID
				, USER_ID
				, USER_LOGIN_DATE
			) values (
				  '$valUserLoginId'
				, '$valUserId'
				, current_timestamp
			);
END_OF_SQL;

			//INSRET 
			$myResult = $mySqlConnObj->query($strSQL) or die($this->fncAlert($mySqlConnObj->error));

	}//end function

// -------------------------------------------------------------
//    DB更新　UPDATE
// -------------------------------------------------------------
	function clsSwUserLoginInfoDbUpdate($mySqlConnObj){
		//ﾌﾟﾛﾊﾟﾃｨ取得
		$valUserLoginId = $this->UserLoginId;
		$valUserId = $this->UserId;
		$valUserLoginDate = $this->UserLoginDate;
		//登録確認
		$strSQL = <<<END_OF_SQL

		SELECT * FROM SW_USER_LOGIN_INFO
				WHERE USER_LOGIN_ID = '$valUserLoginId';
END_OF_SQL;

		//登録確認SQL Execute 
		$myResult = $mySqlConnObj->query($strSQL) or die($this->fncAlert($mySqlConnObj->error));

		//件数取得
		$myRowCnt = $myResult->num_rows;
		if($myRowCnt == 0 ){
			echo $this->fncAlert("clsSwUserLoginInfo ->> NOT ENTRY!");
		}else{
			//UPDATE SQL
			$strSQL = <<<END_OF_SQL

			UPDATE SW_USER_LOGIN_INFO SET 
				  USER_ID = '$valUserId'
				, USER_LOGIN_DATE = current_timestamp
				WHERE USER_LOGIN_ID = '$valUserLoginId';
END_OF_SQL;

			//UPDATESQL Execute 
			$myResult = $mySqlConnObj->query($strSQL) or die($this->fncAlert($mySqlConnObj->error));

		}//end if
	}//end function

// -------------------------------------------------------------
//    DB更新　DELETE
// -------------------------------------------------------------
	function clsSwUserLoginInfoDbDelete($mySqlConnObj){
		//ﾌﾟﾛﾊﾟﾃｨ取得
		$valUserLoginId = $this->UserLoginId;
		//登録確認
		$strSQL = <<<END_OF_SQL

		SELECT * FROM SW_USER_LOGIN_INFO
				WHERE USER_LOGIN_ID = '$valUserLoginId';
END_OF_SQL;

		//登録確認SQL Execute 
		$myResult = $mySqlConnObj->query($strSQL) or die($this->fncAlert($mySqlConnObj->error));

		//件数取得
		$myRowCnt = $myResult->num_rows;
		if($myRowCnt == 0 ){
			echo $this->fncAlert("clsSwUserLoginInfo ->> NOT ENTRY!");
		}else{
			//DELETE SQL
			$strSQL = <<<END_OF_SQL

			DELETE FROM SW_USER_LOGIN_INFO
				WHERE USER_LOGIN_ID = '$valUserLoginId';
END_OF_SQL;

			//DELETE SQL Execute 
			$myResult = $mySqlConnObj->query($strSQL) or die($this->fncAlert($mySqlConnObj->error));

		}//end if
	}//end function

	// ------------------------------------------------------------------------------
	// java script alert
	// ------------------------------------------------------------------------------
	function fncAlert($AlertMsg){
		$strAlert = <<<END_OF_TEXT

			<script language="JavaScript">
				bootbox.alert("$AlertMsg",function() {
			});
			</script>
END_OF_TEXT;

		return $strAlert;
	}
	
} // end class
// -----------------------------------------------------------
?>