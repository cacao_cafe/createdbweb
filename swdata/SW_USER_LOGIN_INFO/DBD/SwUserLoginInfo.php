<?php
// -----------------------------------------------------------
//
// Copyright (C) 2015 ScenarioWriterCafe All Rights Reserved.
// 
//     CREATE TABLE SW_USER_LOGIN_INFO
//
//     createTable_SwUserLoginInfo.php
// -----------------------------------------------------------
$Result = fncCreateTable();
echo $Result;
exit;


// -----------------------------------------------------------
//     fncCreateTable()
// -----------------------------------------------------------
function fncCreateTable(){
	//共通定数
	include_once("../../sw_config/swConstant.php");

	//MySQLに接続
	include_once("../include/ConnectMySQL.php");

	//DROP TABLE SQLを編集
	$strSQL = <<<END_OF_SQL

DROP TABLE IF EXISTS SW_USER_LOGIN_INFO;

END_OF_SQL;

	//DROP TABLE EXECUTE 
	$myResult = $mySqlConnObj->query($strSQL) or die($this->fncAlert($mySqlConnObj->error));

	//CREATE TABLE SQLを編集
	$strSQL = <<<END_OF_SQL

CREATE TABLE SW_USER_LOGIN_INFO(
	  USER_LOGIN_ID                    VARCHAR(32) NOT NULL COMMENT 'USERログインID'
	, USER_ID                          VARCHAR(256)  COMMENT 'USER_ID'
	, USER_LOGIN_DATE                  TIMESTAMP  COMMENT 'USERログイン日時'
	, PRIMARY KEY (USER_LOGIN_ID)
	)DEFAULT CHARACTER SET 'UTF8'
	COMMENT 'USERログイン情報';

END_OF_SQL;

	//CREATE TABLE EXECUTE 
	$myResult = $mySqlConnObj->query($strSQL) or die($this->fncAlert($mySqlConnObj->error));

	//UNIQUE INDEX のSQLを編集
	$strSQL = <<<END_OF_SQL

	CREATE UNIQUE INDEX SW_USER_LOGIN_INFO_KEY_USER_LOGIN_ID ON SW_USER_LOGIN_INFO(USER_LOGIN_ID);
END_OF_SQL;

	//CREATE INDEX EXECUTE 
	$myResult = $mySqlConnObj->query($strSQL) or die($this->fncAlert($mySqlConnObj->error));


	//DB切断
	include_once("../include/DisConnectMySQL.php");
	
	
	return	'CREATE TABLE SW_USER_LOGIN_INFO Success...';
}//end function
// -----------------------------------------------------------
?>