<?php
// -----------------------------------------------------------
//
// Copyright (C) 2015 ScenarioWriterCafe All Rights Reserved.
// 
//     SW_USER_LOGIN_INFO I/O ｼｽﾃﾑ
//
//     SwUserLoginInfo.php
// -----------------------------------------------------------
// ------------------------------------------------------------------------------
	//定数読み込み
	include_once("../sw_config/swConstant.php");
	//DB接続ｸﾗｽの初期化
	include_once("./include/ConnectMySQL.php");
// ------------------------------------------------------------------------------
	//ﾃﾞﾌｫﾙﾄｱｸｼｮﾝ
	$ThisPHP = 'SwUserLoginInfo.php';
	//ﾃﾞｰﾀ管理ｸﾗｽ
	include_once("./class/clsSwUserLoginInfo.php");
	$clsSwUserLoginInfo = new clsSwUserLoginInfo();
	//共通関数をｲﾝｸﾙｰﾄﾞ
	include_once("./include/swFunc.php");
	// ﾍｯﾀﾞ表示
	$HtmlTitle = "USERログイン情報";
	include_once("./include/swHeader.php");

	//ﾌｫｰﾑのﾃﾞｰﾀを読み込む
	fncGetPostItems();

	//MainProcedure
	fncMainProc($mySqlConnObj);

	exit();

// ------------------------------------------------------------------------------
//      POSTされた要素を取得
//          fncGetPostItems()
// ------------------------------------------------------------------------------
function fncGetPostItems(){
	//共通global変数
	global	$SubmitMode;
	//ﾌｫｰﾑ name要素をglobal変数にする
	global	$fdtUserLoginId;
	global	$fdtUserId;
	global	$fdtUserLoginDate;

	//処理ﾓｰﾄが空白ならreturnするﾞ
	if(!isset($_POST['SubmitMode'])){return;}
	//処理ﾓｰﾄﾞ
	$SubmitMode = swFunc_GetPostData('SubmitMode');
	if($SubmitMode == ''){return;}
	//POSTされた要素を取得
	$fdtUserLoginId = swFunc_GetPostData('fdtUserLoginId');          //USERログインID
	$fdtUserId = swFunc_GetPostData('fdtUserId');                    //USER_ID
	$fdtUserLoginDate = swFunc_GetPostData('fdtUserLoginDate');      //USERログイン日時
}//end function

// ------------------------------------------------------------------------------
//      MAIN PROCEDURE
//          fncMainProc($mySqlConnObj)
// ------------------------------------------------------------------------------
function fncMainProc($mySqlConnObj){
	global	$SubmitMode;
	//class
	global	$clsSwUserLoginInfo;

	global	$fdtUserLoginId;
	global	$fdtUserId;
	global	$fdtUserLoginDate;

	//SubmitModeで処理を分岐
	switch($SubmitMode ){
		case 'SELECT':
			//ﾃﾞｰﾀ管理ｸﾗｽの初期化
			$clsSwUserLoginInfo->clsSwUserLoginInfoInit($mySqlConnObj,$fdtUserLoginId);
			//ﾌﾟﾛﾊﾟﾃｨGet
			fncSwUserLoginInfoGetProperty($clsSwUserLoginInfo);
			break;
		case 'INSERT':
			//登録処理
			fncSwUserLoginInfoDBInsert($mySqlConnObj,$clsSwUserLoginInfo);
			//選択状態にする
			$SubmitMode = 'SELECT';
			//ﾃﾞｰﾀ管理ｸﾗｽの初期化
			$clsSwUserLoginInfo->clsSwUserLoginInfoInit($mySqlConnObj,$fdtUserLoginId);
			//ﾌﾟﾛﾊﾟﾃｨGet
			fncSwUserLoginInfoGetProperty($clsSwUserLoginInfo);
			break;
		case 'UPDATE':
			//更新処理
			fncSwUserLoginInfoDBUpdate($mySqlConnObj,$clsSwUserLoginInfo);
			//選択状態にする
			$SubmitMode = 'SELECT';
			//ﾃﾞｰﾀ管理ｸﾗｽの初期化
			$clsSwUserLoginInfo->clsSwUserLoginInfoInit($mySqlConnObj,$fdtUserLoginId);
			//ﾌﾟﾛﾊﾟﾃｨGet
			fncSwUserLoginInfoGetProperty($clsSwUserLoginInfo);
			break;
		case 'DELETE':
			//削除処理
			fncSwUserLoginInfoDBDelete($mySqlConnObj,$clsSwUserLoginInfo);
			//ﾌﾟﾛﾊﾟﾃｨReset
			fncSwUserLoginInfoResetProperty($clsSwUserLoginInfo);
			//初期状態にする
			$SubmitMode = 'RESET';
			break;
		case 'RESET':
			//ﾌﾟﾛﾊﾟﾃｨReset
			fncSwUserLoginInfoResetProperty($clsSwUserLoginInfo);
			//初期状態にする
			$SubmitMode = 'RESET';
			break;
		default:
			break;
	}
	//ﾌｫｰﾑの表示
	fncMainForm($mySqlConnObj);
}//end function

// ------------------------------------------------------------------------------
//      MAIN FORM
//          fncMainForm($mySqlConnObj)
// ------------------------------------------------------------------------------
function fncMainForm($mySqlConnObj){
	global	$ThisPHP,$SubmitMode;
	global	$fdtUserLoginId;
	global	$fdtUserId;
	global	$fdtUserLoginDate;

	//css powerd by Bootstrap ver3
	print <<<END_OF_HTML

	<div class="wrap" >
	
		<div class="container-fluid">
		
		<!-- ﾒﾆｭｰ 制御ﾎﾞﾀﾝ 表示ROW -->
			<div class="row">
				<div class="col-sm-12">
				<legend class="form-inline submenu">
				<ul class="list-inline">
				  <li>
END_OF_HTML;

	//管理メニュー ﾄﾞﾛｯﾌﾟﾀﾞｳﾝﾒﾆｭｰ
	$TargetForm = 'frmSwUserLoginInfo';
	include_once("./include/swMgrDropDownMenu.php");

	
    //SubmitModeで表示するボタンを制御する
	switch($SubmitMode){
		case 'SELECT':
			print <<<END_OF_HTML
			 	</li>
			 	<li>
			        <input type="button" value="RESET"
			            id="btnReset"
			            onclick="fncSwUserLoginInfoSubmit(frmSwUserLoginInfo,'RESET','FALSE','');"
			            class="btn btn-default btn-sm">
			        <span style="width: 120px;"></span>
			        <input type="button" value="INSERT"
			            id="btnInsert"
			            onclick="fncSwUserLoginInfoSubmit(frmSwUserLoginInfo,'INSERT','TRUE','新規登録');"
			            class="btn btn-warning btn-sm">
			        <input type="button" value="UPDATE"
			            id="btnUpdate"
			            onclick="fncSwUserLoginInfoSubmit(frmSwUserLoginInfo,'UPDATE','TRUE','更新');"
			            class="btn btn-warning btn-sm">
			        <input type="button" value="DELETE"
			            id="btnDelete"
			            onclick="fncSwUserLoginInfoSubmit(frmSwUserLoginInfo,'DELETE','TRUE','削除');"
			            class="btn btn-danger btn-sm">
			
				</li>
END_OF_HTML;

	
			break;
        default:
			print <<<END_OF_HTML
			    </li>
			 	<li>
			        <input type="button" value="RESET"
			            id="btnReset"
			            onclick="fncSwUserLoginInfoSubmit(frmSwUserLoginInfo,'RESET','FALSE','');"
			            class="btn btn-default btn-sm">
			        <input type="button" value="INSERT"
			            id="btnInsert"
			            onclick="fncSwUserLoginInfoSubmit(frmSwUserLoginInfo,'INSERT','TRUE','新規登録');"
			            class="btn btn-warning btn-sm">
			
				</li>
END_OF_HTML;
            break;
    }
	print <<<END_OF_HTML

					</ul>
					</legend>
    			</div><!-- col-sm-12 end -->
    		</div><!-- row end -->
    		<!-- ﾒﾆｭｰ 制御ﾎﾞﾀﾝ 表示ROW ここまで-->

    		<!-- ﾌｫｰﾑ と ﾘｽﾄ -->
    		<div class="row row-0">
				<div class="col-sm-7 row-0">
					<form class="form-horizontal" role="form" name="frmSwUserLoginInfo" id="frmSwUserLoginInfo" method="POST" action="$ThisPHP">
						<input type="hidden" name="SubmitMode" id="SubmitMode" value="$SubmitMode">

END_OF_HTML;

	print <<<END_OF_HTML

							<div class="form-group row-0">
								<label for="fdtUserLoginId" class="control-label col-xs-3">USERログインID</label>
								<div class="col-xs-8">
									<input type="text" 
										class="form-control input-sm"
										id="fdtUserLoginId" name="fdtUserLoginId"
										value="$fdtUserLoginId"
										placeholder="USERログインID">
								</div>
							</div>
END_OF_HTML;
	print <<<END_OF_HTML

							<div class="form-group row-0">
								<label for="fdtUserId" class="control-label col-xs-3">USER_ID</label>
								<div class="col-xs-8">
									<input type="text" 
										class="form-control input-sm"
										id="fdtUserId" name="fdtUserId"
										value="$fdtUserId"
										placeholder="USER_ID">
								</div>
							</div>
END_OF_HTML;
	print <<<END_OF_HTML

							<div class="form-group row-0">
								<label for="fdtUserLoginDate" class="control-label col-xs-3">USERログイン日時</label>
								<div class="col-xs-8">
									<input type="text" 
										class="form-control input-sm"
										id="fdtUserLoginDate" name="fdtUserLoginDate"
										value="$fdtUserLoginDate"
										placeholder="USERログイン日時">
								</div>
							</div>
END_OF_HTML;
	print <<<END_OF_HTML

						</form>
					</div>
					<div class="col-sm-5" >
	                	<!-- リストの表示位置 -->
	                    <span id="side_list_area"></span>
					</div>
				</div>
			</div>
 
		</div><!--container-->
	</div><!--wrap-->


    <script type="text/javascript" src="./ajax/ajaxSwUserLoginInfo.js"></script>
    <script type="text/javascript">
        fncMakeSwUserLoginInfoList(frmSwUserLoginInfo);
    </script>


	<!--footer表示位置--><span class="sw-footer"></span>
	<script language="JavaScript">
		AjaxFunc_FooterLoad();
	</script>
	
  </body>
</html>

END_OF_HTML;

}//end function

// ------------------------------------------------------------------------------
//      ﾌﾟﾛﾊﾟﾃｨReset
//          fncSwUserLoginInfoResetProperty($clsSwUserLoginInfo)
// ------------------------------------------------------------------------------
function fncSwUserLoginInfoResetProperty($clsSwUserLoginInfo){
	global	$fdtUserLoginId;
	global	$fdtUserId;
	global	$fdtUserLoginDate;

	//ﾌﾟﾛﾊﾟﾃｨReset
	$fdtUserLoginId = "";                     //USERログインID
	$fdtUserId = "";                          //USER_ID
	$fdtUserLoginDate = "";                   //USERログイン日時
}//end function

// ------------------------------------------------------------------------------
//      ﾌﾟﾛﾊﾟﾃｨGet
//          fncSwUserLoginInfoGetProperty($clsSwUserLoginInfo)
// ------------------------------------------------------------------------------
function fncSwUserLoginInfoGetProperty($clsSwUserLoginInfo){
	global	$fdtUserLoginId;
	global	$fdtUserId;
	global	$fdtUserLoginDate;

	//ﾌﾟﾛﾊﾟﾃｨGet
	$fdtUserLoginId = $clsSwUserLoginInfo->clsSwUserLoginInfoGetUserLoginId();          //USERログインID
	$fdtUserId = $clsSwUserLoginInfo->clsSwUserLoginInfoGetUserId();                    //USER_ID
	$fdtUserLoginDate = $clsSwUserLoginInfo->clsSwUserLoginInfoGetUserLoginDate();      //USERログイン日時
}//end function

// ------------------------------------------------------------------------------
//      ﾌﾟﾛﾊﾟﾃｨSet
//          fncSwUserLoginInfoSetProperty($clsSwUserLoginInfo)
// ------------------------------------------------------------------------------
function fncSwUserLoginInfoSetProperty($clsSwUserLoginInfo){
	global	$fdtUserLoginId;
	global	$fdtUserId;
	global	$fdtUserLoginDate;

	//ﾌﾟﾛﾊﾟﾃｨSet
	$clsSwUserLoginInfo->clsSwUserLoginInfoSetUserLoginId($fdtUserLoginId);          //USERログインID
	$clsSwUserLoginInfo->clsSwUserLoginInfoSetUserId($fdtUserId);                    //USER_ID
	$clsSwUserLoginInfo->clsSwUserLoginInfoSetUserLoginDate($fdtUserLoginDate);      //USERログイン日時
}//end function

// ------------------------------------------------------------------------------
//      DB INSERT
//          fncSwUserLoginInfoDBInsert($mySqlConnObj,$clsSwUserLoginInfo)
// ------------------------------------------------------------------------------
function fncSwUserLoginInfoDBInsert($mySqlConnObj,$clsSwUserLoginInfo){

	//ﾌﾟﾛﾊﾟﾃｨｾｯﾄ
	fncSwUserLoginInfoSetProperty($clsSwUserLoginInfo);
	//ﾃﾞｰﾀ管理ｸﾗｽ DbInsert
	$clsSwUserLoginInfo->clsSwUserLoginInfoDbInsert($mySqlConnObj);
}//end function

// ------------------------------------------------------------------------------
//      DB UPDATE
//          fncSwUserLoginInfoDBUpdate($mySqlConnObj,$clsSwUserLoginInfo)
// ------------------------------------------------------------------------------
function fncSwUserLoginInfoDBUpdate($mySqlConnObj,$clsSwUserLoginInfo){
	//ﾌﾟﾛﾊﾟﾃｨｾｯﾄ
	fncSwUserLoginInfoSetProperty($clsSwUserLoginInfo);
	//ﾃﾞｰﾀ管理ｸﾗｽ DbUpdate
	$clsSwUserLoginInfo->clsSwUserLoginInfoDbUpdate($mySqlConnObj);
}//end function

// ------------------------------------------------------------------------------
//      DB DELETE
//          fncSwUserLoginInfoDBDelete($mySqlConnObj,$clsSwUserLoginInfo)
// ------------------------------------------------------------------------------
function fncSwUserLoginInfoDBDelete($mySqlConnObj,$clsSwUserLoginInfo){
	//ﾌﾟﾛﾊﾟﾃｨｾｯﾄ
	fncSwUserLoginInfoSetProperty($clsSwUserLoginInfo);
	//ﾃﾞｰﾀ管理ｸﾗｽ DbDelete
	$clsSwUserLoginInfo->clsSwUserLoginInfoDbDelete($mySqlConnObj);
}//end function


// -----------------------------------------------------------
?>