<?php
// -----------------------------------------------------------
//
// Copyright (C) 2015 ScenarioWriterCafe All Rights Reserved.
// 
//     SW_SCENARIO_LINES I/O ｼｽﾃﾑ
//
//     SwScenarioLines.php
// -----------------------------------------------------------
// ------------------------------------------------------------------------------
	//定数読み込み
	include_once("../sw_config/swConstant.php");
	//DB接続ｸﾗｽの初期化
	include_once("./include/ConnectMySQL.php");
// ------------------------------------------------------------------------------
	//ﾃﾞﾌｫﾙﾄｱｸｼｮﾝ
	$ThisPHP = 'SwScenarioLines.php';
	//ﾃﾞｰﾀ管理ｸﾗｽ
	include_once("./class/clsSwScenarioLines.php");
	$clsSwScenarioLines = new clsSwScenarioLines();
	//共通関数をｲﾝｸﾙｰﾄﾞ
	include_once("./include/swFunc.php");
	// ﾍｯﾀﾞ表示
	$HtmlTitle = "シナリオ";
	include_once("./include/swHeader.php");

	//ﾌｫｰﾑのﾃﾞｰﾀを読み込む
	fncGetPostItems();

	//MainProcedure
	fncMainProc($mySqlConnObj);

	exit();

// ------------------------------------------------------------------------------
//      POSTされた要素を取得
//          fncGetPostItems()
// ------------------------------------------------------------------------------
function fncGetPostItems(){
	//共通global変数
	global	$SubmitMode;
	//ﾌｫｰﾑ name要素をglobal変数にする
	global	$fdtScenarioLinesId;
	global	$fdtScenarioId;
	global	$fdtSceneId;
	global	$fdtScenarioLinesOrderNo;
	global	$fdtScenarioType;
	global	$fdtCharacterId;
	global	$fdtScenarioLines;

	//処理ﾓｰﾄが空白ならreturnするﾞ
	if(!isset($_POST['SubmitMode'])){return;}
	//処理ﾓｰﾄﾞ
	$SubmitMode = swFunc_GetPostData('SubmitMode');
	if($SubmitMode == ''){return;}
	//POSTされた要素を取得
	$fdtScenarioLinesId = swFunc_GetPostData('fdtScenarioLinesId');  //SCENARIO_LINES_ID
	$fdtScenarioId = swFunc_GetPostData('fdtScenarioId');            //SCENARIO_ID
	$fdtSceneId = swFunc_GetPostData('fdtSceneId');                  //SCENE_ID
	$fdtScenarioLinesOrderNo = swFunc_GetPostData('fdtScenarioLinesOrderNo');//並び順
	$fdtScenarioType = swFunc_GetPostData('fdtScenarioType');        //シナリオ種別
	$fdtCharacterId = swFunc_GetPostData('fdtCharacterId');          //登場人物
	$fdtScenarioLines = swFunc_GetPostData('fdtScenarioLines');      //台詞
}//end function

// ------------------------------------------------------------------------------
//      MAIN PROCEDURE
//          fncMainProc($mySqlConnObj)
// ------------------------------------------------------------------------------
function fncMainProc($mySqlConnObj){
	global	$SubmitMode;
	//class
	global	$clsSwScenarioLines;

	global	$fdtScenarioLinesId;
	global	$fdtScenarioId;
	global	$fdtSceneId;
	global	$fdtScenarioLinesOrderNo;
	global	$fdtScenarioType;
	global	$fdtCharacterId;
	global	$fdtScenarioLines;

	//SubmitModeで処理を分岐
	switch($SubmitMode ){
		case 'SELECT':
			//ﾃﾞｰﾀ管理ｸﾗｽの初期化
			$clsSwScenarioLines->clsSwScenarioLinesInit($mySqlConnObj,$fdtScenarioLinesId);
			//ﾌﾟﾛﾊﾟﾃｨGet
			fncSwScenarioLinesGetProperty($clsSwScenarioLines);
			break;
		case 'INSERT':
			//登録処理
			fncSwScenarioLinesDBInsert($mySqlConnObj,$clsSwScenarioLines);
			//選択状態にする
			$SubmitMode = 'SELECT';
			//ﾃﾞｰﾀ管理ｸﾗｽの初期化
			$clsSwScenarioLines->clsSwScenarioLinesInit($mySqlConnObj,$fdtScenarioLinesId);
			//ﾌﾟﾛﾊﾟﾃｨGet
			fncSwScenarioLinesGetProperty($clsSwScenarioLines);
			break;
		case 'UPDATE':
			//更新処理
			fncSwScenarioLinesDBUpdate($mySqlConnObj,$clsSwScenarioLines);
			//選択状態にする
			$SubmitMode = 'SELECT';
			//ﾃﾞｰﾀ管理ｸﾗｽの初期化
			$clsSwScenarioLines->clsSwScenarioLinesInit($mySqlConnObj,$fdtScenarioLinesId);
			//ﾌﾟﾛﾊﾟﾃｨGet
			fncSwScenarioLinesGetProperty($clsSwScenarioLines);
			break;
		case 'DELETE':
			//削除処理
			fncSwScenarioLinesDBDelete($mySqlConnObj,$clsSwScenarioLines);
			//ﾌﾟﾛﾊﾟﾃｨReset
			fncSwScenarioLinesResetProperty($clsSwScenarioLines);
			//初期状態にする
			$SubmitMode = 'RESET';
			break;
		case 'RESET':
			//ﾌﾟﾛﾊﾟﾃｨReset
			fncSwScenarioLinesResetProperty($clsSwScenarioLines);
			//初期状態にする
			$SubmitMode = 'RESET';
			break;
		default:
			break;
	}
	//ﾌｫｰﾑの表示
	fncMainForm($mySqlConnObj);
}//end function

// ------------------------------------------------------------------------------
//      MAIN FORM
//          fncMainForm($mySqlConnObj)
// ------------------------------------------------------------------------------
function fncMainForm($mySqlConnObj){
	global	$ThisPHP,$SubmitMode;
	global	$fdtScenarioLinesId;
	global	$fdtScenarioId;
	global	$fdtSceneId;
	global	$fdtScenarioLinesOrderNo;
	global	$fdtScenarioType;
	global	$fdtCharacterId;
	global	$fdtScenarioLines;

	//css powerd by Bootstrap ver3
	print <<<END_OF_HTML

	<div class="wrap" >
	
		<div class="container-fluid">
		
		<!-- ﾒﾆｭｰ 制御ﾎﾞﾀﾝ 表示ROW -->
			<div class="row">
				<div class="col-sm-12">
				<legend class="form-inline submenu">
				<ul class="list-inline">
				  <li>
END_OF_HTML;

	//管理メニュー ﾄﾞﾛｯﾌﾟﾀﾞｳﾝﾒﾆｭｰ
	$TargetForm = 'frmSwScenarioLines';
	include_once("./include/swMgrDropDownMenu.php");

	
    //SubmitModeで表示するボタンを制御する
	switch($SubmitMode){
		case 'SELECT':
			print <<<END_OF_HTML
			 	</li>
			 	<li>
			        <input type="button" value="RESET"
			            id="btnReset"
			            onclick="fncSwScenarioLinesSubmit(frmSwScenarioLines,'RESET','FALSE','');"
			            class="btn btn-default btn-sm">
			        <span style="width: 120px;"></span>
			        <input type="button" value="INSERT"
			            id="btnInsert"
			            onclick="fncSwScenarioLinesSubmit(frmSwScenarioLines,'INSERT','TRUE','新規登録');"
			            class="btn btn-warning btn-sm">
			        <input type="button" value="UPDATE"
			            id="btnUpdate"
			            onclick="fncSwScenarioLinesSubmit(frmSwScenarioLines,'UPDATE','TRUE','更新');"
			            class="btn btn-warning btn-sm">
			        <input type="button" value="DELETE"
			            id="btnDelete"
			            onclick="fncSwScenarioLinesSubmit(frmSwScenarioLines,'DELETE','TRUE','削除');"
			            class="btn btn-danger btn-sm">
			
				</li>
END_OF_HTML;

	
			break;
        default:
			print <<<END_OF_HTML
			    </li>
			 	<li>
			        <input type="button" value="RESET"
			            id="btnReset"
			            onclick="fncSwScenarioLinesSubmit(frmSwScenarioLines,'RESET','FALSE','');"
			            class="btn btn-default btn-sm">
			        <input type="button" value="INSERT"
			            id="btnInsert"
			            onclick="fncSwScenarioLinesSubmit(frmSwScenarioLines,'INSERT','TRUE','新規登録');"
			            class="btn btn-warning btn-sm">
			
				</li>
END_OF_HTML;
            break;
    }
	print <<<END_OF_HTML

					</ul>
					</legend>
    			</div><!-- col-sm-12 end -->
    		</div><!-- row end -->
    		<!-- ﾒﾆｭｰ 制御ﾎﾞﾀﾝ 表示ROW ここまで-->

    		<!-- ﾌｫｰﾑ と ﾘｽﾄ -->
    		<div class="row row-0">
				<div class="col-sm-7 row-0">
					<form class="form-horizontal" role="form" name="frmSwScenarioLines" id="frmSwScenarioLines" method="POST" action="$ThisPHP">
						<input type="hidden" name="SubmitMode" id="SubmitMode" value="$SubmitMode">

END_OF_HTML;

	print <<<END_OF_HTML

							<div class="form-group row-0">
								<label for="fdtScenarioLinesId" class="control-label col-xs-3">SCENARIO_LINES_ID</label>
								<div class="col-xs-8">
									<input type="number"
										class="form-control input-sm text-right"
										id="fdtScenarioLinesId" name="fdtScenarioLinesId"
										value="$fdtScenarioLinesId"
										onkeyup="AjaxFunc_AddFigure(frmSwScenarioLines,'fdtScenarioLinesId');"
										placeholder="SCENARIO_LINES_ID">
								</div>
							</div>
END_OF_HTML;
	print <<<END_OF_HTML

							<div class="form-group row-0">
								<label for="fdtScenarioId" class="control-label col-xs-3">SCENARIO_ID</label>
								<div class="col-xs-8">
									<input type="number"
										class="form-control input-sm text-right"
										id="fdtScenarioId" name="fdtScenarioId"
										value="$fdtScenarioId"
										onkeyup="AjaxFunc_AddFigure(frmSwScenarioLines,'fdtScenarioId');"
										placeholder="SCENARIO_ID">
								</div>
							</div>
END_OF_HTML;
	print <<<END_OF_HTML

							<div class="form-group row-0">
								<label for="fdtSceneId" class="control-label col-xs-3">SCENE_ID</label>
								<div class="col-xs-8">
									<input type="number"
										class="form-control input-sm text-right"
										id="fdtSceneId" name="fdtSceneId"
										value="$fdtSceneId"
										onkeyup="AjaxFunc_AddFigure(frmSwScenarioLines,'fdtSceneId');"
										placeholder="SCENE_ID">
								</div>
							</div>
END_OF_HTML;
	print <<<END_OF_HTML

							<div class="form-group row-0">
								<label for="fdtScenarioLinesOrderNo" class="control-label col-xs-3">並び順</label>
								<div class="col-xs-8">
									<input type="number"
										class="form-control input-sm text-right"
										id="fdtScenarioLinesOrderNo" name="fdtScenarioLinesOrderNo"
										value="$fdtScenarioLinesOrderNo"
										onkeyup="AjaxFunc_AddFigure(frmSwScenarioLines,'fdtScenarioLinesOrderNo');"
										placeholder="並び順">
								</div>
							</div>
END_OF_HTML;
	print <<<END_OF_HTML

							<div class="form-group row-0">
								<label for="fdtScenarioType" class="control-label col-xs-3">シナリオ種別</label>
								<div class="col-xs-8">
								
END_OF_HTML;

	//------------------------------------------------------------
	//CSVﾃﾞｰﾀからselect box を作成する
	$ObjName = "fdtScenarioType";		//select box の名称.ID
	//ここでfunctionからCSVﾃﾞｰﾀを取得
	$csvArray = swFunc_MakeSelectItemsCsvSample();	//CSVﾃﾞｰﾀ
	$default = "$fdtScenarioType";		//ﾃﾞﾌｫﾙﾄ値
	$onChange = '';					//onChange で起動する javascript or jQuery
	$ViewCode = FALSE;				//ｺｰﾄﾞを表示する場合はTRUE
	//SelectBoxHtml出力
	print swFunc_MakeSelectBox($ObjName,$csvArray,$default,$onChange,$ViewCode);
//------------------------------------------------------------

	print <<<END_OF_HTML

								</div>
							</div>
END_OF_HTML;
	print <<<END_OF_HTML

							<div class="form-group row-0">
								<label for="fdtCharacterId" class="control-label col-xs-3">登場人物</label>
								<div class="col-xs-8">
								
END_OF_HTML;

	//------------------------------------------------------------
	//CSVﾃﾞｰﾀからselect box を作成する
	$ObjName = "fdtCharacterId";		//select box の名称.ID
	//ここでfunctionからCSVﾃﾞｰﾀを取得
	$csvArray = swFunc_MakeSelectItemsCsvSample();	//CSVﾃﾞｰﾀ
	$default = "$fdtCharacterId";		//ﾃﾞﾌｫﾙﾄ値
	$onChange = '';					//onChange で起動する javascript or jQuery
	$ViewCode = FALSE;				//ｺｰﾄﾞを表示する場合はTRUE
	//SelectBoxHtml出力
	print swFunc_MakeSelectBox($ObjName,$csvArray,$default,$onChange,$ViewCode);
//------------------------------------------------------------

	print <<<END_OF_HTML

								</div>
							</div>
END_OF_HTML;
	print <<<END_OF_HTML

							<div class="form-group row-0">
								<label for="fdtScenarioLines" class="control-label col-xs-3">台詞</label>
								<div class="col-xs-8">
									<textarea placeholder="台詞" 
										class="form-control input-sm"
										id="fdtScenarioLines" name="fdtScenarioLines"
										rows="3"
										id="InputTextarea">$fdtScenarioLines</textarea>
								</div>
							</div>
END_OF_HTML;
	print <<<END_OF_HTML

						</form>
					</div>
					<div class="col-sm-5" >
	                	<!-- リストの表示位置 -->
	                    <span id="side_list_area"></span>
					</div>
				</div>
			</div>
 
		</div><!--container-->
	</div><!--wrap-->


    <script type="text/javascript" src="./ajax/ajaxSwScenarioLines.js"></script>
    <script type="text/javascript">
        fncMakeSwScenarioLinesList(frmSwScenarioLines);
    </script>


	<!--footer表示位置--><span class="sw-footer"></span>
	<script language="JavaScript">
		AjaxFunc_FooterLoad();
	</script>
	
  </body>
</html>

END_OF_HTML;

}//end function

// ------------------------------------------------------------------------------
//      ﾌﾟﾛﾊﾟﾃｨReset
//          fncSwScenarioLinesResetProperty($clsSwScenarioLines)
// ------------------------------------------------------------------------------
function fncSwScenarioLinesResetProperty($clsSwScenarioLines){
	global	$fdtScenarioLinesId;
	global	$fdtScenarioId;
	global	$fdtSceneId;
	global	$fdtScenarioLinesOrderNo;
	global	$fdtScenarioType;
	global	$fdtCharacterId;
	global	$fdtScenarioLines;

	//ﾌﾟﾛﾊﾟﾃｨReset
	$fdtScenarioLinesId = "";                 //SCENARIO_LINES_ID
	$fdtScenarioId = "";                      //SCENARIO_ID
	$fdtSceneId = "";                         //SCENE_ID
	$fdtScenarioLinesOrderNo = "";            //並び順
	$fdtScenarioType = "";                    //シナリオ種別
	$fdtCharacterId = "";                     //登場人物
	$fdtScenarioLines = "";                   //台詞
}//end function

// ------------------------------------------------------------------------------
//      ﾌﾟﾛﾊﾟﾃｨGet
//          fncSwScenarioLinesGetProperty($clsSwScenarioLines)
// ------------------------------------------------------------------------------
function fncSwScenarioLinesGetProperty($clsSwScenarioLines){
	global	$fdtScenarioLinesId;
	global	$fdtScenarioId;
	global	$fdtSceneId;
	global	$fdtScenarioLinesOrderNo;
	global	$fdtScenarioType;
	global	$fdtCharacterId;
	global	$fdtScenarioLines;

	//ﾌﾟﾛﾊﾟﾃｨGet
	$fdtScenarioLinesId = $clsSwScenarioLines->clsSwScenarioLinesGetScenarioLinesId();  //SCENARIO_LINES_ID
	$fdtScenarioId = $clsSwScenarioLines->clsSwScenarioLinesGetScenarioId();            //SCENARIO_ID
	$fdtSceneId = $clsSwScenarioLines->clsSwScenarioLinesGetSceneId();                  //SCENE_ID
	$fdtScenarioLinesOrderNo = $clsSwScenarioLines->clsSwScenarioLinesGetScenarioLinesOrderNo();//並び順
	$fdtScenarioType = $clsSwScenarioLines->clsSwScenarioLinesGetScenarioType();        //シナリオ種別
	$fdtCharacterId = $clsSwScenarioLines->clsSwScenarioLinesGetCharacterId();          //登場人物

	//台詞は<br>を\nに変換する
	$fdtScenarioLines = $clsSwScenarioLines->clsSwScenarioLinesGetScenarioLines();      //台詞
	$fdtScenarioLines = str_replace("<br>","\n",$fdtScenarioLines);

}//end function

// ------------------------------------------------------------------------------
//      ﾌﾟﾛﾊﾟﾃｨSet
//          fncSwScenarioLinesSetProperty($clsSwScenarioLines)
// ------------------------------------------------------------------------------
function fncSwScenarioLinesSetProperty($clsSwScenarioLines){
	global	$fdtScenarioLinesId;
	global	$fdtScenarioId;
	global	$fdtSceneId;
	global	$fdtScenarioLinesOrderNo;
	global	$fdtScenarioType;
	global	$fdtCharacterId;
	global	$fdtScenarioLines;

	//ﾌﾟﾛﾊﾟﾃｨSet

	//SCENARIO_LINES_IDはｶﾝﾏを取り除いてからSetする
	$fdtScenarioLinesId = str_replace(",","",$fdtScenarioLinesId);
	$clsSwScenarioLines->clsSwScenarioLinesSetScenarioLinesId($fdtScenarioLinesId);  //SCENARIO_LINES_ID


	//SCENARIO_IDはｶﾝﾏを取り除いてからSetする
	$fdtScenarioId = str_replace(",","",$fdtScenarioId);
	$clsSwScenarioLines->clsSwScenarioLinesSetScenarioId($fdtScenarioId);            //SCENARIO_ID


	//SCENE_IDはｶﾝﾏを取り除いてからSetする
	$fdtSceneId = str_replace(",","",$fdtSceneId);
	$clsSwScenarioLines->clsSwScenarioLinesSetSceneId($fdtSceneId);                  //SCENE_ID


	//並び順はｶﾝﾏを取り除いてからSetする
	$fdtScenarioLinesOrderNo = str_replace(",","",$fdtScenarioLinesOrderNo);
	$clsSwScenarioLines->clsSwScenarioLinesSetScenarioLinesOrderNo($fdtScenarioLinesOrderNo);//並び順

	$clsSwScenarioLines->clsSwScenarioLinesSetScenarioType($fdtScenarioType);        //シナリオ種別
	$clsSwScenarioLines->clsSwScenarioLinesSetCharacterId($fdtCharacterId);          //登場人物

	//台詞は改行を<br>にしてからSetする
	$fdtScenarioLines = str_replace("\r\n","<br>",$fdtScenarioLines);
	$fdtScenarioLines = str_replace("\r","<br>",$fdtScenarioLines);
	$fdtScenarioLines = str_replace("\n","<br>",$fdtScenarioLines);
	$clsSwScenarioLines->clsSwScenarioLinesSetScenarioLines($fdtScenarioLines);      //台詞

}//end function

// ------------------------------------------------------------------------------
//      DB INSERT
//          fncSwScenarioLinesDBInsert($mySqlConnObj,$clsSwScenarioLines)
// ------------------------------------------------------------------------------
function fncSwScenarioLinesDBInsert($mySqlConnObj,$clsSwScenarioLines){
	global	$fdtScenarioLinesId;

	//ﾌﾟﾛﾊﾟﾃｨｾｯﾄ
	fncSwScenarioLinesSetProperty($clsSwScenarioLines);
	//ﾃﾞｰﾀ管理ｸﾗｽ DbInsert
	$fdtScenarioLinesId = $clsSwScenarioLines->clsSwScenarioLinesDbInsert($mySqlConnObj);
}//end function

// ------------------------------------------------------------------------------
//      DB UPDATE
//          fncSwScenarioLinesDBUpdate($mySqlConnObj,$clsSwScenarioLines)
// ------------------------------------------------------------------------------
function fncSwScenarioLinesDBUpdate($mySqlConnObj,$clsSwScenarioLines){
	//ﾌﾟﾛﾊﾟﾃｨｾｯﾄ
	fncSwScenarioLinesSetProperty($clsSwScenarioLines);
	//ﾃﾞｰﾀ管理ｸﾗｽ DbUpdate
	$clsSwScenarioLines->clsSwScenarioLinesDbUpdate($mySqlConnObj);
}//end function

// ------------------------------------------------------------------------------
//      DB DELETE
//          fncSwScenarioLinesDBDelete($mySqlConnObj,$clsSwScenarioLines)
// ------------------------------------------------------------------------------
function fncSwScenarioLinesDBDelete($mySqlConnObj,$clsSwScenarioLines){
	//ﾌﾟﾛﾊﾟﾃｨｾｯﾄ
	fncSwScenarioLinesSetProperty($clsSwScenarioLines);
	//ﾃﾞｰﾀ管理ｸﾗｽ DbDelete
	$clsSwScenarioLines->clsSwScenarioLinesDbDelete($mySqlConnObj);
}//end function


// -----------------------------------------------------------
?>