<?php
// -----------------------------------------------------------
//
// Copyright (C) 2015 ScenarioWriterCafe All Rights Reserved.
// 
//     CREATE TABLE SW_SCENARIO_LINES
//
//     createTable_SwScenarioLines.php
// -----------------------------------------------------------
$Result = fncCreateTable();
echo $Result;
exit;


// -----------------------------------------------------------
//     fncCreateTable()
// -----------------------------------------------------------
function fncCreateTable(){
	//共通定数
	include_once("../../sw_config/swConstant.php");

	//MySQLに接続
	include_once("../include/ConnectMySQL.php");

	//DROP TABLE SQLを編集
	$strSQL = <<<END_OF_SQL

DROP TABLE IF EXISTS SW_SCENARIO_LINES;

END_OF_SQL;

	//DROP TABLE EXECUTE 
	$myResult = $mySqlConnObj->query($strSQL) or die($this->fncAlert($mySqlConnObj->error));

	//CREATE TABLE SQLを編集
	$strSQL = <<<END_OF_SQL

CREATE TABLE SW_SCENARIO_LINES(
	  SCENARIO_LINES_ID                INT NOT NULL AUTO_INCREMENT COMMENT 'SCENARIO_LINES_ID'
	, SCENARIO_ID                      INT  COMMENT 'SCENARIO_ID'
	, SCENE_ID                         INT  COMMENT 'SCENE_ID'
	, SCENARIO_LINES_ORDER_NO          DOUBLE  COMMENT '並び順'
	, SCENARIO_TYPE                    INT  COMMENT 'シナリオ種別'
	, CHARACTER_ID                     INT  COMMENT '登場人物'
	, SCENARIO_LINES                   MEDIUMTEXT  COMMENT '台詞'
	, PRIMARY KEY (SCENARIO_LINES_ID)
	)DEFAULT CHARACTER SET 'UTF8'
	COMMENT 'シナリオ';

END_OF_SQL;

	//CREATE TABLE EXECUTE 
	$myResult = $mySqlConnObj->query($strSQL) or die($this->fncAlert($mySqlConnObj->error));

	//UNIQUE INDEX のSQLを編集
	$strSQL = <<<END_OF_SQL

	CREATE UNIQUE INDEX SW_SCENARIO_LINES_KEY_SCENARIO_LINES_ID ON SW_SCENARIO_LINES(SCENARIO_LINES_ID);
END_OF_SQL;

	//CREATE INDEX EXECUTE 
	$myResult = $mySqlConnObj->query($strSQL) or die($this->fncAlert($mySqlConnObj->error));


	//DB切断
	include_once("../include/DisConnectMySQL.php");
	
	
	return	'CREATE TABLE SW_SCENARIO_LINES Success...';
}//end function
// -----------------------------------------------------------
?>