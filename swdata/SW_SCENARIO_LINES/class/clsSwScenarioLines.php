<?php
// -----------------------------------------------------------
//
// Copyright (C) 2015 ScenarioWriterCafe All Rights Reserved.
// 
//     シナリオ ﾃﾞｰﾀ管理ｸﾗｽ
//     clsSwScenarioLines
// -----------------------------------------------------------
class clsSwScenarioLines{
	var $ScenarioLinesId;                 //SCENARIO_LINES_ID
	var $ScenarioId;                      //SCENARIO_ID
	var $SceneId;                         //SCENE_ID
	var $ScenarioLinesOrderNo;            //並び順
	var $ScenarioType;                    //シナリオ種別
	var $CharacterId;                     //登場人物
	var $ScenarioLines;                   //台詞
// ｺﾝｽﾄﾗｸﾀ
    function clsSwScenarioLines(){
    }
// ﾌﾟﾛﾊﾟﾃｨ
// --------------------------------------
// ﾌﾟﾛﾊﾟﾃｨ Get ScenarioLinesId
//    SCENARIO_LINES_ID を返す
// --------------------------------------
	function clsSwScenarioLinesGetScenarioLinesId(){
		return $this->ScenarioLinesId;
	}
// --------------------------------------
// ﾌﾟﾛﾊﾟﾃｨ Set ScenarioLinesId
//    SCENARIO_LINES_ID を設定する
// --------------------------------------
	function clsSwScenarioLinesSetScenarioLinesId($valScenarioLinesId){
		$this->ScenarioLinesId = $valScenarioLinesId;
	}
// --------------------------------------
// ﾌﾟﾛﾊﾟﾃｨ Get ScenarioId
//    SCENARIO_ID を返す
// --------------------------------------
	function clsSwScenarioLinesGetScenarioId(){
		return $this->ScenarioId;
	}
// --------------------------------------
// ﾌﾟﾛﾊﾟﾃｨ Set ScenarioId
//    SCENARIO_ID を設定する
// --------------------------------------
	function clsSwScenarioLinesSetScenarioId($valScenarioId){
		$this->ScenarioId = $valScenarioId;
	}
// --------------------------------------
// ﾌﾟﾛﾊﾟﾃｨ Get SceneId
//    SCENE_ID を返す
// --------------------------------------
	function clsSwScenarioLinesGetSceneId(){
		return $this->SceneId;
	}
// --------------------------------------
// ﾌﾟﾛﾊﾟﾃｨ Set SceneId
//    SCENE_ID を設定する
// --------------------------------------
	function clsSwScenarioLinesSetSceneId($valSceneId){
		$this->SceneId = $valSceneId;
	}
// --------------------------------------
// ﾌﾟﾛﾊﾟﾃｨ Get ScenarioLinesOrderNo
//    並び順 を返す
// --------------------------------------
	function clsSwScenarioLinesGetScenarioLinesOrderNo(){
		return $this->ScenarioLinesOrderNo;
	}
// --------------------------------------
// ﾌﾟﾛﾊﾟﾃｨ Set ScenarioLinesOrderNo
//    並び順 を設定する
// --------------------------------------
	function clsSwScenarioLinesSetScenarioLinesOrderNo($valScenarioLinesOrderNo){
		$this->ScenarioLinesOrderNo = $valScenarioLinesOrderNo;
	}
// --------------------------------------
// ﾌﾟﾛﾊﾟﾃｨ Get ScenarioType
//    シナリオ種別 を返す
// --------------------------------------
	function clsSwScenarioLinesGetScenarioType(){
		return $this->ScenarioType;
	}
// --------------------------------------
// ﾌﾟﾛﾊﾟﾃｨ Set ScenarioType
//    シナリオ種別 を設定する
// --------------------------------------
	function clsSwScenarioLinesSetScenarioType($valScenarioType){
		$this->ScenarioType = $valScenarioType;
	}
// --------------------------------------
// ﾌﾟﾛﾊﾟﾃｨ Get CharacterId
//    登場人物 を返す
// --------------------------------------
	function clsSwScenarioLinesGetCharacterId(){
		return $this->CharacterId;
	}
// --------------------------------------
// ﾌﾟﾛﾊﾟﾃｨ Set CharacterId
//    登場人物 を設定する
// --------------------------------------
	function clsSwScenarioLinesSetCharacterId($valCharacterId){
		$this->CharacterId = $valCharacterId;
	}
// --------------------------------------
// ﾌﾟﾛﾊﾟﾃｨ Get ScenarioLines
//    台詞 を返す
// --------------------------------------
	function clsSwScenarioLinesGetScenarioLines(){
		return $this->ScenarioLines;
	}
// --------------------------------------
// ﾌﾟﾛﾊﾟﾃｨ Set ScenarioLines
//    台詞 を設定する
// --------------------------------------
	function clsSwScenarioLinesSetScenarioLines($valScenarioLines){
		$this->ScenarioLines = $valScenarioLines;
	}
// -------------------------------------------------------------
//    ｸﾗｽ初期化
// -------------------------------------------------------------
	function clsSwScenarioLinesInit($mySqlConnObj,$valScenarioLinesId){
		//ﾌﾟﾛﾊﾟﾃｨの初期化
		$this->ScenarioLinesId = '';                 //SCENARIO_LINES_ID
		$this->ScenarioId = '';                      //SCENARIO_ID
		$this->SceneId = '';                         //SCENE_ID
		$this->ScenarioLinesOrderNo = '';            //並び順
		$this->ScenarioType = '';                    //シナリオ種別
		$this->CharacterId = '';                     //登場人物
		$this->ScenarioLines = '';                   //台詞
		//DB から ﾃﾞｰﾀを取得しﾌﾟﾛﾊﾟﾃｨにｾｯﾄする
		$strSQL = <<<END_OF_SQL

		SELECT * FROM SW_SCENARIO_LINES
				WHERE SCENARIO_LINES_ID = '$valScenarioLinesId';
END_OF_SQL;

		$myResult = $mySqlConnObj->query($strSQL) or die($this->fncAlert($mySqlConnObj->error));
		while($myRow = $myResult->fetch_assoc( )){
			$this->ScenarioLinesId = $myRow['SCENARIO_LINES_ID'];
			$this->ScenarioId = $myRow['SCENARIO_ID'];
			$this->SceneId = $myRow['SCENE_ID'];
			$this->ScenarioLinesOrderNo = $myRow['SCENARIO_LINES_ORDER_NO'];
			$this->ScenarioType = $myRow['SCENARIO_TYPE'];
			$this->CharacterId = $myRow['CHARACTER_ID'];
			$this->ScenarioLines = $myRow['SCENARIO_LINES'];
		}//end while
    }//end function

// -------------------------------------------------------------
//    DB更新　INSERT
// -------------------------------------------------------------
	function clsSwScenarioLinesDbInsert($mySqlConnObj){
		//ﾌﾟﾛﾊﾟﾃｨ取得
		$valScenarioLinesId = $this->ScenarioLinesId;
		$valScenarioId = $this->ScenarioId;
		$valSceneId = $this->SceneId;
		$valScenarioLinesOrderNo = $this->ScenarioLinesOrderNo;
		$valScenarioType = $this->ScenarioType;
		$valCharacterId = $this->CharacterId;
		$valScenarioLines = $this->ScenarioLines;
			//INSERT SQL
			$strSQL = <<<END_OF_SQL

			INSERT INTO SW_SCENARIO_LINES(
				  SCENARIO_ID
				, SCENE_ID
				, SCENARIO_LINES_ORDER_NO
				, SCENARIO_TYPE
				, CHARACTER_ID
				, SCENARIO_LINES
			) values (
				  '$valScenarioId'
				, '$valSceneId'
				, '$valScenarioLinesOrderNo'
				, '$valScenarioType'
				, '$valCharacterId'
				, '$valScenarioLines'
			);
END_OF_SQL;

			//INSRET 
			$myResult = $mySqlConnObj->query($strSQL) or die($this->fncAlert($mySqlConnObj->error));

			//AUTO_INCREMENT値取得
			$LastInsertId = $mySqlConnObj->insert_id;
			//AUTO_INCREMENT値を返す
			return $LastInsertId;
	}//end function

// -------------------------------------------------------------
//    DB更新　UPDATE
// -------------------------------------------------------------
	function clsSwScenarioLinesDbUpdate($mySqlConnObj){
		//ﾌﾟﾛﾊﾟﾃｨ取得
		$valScenarioLinesId = $this->ScenarioLinesId;
		$valScenarioId = $this->ScenarioId;
		$valSceneId = $this->SceneId;
		$valScenarioLinesOrderNo = $this->ScenarioLinesOrderNo;
		$valScenarioType = $this->ScenarioType;
		$valCharacterId = $this->CharacterId;
		$valScenarioLines = $this->ScenarioLines;
		//登録確認
		$strSQL = <<<END_OF_SQL

		SELECT * FROM SW_SCENARIO_LINES
				WHERE SCENARIO_LINES_ID = '$valScenarioLinesId';
END_OF_SQL;

		//登録確認SQL Execute 
		$myResult = $mySqlConnObj->query($strSQL) or die($this->fncAlert($mySqlConnObj->error));

		//件数取得
		$myRowCnt = $myResult->num_rows;
		if($myRowCnt == 0 ){
			echo $this->fncAlert("clsSwScenarioLines ->> NOT ENTRY!");
		}else{
			//UPDATE SQL
			$strSQL = <<<END_OF_SQL

			UPDATE SW_SCENARIO_LINES SET 
				  SCENARIO_ID = '$valScenarioId'
				, SCENE_ID = '$valSceneId'
				, SCENARIO_LINES_ORDER_NO = '$valScenarioLinesOrderNo'
				, SCENARIO_TYPE = '$valScenarioType'
				, CHARACTER_ID = '$valCharacterId'
				, SCENARIO_LINES = '$valScenarioLines'
				WHERE SCENARIO_LINES_ID = '$valScenarioLinesId';
END_OF_SQL;

			//UPDATESQL Execute 
			$myResult = $mySqlConnObj->query($strSQL) or die($this->fncAlert($mySqlConnObj->error));

		}//end if
	}//end function

// -------------------------------------------------------------
//    DB更新　DELETE
// -------------------------------------------------------------
	function clsSwScenarioLinesDbDelete($mySqlConnObj){
		//ﾌﾟﾛﾊﾟﾃｨ取得
		$valScenarioLinesId = $this->ScenarioLinesId;
		//登録確認
		$strSQL = <<<END_OF_SQL

		SELECT * FROM SW_SCENARIO_LINES
				WHERE SCENARIO_LINES_ID = '$valScenarioLinesId';
END_OF_SQL;

		//登録確認SQL Execute 
		$myResult = $mySqlConnObj->query($strSQL) or die($this->fncAlert($mySqlConnObj->error));

		//件数取得
		$myRowCnt = $myResult->num_rows;
		if($myRowCnt == 0 ){
			echo $this->fncAlert("clsSwScenarioLines ->> NOT ENTRY!");
		}else{
			//DELETE SQL
			$strSQL = <<<END_OF_SQL

			DELETE FROM SW_SCENARIO_LINES
				WHERE SCENARIO_LINES_ID = '$valScenarioLinesId';
END_OF_SQL;

			//DELETE SQL Execute 
			$myResult = $mySqlConnObj->query($strSQL) or die($this->fncAlert($mySqlConnObj->error));

		}//end if
	}//end function

	// ------------------------------------------------------------------------------
	// java script alert
	// ------------------------------------------------------------------------------
	function fncAlert($AlertMsg){
		$strAlert = <<<END_OF_TEXT

			<script language="JavaScript">
				bootbox.alert("$AlertMsg",function() {
			});
			</script>
END_OF_TEXT;

		return $strAlert;
	}
	
} // end class
// -----------------------------------------------------------
?>