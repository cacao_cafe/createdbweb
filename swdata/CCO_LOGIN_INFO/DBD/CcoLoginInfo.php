<?php
// -----------------------------------------------------------
//
// Copyright (C) 2016 ScenarioWriterCafe All Rights Reserved.
// 
//     CREATE TABLE CCO_LOGIN_INFO
//
//     createTable_CcoLoginInfo.php
// -----------------------------------------------------------
$Result = fncCreateTable();
echo $Result;
exit;


// -----------------------------------------------------------
//     fncCreateTable()
// -----------------------------------------------------------
function fncCreateTable(){
	//共通定数
	include_once("../../sw_config/swConstant.php");

	//MySQLに接続
	include_once("../include/ConnectMySQL.php");

	//DROP TABLE SQLを編集
	$strSQL = <<<END_OF_SQL

DROP TABLE IF EXISTS CCO_LOGIN_INFO;

END_OF_SQL;

	//DROP TABLE EXECUTE 
	$stmt = $mySqlConnObj->prepare($strSQL);
	$stmt->execute();

	//CREATE TABLE SQLを編集
	$strSQL = <<<END_OF_SQL

CREATE TABLE CCO_LOGIN_INFO(
	  CMSLOGIN_ID                      VARCHAR(32) NOT NULL COMMENT 'CMSログインID'
	, CMSUSER_ID                       VARCHAR(256)  COMMENT 'CMSユーザーID'
	, CMSLOGIN_DATE                    TIMESTAMP  COMMENT 'ログイン日時'
	, PRIMARY KEY (CMSLOGIN_ID)
	)DEFAULT CHARACTER SET 'UTF8'
	COMMENT 'ログイン情報';

END_OF_SQL;

	//CREATE TABLE EXECUTE 
	$stmt = $mySqlConnObj->prepare($strSQL);
	$stmt->execute();

	//UNIQUE INDEX のSQLを編集
	$strSQL = <<<END_OF_SQL

	CREATE UNIQUE INDEX CCO_LOGIN_INFO_KEY_CMSLOGIN_ID ON CCO_LOGIN_INFO(CMSLOGIN_ID);
END_OF_SQL;

	//CREATE INDEX EXECUTE 
	$stmt = $mySqlConnObj->prepare($strSQL);
	$stmt->execute();


	//DB切断
	include_once("../include/DisConnectMySQL.php");
	
	
	return	'CREATE TABLE CCO_LOGIN_INFO Success...';
}//end function
// -----------------------------------------------------------
?>