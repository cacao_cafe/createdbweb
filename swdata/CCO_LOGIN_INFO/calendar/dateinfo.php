<?php
//	--------------------------------------------------------------------------------
//	Copyright (C) 2009 Minamikyushu City Office All Rights Reserved.
//
// 	日付情報クラス
//	--------------------------------------------------------------------------------


class DateInfo
{
	// PHPLib Session用
	var $classname = "DateInfo";
	var $persistent_slots = array("dateTime",
								"dateData",
								"timeData",
								"year",
								"month",
								"day",
								"hour",
								"minute",
								"second",
								"dateNameArray");

	var $dateTime;		// 日時
	var $dateData;		// 日付
	var $timeData;		// 時刻

	var	$year;			// 年
	var $month;			// 月
	var $day;			// 日
	var $hour;			// 時
	var $minute;		// 分
	var $second;		// 秒

	var $dateNameArray;	// 曜日名の配列
	var $holidayName;	// 祝日名


	// コンストラクタ
	function DateInfo()
	{
		// 現在の日付をセットする
		$nowDateArray = $this->getNowDate();
		$this->setYear($nowDateArray[0]);
		$this->setMonth($nowDateArray[1]);
		$this->setDay($nowDateArray[2]);

		$this->dateNameArray = array("日", "月", "火", "水", "木", "金", "土");
	}

	// 祝日名の取得
	function getHolidayName($conn, $dbSqlObj, $dbExecObj)
	{
		$tmpDate = $this->getYear() . "/" . $this->getMonth() . "/" . $this->getDay();
		if ($tmpDate != "//")
		{
			$strSql = $dbSqlObj->selectHolidayAllWithHDateSortedHDate($tmpDate,$tmpDate);
			$result = $dbExecObj->exeSql($conn, $strSql);
			if (!$result)
			{
				echo "can't connect to DB";
				exit;
			}

			if ($dbExecObj->getNumRows($result) == 0)
			{
				$tmpString = "";
			}
			else
			{
				$tmpString = $dbExecObj->getResult($result, 0, "hname");
			}

			$this->holidayName = $tmpString;
		}
		return $this->holidayName;
	}

	// 日時の取得
	function getDateTime()
	{
		return $this->dateTime;
	}

	// 日時の設定
	function setDateTime($dateTime)
	{
		$this->dateTime = $dateTime;

		if ($dateTime != "")
		{
			// 日時を分割して設定
			$splitDateTime = split("[/- :+]", $dateTime, 6);

			$this->setYear($splitDateTime[0]);
			$this->setMonth($splitDateTime[1]);
			$this->setDay($splitDateTime[2]);
			$this->setHour($splitDateTime[3]);
			$this->setMinute($splitDateTime[4]);
			$this->setSecond($splitDateTime[5]);
		}
		else
		{
			$this->setYear("");
			$this->setMonth("");
			$this->setDay("");
			$this->setHour("");
			$this->setMinute("");
			$this->setSecond("");
		}
	}


	// 日付の取得
	function getDateData()
	{
		return $this->dateData;
	}

	// 日付の設定
	function setDateData($dateData)
	{
		$this->dateData = $dateData;

		if ($dateData != "")
		{
			// 日付を分割して設定
			$splitDate = split("[/-]", $dateData, 3);

			$this->setYear($splitDate[0]);
			$this->setMonth($splitDate[1]);
			$this->setDay($splitDate[2]);
		}
		else
		{
			$this->setYear("");
			$this->setMonth("");
			$this->setDay("");
		}
	}


	// 時刻の取得
	function getTimeData()
	{
		return $this->timeData;
	}

	// 時刻の設定
	function setTimeData($timeData)
	{
		$this->timeData = $timeData;

		if ($timeData != "")
		{
			// 時刻を分割して設定
			$splitTime = split(":", $timeData, 2);

			$this->setHour($splitTime[0]);
			$this->setMinute($splitTime[1]);
		}
		else
		{
			$this->setHour("");
			$this->setMinute("");
		}
	}


	// yearの取得
	function getYear()
	{
		return $this->year;
	}

	// yearの設定
	function setYear($year)
	{
		$this->year = $year;
	}

	// monthの取得
	function getMonth()
	{
		return $this->month;
	}

	// monthの設定
	function setMonth($month)
	{
		$this->month = $month;
	}

	// dayの取得
	function getDay()
	{
		return $this->day;
	}

	// dayの設定
	function setDay($day)
	{
		$this->day = $day;
	}

	// hourの取得
	function getHour()
	{
		return $this->hour;
	}

	// hourの設定
	function setHour($hour)
	{
		$this->hour = $hour;
	}

	// minuteの取得
	function getMinute()
	{
		return $this->minute;
	}

	// minuteの設定
	function setMinute($minute)
	{
		$this->minute = $minute;
	}

	// secondの取得
	function getSecond()
	{
		return $this->second;
	}

	// hourの設定
	function setSecond($second)
	{
		$this->second = $second;
	}


	// 現在日付の取得
	function getNowDate()
	{
		$year = date("Y");
		$month = date("n");
		$day = date("j");

		$nowDateArray = array($year, $month, $day);

		return $nowDateArray;
	}

	// 次の年の日付の取得
	function getNextYear()
	{
		$year = $this->getYear();
		$month = $this->getMonth();
		$day = $this->getDay();

		$unixTime = mktime(0, 0, 0, $month, $day, $year + 1);

		$year = date("Y", $unixTime);
		$month = date("n", $unixTime);
		$day = date("j", $unixTime);

		$nextYearArray = array($year, $month, $day);

		return $nextYearArray;
	}

	// 前の年の日付の取得
	function getBeforeYear()
	{
		$year = $this->getYear();
		$month = $this->getMonth();
		$day = $this->getDay();

		$unixTime = mktime(0, 0, 0, $month, $day, $year - 1);

		$year = date("Y", $unixTime);
		$month = date("n", $unixTime);
		$day = date("j", $unixTime);

		$beforeYearArray = array($year, $month, $day);

		return $beforeYearArray;
	}

	// 次の月の日付の取得
	function getNextMonth()
	{
		$year = $this->getYear();
		$month = $this->getMonth();
		$day = $this->getDay();
		$day = 1;

		$unixTime = mktime(0, 0, 0, $month + 1, $day, $year);

		$year = date("Y", $unixTime);
		$month = date("n", $unixTime);
		$day = date("j", $unixTime);

		$nextMonthArray = array($year, $month, $day);

		return $nextMonthArray;
	}

	// 前の月の日付の取得
	function getBeforeMonth()
	{
		$year = $this->getYear();
		$month = $this->getMonth();
		$day = $this->getDay();
		$day = 1;

		$unixTime = mktime(0, 0, 0, $month - 1, $day, $year);

		$year = date("Y", $unixTime);
		$month = date("n", $unixTime);
		$day = date("j", $unixTime);

		$beforeMonthArray = array($year, $month, $day);

		return $beforeMonthArray;
	}


	// 次の週の日付の取得
	function getNextWeek()
	{
		$year = $this->getYear();
		$month = $this->getMonth();
		$day = $this->getDay();

		$unixTime = mktime(0, 0, 0, $month, $day, $year) + 24 * 60 * 60 * 7;

		$year = date("Y", $unixTime);
		$month = date("n", $unixTime);
		$day = date("j", $unixTime);

		$nextWeekArray = array($year, $month, $day);
		
		return $nextWeekArray;
	}

	// 前の週の日付の取得
	function getBeforeWeek()
	{
		$year = $this->getYear();
		$month = $this->getMonth();
		$day = $this->getDay();

		$unixTime = mktime(0, 0, 0, $month, $day, $year) - 24 * 60 * 60 * 7;

		$year = date("Y", $unixTime);
		$month = date("n", $unixTime);
		$day = date("j", $unixTime);

		$beforeWeekArray = array($year, $month, $day);
		
		return $beforeWeekArray;
	}


	// 次の日の日付の取得
	function getNextDay()
	{
		$year = $this->getYear();
		$month = $this->getMonth();
		$day = $this->getDay();

		$unixTime = mktime(0, 0, 0, $month, $day, $year) + 24 * 60 * 60;

		$year = date("Y", $unixTime);
		$month = date("n", $unixTime);
		$day = date("j", $unixTime);

		$nextDayArray = array($year, $month, $day);
		
		return $nextDayArray;
	}

	// 前の日の日付の取得
	function getBeforeDay()
	{
		$year = $this->getYear();
		$month = $this->getMonth();
		$day = $this->getDay();

		$unixTime = mktime(0, 0, 0, $month, $day, $year) - 24 * 60 * 60;

		$year = date("Y", $unixTime);
		$month = date("n", $unixTime);
		$day = date("j", $unixTime);

		$beforeDayArray = array($year, $month, $day);
		
		return $beforeDayArray;
	}


	// 週の最初の日付の取得
	function getFirstWeek()
	{
		$year = $this->getYear();
		$month = $this->getMonth();
		$day = $this->getDay();

		$baseTime = mktime(0, 0, 0, $month, $day, $year);
		$firstDate = $baseTime - date("w", $baseTime) * 60 * 60 * 24;

		$year = date("Y", $firstDate);
		$month = date("n", $firstDate);
		$day = date("j", $firstDate);

		$firstWeekArray = array($year, $month, $day);
		
		return $firstWeekArray;

	}

	// 週の最後の日付の取得
	function getLastWeek()
	{
		$year = $this->getYear();
		$month = $this->getMonth();
		$day = $this->getDay();

		$baseTime = mktime(0, 0, 0, $month, $day, $year);
		$lastDate = $baseTime + ((6 - date("w", $baseTime)) * 60 * 60 * 24);

		$year = date("Y", $lastDate);
		$month = date("n", $lastDate);
		$day = date("j", $lastDate);

		$lastWeekArray = array($year, $month, $day);
		
		return $lastWeekArray;

	}


	// 月の最終日の取得
	function getLastMonth()
	{
		$year = $this->getYear();
		$month = $this->getMonth();
		$day = 1;

		while (checkdate($month, $day, $year)) 
		{
			$day++;
		}
		$day--;

		$lastMonthArray = array($year, $month, $day);
		
		return $lastMonthArray;
	}


	// 曜日名の取得
	function getDateName()
	{
		$baseTime = mktime(0, 0, 0, $this->month, $this->day, $this->year);
		$dateNumber = date("w", $baseTime);

		return $this->dateNameArray[$dateNumber];
	}


	// 曜日の番号の取得
	// 0 = 日 … 6 = 土
	function getDateNumber()
	{
		$baseTime = mktime(0, 0, 0, $this->month, $this->day, $this->year);
		$dateNumber = date("w", $baseTime);

		return $dateNumber;
	}


	// Y-mの取得
	function getYM()
	{
		$month = sprintf("%02d", $this->getMonth());

		return $this->getYear() . "-" . $month;
	}

	// Y-m-dの取得
	function getYMD()
	{
		$month = sprintf("%02d", $this->getMonth());
		$day = sprintf("%02d", $this->getDay());

		return $this->getYear() . "-" . $month . "-" . $day;
	}

	// m/dの取得
	function getMD()
	{
		$month = sprintf("%d", $this->getMonth());
		$day = sprintf("%d", $this->getDay());

		return $month . "/" . $day;
	}

	// 日を移動させる
	// +XでX日進む
	// -XでX日戻る
	function moveDay($days)
	{
		$year = $this->getYear();
		$month = $this->getMonth();
		$day = $this->getDay();

		$unixTime = mktime(0, 0, 0, $month, $day, $year) + 24 * 60 * 60 * $days;

		$this->setYear(date("Y", $unixTime));
		$this->setMonth(date("n", $unixTime));
		$this->setDay(date("j", $unixTime));
	}


	// 二つの年月日から、その間の日数を求める
	function getDayDifference($fromYear, $fromMonth, $fromDay, $toYear, $toMonth, $toDay)
	{
		// 最初の年と最後の年が同じ時は除く
		if ($fromYear != $toYear)
		{
			// 最初の年はうるう年？
			$isLeap = date("L", mktime(0, 0, 0, 1, 1, $fromYear));

			// 最初の年の年間の積算日
			$daysSum =date("z", mktime(0, 0, 0, $fromMonth, $fromDay, $toYear));

			//うるう年だったら、366日から年間の積算日を引く、うるう年でなければ365日から引く
			if ($isLeap == 1)
			{
				$dayDifference = 366 - $daysSum;
			}
			else
			{
				$dayDifference = 365 - $daysSum;
			}

			//2年目から最後の年の前年までは、365か366日を足し算していく
			for ($i = ($fromYear + 1); $i <= ($toYear - 1) ; $i++)
			{
				if (date("L", mktime(0, 0, 0, 1, 1, $i)) == 0) 
				{
					$dayDifference += 365;
				}
				else
				{
					$dayDifference += 366;
				} 
			}

			//最後の年の年間積算日を足す
			$dayDifference += date("z", mktime(0, 0, 0, $toMonth, $toDay, $toYear));
		}
		//最初の年と最後の年が同じ時の場合、今年の積算日の差を$dayDifferenceにする
		else 
		{
			$dayDifference = date("z", mktime(0, 0, 0, $toMonth, $toDay, $toYear)) - date("z", mktime(0, 0, 0, $fromMonth, $fromDay, $fromYear));
		}

		return $dayDifference;
	}

}

?>
