<?php
// ------------------------------------------------------------------------------
//
// Copyright (C) 2015 dbu@mac.com All Rights Reserved.
//
//          共通関数
//
//          swFunc.php
//
// ------------------------------------------------------------------------------


//------------------------------------------------------------------------------
//	$NewPassword = swFunc_MakePassword()
//	仮のパスワード生成
// ------------------------------------------------------------------------------
function swFunc_MakeNewPassword(){
	//仮のﾊﾟｽﾜｰﾄﾞ文字列設定
	return substr(str_shuffle('1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'), 0, 6);
}
// ------------------------------------------------------------------------------
//	swFunc_Hash($str,$pwd)
//	ハッシュ化 
// ------------------------------------------------------------------------------
function swFunc_Hash($str,$pwd){
	//$strをhash256でハッシュ化
	$hash = hash('sha256', $str);
	
	//ハッシュ化した文字列に、$pwdを追加する
	$hash = $pwd.$hash.$pwd;
	
	//再度hash256でハッシュ化
	$hash = hash('sha256', $hash);
	
	return	$hash;
}
// ------------------------------------------------------------------------------
//	swFunc_MakeRandStr($length)
//	ランダム文字列生成 (英数字) 
// ------------------------------------------------------------------------------
function swFunc_MakeRandStr($length) {
	$str = array_merge(range('a', 'z'), range('0', '9'), range('A', 'Z"'));
	$retStr = '';
	for ($i = 0; $i < $length; $i++) {
		$retStr .= $str[rand(0, count($str)-1)];	
	}		
	return $retStr;
}
// ------------------------------------------------------------------------------
//	swFunc_SanitizeStrings
//	ｻﾆﾀｲｽﾞ
// ------------------------------------------------------------------------------
function swFunc_SanitizeStrings($str){
	
	$SanitizeStr = htmlspecialchars($str, ENT_QUOTES | ENT_SUBSTITUTE, 'UTF-8');
	return	$SanitizeStr;
}
// ------------------------------------------------------------------------------
//	swFunc_GetPostData($index)
//	POSTﾃﾞｰﾀから内容を取得
// ------------------------------------------------------------------------------
function swFunc_GetPostData($index){
	$postData = '';
	if (isset($_POST[$index])) {
		//ｻﾆﾀｲｽﾞは，HTML出力時にhtmlspecialchars化する
		$postData = $_POST[$index];
	}else{
		$postData = '';
	}
	return	$postData;
}
// ------------------------------------------------------------------------------
//	swFunc_GetSessionData($index)
//	POSTﾃﾞｰﾀから内容を取得
// ------------------------------------------------------------------------------
function swFunc_GetSessionData($index){
	$sessionData = '';
	if (isset($_SESSION[$index])) {
		$sessionData = $_SESSION[$index];
	}else{
		$sessionData = '';
	}
	return	$sessionData;
}

// ------------------------------------------------------------------------------
// YES/NOのCSVﾃﾞｰﾀ を 取得する
//		$csvArray = swFunc_MakeYesNoCsv()
// ------------------------------------------------------------------------------
function swFunc_MakeYesNoCsv(){
	$csvArray = array();
	array_push($csvArray,"0:はい");
	array_push($csvArray,"1:いいえ");
	return	$csvArray;
}
//--------------------------------------------------------------------------------
//	CSVからマッチする項目を返す
//--------------------------------------------------------------------------------
function swFunc_GetArrayItem($Code,$csvArray){
	foreach ($csvArray as $tmp) {
    	$wArray = explode(':',$tmp);
    	$wCode = $wArray[0];
    	$wCodeMei = $wArray[1];
    	
		if ($wCode == $Code) {
			$wCodeMei = str_replace('---','',$wCodeMei);
			return $wCodeMei;
		}
	}
}
// ------------------------------------------------------------------------------
// 月のCSVﾃﾞｰﾀ を 取得する
//		$csvArray = swFunc_MakeMmCsv()
// ------------------------------------------------------------------------------
function swFunc_MakeMmCsv(){
	$csvArray = array();
	for( $i = 1 ; $i <= 12 ;$i++ ){
		$mm_value = $i;
		array_push($csvArray,$mm_value.":".$i."月");
	}
	return	$csvArray;
}
// ------------------------------------------------------------------------------
// CSVﾃﾞｰﾀ sample
// swFunc_MakeSelectItemsCsvSample
// ------------------------------------------------------------------------------
function swFunc_MakeSelectItemsCsvSample(){
	
	$csvArray = array();
	array_push($csvArray,"-1:選択");
	array_push($csvArray,"1:SELECT 1");
	array_push($csvArray,"2:SELECT 2");
	array_push($csvArray,"3:SELECT 3");
	array_push($csvArray,"5:SELECT 5");
	array_push($csvArray,"7:SELECT 7");
	array_push($csvArray,"9:SELECT 9");
	
	return	$csvArray;
}
// ------------------------------------------------------------------------------
// CSVﾃﾞｰﾀからselect box を作成する Bootstrap3
//		$ObjName		: select box の名称
//		$csvArray	: CSVﾃﾞｰﾀ
//		$default	: ﾃﾞﾌｫﾙﾄ値
//		$onChange	: onChange で起動する java script関数名
//		$ViewCode	: ｺｰﾄﾞを表示する
//		$retHTML = swFunc_MakeSelectBox($ObjName,$csvArray,$default,$onChange,$ViewCode)
// ------------------------------------------------------------------------------
function swFunc_MakeSelectBox($ObjName,$csvArray,$default,$onChange,$ViewCode){
	// onChangeを使わない場合
	if ( $onChange == ''){
		$retHTML = "<select class=\"form-control input-sm\"";
		$retHTML .= " name=\"$ObjName\" id=\"$ObjName\"";
		$retHTML .= ">\n";
	}else{
		$retHTML = "<select class=\"form-control input-sm\"";
		$retHTML .= " name=\"$ObjName\" id=\"$ObjName\"";
		$retHTML .= " onChange=\"$onChange\"";
		$retHTML .= ">\n";
	}
	//CSVの要素を取り出す
	$retOption = "";
	//1件以下はselectedにする
	if ( count($csvArray) <= 1 ){
		$i = 0;
		//valueとitemに分ける
		$arr = explode(":",$csvArray[$i]);
		//Option生成
		//ｺｰﾄﾞを表示するか
		if ( $ViewCode == TRUE ){
			$csvArray[$i] = str_replace('-1:','',$csvArray[$i]);
			$retOption .= "<option value=\"" .$arr[0]."\" selected>".$csvArray[$i]."</option>\n";
		}else{
			if(isset($arr[1])){
				$retOption .= "<option value=\"" .$arr[0]."\" selected>".$arr[1]."</option>\n";
			}
		}
	}else{
		//ﾏｯﾁしたかをﾁｪｯｸする
		$retOption = "";
		$matchCode = FALSE;
		for($i = 0; $i < count($csvArray); $i++){
			//valueとitemに分ける
			$arr = explode(":",$csvArray[$i]);
			//Option生成
			//ｺｰﾄﾞを表示するか
			if ( $ViewCode == TRUE ){
				$csvArray[$i] = str_replace('-1:','',$csvArray[$i]);
				
				if ($default == $arr[0]) {
					$matchCode = TRUE;
					$retOption .= "<option value=\"" .$arr[0]."\" selected>".$csvArray[$i]."</option>\n";
				}
				else {
					$retOption .= "<option value=\"" .$arr[0]."\">".$csvArray[$i]."</option>\n";
				}
			}else{
				if ($default == $arr[0]) {
					$matchCode = TRUE;
					$retOption .= "<option value=\"" .$arr[0]."\" selected>".$arr[1]."</option>\n";
				}
				else {
					$retOption .= "<option value=\"" .$arr[0]."\">".$arr[1]."</option>\n";
				}
			}
	    }
	    //ﾏｯﾁしていない場合，最初の要素をselectedにする
	    if ( !$matchCode ){
	    	$retOption = "";
			for($i = 0; $i < count($csvArray); $i++){
				//valueとitemに分ける
				$arr = explode(":",$csvArray[$i]);
				//Option生成
				if ( $ViewCode == TRUE ){
					if ($i == 0) {
						$retOption .= "<option value=\"" .$arr[0]."\" selected>".$csvArray[$i]."</option>\n";
					}
					else {
						$retOption .= "<option value=\"" .$arr[0]."\">".$csvArray[$i]."</option>\n";
					}
				}else{
					if ($i == 0) {
						$retOption .= "<option value=\"" .$arr[0]."\" selected>".$arr[1]."</option>\n";
					}
					else {
						$retOption .= "<option value=\"" .$arr[0]."\">".$arr[1]."</option>\n";
					}
				}
		    }
	    }
    }
	$retHTML .= $retOption."</select>\n";
	
	return	$retHTML;
}

// ------------------------------------------------------------------------------
// 現在の日付を取得する
//		$currentDate = swFunc_GetCurrentDate();
// ------------------------------------------------------------------------------
function swFunc_GetCurrentDate(){
	return date("Y\/m\/d");
}
// ------------------------------------------------------------------------------
//		現在の日付を取得する
// ------------------------------------------------------------------------------
function swFunc_GetNowDate(){
	//現在の日付を取得
	$today = getdate();
	$THIS_YEAR = sprintf( "%04d",$today["year"]);
	$THIS_MON = sprintf( "%02d",$today["mon"]);
	$THIS_DAY = sprintf( "%02d",$today["mday"]);
	
	return $THIS_YEAR.'/'.$THIS_MON.'/'.$THIS_DAY;
}
// ------------------------------------------------------------------------------
// 現在の年度を取得する
//		$currentNendo = swFunc_GetCurrentNendo($mySqlConnObj,$EmpCd);
// ------------------------------------------------------------------------------
function swFunc_GetCurrentNendo($mySqlConnObj,$EmpCd,$CompanyCd='-1'){
	$today_date = date("Y\/m\/d");
	
	// 年度取得
	if (substr($today_date,5,2) < 4){
		$currentNendo = date("Y") - 1;
	}else {
		$currentNendo = date("Y");
	}
	return $currentNendo;
}
// ------------------------------------------------------------------------------
// ｶﾚﾝﾀﾞｰ付日付選択を作成
//		$mySqlConnObj	: SQLｸﾗｽｵﾌﾞｼﾞｪｸﾄ
//		$ObjName		: Select Name
//		$default 	: 日付ﾃﾞｰﾀ ( YYYY/MM/DD )
//		$mode		: 初期値を現在にする
//		$retHtml = swFunc_MakeCalendarBox($mySqlConnObj,$ObjName,$default,$mode,$onChange)
// ------------------------------------------------------------------------------
function swFunc_MakeCalendarBox($mySqlConnObj,$formName,$ObjName,$defaultDate,$mode,$onChange){
	$YearBoxNmae = $ObjName.'YY';
	$MonthBoxNmae = $ObjName.'MM';
	$DayBoxNmae = $ObjName.'DD';
	
	$retHtml = '';
	//年
	$retHtml .= swFunc_MakeYearSelectBox($mySqlConnObj,$YearBoxNmae,$defaultDate,$mode,$onChange);
	//月
	$retHtml .= swFunc_MakeMonthSelectBox($MonthBoxNmae,$defaultDate,$mode,$onChange);
	//日
	$retHtml .= swFunc_MakeDaySelectBox($DayBoxNmae,$defaultDate,$mode,$onChange);
	//ｶﾚﾝﾀﾞｰ
	if($defaultDate == ''){$defaultDate = '///';}
	if($defaultDate == ''){$defaultDate = '---';}
	$cDate = explode("/",$defaultDate);
	if(!isset($cDate[1])){
		$cDate = explode("-",$defaultDate);
		if(!isset($cDate[1])){
			return $retHtml;
		}
	}
	$retHtml .= <<<END_OF_HTML
		        	<img src="./calendar/images/calender_ico.gif" 
		        		alt="カレンダーから指定" 
		        		width="19" height="19" border="0" align="middle"
		        		style="cursor: pointer;"
		        		onClick="window.open('./calendar/CalendarView.php?viewMonth=2&frmName=$formName&yearName=$YearBoxNmae&monthName=$MonthBoxNmae&dayName=$DayBoxNmae&year=$cDate[0]&month=$cDate[1]&day=$cDate[2]', 'CAL', 'width=660,height=250')"
		        	>
END_OF_HTML;

	return $retHtml;
	
}
// ------------------------------------------------------------------------------
//		西暦 → 和暦
//
//		変換モード
//		$hMode:0 --> 年月日変換
//		$hMode:1 --> 年変換
//
//		元号表現
//		$gMode:0 --> 平成
//		$gMode:1 --> 平
//		$gMode:2 --> H
//		$gMode:3 --> 4
//
//		前ゼロ
//		$zMode:0 --> 前ゼロなし
//		$zMode:1 --> 前ゼロあり
//
//		区切り文字
//		$sep:0 --> 年月日
//		$sep:1 --> ///
//		$sep:2 --> ...
//
//		$wDate = swFunc_SWHenkan($sDate,$hMode,$gMode,$nMode,$sMode)
// ------------------------------------------------------------------------------
function swFunc_SWHenkan($sDate,$hMode='0',$gMode='0',$zMode='0',$sep='0'){
	//$sDateを指定しない場合はtoday
	if($sDate == '' or strpos($sDate,'/') ===false ){
		$sDate = swFunc_GetNowDate();
	}
	//西暦を分解
    list($year, $month, $day) = explode('/', $sDate);
    //西暦ﾁｪｯｸ
    if (!@checkdate($month, $day, $year) or $year < 1869 or strlen($year) !== 4
            or strlen($month) !== 2 or strlen($day) !== 2){
       return '';
	}
	//日付ｾﾊﾟﾚｰﾀを削除
    $sDate = str_replace('/', '', $sDate);
    
	//元号を判定
	$gengo = '0';
    if ($sDate >= 19890108) {
        $gengo = '4';
        $wYear = $year - 1988;
    } elseif ($sDate >= 19261225) {
        $gengo = '3';
        $wYear = $year - 1925;
    } elseif ($sDate >= 19120730) {
        $gengo = '2';
        $wYear = $year - 1911;
    } else {
        $gengo = '1';
        $wYear = $year - 1868;
    }
    //元号配列
    switch ($gMode){
    	case 0:
    		$gArray = array();
			array_push($gArray,"---");
			array_push($gArray,"明治");
			array_push($gArray,"大正");
			array_push($gArray,"昭和");
    		array_push($gArray,"平成");
            break;
    	case 1:
    		$gArray = array();
			array_push($gArray,"---");
			array_push($gArray,"明");
			array_push($gArray,"大");
			array_push($gArray,"昭");
    		array_push($gArray,"平");
            break;
    	case 2:
    		$gArray = array();
			array_push($gArray,"---");
			array_push($gArray,"M");
			array_push($gArray,"T");
			array_push($gArray,"S");
    		array_push($gArray,"H");
            break;
        default:
    		$gArray = array();
			array_push($gArray,"---");
			array_push($gArray,"1");
			array_push($gArray,"2");
			array_push($gArray,"3");
    		array_push($gArray,"4");
	}
	//配列から元号を取得
	if(isset($gArray[$gengo])){
		$wGengo = $gArray[$gengo];
	}else{
		$wGengo = '??';
	}
	//前ゼロ
	if($zMode == '0'){
		$wYear = LTRIM($wYear,'0');
		$month = LTRIM($month,'0');
		$day = LTRIM($day,'0');
		if($wYear == '1'){
			$wYear = '元';
		}
	}
	//変換ﾓｰﾄﾞ(年ﾓｰﾄﾞ）
	if($hMode == '1'){
		switch ($sep){
			case 0:
				$wDate = $wGengo.$wYear.'年';
            	break;
        	default:
            	$wDate = $wGengo.$wYear;
    	}
		return	$wDate;
	}
//		$sep:0 --> 年月日
//		$sep:1 --> ///
//		$sep:2 --> ...
	switch ($sep){
		case 1://$sep:1 --> ///
			$wDate = $wGengo.$wYear.'/'.$month.'/'.$day;
        	break;
		case 2://$sep:2 --> ...
			$wDate = $wGengo.$wYear.'.'.$month.'.'.$day;
        	break;
    	default://$sep:0 --> 年月日
        	$wDate = $wGengo.$wYear.'年'.$month.'月'.$day.'日';
	}
    return $wDate;
}

// ------------------------------------------------------------------------------
//		年フィールド 西暦 → 和暦
//		$date (yyyy/mm/dd)
//		$mode TRUE -> 現在の日付をｾｯﾄする
//      $retHtml = swFunc_MakeYearSelectBox($mySqlConnObj,$ObjName,$date,$mode,$onChange)
// ------------------------------------------------------------------------------
function swFunc_MakeYearSelectBox($mySqlConnObj,$ObjName,$date,$mode,$onChange){
	$ymd = swFunc_SplitDate($date);
	$date = sprintf('%04d/%02d/%02d', $ymd[0], $ymd[1], $ymd[2]);
	//現在の日付を取得
	$today = getdate();
	$THIS_YEAR = sprintf( "%04d",$today['year']);
	$tgtYEAR = sprintf( "%04d",substr($date,0,4));
	if ($mode == TRUE){
		if ($tgtYEAR == '0000'){$tgtYEAR = $THIS_YEAR;}
	}
	// 和暦CSV を編集
	$stYear = $THIS_YEAR - _STNEN_;
	$edYear = $THIS_YEAR + _EDNEN_;
	$csvArray = array();
	array_push($csvArray,"0000:----");
	for( $i = $stYear ; $i <= $edYear ;$i++ ){
		//西暦-->和暦
		$seireki = $i.'/12/31';
		$wareki = swFunc_SWHenkan($seireki,'1','2','0','2');
		array_push($csvArray,$i.":".$wareki);
	}

	// CSVﾃﾞｰﾀからselect box を作成する
	$retHtml = swFunc_MakeSelectBox($ObjName,$csvArray,$tgtYEAR,65,$onChange,FALSE);
	
	$retHtml .= <<<END_OF_HTML
	<span class="fBlack10100">年</span>
END_OF_HTML;
	return $retHtml;
	
}
// ------------------------------------------------------------------------------
//		月フィールド
//		$date (yyyy/mm/dd)
//		$mode TRUE -> 現在の日付をｾｯﾄする
//      $retHtml = swFunc_MakeMonthSelectBox($ObjName,$date,$mode,$onChange)
// ------------------------------------------------------------------------------
function swFunc_MakeMonthSelectBox($ObjName,$date,$mode,$onChange){
	$ymd = swFunc_SplitDate($date);
	$date = sprintf('%04d/%02d/%02d', $ymd[0], $ymd[1], $ymd[2]);
	//現在の日付を取得
	$today = getdate();
	$THIS_MON = sprintf( "%02d",$today['mon']);
	$tgtMON = sprintf( "%02d",substr($date,5,2));
	if ($mode == TRUE){
		if ($tgtMON == '00'){$tgtMON = $THIS_MON;}
	}
	$csvArray = array();
	array_push($csvArray,"00:--");
	for( $i = 1 ; $i <= 12 ;$i++ ){
		//$mm_value = sprintf( "%02d",$i);
		$mm_value = $i;
		array_push($csvArray,$mm_value.":".$i);
	}
	// CSVﾃﾞｰﾀからselect box を作成する
	$retHtml = swFunc_MakeSelectBox($ObjName,$csvArray,$tgtMON,40,$onChange,FALSE);
	
	$retHtml .= <<<END_OF_HTML
	<span class="fBlack10100">月</span>
END_OF_HTML;
	return $retHtml;
}
// ------------------------------------------------------------------------------
//		日フィールド
//		$mode TRUE -> 現在の日付をｾｯﾄする
//      $retHtml = swFunc_MakeDaySelectBox($ObjName,$date,$mode,$onChange)
// ------------------------------------------------------------------------------
function swFunc_MakeDaySelectBox($ObjName,$date,$mode,$onChange){
	$ymd = swFunc_SplitDate($date);
	$date = sprintf('%04d/%02d/%02d', $ymd[0], $ymd[1], $ymd[2]);
	//現在の日付を取得
	$today = getdate();
	$THIS_DAY = sprintf( "%02d",$today['mday']);
	$tgtDAY = sprintf( "%02d",substr($date,8,2));
	if ($mode == TRUE){
		if ($tgtDAY == '00'){$tgtDAY = $THIS_DAY;}
	}
	$csvArray = array();
	array_push($csvArray,"00:--");
	for( $i = 1 ; $i <= 31 ;$i++ ){
		//$dd_value = sprintf( "%02d",$i);
		$dd_value = $i;
		array_push($csvArray,$dd_value.":".$i);
	}
	// CSVﾃﾞｰﾀからselect box を作成する
	$retHtml = swFunc_MakeSelectBox($ObjName,$csvArray,$tgtDAY,40,$onChange,FALSE);
	$retHtml .= <<<END_OF_HTML
	<span class="fBlack10100">日</span>
END_OF_HTML;
	return $retHtml;
}

//--------------------------------------------------------------------------------
//	date を 年，月，日に分割
//	$SpritYmd = swFunc_SplitDate($date)
//--------------------------------------------------------------------------------
function swFunc_SplitDate($date){
	if( $date == ''){
		$date = '///';
	}
	if ( mb_strpos($date,'-') > 0 ){
		$SpritYmd = explode("-",$date);
	}else{
		$SpritYmd = explode("/",$date);
	}
	return	$SpritYmd;
}
// ------------------------------------------------------------------------------
// Resultを編集してCSV配列を作る
//		$retArray = swFunc_ResultToCsvArray($mySqlConnObj,$strSQL,$NoneCd,$msg);
// ------------------------------------------------------------------------------
function swFunc_ResultToCsvArray($mySqlConnObj,$strSQL,$NoneCd,$msg){
	//SQL実行
    $myResult = $mySqlConnObj->query($strSQL) or die($mySqlConnObj->error);
    //件数取得
    $myRowCnt = $myResult->num_rows;
	// 0 件のときの制御
	if ( $NoneCd == FALSE){
		//制御をしない場合
		if ( $myRowCnt == 0 ){
			//0件なら ---- を表示
			$csvArray = array("-1:----");
		}else{
			//特定ﾒｯｾｰｼﾞがないときは配列を初期化
			if ( $msg == ''){
				$csvArray = array();
			}else{
				$csvArray = array("-1:$msg");
			}
		}
	}else{
		//制御をする場合
		if ( $msg == ''){
			//特定ﾒｯｾｰｼﾞがないとき
			if ( $myRowCnt == 0 ){
				//0件なら ---- を表示
				$csvArray = array("-1:----");
			}else{
				//0件以上なら 選択 を表示
				$csvArray = array("-1:選択");
			}
		}else{
			//特定ﾒｯｾｰｼﾞがあるとき
			$csvArray = array("-1:$msg");
		}
	}
	//特定ﾒｯｾｰｼﾞがNONEのとき配列を初期化
	if ( $msg == 'NONE'){
		$csvArray = array();
	}
	
	$brakeItem = '';
	while($myRow = $myResult->fetch_assoc( )){
		$CSVITEM = $myRow["CSVITEM"];
		if ( $CSVITEM == $brakeItem){
		}else{
			array_push($csvArray,"$CSVITEM");
			$brakeItem = $CSVITEM;
		}
	}
	return $csvArray;
}
// ------------------------------------------------------------------------------
// JAVA SCRIPT でダイアログを表示する
// ------------------------------------------------------------------------------
function swFunc_Alert($AlertMsg){
	print <<<END_OF_HTML
		<Script language="JavaScript">
			alert("$AlertMsg");
		</Script>
END_OF_HTML;
}
// ------------------------------------------------------------------------------
// Debug Print
// ------------------------------------------------------------------------------
function swFunc_DebugPrint($item){
	echo "Debug = $item<br>";
	
}
// ------------------------------------------------------------------------------
// Debug Exit
// ------------------------------------------------------------------------------
function swFunc_DebugExit($item){
	echo "Debug = $item<br>";
	exit;
	
}
// ------------------------------------------------------------------------------
// 使用する/使用しないのCSVﾃﾞｰﾀ を 取得する
//		$csvArray = swFunc_MakeUseOrNotCsv()
// ------------------------------------------------------------------------------
function swFunc_MakeUseOrNotCsv(){
	$csvArray = array();
	array_push($csvArray,"-1:選択してください");
	array_push($csvArray,"0:使用しない");
	array_push($csvArray,"1:使用する");
	return	$csvArray;
}
// ------------------------------------------------------------------------------
// 数字編集
//		$ret = swFunc_NumberFormat($Num)
// ------------------------------------------------------------------------------
function swFunc_NumberFormat($Num){
	if ( $Num == '0' ){
		return	'';
	}else{
		return	number_format($Num);
	}
}
// ------------------------------------------------------------------------------
// 西暦-> 和暦
//		$date(yyyy/mm/dd) --> 年号 xx 年 xx 月 xx 日
//	変換ﾊﾟﾀｰﾝ(012345)
//		0= K	漢字区切り 　　　年　　月　　日
//		0= S	/ 区切り 　　　/　　/　　/
//		0= P	. 区切り 　　　.　　.　　.
//		1= W	和暦
//		1= w	和暦 1文字
//		1= g	MTSH 1文字
//		2= y	和暦年　前ゼロあり
//		2= Y	和暦年　前ゼロなし
//		3= m	和暦月　前ゼロあり
//		3= M	和暦月　前ゼロなし
//		4= d	和暦日　前ゼロあり
//		4= D	和暦日　前ゼロなし
//		5= S	数字と区切りに空白なし
// ------------------------------------------------------------------------------
function swFunc_S2WDate($date,$pattern='KWYMD'){
	$PatArr = array(substr($pattern,0,1)
					,substr($pattern,1,1)
					,substr($pattern,2,1)
					,substr($pattern,3,1)
					,substr($pattern,4,1)
					,substr($pattern,5,1));
	$SepPat = $PatArr[0].$PatArr[5];
	if ( $date == NULL ){
		return "";
	}
	//日付分割
	$sDate = "$date";
	$tgtDate = swFunc_explodeDate($sDate);
	
	//元号配列
	//		1= W	和暦
	//		1= w	和暦 1文字
	//		1= g	MTSH 1文字
	switch($PatArr[1]){
		case 'W':
			$GENGO = array('明治 ','大正 ','昭和 ','平成 ');
			break;
		case 'w':
			$GENGO = array('明','大','昭','平');
			break;
		case 'g':
			$GENGO = array('M','T','S','H');
			break;
		default:
			$GENGO = array('明治','大正','昭和','平成');
			break;
	}
	
    $year = $tgtDate[0];
    if ($sDate <= "19120729") {
        $Gengo = $GENGO[0];
        $wareki = $year - 1867;
    } else if ($sDate >= "19120730" && $sDate <= "19261224") {
        $Gengo = $GENGO[1];
        $wareki = $year - 1911;
    } else if ($sDate >= "19261225" && $sDate <= "19890107") {
        $Gengo = $GENGO[2];
        $wareki = $year - 1925;
    } else if ($sDate >= "19890108") {
        $Gengo = $GENGO[3];
        $wareki = $year - 1988;
    }
	//区切り文字
	//		0= K	漢字区切り 　　　年　　月　　日
	//		0= S	/ 区切り 　　　/　　/　　/
	//		0= P	. 区切り 　　　.　　.　　.
	//		5= S	数字と区切りに空白なし
	$SepPat = $PatArr[0].$PatArr[5];
	switch($SepPat){
		case 'KS':
			$STR_SEP = array(' 年 ','月 ','日');
			break;
		case 'SS':
			$STR_SEP = array(' /',' /','');
			break;
		case 'PS':
			$STR_SEP = array(' .',' .','');
			break;
		case 'K':
			$STR_SEP = array('年','月','日');
			break;
		case 'S':
			$STR_SEP = array('/','/','');
			break;
		case 'P':
			$STR_SEP = array('.','.','');
			break;
		default:
			$STR_SEP = array(' 年',' 月',' 日');
			break;
	}
	//		2= y	和暦年　前ゼロあり
	//		2= Y	和暦年　前ゼロなし
	switch($PatArr[2]){
		case 'Y':
			$WYear = LTRIM($wareki,'0');
			break;
		case 'y':
			$WYear = $wareki;
			break;
		default:
			$WYear = LTRIM($wareki,'0');
			break;
	}
	//		3= m	和暦月　前ゼロあり
	//		3= M	和暦月　前ゼロなし
	switch($PatArr[3]){
		case 'M':
			$Month = LTRIM($tgtDate[1],'0');
			break;
		case 'm':
			$Month = $tgtDate[1];
			break;
		default:
			$Month = LTRIM($tgtDate[1],'0');
			break;
	}
	//		4= d	和暦日　前ゼロあり
	//		4= D	和暦日　前ゼロなし
	switch($PatArr[4]){
		case 'D':
			$Day = LTRIM($tgtDate[2],'0');
			break;
		case 'd':
			$Day = $tgtDate[2];
			break;
		default:
			$Day = LTRIM($tgtDate[2],'0');
			break;
	}
	//文字列連結(前ゼロなし）
	$retWDate =  $Gengo.$WYear.$STR_SEP[0].$Month.$STR_SEP[1].$Day.$STR_SEP[2];
	return $retWDate;
}
//--------------------------------------------------------------------------------
//	date を 年，月，日に分割
//	$SpritYmd = swFunc_explodeDate($date)
//--------------------------------------------------------------------------------
function swFunc_explodeDate($date){
	if ( mb_strpos($date,'-') > 0 ){
		$SpritYmd = explode("-",$date);
	}else{
		$SpritYmd = explode("/",$date);
	}
	return	$SpritYmd;
}
// ------------------------------------------------------------------------------
// 年月日と加算日からn日後、n日前を求める関数
// $year 年
// $month 月
// $day 日
// $addDays 加算日。マイナス指定でn日前も設定可能
// ------------------------------------------------------------------------------
function swFunc_ComputeDate($year, $month, $day, $addDays){
    $baseSec = mktime(0, 0, 0, $month, $day, $year);//基準日を秒で取得
    $addSec = $addDays * 86400;//日数×１日の秒数
    $targetSec = $baseSec + $addSec;
    
    return date("Y/m/d", $targetSec);
}
// ------------------------------------------------------------------------------
// $start_ymdから$end_ymdの日数を求める関数
//	$diffDay = swFunc_GetDays($start_ymd,$end_ymd)
// 		$start_ymd	(yyyy/mm/dd) 
// 		$end_ymd	(yyyy/mm/dd)
// ------------------------------------------------------------------------------
function swFunc_GetDays($start_ymd,$end_ymd){
	if ($start_ymd == ''){return 0;}
	if ($end_ymd == ''){return 0;}
	$diffDay = swFunc_GetDiffDays($start_ymd,$end_ymd);
	$Days = $diffDay + 1;
	
	return $Days;
}
//------------------------------------------------------------------------
// 任意の年月の第n 曜日の日付を求める関数
// 		$year 年
// 		$month 月
// 		$number 何番目の曜日か、第1曜日なら1。第3曜日なら3
// 		$dayOfWeek 求めたい曜日。0-6までの数字で曜日の日～土を指定する
//------------------------------------------------------------------------
function swFunc_GetWhatDayOfWeek($year, $month, $number, $dayOfWeek) {
	//指定した年月の1日の曜日を取得
    $firstDayOfWeek = date("w", mktime(0, 0, 0, $month, 1, $year));
    $day = $dayOfWeek - $firstDayOfWeek + 1;
    //1週間を足す
    if($day <= 0) $day += 7;
    $dt = mktime(0, 0, 0, $month, $day, $year);
    //n曜日まで1週間を足し込み
    $dt += (86400 * 7 * ($number - 1));
    
    $date = date("Y/m/d", $dt);
    $ymd = swFunc_SplitDate($date);
    
    $retYmd = sprintf("%04d/%02d/%02d", $ymd[0], $ymd[1], $ymd[2]);
    
    return	$retYmd;
}
//------------------------------------------------------------------------
// 指定した日の曜日を取得
//------------------------------------------------------------------------
function swFunc_GetDayOfWeek($date){
	$sDate = "$date";
	if ($sDate == ''){return '';}
	if ( mb_strpos($date,'-') > 0 ){
		$tgtDate = explode("-",$sDate);
	}else{
		$tgtDate = explode("/",$sDate);
	}
	if(count($tgtDate) == 0 ){
		return '';
	}
	//指定した年月日の曜日を取得
    $DayOfWeek = date("w", mktime(0, 0, 0, $tgtDate[1],$tgtDate[2], $tgtDate[0]));
    return swFunc_GetWDay($DayOfWeek);
}
//------------------------------------------------------------------------
// 指定した月の末日を取得
//------------------------------------------------------------------------
function swFunc_GetLastDayOfMonth($date){
	$sDate = "$date";
	if ( mb_strpos($date,'-') > 0 ){
		$tgtDate = explode("-",$sDate);
	}else{
		$tgtDate = explode("/",$sDate);
	}
	$LastDayDate = mktime(0, 0, 0, $tgtDate[1] + 1, 0, $tgtDate[0]);
	return date("d", $LastDayDate);
}
// ------------------------------------------------------------------------------
// yyyy/mm/dd を年月日に配列化する
//	$ymd_array = swFunc_DivideYmd($ymd)
// 		$start_ymd	(yyyy/mm/dd) 
// 		$end_ymd	(yyyy/mm/dd)
// ------------------------------------------------------------------------------
function swFunc_DivideYmd($ymd){
	$ymd_array = swFunc_SplitDate($ymd);
	$ymd_array[0] = sprintf( "%04d",$ymd_array[0]);
	$ymd_array[1] = sprintf( "%02d",$ymd_array[1]);
	$ymd_array[2] = sprintf( "%02d",$ymd_array[2]);
	
	return	$ymd_array;
}
// ------------------------------------------------------------------------------
// $start_ymdから$end_ymdの差を求める関数
//	$diffDay = swFunc_GetDiffDays($start_ymd,$end_ymd)
// 		$start_ymd	(yyyy/mm/dd) 
// 		$end_ymd	(yyyy/mm/dd)
// ------------------------------------------------------------------------------
function swFunc_GetDiffDays($start_ymd,$end_ymd){
	//echo "$end_ymd";
	if ($start_ymd == ''){return 0;}
	if ($end_ymd == ''){return 0;}
	
	//開始日
	$ymd = swFunc_SplitDate($start_ymd);
	$start_year = $ymd[0];
	$start_month = $ymd[1];
	$start_day = $ymd[2];
	//終了日
	$ymd = swFunc_SplitDate($end_ymd);
	$end_year = $ymd[0];
	$end_month = $ymd[1];
	$end_day = $ymd[2];
	
	//dateﾀｲﾌﾟに変換
    $start_date = mktime(0, 0, 0, $start_month, $start_day, $start_year);
    $end_date = mktime(0, 0, 0, $end_month, $end_day, $end_year);
    //差を求める
    $Difference = $end_date - $start_date;
    //1日は86400秒　日数に変換
    $diffDay = $Difference / 86400;
    return $diffDay;
}
// ------------------------------------------------------------------------------
// CloseButtonを作成
// ------------------------------------------------------------------------------
function swFunc_GetCloseButton($onClick,$alt){
	$retHTML = <<<END_OF_HTML
		<img src="./css/images/closeButton.png" alt="$alt"
			onclick="$onClick"
			style="cursor: pointer; margin: 6px 10px 0px 4px;"
		>
END_OF_HTML;
	return	$retHTML;
}
// ------------------------------------------------------------------------------
// 時間編集ｷｬﾝｾﾙﾎﾞﾀﾝ
// ------------------------------------------------------------------------------
function swFunc_GetReservedTimeEditCancelButton($onClick,$alt){
	$retHTML = <<<END_OF_HTML
		<img src="./css/images/closeButton.png" alt="$alt"
			onclick="$onClick"
			style="cursor: pointer;"
		>
END_OF_HTML;
	return	$retHTML;
}
// ------------------------------------------------------------------------------
// ｷｬﾝｾﾙﾎﾞﾀﾝ
// ------------------------------------------------------------------------------
function swFunc_MakeCancelButton($onClick,$alt){
	$retHTML = <<<END_OF_HTML
		<img src="./css/images/closeButton.png" alt="$alt"
			onclick="$onClick"
			style="cursor: pointer;"
		>
END_OF_HTML;
	return	$retHTML;
}
// ------------------------------------------------------------------------------
// 日本語曜日選択のSELECT BOX を取得する
//		swFunc_MakeWDaySelectBox($ObjName,$Default,$onchange)
// ------------------------------------------------------------------------------
function swFunc_MakeWDaySelectBox($ObjName,$Default,$onchange){
	//曜日
	$csvArray = array( "0:日", "1:月", "2:火", "3:水", "4:木", "5:金", "6:土" );
	// CSVﾃﾞｰﾀからSelectBox を作成する
	return swFunc_MakeSelectBox($ObjName,$csvArray,$Default,50,$onchange,FALSE);
}
// ------------------------------------------------------------------------------
// 日本語曜日 を 取得する
//		swFunc_GetWDay($wDay)
// ------------------------------------------------------------------------------
function swFunc_GetWDay($wDay){
	//曜日
	$WeekDayArray = array( "日", "月", "火", "水", "木", "金", "土" );
	return	$WeekDayArray[$wDay];
}
// ------------------------------------------------------------------------------
// ﾁｪｯｸﾎﾞｯｸｽを作成する
//		$ObjName		: ﾁｪｯｸﾎﾞｯｸｽ の名称
//		$value		: ﾁｪｯｸﾎﾞｯｸｽ の値
//		$Default	: ﾃﾞﾌｫﾙﾄ値
//		$onChange	: onChange で起動する java script関数名
//		$retHTML = swFunc_MakeCheckBox($ObjName,$csvArray,$Default,$onChange)
// ------------------------------------------------------------------------------
function swFunc_MakeCheckBox($ObjName,$value,$Default,$onChange){
	
	//echo "name = $ObjName";
	
	if ( $value == $Default ){
		$retHtml = "<input name=\"$ObjName\" value=\"$value\" type=\"checkbox\" checked>\n";
	}else{
		$retHtml = "<input name=\"$ObjName\" value=\"$value\" type=\"checkbox\">\n";
	}
	return	$retHtml;
}
// ------------------------------------------------------------------------------
// ランダムなパスワードを生成する
// ------------------------------------------------------------------------------
function swFunc_CreatePassword($Length = 10){
    $Min = 48;
    $Max = 122;
    $Password = '';
    while (strlen($Password) < $Length) {
        $char = chr(mt_rand($Min, $Max));
        if (preg_match('#[2-9a-np-zA-HJ-NP-Z]#', $char)) {
            $Password .= $char;
        }
    }
    return $Password;
}
// ------------------------------------------------------------------------------
// DEBUG ECHO
// ------------------------------------------------------------------------------
function swFunc_Debug($ObjName){
	if(DEBUG_MODE){
		echo "$ObjName<br>";
	}
}
//-------------------------------------------------------------------------------------------------------
?>
