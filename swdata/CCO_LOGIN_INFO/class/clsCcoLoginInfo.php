<?php
// -----------------------------------------------------------
//
// Copyright (C) 2016 ScenarioWriterCafe All Rights Reserved.
// 
//     ログイン情報 ﾃﾞｰﾀ管理ｸﾗｽ
//     clsCcoLoginInfo
// -----------------------------------------------------------
class clsCcoLoginInfo{
	var $CmsloginId;                      //CMSログインID
	var $CmsuserId;                       //CMSユーザーID
	var $CmsloginDate;                    //ログイン日時
// ｺﾝｽﾄﾗｸﾀ
    function clsfncCcoLoginInfo(){
    }
// ﾌﾟﾛﾊﾟﾃｨ
// --------------------------------------
// ﾌﾟﾛﾊﾟﾃｨGet CmsloginId
//    CMSログインID を返す
// --------------------------------------
	function clsCcoLoginInfoGetCmsloginId()
		return $this->CmsloginId;
	}
// --------------------------------------
// ﾌﾟﾛﾊﾟﾃｨSet CmsloginId
//    CMSログインID を設定する
// --------------------------------------
	function clsCcoLoginInfoSetCmsloginId($valCmsloginId){
		$this->CmsloginId = valCmsloginId;
	}
// --------------------------------------
// ﾌﾟﾛﾊﾟﾃｨGet CmsuserId
//    CMSユーザーID を返す
// --------------------------------------
	function clsCcoLoginInfoGetCmsuserId()
		return $this->CmsuserId;
	}
// --------------------------------------
// ﾌﾟﾛﾊﾟﾃｨSet CmsuserId
//    CMSユーザーID を設定する
// --------------------------------------
	function clsCcoLoginInfoSetCmsuserId($valCmsuserId){
		$this->CmsuserId = valCmsuserId;
	}
// --------------------------------------
// ﾌﾟﾛﾊﾟﾃｨGet CmsloginDate
//    ログイン日時 を返す
// --------------------------------------
	function clsCcoLoginInfoGetCmsloginDate()
		return $this->CmsloginDate;
	}
// --------------------------------------
// ﾌﾟﾛﾊﾟﾃｨSet CmsloginDate
//    ログイン日時 を設定する
// --------------------------------------
	function clsCcoLoginInfoSetCmsloginDate($valCmsloginDate){
		$this->CmsloginDate = valCmsloginDate;
	}

// -------------------------------------------------------------
//    ｸﾗｽ初期化
// -------------------------------------------------------------
	function clsCcoLoginInfoInit($mySqlConnObj,$valCmsloginId){
		//ﾌﾟﾛﾊﾟﾃｨの初期化
		$this->CmsloginId = '';                      //CMSログインID
		$this->CmsuserId = '';                       //CMSユーザーID
		$this->CmsloginDate = '';                    //ログイン日時
		//DB から ﾃﾞｰﾀを取得しﾌﾟﾛﾊﾟﾃｨにｾｯﾄする
		$strSQL = <<<END_OF_SQL

		SELECT * FROM CCO_LOGIN_INFO
			WHERE CMSLOGIN_ID = :CmsloginId;
END_OF_SQL;

		$stmt = $mySqlConnObj->prepare($strSQL);
		$stmt->setFetchMode(PDO::FETCH_ASSOC);
		//パラメータのセット
		$stmt->bindParam(':CmsloginId', $valCmsloginId, PDO::PARAM_INT);	
		$stmt->execute();
		while($myRow = $stmt -> fetch(PDO::FETCH_ASSOC)) {
			$this->CmsloginId = $myRow['CMSLOGIN_ID'];
			$this->CmsuserId = $myRow['CMSUSER_ID'];
			$this->CmsloginDate = $myRow['CMSLOGIN_DATE'];
		}//end while
    }//end function

// -------------------------------------------------------------
//    DB更新　INSERT
// -------------------------------------------------------------
	function clsCcoLoginInfoDbInsert($mySqlConnObj){
		//ﾌﾟﾛﾊﾟﾃｨ取得
		$valCmsloginId = $this->CmsloginId;
		$valCmsuserId = $this->CmsuserId;
		$valCmsloginDate = $this->CmsloginDate;
		//INSERT SQL
		$strSQL = <<<END_OF_SQL

		INSERT INTO CCO_LOGIN_INFO(
			  CMSLOGIN_ID
			, CMSUSER_ID
			, CMSLOGIN_DATE
			) values (
			  :CmsloginId
			, :CmsuserId
			, current_timestamp
			);
END_OF_SQL;


		$stmt = $mySqlConnObj->prepare($strSQL);
		$stmt->bindParam(':CmsloginId', $valCmsloginId, PDO::PARAM_INT);
		$stmt->bindParam(':CmsuserId', $valCmsuserId, PDO::PARAM_INT);
		$stmt->bindParam(':CmsloginDate', $valCmsloginDate, PDO::PARAM_INT);	
		$stmt->execute();
	}//end function

// -------------------------------------------------------------
//    DB更新　UPDATE
// -------------------------------------------------------------
	function clsCcoLoginInfoDbUpdate($mySqlConnObj){
		//プロパティ取得
		$valCmsloginId = $this->CmsloginId;
		$valCmsuserId = $this->CmsuserId;
		$valCmsloginDate = $this->CmsloginDate;
		//登録確認
		$strSQL = <<<END_OF_SQL

		SELECT * FROM CCO_LOGIN_INFO
		WHERE CMSLOGIN_ID = :CmsloginId;
END_OF_SQL;


		$stmt = $mySqlConnObj->prepare($strSQL);
		//パラメータのセット
		$stmt->bindParam(':CmsloginId', $valCmsloginId, PDO::PARAM_INT);	
		$stmt->execute();
		//件数取得
		$myRowCnt = $stmt->rowCount();
		if($myRowCnt == 0 ){
            echo $this->fncAlert("clsCcoLoginInfo ->> NOT ENTRY!");
        }else{
			//UPDATE SQL
			$strSQL = <<<END_OF_SQL

			UPDATE CCO_LOGIN_INFO SET 
				  CMSUSER_ID = '$valCmsuserId'
				, CMSLOGIN_DATE = current_timestamp
		WHERE CMSLOGIN_ID = :CmsloginId;
END_OF_SQL;


			$stmt = $mySqlConnObj->prepare($strSQL);	
		//パラメータのセット
		$stmt->bindParam(':CmsloginId', $valCmsloginId, PDO::PARAM_INT);	
			$stmt->execute();
		}//end if
	}//end function

// -------------------------------------------------------------
//    DB更新　DELETE
// -------------------------------------------------------------
	function clsCcoLoginInfoDbDelete($mySqlConnObj){
		//ﾌﾟﾛﾊﾟﾃｨ取得
		$valCmsloginId = $this->CmsloginId;
		//登録確認
		$strSQL = <<<END_OF_SQL

		SELECT * FROM CCO_LOGIN_INFO
			WHERE CMSLOGIN_ID = :CmsloginId;
END_OF_SQL;

		$stmt = $mySqlConnObj->prepare($strSQL);
		//パラメータのセット
		$stmt->bindParam(':CmsloginId', $valCmsloginId, PDO::PARAM_INT);	
		$stmt->execute();
		//件数取得
		$myRowCnt = $myResult->num_rows;
		if($myRowCnt == 0 ){
            echo $this->fncAlert("clsCcoLoginInfo ->> NOT ENTRY!");
        }else{
			//DELETE SQL
			$strSQL = <<<END_OF_SQL

			DELETE FROM CCO_LOGIN_INFO
			WHERE CMSLOGIN_ID = :CmsloginId;
END_OF_SQL;


			$stmt = $mySqlConnObj->prepare($strSQL);	
		//パラメータのセット
		$stmt->bindParam(':CmsloginId', $valCmsloginId, PDO::PARAM_INT);	
			$stmt->execute();
		}//end if
	}//end function
	// ------------------------------------------------------------------------------
	// java script alert
	// ------------------------------------------------------------------------------
	function fncAlert($AlertMsg){
		$strAlert = <<<END_OF_TEXT

			<script language="JavaScript">
				bootbox.alert("$AlertMsg",function() {
			});
			</script>
END_OF_TEXT;

		return $strAlert;
	}
	
} // end class
// -----------------------------------------------------------
?>