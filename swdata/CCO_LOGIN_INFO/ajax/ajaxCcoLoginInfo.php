<?php
// -----------------------------------------------------------
//
// Copyright (C) 2016 ScenarioWriterCafe All Rights Reserved.
// 
//     CCO_LOGIN_INFO I/O ｼｽﾃﾑ
//
//     ajaxCcoLoginInfo.php
// -----------------------------------------------------------
// ------------------------------------------------------------------------------
	//定数読み込み
	include_once("../../sw_config/swConstant.php");
	//DB接続ｸﾗｽの初期化
	include_once("../include/ConnectMySQL.php");
// ------------------------------------------------------------------------------
	//共通関数をｲﾝｸﾙｰﾄﾞ
	include_once("../include/swFunc.php");
	//ﾌｫｰﾑのﾃﾞｰﾀを読み込む
	fncGetPostItems();

	//MainProcedure
	fncMainProc($mySqlConnObj);

	exit();

// ------------------------------------------------------------------------------
//      POSTされた要素を取得
//          fncGetPostItems()
// ------------------------------------------------------------------------------
function fncGetPostItems(){
	//共通global変数
	global	$SubMode;
	//ﾌｫｰﾑ name要素をglobal変数にする
	global	$fdtCmsloginId;
	global	$fdtCmsuserId;
	global	$fdtCmsloginDate;

	//処理ﾓｰﾄが空白ならreturnするﾞ
	if(!isset($_POST['SubMode'])){return;}
	//処理ﾓｰﾄﾞ
	$SubMode = swFunc_GetPostData('SubMode');
	if($SubMode == ''){return;}
	//POSTされた要素を取得
	$fdtCmsloginId = swFunc_GetPostData('fdtCmsloginId');            //CMSログインID
	$fdtCmsuserId = swFunc_GetPostData('fdtCmsuserId');              //CMSユーザーID
	$fdtCmsloginDate = swFunc_GetPostData('fdtCmsloginDate');        //ログイン日時
}//end function

// ------------------------------------------------------------------------------
//      MAIN PROCEDURE
//          fncMainProc($mySqlConnObj)
// ------------------------------------------------------------------------------
function fncMainProc($mySqlConnObj){
	global	$SubMode;
	//global 変数
	global	$fdtCmsloginId;
	global	$fdtCmsuserId;
	global	$fdtCmsloginDate;

	//SubModeで処理を制御
	if($SubMode == 'CcoLoginInfoList'){
		$resultHtml = fncMakeCcoLoginInfoList($mySqlConnObj);
	}

	// 出力charsetをUTF-8に指定
	mb_http_output ( 'UTF-8' );
	// 出力
	echo($resultHtml);
}//end function

//--------------------------------------------------------------------------------
// CCO_LOGIN_INFO ALL LIST
//--------------------------------------------------------------------------------
function fncMakeCcoLoginInfoList($mySqlConnObj){
	//ﾘｽﾄのﾍｯﾀﾞｰ
	$retHtml = <<<END_OF_HTML
	
		<div class="scroll_div">
		<table class="table" _fixedhead="rows:1;div-full-mode: no;">
			<tr>
				<th>CMSログインID</th>
				<th>CMSユーザーID</th>
			</tr>
END_OF_HTML;

	//DB から ﾃﾞｰﾀを取得しﾌﾟﾛﾊﾟﾃｨにｾｯﾄする
	$strSQL = <<<END_OF_SQL
		SELECT *,CMSLOGIN_ID as KEY_ITEM
			FROM CCO_LOGIN_INFO
					ORDER BY CMSLOGIN_ID;
END_OF_SQL;
	//SQLを実行
	$myResult = $mySqlConnObj->query($strSQL);
	while($myRow = $myResult->fetch_assoc()){
		$valCmsloginId = $myRow['CMSLOGIN_ID'];
		$valCmsuserId = $myRow['CMSUSER_ID'];
		$valKeyItem = $myRow['KEY_ITEM'];
		//ﾘｽﾄ
		$retHtml .= <<<END_OF_HTML
		
			<tr onclick="fncSelectCcoLoginInfo('$valKeyItem');">
				<td>$valCmsloginId</td>
				<td>$valCmsuserId</td>
			</tr>
END_OF_HTML;
		}//end while

		$retHtml .= <<<END_OF_HTML
		
		</table>
		</div>
END_OF_HTML;
	//結果セットを開放
	$myResult->free();
	//HTMLを返す
	return $retHtml;
}
