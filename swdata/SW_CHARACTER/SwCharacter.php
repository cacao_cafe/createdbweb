<?php
// -----------------------------------------------------------
//
// Copyright (C) 2016 ScenarioWriterCafe All Rights Reserved.
// 
//     SW_CHARACTER I/O ｼｽﾃﾑ
//
//     SwCharacter.php
// -----------------------------------------------------------
// ------------------------------------------------------------------------------
	//定数読み込み
	include_once("../sw_config/swConstant.php");
	//DB接続ｸﾗｽの初期化
	include_once("./include/ConnectMySQL.php");
// ------------------------------------------------------------------------------
	//ﾃﾞﾌｫﾙﾄｱｸｼｮﾝ
	$ThisPHP = 'SwCharacter.php';
	//ﾃﾞｰﾀ管理ｸﾗｽ
	include_once("./class/clsSwCharacter.php");
	$clsSwCharacter = new clsSwCharacter();
	//共通関数をｲﾝｸﾙｰﾄﾞ
	include_once("./include/swFunc.php");
	// ﾍｯﾀﾞ表示
	$HtmlTitle = "登場人物設定";
	include_once("./include/swHeader.php");

	//ﾌｫｰﾑのﾃﾞｰﾀを読み込む
	fncGetPostItems();

	//MainProcedure
	fncMainProc($mySqlConnObj);

	exit();

// ------------------------------------------------------------------------------
//      POSTされた要素を取得
//          fncGetPostItems()
// ------------------------------------------------------------------------------
function fncGetPostItems(){
	//共通global変数
	global	$SubmitMode;
	//ﾌｫｰﾑ name要素をglobal変数にする
	global	$fdtCharacterId;
	global	$fdtScenarioId;
	global	$fdtCharacterOrderNo;
	global	$fdtCharacterName;
	global	$fdtCharacterChara;
	global	$fdtCopySourceId;

	//処理ﾓｰﾄが空白ならreturnするﾞ
	if(!isset($_POST['SubmitMode'])){return;}
	//処理ﾓｰﾄﾞ
	$SubmitMode = swFunc_GetPostData('SubmitMode');
	if($SubmitMode == ''){return;}
	//POSTされた要素を取得
	$fdtCharacterId = swFunc_GetPostData('fdtCharacterId');          //CHARACTER_ID
	$fdtScenarioId = swFunc_GetPostData('fdtScenarioId');            //SCENARIO_ID
	$fdtCharacterOrderNo = swFunc_GetPostData('fdtCharacterOrderNo');//並び順
	$fdtCharacterName = swFunc_GetPostData('fdtCharacterName');      //登場人物名
	$fdtCharacterChara = swFunc_GetPostData('fdtCharacterChara');    //登場人物説明
	$fdtCopySourceId = swFunc_GetPostData('fdtCopySourceId');        //複写元
}//end function

// ------------------------------------------------------------------------------
//      MAIN PROCEDURE
//          fncMainProc($mySqlConnObj)
// ------------------------------------------------------------------------------
function fncMainProc($mySqlConnObj){
	global	$SubmitMode;
	//class
	global	$clsSwCharacter;

	global	$fdtCharacterId;
	global	$fdtScenarioId;
	global	$fdtCharacterOrderNo;
	global	$fdtCharacterName;
	global	$fdtCharacterChara;
	global	$fdtCopySourceId;

	//SubmitModeで処理を分岐
	switch($SubmitMode ){
		case 'SELECT':
			//ﾃﾞｰﾀ管理ｸﾗｽの初期化
			$clsSwCharacter->clsSwCharacterInit($mySqlConnObj,$fdtCharacterId);
			//ﾌﾟﾛﾊﾟﾃｨGet
			fncSwCharacterGetProperty($clsSwCharacter);
			break;
		case 'INSERT':
			//登録処理
			fncSwCharacterDBInsert($mySqlConnObj,$clsSwCharacter);
			//選択状態にする
			$SubmitMode = 'SELECT';
			//ﾃﾞｰﾀ管理ｸﾗｽの初期化
			$clsSwCharacter->clsSwCharacterInit($mySqlConnObj,$fdtCharacterId);
			//ﾌﾟﾛﾊﾟﾃｨGet
			fncSwCharacterGetProperty($clsSwCharacter);
			break;
		case 'UPDATE':
			//更新処理
			fncSwCharacterDBUpdate($mySqlConnObj,$clsSwCharacter);
			//選択状態にする
			$SubmitMode = 'SELECT';
			//ﾃﾞｰﾀ管理ｸﾗｽの初期化
			$clsSwCharacter->clsSwCharacterInit($mySqlConnObj,$fdtCharacterId);
			//ﾌﾟﾛﾊﾟﾃｨGet
			fncSwCharacterGetProperty($clsSwCharacter);
			break;
		case 'DELETE':
			//削除処理
			fncSwCharacterDBDelete($mySqlConnObj,$clsSwCharacter);
			//ﾌﾟﾛﾊﾟﾃｨReset
			fncSwCharacterResetProperty($clsSwCharacter);
			//初期状態にする
			$SubmitMode = 'RESET';
			break;
		case 'RESET':
			//ﾌﾟﾛﾊﾟﾃｨReset
			fncSwCharacterResetProperty($clsSwCharacter);
			//初期状態にする
			$SubmitMode = 'RESET';
			break;
		default:
			break;
	}
	//ﾌｫｰﾑの表示
	fncMainForm($mySqlConnObj);
}//end function

// ------------------------------------------------------------------------------
//      MAIN FORM
//          fncMainForm($mySqlConnObj)
// ------------------------------------------------------------------------------
function fncMainForm($mySqlConnObj){
	global	$ThisPHP,$SubmitMode;
	global	$fdtCharacterId;
	global	$fdtScenarioId;
	global	$fdtCharacterOrderNo;
	global	$fdtCharacterName;
	global	$fdtCharacterChara;
	global	$fdtCopySourceId;

	//css powerd by Bootstrap ver3
	print <<<END_OF_HTML

	<div class="wrap" >
	
		<div class="container-fluid">
		
		<!-- ﾒﾆｭｰ 制御ﾎﾞﾀﾝ 表示ROW -->
			<div class="row">
				<div class="col-sm-12">
				<legend class="form-inline submenu">
				<ul class="list-inline">
				  <li>
END_OF_HTML;

	//管理メニュー ﾄﾞﾛｯﾌﾟﾀﾞｳﾝﾒﾆｭｰ
	$TargetForm = 'frmSwCharacter';
	include_once("./include/swMgrDropDownMenu.php");

	
    //SubmitModeで表示するボタンを制御する
	switch($SubmitMode){
		case 'SELECT':
			print <<<END_OF_HTML
			 	</li>
			 	<li>
			        <input type="button" value="RESET"
			            id="btnReset"
			            onclick="fncSwCharacterSubmit(frmSwCharacter,'RESET','FALSE','');"
			            class="btn btn-default btn-sm">
			        <span style="width: 120px;"></span>
			        <input type="button" value="INSERT"
			            id="btnInsert"
			            onclick="fncSwCharacterSubmit(frmSwCharacter,'INSERT','TRUE','新規登録');"
			            class="btn btn-warning btn-sm">
			        <input type="button" value="UPDATE"
			            id="btnUpdate"
			            onclick="fncSwCharacterSubmit(frmSwCharacter,'UPDATE','TRUE','更新');"
			            class="btn btn-warning btn-sm">
			        <input type="button" value="DELETE"
			            id="btnDelete"
			            onclick="fncSwCharacterSubmit(frmSwCharacter,'DELETE','TRUE','削除');"
			            class="btn btn-danger btn-sm">
			
				</li>
END_OF_HTML;

	
			break;
        default:
			print <<<END_OF_HTML
			    </li>
			 	<li>
			        <input type="button" value="RESET"
			            id="btnReset"
			            onclick="fncSwCharacterSubmit(frmSwCharacter,'RESET','FALSE','');"
			            class="btn btn-default btn-sm">
			        <input type="button" value="INSERT"
			            id="btnInsert"
			            onclick="fncSwCharacterSubmit(frmSwCharacter,'INSERT','TRUE','新規登録');"
			            class="btn btn-warning btn-sm">
			
				</li>
END_OF_HTML;
            break;
    }
	print <<<END_OF_HTML

					</ul>
					</legend>
    			</div><!-- col-sm-12 end -->
    		</div><!-- row end -->
    		<!-- ﾒﾆｭｰ 制御ﾎﾞﾀﾝ 表示ROW ここまで-->

    		<!-- ﾌｫｰﾑ と ﾘｽﾄ -->
    		<div class="row row-0">
				<div class="col-sm-7 row-0">
					<form class="form-horizontal" role="form" name="frmSwCharacter" id="frmSwCharacter" method="POST" action="$ThisPHP">
						<input type="hidden" name="SubmitMode" id="SubmitMode" value="$SubmitMode">

END_OF_HTML;

	print <<<END_OF_HTML

							<div class="form-group row-0">
								<label for="fdtCharacterId" class="control-label col-xs-3">CHARACTER_ID</label>
								<div class="col-xs-8">
									<input type="number"
										class="form-control input-sm text-right"
										id="fdtCharacterId" name="fdtCharacterId"
										value="$fdtCharacterId"
										onkeyup="AjaxFunc_AddFigure(frmSwCharacter,'fdtCharacterId');"
										placeholder="CHARACTER_ID">
								</div>
							</div>
END_OF_HTML;
	print <<<END_OF_HTML

							<div class="form-group row-0">
								<label for="fdtScenarioId" class="control-label col-xs-3">SCENARIO_ID</label>
								<div class="col-xs-8">
									<input type="number"
										class="form-control input-sm text-right"
										id="fdtScenarioId" name="fdtScenarioId"
										value="$fdtScenarioId"
										onkeyup="AjaxFunc_AddFigure(frmSwCharacter,'fdtScenarioId');"
										placeholder="SCENARIO_ID">
								</div>
							</div>
END_OF_HTML;
	print <<<END_OF_HTML

							<div class="form-group row-0">
								<label for="fdtCharacterOrderNo" class="control-label col-xs-3">並び順</label>
								<div class="col-xs-8">
								
END_OF_HTML;

	//------------------------------------------------------------
	//CSVﾃﾞｰﾀからselect box を作成する
	$ObjName = "fdtCharacterOrderNo";		//select box の名称.ID
	//ここでfunctionからCSVﾃﾞｰﾀを取得
	$csvArray = swFunc_MakeSelectItemsCsvSample();	//CSVﾃﾞｰﾀ
	$default = "$fdtCharacterOrderNo";		//ﾃﾞﾌｫﾙﾄ値
	$onChange = '';					//onChange で起動する javascript or jQuery
	$ViewCode = FALSE;				//ｺｰﾄﾞを表示する場合はTRUE
	//SelectBoxHtml出力
	print swFunc_MakeSelectBox($ObjName,$csvArray,$default,$onChange,$ViewCode);
//------------------------------------------------------------

	print <<<END_OF_HTML

								</div>
							</div>
END_OF_HTML;
	print <<<END_OF_HTML

							<div class="form-group row-0">
								<label for="fdtCharacterName" class="control-label col-xs-3">登場人物名</label>
								<div class="col-xs-8">
									<input type="text" 
										class="form-control input-sm"
										id="fdtCharacterName" name="fdtCharacterName"
										value="$fdtCharacterName"
										placeholder="登場人物名">
								</div>
							</div>
END_OF_HTML;
	print <<<END_OF_HTML

							<div class="form-group row-0">
								<label for="fdtCharacterChara" class="control-label col-xs-3">登場人物説明</label>
								<div class="col-xs-8">
									<textarea placeholder="登場人物説明" 
										class="form-control input-sm"
										id="fdtCharacterChara" name="fdtCharacterChara"
										rows="3"
										id="InputTextarea">$fdtCharacterChara</textarea>
								</div>
							</div>
END_OF_HTML;
	print <<<END_OF_HTML

							<div class="form-group row-0">
								<label for="fdtCopySourceId" class="control-label col-xs-3">複写元</label>
								<div class="col-xs-8">
									<input type="number"
										class="form-control input-sm text-right"
										id="fdtCopySourceId" name="fdtCopySourceId"
										value="$fdtCopySourceId"
										onkeyup="AjaxFunc_AddFigure(frmSwCharacter,'fdtCopySourceId');"
										placeholder="複写元">
								</div>
							</div>
END_OF_HTML;
	print <<<END_OF_HTML

						</form>
					</div>
					<div class="col-sm-5" >
	                	<!-- リストの表示位置 -->
	                    <span id="side_list_area"></span>
					</div>
				</div>
			</div>
 
		</div><!--container-->
	</div><!--wrap-->


    <script type="text/javascript" src="./ajax/ajaxSwCharacter.js"></script>
    <script type="text/javascript">
        fncMakeSwCharacterList(frmSwCharacter);
    </script>


	<!--footer表示位置--><span class="sw-footer"></span>
	<script language="JavaScript">
		AjaxFunc_FooterLoad();
	</script>
	
  </body>
</html>

END_OF_HTML;

}//end function

// ------------------------------------------------------------------------------
//      ﾌﾟﾛﾊﾟﾃｨReset
//          fncSwCharacterResetProperty($clsSwCharacter)
// ------------------------------------------------------------------------------
function fncSwCharacterResetProperty($clsSwCharacter){
	global	$fdtCharacterId;
	global	$fdtScenarioId;
	global	$fdtCharacterOrderNo;
	global	$fdtCharacterName;
	global	$fdtCharacterChara;
	global	$fdtCopySourceId;

	//ﾌﾟﾛﾊﾟﾃｨReset
	$fdtCharacterId = "";                     //CHARACTER_ID
	$fdtScenarioId = "";                      //SCENARIO_ID
	$fdtCharacterOrderNo = "";                //並び順
	$fdtCharacterName = "";                   //登場人物名
	$fdtCharacterChara = "";                  //登場人物説明
	$fdtCopySourceId = "";                    //複写元
}//end function

// ------------------------------------------------------------------------------
//      ﾌﾟﾛﾊﾟﾃｨGet
//          fncSwCharacterGetProperty($clsSwCharacter)
// ------------------------------------------------------------------------------
function fncSwCharacterGetProperty($clsSwCharacter){
	global	$fdtCharacterId;
	global	$fdtScenarioId;
	global	$fdtCharacterOrderNo;
	global	$fdtCharacterName;
	global	$fdtCharacterChara;
	global	$fdtCopySourceId;

	//ﾌﾟﾛﾊﾟﾃｨGet
	$fdtCharacterId = $clsSwCharacter->clsSwCharacterGetCharacterId();          //CHARACTER_ID
	$fdtScenarioId = $clsSwCharacter->clsSwCharacterGetScenarioId();            //SCENARIO_ID
	$fdtCharacterOrderNo = $clsSwCharacter->clsSwCharacterGetCharacterOrderNo();//並び順
	$fdtCharacterName = $clsSwCharacter->clsSwCharacterGetCharacterName();      //登場人物名

	//登場人物説明は<br>を\nに変換する
	$fdtCharacterChara = $clsSwCharacter->clsSwCharacterGetCharacterChara();    //登場人物説明
	$fdtCharacterChara = str_replace("<br>","\n",$fdtCharacterChara);

	$fdtCopySourceId = $clsSwCharacter->clsSwCharacterGetCopySourceId();        //複写元
}//end function

// ------------------------------------------------------------------------------
//      ﾌﾟﾛﾊﾟﾃｨSet
//          fncSwCharacterSetProperty($clsSwCharacter)
// ------------------------------------------------------------------------------
function fncSwCharacterSetProperty($clsSwCharacter){
	global	$fdtCharacterId;
	global	$fdtScenarioId;
	global	$fdtCharacterOrderNo;
	global	$fdtCharacterName;
	global	$fdtCharacterChara;
	global	$fdtCopySourceId;

	//ﾌﾟﾛﾊﾟﾃｨSet

	//CHARACTER_IDはｶﾝﾏを取り除いてからSetする
	$fdtCharacterId = str_replace(",","",$fdtCharacterId);
	$clsSwCharacter->clsSwCharacterSetCharacterId($fdtCharacterId);          //CHARACTER_ID


	//SCENARIO_IDはｶﾝﾏを取り除いてからSetする
	$fdtScenarioId = str_replace(",","",$fdtScenarioId);
	$clsSwCharacter->clsSwCharacterSetScenarioId($fdtScenarioId);            //SCENARIO_ID

	$clsSwCharacter->clsSwCharacterSetCharacterOrderNo($fdtCharacterOrderNo);//並び順
	$clsSwCharacter->clsSwCharacterSetCharacterName($fdtCharacterName);      //登場人物名

	//登場人物説明は改行を<br>にしてからSetする
	$fdtCharacterChara = str_replace("\r\n","<br>",$fdtCharacterChara);
	$fdtCharacterChara = str_replace("\r","<br>",$fdtCharacterChara);
	$fdtCharacterChara = str_replace("\n","<br>",$fdtCharacterChara);
	$clsSwCharacter->clsSwCharacterSetCharacterChara($fdtCharacterChara);    //登場人物説明


	//複写元はｶﾝﾏを取り除いてからSetする
	$fdtCopySourceId = str_replace(",","",$fdtCopySourceId);
	$clsSwCharacter->clsSwCharacterSetCopySourceId($fdtCopySourceId);        //複写元

}//end function

// ------------------------------------------------------------------------------
//      DB INSERT
//          fncSwCharacterDBInsert($mySqlConnObj,$clsSwCharacter)
// ------------------------------------------------------------------------------
function fncSwCharacterDBInsert($mySqlConnObj,$clsSwCharacter){
	global	$fdtCharacterId;

	//ﾌﾟﾛﾊﾟﾃｨｾｯﾄ
	fncSwCharacterSetProperty($clsSwCharacter);
	//ﾃﾞｰﾀ管理ｸﾗｽ DbInsert
	$fdtCharacterId = $clsSwCharacter->clsSwCharacterDbInsert($mySqlConnObj);
}//end function

// ------------------------------------------------------------------------------
//      DB UPDATE
//          fncSwCharacterDBUpdate($mySqlConnObj,$clsSwCharacter)
// ------------------------------------------------------------------------------
function fncSwCharacterDBUpdate($mySqlConnObj,$clsSwCharacter){
	//ﾌﾟﾛﾊﾟﾃｨｾｯﾄ
	fncSwCharacterSetProperty($clsSwCharacter);
	//ﾃﾞｰﾀ管理ｸﾗｽ DbUpdate
	$clsSwCharacter->clsSwCharacterDbUpdate($mySqlConnObj);
}//end function

// ------------------------------------------------------------------------------
//      DB DELETE
//          fncSwCharacterDBDelete($mySqlConnObj,$clsSwCharacter)
// ------------------------------------------------------------------------------
function fncSwCharacterDBDelete($mySqlConnObj,$clsSwCharacter){
	//ﾌﾟﾛﾊﾟﾃｨｾｯﾄ
	fncSwCharacterSetProperty($clsSwCharacter);
	//ﾃﾞｰﾀ管理ｸﾗｽ DbDelete
	$clsSwCharacter->clsSwCharacterDbDelete($mySqlConnObj);
}//end function


// -----------------------------------------------------------
?>