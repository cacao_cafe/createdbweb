<?php
// -----------------------------------------------------------
//
// Copyright (C) 2016 ScenarioWriterCafe All Rights Reserved.
// 
//     CREATE TABLE SW_CHARACTER
//
//     createTable_SwCharacter.php
// -----------------------------------------------------------
$Result = fncCreateTable();
echo $Result;
exit;


// -----------------------------------------------------------
//     fncCreateTable()
// -----------------------------------------------------------
function fncCreateTable(){
	//共通定数
	include_once("../../sw_config/swConstant.php");

	//MySQLに接続
	include_once("../include/ConnectMySQL.php");

	//DROP TABLE SQLを編集
	$strSQL = <<<END_OF_SQL

DROP TABLE IF EXISTS SW_CHARACTER;

END_OF_SQL;

	//DROP TABLE EXECUTE 
	$stmt = $mySqlConnObj->prepare($strSQL);
	$stmt->execute();

	//CREATE TABLE SQLを編集
	$strSQL = <<<END_OF_SQL

CREATE TABLE SW_CHARACTER(
	  CHARACTER_ID                     INT NOT NULL AUTO_INCREMENT COMMENT 'CHARACTER_ID'
	, SCENARIO_ID                      INT  COMMENT 'SCENARIO_ID'
	, CHARACTER_ORDER_NO               DOUBLE  COMMENT '並び順'
	, CHARACTER_NAME                   VARCHAR(256)  COMMENT '登場人物名'
	, CHARACTER_CHARA                  MEDIUMTEXT  COMMENT '登場人物説明'
	, COPY_SOURCE_ID                   INT  COMMENT '複写元'
	, PRIMARY KEY (CHARACTER_ID)
	)DEFAULT CHARACTER SET 'UTF8'
	COMMENT '登場人物設定';

END_OF_SQL;

	//CREATE TABLE EXECUTE 
	$stmt = $mySqlConnObj->prepare($strSQL);
	$stmt->execute();

	//UNIQUE INDEX のSQLを編集
	$strSQL = <<<END_OF_SQL

	CREATE UNIQUE INDEX SW_CHARACTER_KEY_CHARACTER_ID ON SW_CHARACTER(CHARACTER_ID);
END_OF_SQL;

	//CREATE INDEX EXECUTE 
	$stmt = $mySqlConnObj->prepare($strSQL);
	$stmt->execute();


	//DB切断
	include_once("../include/DisConnectMySQL.php");
	
	
	return	'CREATE TABLE SW_CHARACTER Success...';
}//end function
// -----------------------------------------------------------
?>