<?php
// -----------------------------------------------------------
//
// Copyright (C) 2016 ScenarioWriterCafe All Rights Reserved.
// 
//     登場人物設定 ﾃﾞｰﾀ管理ｸﾗｽ
//     clsSwCharacter
// -----------------------------------------------------------
class clsSwCharacter{
	var $CharacterId;                     //CHARACTER_ID
	var $ScenarioId;                      //SCENARIO_ID
	var $CharacterOrderNo;                //並び順
	var $CharacterName;                   //登場人物名
	var $CharacterChara;                  //登場人物説明
	var $CopySourceId;                    //複写元
// ｺﾝｽﾄﾗｸﾀ
    function clsfncSwCharacter(){
    }
// ﾌﾟﾛﾊﾟﾃｨ
// --------------------------------------
// ﾌﾟﾛﾊﾟﾃｨGet CharacterId
//    CHARACTER_ID を返す
// --------------------------------------
	function clsSwCharacterGetCharacterId()
		return $this->CharacterId;
	}
// --------------------------------------
// ﾌﾟﾛﾊﾟﾃｨSet CharacterId
//    CHARACTER_ID を設定する
// --------------------------------------
	function clsSwCharacterSetCharacterId($valCharacterId){
		$this->CharacterId = valCharacterId;
	}
// --------------------------------------
// ﾌﾟﾛﾊﾟﾃｨGet ScenarioId
//    SCENARIO_ID を返す
// --------------------------------------
	function clsSwCharacterGetScenarioId()
		return $this->ScenarioId;
	}
// --------------------------------------
// ﾌﾟﾛﾊﾟﾃｨSet ScenarioId
//    SCENARIO_ID を設定する
// --------------------------------------
	function clsSwCharacterSetScenarioId($valScenarioId){
		$this->ScenarioId = valScenarioId;
	}
// --------------------------------------
// ﾌﾟﾛﾊﾟﾃｨGet CharacterOrderNo
//    並び順 を返す
// --------------------------------------
	function clsSwCharacterGetCharacterOrderNo()
		return $this->CharacterOrderNo;
	}
// --------------------------------------
// ﾌﾟﾛﾊﾟﾃｨSet CharacterOrderNo
//    並び順 を設定する
// --------------------------------------
	function clsSwCharacterSetCharacterOrderNo($valCharacterOrderNo){
		$this->CharacterOrderNo = valCharacterOrderNo;
	}
// --------------------------------------
// ﾌﾟﾛﾊﾟﾃｨGet CharacterName
//    登場人物名 を返す
// --------------------------------------
	function clsSwCharacterGetCharacterName()
		return $this->CharacterName;
	}
// --------------------------------------
// ﾌﾟﾛﾊﾟﾃｨSet CharacterName
//    登場人物名 を設定する
// --------------------------------------
	function clsSwCharacterSetCharacterName($valCharacterName){
		$this->CharacterName = valCharacterName;
	}
// --------------------------------------
// ﾌﾟﾛﾊﾟﾃｨGet CharacterChara
//    登場人物説明 を返す
// --------------------------------------
	function clsSwCharacterGetCharacterChara()
		return $this->CharacterChara;
	}
// --------------------------------------
// ﾌﾟﾛﾊﾟﾃｨSet CharacterChara
//    登場人物説明 を設定する
// --------------------------------------
	function clsSwCharacterSetCharacterChara($valCharacterChara){
		$this->CharacterChara = valCharacterChara;
	}
// --------------------------------------
// ﾌﾟﾛﾊﾟﾃｨGet CopySourceId
//    複写元 を返す
// --------------------------------------
	function clsSwCharacterGetCopySourceId()
		return $this->CopySourceId;
	}
// --------------------------------------
// ﾌﾟﾛﾊﾟﾃｨSet CopySourceId
//    複写元 を設定する
// --------------------------------------
	function clsSwCharacterSetCopySourceId($valCopySourceId){
		$this->CopySourceId = valCopySourceId;
	}

// -------------------------------------------------------------
//    ｸﾗｽ初期化
// -------------------------------------------------------------
	function clsSwCharacterInit($mySqlConnObj,$valCharacterId){
		//ﾌﾟﾛﾊﾟﾃｨの初期化
		$this->CharacterId = '';                     //CHARACTER_ID
		$this->ScenarioId = '';                      //SCENARIO_ID
		$this->CharacterOrderNo = '';                //並び順
		$this->CharacterName = '';                   //登場人物名
		$this->CharacterChara = '';                  //登場人物説明
		$this->CopySourceId = '';                    //複写元
		//DB から ﾃﾞｰﾀを取得しﾌﾟﾛﾊﾟﾃｨにｾｯﾄする
		$strSQL = <<<END_OF_SQL

		SELECT * FROM SW_CHARACTER
		WHERE CHARACTER_ID = :CharacterId;
END_OF_SQL;

		$stmt = $mySqlConnObj->prepare($strSQL);
		$stmt->setFetchMode(PDO::FETCH_ASSOC);
		//パラメータのセット
		$stmt->bindParam(':CharacterId', $valCharacterId, PDO::PARAM_INT);	
		$stmt->execute();
		while($myRow = $stmt -> fetch(PDO::FETCH_ASSOC)) {
			$this->CharacterId = $myRow['CHARACTER_ID];
			$this->ScenarioId = $myRow['SCENARIO_ID];
			$this->CharacterOrderNo = $myRow['CHARACTER_ORDER_NO];
			$this->CharacterName = $myRow['CHARACTER_NAME];
			$this->CharacterChara = $myRow['CHARACTER_CHARA];
			$this->CopySourceId = $myRow['COPY_SOURCE_ID];
		}//end while
    }//end function

// -------------------------------------------------------------
//    DB更新　INSERT
// -------------------------------------------------------------
	function clsSwCharacterDbInsert($mySqlConnObj){
		//ﾌﾟﾛﾊﾟﾃｨ取得
		$valCharacterId = $this->CharacterId;
		$valScenarioId = $this->ScenarioId;
		$valCharacterOrderNo = $this->CharacterOrderNo;
		$valCharacterName = $this->CharacterName;
		$valCharacterChara = $this->CharacterChara;
		$valCopySourceId = $this->CopySourceId;
		//INSERT SQL
		$strSQL = <<<END_OF_SQL

		INSERT INTO SW_CHARACTER(
			  SCENARIO_ID
			, CHARACTER_ORDER_NO
			, CHARACTER_NAME
			, CHARACTER_CHARA
			, COPY_SOURCE_ID
			) values (
			  :ScenarioId
			, :CharacterOrderNo
			, :CharacterName
			, :CharacterChara
			, :CopySourceId
			);
END_OF_SQL;


		$stmt = $mySqlConnObj->prepare($strSQL);
		$stmt->bindParam(':ScenarioId', $valScenarioId, PDO::PARAM_INT);
		$stmt->bindParam(':CharacterOrderNo', $valCharacterOrderNo, PDO::PARAM_INT);
		$stmt->bindParam(':CharacterName', $valCharacterName, PDO::PARAM_INT);
		$stmt->bindParam(':CharacterChara', $valCharacterChara, PDO::PARAM_INT);
		$stmt->bindParam(':CopySourceId', $valCopySourceId, PDO::PARAM_INT);	
		$stmt->execute();
		//AUTO_INCREMENT値取得
		$LastInsertId = $mySqlConnObj->lastInsertId();
		//AUTO_INCREMENT値を返す
		return $LastInsertId;
	}//end function

// -------------------------------------------------------------
//    DB更新　UPDATE
// -------------------------------------------------------------
	function clsSwCharacterDbUpdate($mySqlConnObj){
		//プロパティ取得
		$valCharacterId = $this->CharacterId;
		$valScenarioId = $this->ScenarioId;
		$valCharacterOrderNo = $this->CharacterOrderNo;
		$valCharacterName = $this->CharacterName;
		$valCharacterChara = $this->CharacterChara;
		$valCopySourceId = $this->CopySourceId;
		//登録確認
		$strSQL = <<<END_OF_SQL

		SELECT * FROM SW_CHARACTER
		WHERE CHARACTER_ID = :CharacterId;
END_OF_SQL;


		$stmt = $mySqlConnObj->prepare($strSQL);
		//パラメータのセット
		$stmt->bindParam(':CharacterId', $valCharacterId, PDO::PARAM_INT);	
		$stmt->execute();
		//件数取得
		$myRowCnt = $stmt->rowCount();
		if($myRowCnt == 0 ){
            echo $this->fncAlert("clsSwCharacter ->> NOT ENTRY!");
        }else{
			//UPDATE SQL
			$strSQL = <<<END_OF_SQL

			UPDATE SW_CHARACTER SET 
				  SCENARIO_ID = '$valScenarioId'
				, CHARACTER_ORDER_NO = '$valCharacterOrderNo'
				, CHARACTER_NAME = '$valCharacterName'
				, CHARACTER_CHARA = '$valCharacterChara'
				, COPY_SOURCE_ID = '$valCopySourceId'
		WHERE CHARACTER_ID = :CharacterId;
END_OF_SQL;


			$stmt = $mySqlConnObj->prepare($strSQL);	
		//パラメータのセット
		$stmt->bindParam(':CharacterId', $valCharacterId, PDO::PARAM_INT);	
			$stmt->execute();
		}//end if
	}//end function

// -------------------------------------------------------------
//    DB更新　DELETE
// -------------------------------------------------------------
	function $classNameDbDelete($mySqlConnObj){
		//ﾌﾟﾛﾊﾟﾃｨ取得
		$valCharacterId = $this->CharacterId;
		//登録確認
		$strSQL = <<<END_OF_SQL

		SELECT * FROM SW_CHARACTER
			WHERE CHARACTER_ID = :CharacterId;
END_OF_SQL;

		$stmt = $mySqlConnObj->prepare($strSQL);
		//パラメータのセット
		$stmt->bindParam(':CharacterId', $valCharacterId, PDO::PARAM_INT);	
		$stmt->execute();
		//件数取得
		$myRowCnt = $myResult->num_rows;
		if($myRowCnt == 0 ){
            echo $this->fncAlert("clsSwCharacter ->> NOT ENTRY!");
        }else{
			//DELETE SQL
			$strSQL = <<<END_OF_SQL

			DELETE FROM SW_CHARACTER
			WHERE CHARACTER_ID = :CharacterId;
END_OF_SQL;


			$stmt = $mySqlConnObj->prepare($strSQL);	
		//パラメータのセット
		$stmt->bindParam(':CharacterId', $valCharacterId, PDO::PARAM_INT);	
			$stmt->execute();
		}//end if
	}//end function
	// ------------------------------------------------------------------------------
	// java script alert
	// ------------------------------------------------------------------------------
	function fncAlert($AlertMsg){
		$strAlert = <<<END_OF_TEXT

			<script language="JavaScript">
				bootbox.alert("$AlertMsg",function() {
			});
			</script>
END_OF_TEXT;

		return $strAlert;
	}
	
} // end class
// -----------------------------------------------------------
?>