<?php
// -----------------------------------------------------------
//
// Copyright (C) 2016 ScenarioWriterCafe All Rights Reserved.
// 
//     SW_CHARACTER I/O ｼｽﾃﾑ
//
//     ajaxSwCharacter.php
// -----------------------------------------------------------
// ------------------------------------------------------------------------------
	//定数読み込み
	include_once("../../sw_config/swConstant.php");
	//DB接続ｸﾗｽの初期化
	include_once("../include/ConnectMySQL.php");
// ------------------------------------------------------------------------------
	//共通関数をｲﾝｸﾙｰﾄﾞ
	include_once("../include/swFunc.php");
	//ﾌｫｰﾑのﾃﾞｰﾀを読み込む
	fncGetPostItems();

	//MainProcedure
	fncMainProc($mySqlConnObj);

	exit();

// ------------------------------------------------------------------------------
//      POSTされた要素を取得
//          fncGetPostItems()
// ------------------------------------------------------------------------------
function fncGetPostItems(){
	//共通global変数
	global	$SubMode;
	//ﾌｫｰﾑ name要素をglobal変数にする
	global	$fdtCharacterId;
	global	$fdtScenarioId;
	global	$fdtCharacterOrderNo;
	global	$fdtCharacterName;
	global	$fdtCharacterChara;
	global	$fdtCopySourceId;

	//処理ﾓｰﾄが空白ならreturnするﾞ
	if(!isset($_POST['SubMode'])){return;}
	//処理ﾓｰﾄﾞ
	$SubMode = swFunc_GetPostData('SubMode');
	if($SubMode == ''){return;}
	//POSTされた要素を取得
	$fdtCharacterId = swFunc_GetPostData('fdtCharacterId');          //CHARACTER_ID
	$fdtScenarioId = swFunc_GetPostData('fdtScenarioId');            //SCENARIO_ID
	$fdtCharacterOrderNo = swFunc_GetPostData('fdtCharacterOrderNo');//並び順
	$fdtCharacterName = swFunc_GetPostData('fdtCharacterName');      //登場人物名
	$fdtCharacterChara = swFunc_GetPostData('fdtCharacterChara');    //登場人物説明
	$fdtCopySourceId = swFunc_GetPostData('fdtCopySourceId');        //複写元
}//end function

// ------------------------------------------------------------------------------
//      MAIN PROCEDURE
//          fncMainProc($mySqlConnObj)
// ------------------------------------------------------------------------------
function fncMainProc($mySqlConnObj){
	global	$SubMode;
	//global 変数
	global	$fdtCharacterId;
	global	$fdtScenarioId;
	global	$fdtCharacterOrderNo;
	global	$fdtCharacterName;
	global	$fdtCharacterChara;
	global	$fdtCopySourceId;

	//SubModeで処理を制御
	if($SubMode == 'SwCharacterList'){
		$resultHtml = fncMakeSwCharacterList($mySqlConnObj);
	}

	// 出力charsetをUTF-8に指定
	mb_http_output ( 'UTF-8' );
	// 出力
	echo($resultHtml);
}//end function

//--------------------------------------------------------------------------------
// SW_CHARACTER ALL LIST
//--------------------------------------------------------------------------------
function fncMakeSwCharacterList($mySqlConnObj){
	//ﾘｽﾄのﾍｯﾀﾞｰ
	$retHtml = <<<END_OF_HTML
	
		<div class="scroll_div">
		<table class="table" _fixedhead="rows:1;div-full-mode: no;">
			<tr>
			</tr>
END_OF_HTML;

	//DB から ﾃﾞｰﾀを取得しﾌﾟﾛﾊﾟﾃｨにｾｯﾄする
	$strSQL = <<<END_OF_SQL
		SELECT *,CHARACTER_ID as KEY_ITEM
			FROM SW_CHARACTER
					ORDER BY CHARACTER_ID;
END_OF_SQL;
	//SQLを実行
	$myResult = $mySqlConnObj->query($strSQL);
	while($myRow = $myResult->fetch_assoc()){
		$valKeyItem = $myRow['KEY_ITEM'];
		//ﾘｽﾄ
		$retHtml .= <<<END_OF_HTML
		
			<tr onclick="fncSelectSwCharacter('$valKeyItem');">
			</tr>
END_OF_HTML;
		}//end while

		$retHtml .= <<<END_OF_HTML
		
		</table>
		</div>
END_OF_HTML;
	//結果セットを開放
	$myResult->free();
	//HTMLを返す
	return $retHtml;
}
