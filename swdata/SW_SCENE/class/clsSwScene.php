<?php
// -----------------------------------------------------------
//
// Copyright (C) 2016 ScenarioWriterCafe All Rights Reserved.
// 
//     場面設定 ﾃﾞｰﾀ管理ｸﾗｽ
//     clsSwScene
// -----------------------------------------------------------
class clsSwScene{
	var $SceneId;                         //SCENE_ID
	var $ScenarioId;                      //SCENARIO_ID
	var $SceneOrderNo;                    //場面順番
	var $SceneValidCd;                    //有効
	var $SceneName;                       //場面名称
	var $SceneDescription;                //場面説明
	var $SceneTimeMin;                    //時間（分）
	var $SceneTimeSec;                    //時間（秒）
	var $CopySourceId;                    //複写元
// ｺﾝｽﾄﾗｸﾀ
    function clsfncSwScene(){
    }
// ﾌﾟﾛﾊﾟﾃｨ
// --------------------------------------
// ﾌﾟﾛﾊﾟﾃｨGet SceneId
//    SCENE_ID を返す
// --------------------------------------
	function clsSwSceneGetSceneId()
		return $this->SceneId;
	}
// --------------------------------------
// ﾌﾟﾛﾊﾟﾃｨSet SceneId
//    SCENE_ID を設定する
// --------------------------------------
	function clsSwSceneSetSceneId($valSceneId){
		$this->SceneId = valSceneId;
	}
// --------------------------------------
// ﾌﾟﾛﾊﾟﾃｨGet ScenarioId
//    SCENARIO_ID を返す
// --------------------------------------
	function clsSwSceneGetScenarioId()
		return $this->ScenarioId;
	}
// --------------------------------------
// ﾌﾟﾛﾊﾟﾃｨSet ScenarioId
//    SCENARIO_ID を設定する
// --------------------------------------
	function clsSwSceneSetScenarioId($valScenarioId){
		$this->ScenarioId = valScenarioId;
	}
// --------------------------------------
// ﾌﾟﾛﾊﾟﾃｨGet SceneOrderNo
//    場面順番 を返す
// --------------------------------------
	function clsSwSceneGetSceneOrderNo()
		return $this->SceneOrderNo;
	}
// --------------------------------------
// ﾌﾟﾛﾊﾟﾃｨSet SceneOrderNo
//    場面順番 を設定する
// --------------------------------------
	function clsSwSceneSetSceneOrderNo($valSceneOrderNo){
		$this->SceneOrderNo = valSceneOrderNo;
	}
// --------------------------------------
// ﾌﾟﾛﾊﾟﾃｨGet SceneValidCd
//    有効 を返す
// --------------------------------------
	function clsSwSceneGetSceneValidCd()
		return $this->SceneValidCd;
	}
// --------------------------------------
// ﾌﾟﾛﾊﾟﾃｨSet SceneValidCd
//    有効 を設定する
// --------------------------------------
	function clsSwSceneSetSceneValidCd($valSceneValidCd){
		$this->SceneValidCd = valSceneValidCd;
	}
// --------------------------------------
// ﾌﾟﾛﾊﾟﾃｨGet SceneName
//    場面名称 を返す
// --------------------------------------
	function clsSwSceneGetSceneName()
		return $this->SceneName;
	}
// --------------------------------------
// ﾌﾟﾛﾊﾟﾃｨSet SceneName
//    場面名称 を設定する
// --------------------------------------
	function clsSwSceneSetSceneName($valSceneName){
		$this->SceneName = valSceneName;
	}
// --------------------------------------
// ﾌﾟﾛﾊﾟﾃｨGet SceneDescription
//    場面説明 を返す
// --------------------------------------
	function clsSwSceneGetSceneDescription()
		return $this->SceneDescription;
	}
// --------------------------------------
// ﾌﾟﾛﾊﾟﾃｨSet SceneDescription
//    場面説明 を設定する
// --------------------------------------
	function clsSwSceneSetSceneDescription($valSceneDescription){
		$this->SceneDescription = valSceneDescription;
	}
// --------------------------------------
// ﾌﾟﾛﾊﾟﾃｨGet SceneTimeMin
//    時間（分） を返す
// --------------------------------------
	function clsSwSceneGetSceneTimeMin()
		return $this->SceneTimeMin;
	}
// --------------------------------------
// ﾌﾟﾛﾊﾟﾃｨSet SceneTimeMin
//    時間（分） を設定する
// --------------------------------------
	function clsSwSceneSetSceneTimeMin($valSceneTimeMin){
		$this->SceneTimeMin = valSceneTimeMin;
	}
// --------------------------------------
// ﾌﾟﾛﾊﾟﾃｨGet SceneTimeSec
//    時間（秒） を返す
// --------------------------------------
	function clsSwSceneGetSceneTimeSec()
		return $this->SceneTimeSec;
	}
// --------------------------------------
// ﾌﾟﾛﾊﾟﾃｨSet SceneTimeSec
//    時間（秒） を設定する
// --------------------------------------
	function clsSwSceneSetSceneTimeSec($valSceneTimeSec){
		$this->SceneTimeSec = valSceneTimeSec;
	}
// --------------------------------------
// ﾌﾟﾛﾊﾟﾃｨGet CopySourceId
//    複写元 を返す
// --------------------------------------
	function clsSwSceneGetCopySourceId()
		return $this->CopySourceId;
	}
// --------------------------------------
// ﾌﾟﾛﾊﾟﾃｨSet CopySourceId
//    複写元 を設定する
// --------------------------------------
	function clsSwSceneSetCopySourceId($valCopySourceId){
		$this->CopySourceId = valCopySourceId;
	}

// -------------------------------------------------------------
//    ｸﾗｽ初期化
// -------------------------------------------------------------
	function clsSwSceneInit($mySqlConnObj,$valSceneId){
		//ﾌﾟﾛﾊﾟﾃｨの初期化
		$this->SceneId = '';                         //SCENE_ID
		$this->ScenarioId = '';                      //SCENARIO_ID
		$this->SceneOrderNo = '';                    //場面順番
		$this->SceneValidCd = '';                    //有効
		$this->SceneName = '';                       //場面名称
		$this->SceneDescription = '';                //場面説明
		$this->SceneTimeMin = '';                    //時間（分）
		$this->SceneTimeSec = '';                    //時間（秒）
		$this->CopySourceId = '';                    //複写元
		//DB から ﾃﾞｰﾀを取得しﾌﾟﾛﾊﾟﾃｨにｾｯﾄする
		$strSQL = <<<END_OF_SQL

		SELECT * FROM SW_SCENE
		WHERE SCENE_ID = :SceneId;
END_OF_SQL;

		$stmt = $mySqlConnObj->prepare($strSQL);
		$stmt->setFetchMode(PDO::FETCH_ASSOC);
		//パラメータのセット
		$stmt->bindParam(':SceneId', $valSceneId, PDO::PARAM_INT);	
		$stmt->execute();
		while($myRow = $stmt -> fetch(PDO::FETCH_ASSOC)) {
			$this->SceneId = $myRow['SCENE_ID];
			$this->ScenarioId = $myRow['SCENARIO_ID];
			$this->SceneOrderNo = $myRow['SCENE_ORDER_NO];
			$this->SceneValidCd = $myRow['SCENE_VALID_CD];
			$this->SceneName = $myRow['SCENE_NAME];
			$this->SceneDescription = $myRow['SCENE_DESCRIPTION];
			$this->SceneTimeMin = $myRow['SCENE_TIME_MIN];
			$this->SceneTimeSec = $myRow['SCENE_TIME_SEC];
			$this->CopySourceId = $myRow['COPY_SOURCE_ID];
		}//end while
    }//end function

// -------------------------------------------------------------
//    DB更新　INSERT
// -------------------------------------------------------------
	function clsSwSceneDbInsert($mySqlConnObj){
		//ﾌﾟﾛﾊﾟﾃｨ取得
		$valSceneId = $this->SceneId;
		$valScenarioId = $this->ScenarioId;
		$valSceneOrderNo = $this->SceneOrderNo;
		$valSceneValidCd = $this->SceneValidCd;
		$valSceneName = $this->SceneName;
		$valSceneDescription = $this->SceneDescription;
		$valSceneTimeMin = $this->SceneTimeMin;
		$valSceneTimeSec = $this->SceneTimeSec;
		$valCopySourceId = $this->CopySourceId;
		//INSERT SQL
		$strSQL = <<<END_OF_SQL

		INSERT INTO SW_SCENE(
			  SCENARIO_ID
			, SCENE_ORDER_NO
			, SCENE_VALID_CD
			, SCENE_NAME
			, SCENE_DESCRIPTION
			, SCENE_TIME_MIN
			, SCENE_TIME_SEC
			, COPY_SOURCE_ID
			) values (
			  :ScenarioId
			, :SceneOrderNo
			, :SceneValidCd
			, :SceneName
			, :SceneDescription
			, :SceneTimeMin
			, :SceneTimeSec
			, :CopySourceId
			);
END_OF_SQL;


		$stmt = $mySqlConnObj->prepare($strSQL);
		$stmt->bindParam(':ScenarioId', $valScenarioId, PDO::PARAM_INT);
		$stmt->bindParam(':SceneOrderNo', $valSceneOrderNo, PDO::PARAM_INT);
		$stmt->bindParam(':SceneValidCd', $valSceneValidCd, PDO::PARAM_INT);
		$stmt->bindParam(':SceneName', $valSceneName, PDO::PARAM_INT);
		$stmt->bindParam(':SceneDescription', $valSceneDescription, PDO::PARAM_INT);
		$stmt->bindParam(':SceneTimeMin', $valSceneTimeMin, PDO::PARAM_INT);
		$stmt->bindParam(':SceneTimeSec', $valSceneTimeSec, PDO::PARAM_INT);
		$stmt->bindParam(':CopySourceId', $valCopySourceId, PDO::PARAM_INT);	
		$stmt->execute();
		//AUTO_INCREMENT値取得
		$LastInsertId = $mySqlConnObj->lastInsertId();
		//AUTO_INCREMENT値を返す
		return $LastInsertId;
	}//end function

// -------------------------------------------------------------
//    DB更新　UPDATE
// -------------------------------------------------------------
	function clsSwSceneDbUpdate($mySqlConnObj){
		//プロパティ取得
		$valSceneId = $this->SceneId;
		$valScenarioId = $this->ScenarioId;
		$valSceneOrderNo = $this->SceneOrderNo;
		$valSceneValidCd = $this->SceneValidCd;
		$valSceneName = $this->SceneName;
		$valSceneDescription = $this->SceneDescription;
		$valSceneTimeMin = $this->SceneTimeMin;
		$valSceneTimeSec = $this->SceneTimeSec;
		$valCopySourceId = $this->CopySourceId;
		//登録確認
		$strSQL = <<<END_OF_SQL

		SELECT * FROM SW_SCENE
		WHERE SCENE_ID = :SceneId;
END_OF_SQL;


		$stmt = $mySqlConnObj->prepare($strSQL);
		//パラメータのセット
		$stmt->bindParam(':SceneId', $valSceneId, PDO::PARAM_INT);	
		$stmt->execute();
		//件数取得
		$myRowCnt = $stmt->rowCount();
		if($myRowCnt == 0 ){
            echo $this->fncAlert("clsSwScene ->> NOT ENTRY!");
        }else{
			//UPDATE SQL
			$strSQL = <<<END_OF_SQL

			UPDATE SW_SCENE SET 
				  SCENARIO_ID = '$valScenarioId'
				, SCENE_ORDER_NO = '$valSceneOrderNo'
				, SCENE_VALID_CD = '$valSceneValidCd'
				, SCENE_NAME = '$valSceneName'
				, SCENE_DESCRIPTION = '$valSceneDescription'
				, SCENE_TIME_MIN = '$valSceneTimeMin'
				, SCENE_TIME_SEC = '$valSceneTimeSec'
				, COPY_SOURCE_ID = '$valCopySourceId'
		WHERE SCENE_ID = :SceneId;
END_OF_SQL;


			$stmt = $mySqlConnObj->prepare($strSQL);	
		//パラメータのセット
		$stmt->bindParam(':SceneId', $valSceneId, PDO::PARAM_INT);	
			$stmt->execute();
		}//end if
	}//end function

// -------------------------------------------------------------
//    DB更新　DELETE
// -------------------------------------------------------------
	function $classNameDbDelete($mySqlConnObj){
		//ﾌﾟﾛﾊﾟﾃｨ取得
		$valSceneId = $this->SceneId;
		//登録確認
		$strSQL = <<<END_OF_SQL

		SELECT * FROM SW_SCENE
			WHERE SCENE_ID = :SceneId;
END_OF_SQL;

		$stmt = $mySqlConnObj->prepare($strSQL);
		//パラメータのセット
		$stmt->bindParam(':SceneId', $valSceneId, PDO::PARAM_INT);	
		$stmt->execute();
		//件数取得
		$myRowCnt = $myResult->num_rows;
		if($myRowCnt == 0 ){
            echo $this->fncAlert("clsSwScene ->> NOT ENTRY!");
        }else{
			//DELETE SQL
			$strSQL = <<<END_OF_SQL

			DELETE FROM SW_SCENE
			WHERE SCENE_ID = :SceneId;
END_OF_SQL;


			$stmt = $mySqlConnObj->prepare($strSQL);	
		//パラメータのセット
		$stmt->bindParam(':SceneId', $valSceneId, PDO::PARAM_INT);	
			$stmt->execute();
		}//end if
	}//end function
	// ------------------------------------------------------------------------------
	// java script alert
	// ------------------------------------------------------------------------------
	function fncAlert($AlertMsg){
		$strAlert = <<<END_OF_TEXT

			<script language="JavaScript">
				bootbox.alert("$AlertMsg",function() {
			});
			</script>
END_OF_TEXT;

		return $strAlert;
	}
	
} // end class
// -----------------------------------------------------------
?>