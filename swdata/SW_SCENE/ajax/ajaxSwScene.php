<?php
// -----------------------------------------------------------
//
// Copyright (C) 2016 ScenarioWriterCafe All Rights Reserved.
// 
//     SW_SCENE I/O ｼｽﾃﾑ
//
//     ajaxSwScene.php
// -----------------------------------------------------------
// ------------------------------------------------------------------------------
	//定数読み込み
	include_once("../../sw_config/swConstant.php");
	//DB接続ｸﾗｽの初期化
	include_once("../include/ConnectMySQL.php");
// ------------------------------------------------------------------------------
	//共通関数をｲﾝｸﾙｰﾄﾞ
	include_once("../include/swFunc.php");
	//ﾌｫｰﾑのﾃﾞｰﾀを読み込む
	fncGetPostItems();

	//MainProcedure
	fncMainProc($mySqlConnObj);

	exit();

// ------------------------------------------------------------------------------
//      POSTされた要素を取得
//          fncGetPostItems()
// ------------------------------------------------------------------------------
function fncGetPostItems(){
	//共通global変数
	global	$SubMode;
	//ﾌｫｰﾑ name要素をglobal変数にする
	global	$fdtSceneId;
	global	$fdtScenarioId;
	global	$fdtSceneOrderNo;
	global	$fdtSceneValidCd;
	global	$fdtSceneName;
	global	$fdtSceneDescription;
	global	$fdtSceneTimeMin;
	global	$fdtSceneTimeSec;
	global	$fdtCopySourceId;

	//処理ﾓｰﾄが空白ならreturnするﾞ
	if(!isset($_POST['SubMode'])){return;}
	//処理ﾓｰﾄﾞ
	$SubMode = swFunc_GetPostData('SubMode');
	if($SubMode == ''){return;}
	//POSTされた要素を取得
	$fdtSceneId = swFunc_GetPostData('fdtSceneId');                  //SCENE_ID
	$fdtScenarioId = swFunc_GetPostData('fdtScenarioId');            //SCENARIO_ID
	$fdtSceneOrderNo = swFunc_GetPostData('fdtSceneOrderNo');        //場面順番
	$fdtSceneValidCd = swFunc_GetPostData('fdtSceneValidCd');        //有効
	$fdtSceneName = swFunc_GetPostData('fdtSceneName');              //場面名称
	$fdtSceneDescription = swFunc_GetPostData('fdtSceneDescription');//場面説明
	$fdtSceneTimeMin = swFunc_GetPostData('fdtSceneTimeMin');        //時間（分）
	$fdtSceneTimeSec = swFunc_GetPostData('fdtSceneTimeSec');        //時間（秒）
	$fdtCopySourceId = swFunc_GetPostData('fdtCopySourceId');        //複写元
}//end function

// ------------------------------------------------------------------------------
//      MAIN PROCEDURE
//          fncMainProc($mySqlConnObj)
// ------------------------------------------------------------------------------
function fncMainProc($mySqlConnObj){
	global	$SubMode;
	//global 変数
	global	$fdtSceneId;
	global	$fdtScenarioId;
	global	$fdtSceneOrderNo;
	global	$fdtSceneValidCd;
	global	$fdtSceneName;
	global	$fdtSceneDescription;
	global	$fdtSceneTimeMin;
	global	$fdtSceneTimeSec;
	global	$fdtCopySourceId;

	//SubModeで処理を制御
	if($SubMode == 'SwSceneList'){
		$resultHtml = fncMakeSwSceneList($mySqlConnObj);
	}

	// 出力charsetをUTF-8に指定
	mb_http_output ( 'UTF-8' );
	// 出力
	echo($resultHtml);
}//end function

//--------------------------------------------------------------------------------
// SW_SCENE ALL LIST
//--------------------------------------------------------------------------------
function fncMakeSwSceneList($mySqlConnObj){
	//ﾘｽﾄのﾍｯﾀﾞｰ
	$retHtml = <<<END_OF_HTML
	
		<div class="scroll_div">
		<table class="table" _fixedhead="rows:1;div-full-mode: no;">
			<tr>
				<th>SCENE_ID</th>
				<th>SCENARIO_ID</th>
				<th>場面順番</th>
				<th>有効</th>
				<th>場面名称</th>
			</tr>
END_OF_HTML;

	//DB から ﾃﾞｰﾀを取得しﾌﾟﾛﾊﾟﾃｨにｾｯﾄする
	$strSQL = <<<END_OF_SQL
		SELECT *,SCENE_ID as KEY_ITEM
			FROM SW_SCENE
					ORDER BY SCENE_ID;
END_OF_SQL;
	//SQLを実行
	$myResult = $mySqlConnObj->query($strSQL);
	while($myRow = $myResult->fetch_assoc()){
		$valSceneId = $myRow['SCENE_ID'];
		$valScenarioId = $myRow['SCENARIO_ID'];
		$valSceneOrderNo = $myRow['SCENE_ORDER_NO'];
		$valSceneValidCd = $myRow['SCENE_VALID_CD'];
		$valSceneName = $myRow['SCENE_NAME'];
		$valKeyItem = $myRow['KEY_ITEM'];
		//ﾘｽﾄ
		$retHtml .= <<<END_OF_HTML
		
			<tr onclick="fncSelectSwScene('$valKeyItem');">
				<td>$valSceneId</td>
				<td>$valScenarioId</td>
				<td>$valSceneOrderNo</td>
				<td>$valSceneValidCd</td>
				<td>$valSceneName</td>
			</tr>
END_OF_HTML;
		}//end while

		$retHtml .= <<<END_OF_HTML
		
		</table>
		</div>
END_OF_HTML;
	//結果セットを開放
	$myResult->free();
	//HTMLを返す
	return $retHtml;
}
