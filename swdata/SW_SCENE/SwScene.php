<?php
// -----------------------------------------------------------
//
// Copyright (C) 2016 ScenarioWriterCafe All Rights Reserved.
// 
//     SW_SCENE I/O ｼｽﾃﾑ
//
//     SwScene.php
// -----------------------------------------------------------
// ------------------------------------------------------------------------------
	//定数読み込み
	include_once("../sw_config/swConstant.php");
	//DB接続ｸﾗｽの初期化
	include_once("./include/ConnectMySQL.php");
// ------------------------------------------------------------------------------
	//ﾃﾞﾌｫﾙﾄｱｸｼｮﾝ
	$ThisPHP = 'SwScene.php';
	//ﾃﾞｰﾀ管理ｸﾗｽ
	include_once("./class/clsSwScene.php");
	$clsSwScene = new clsSwScene();
	//共通関数をｲﾝｸﾙｰﾄﾞ
	include_once("./include/swFunc.php");
	// ﾍｯﾀﾞ表示
	$HtmlTitle = "場面設定";
	include_once("./include/swHeader.php");

	//ﾌｫｰﾑのﾃﾞｰﾀを読み込む
	fncGetPostItems();

	//MainProcedure
	fncMainProc($mySqlConnObj);

	exit();

// ------------------------------------------------------------------------------
//      POSTされた要素を取得
//          fncGetPostItems()
// ------------------------------------------------------------------------------
function fncGetPostItems(){
	//共通global変数
	global	$SubmitMode;
	//ﾌｫｰﾑ name要素をglobal変数にする
	global	$fdtSceneId;
	global	$fdtScenarioId;
	global	$fdtSceneOrderNo;
	global	$fdtSceneValidCd;
	global	$fdtSceneName;
	global	$fdtSceneDescription;
	global	$fdtSceneTimeMin;
	global	$fdtSceneTimeSec;
	global	$fdtCopySourceId;

	//処理ﾓｰﾄが空白ならreturnするﾞ
	if(!isset($_POST['SubmitMode'])){return;}
	//処理ﾓｰﾄﾞ
	$SubmitMode = swFunc_GetPostData('SubmitMode');
	if($SubmitMode == ''){return;}
	//POSTされた要素を取得
	$fdtSceneId = swFunc_GetPostData('fdtSceneId');                  //SCENE_ID
	$fdtScenarioId = swFunc_GetPostData('fdtScenarioId');            //SCENARIO_ID
	$fdtSceneOrderNo = swFunc_GetPostData('fdtSceneOrderNo');        //場面順番
	$fdtSceneValidCd = swFunc_GetPostData('fdtSceneValidCd');        //有効
	$fdtSceneName = swFunc_GetPostData('fdtSceneName');              //場面名称
	$fdtSceneDescription = swFunc_GetPostData('fdtSceneDescription');//場面説明
	$fdtSceneTimeMin = swFunc_GetPostData('fdtSceneTimeMin');        //時間（分）
	$fdtSceneTimeSec = swFunc_GetPostData('fdtSceneTimeSec');        //時間（秒）
	$fdtCopySourceId = swFunc_GetPostData('fdtCopySourceId');        //複写元
}//end function

// ------------------------------------------------------------------------------
//      MAIN PROCEDURE
//          fncMainProc($mySqlConnObj)
// ------------------------------------------------------------------------------
function fncMainProc($mySqlConnObj){
	global	$SubmitMode;
	//class
	global	$clsSwScene;

	global	$fdtSceneId;
	global	$fdtScenarioId;
	global	$fdtSceneOrderNo;
	global	$fdtSceneValidCd;
	global	$fdtSceneName;
	global	$fdtSceneDescription;
	global	$fdtSceneTimeMin;
	global	$fdtSceneTimeSec;
	global	$fdtCopySourceId;

	//SubmitModeで処理を分岐
	switch($SubmitMode ){
		case 'SELECT':
			//ﾃﾞｰﾀ管理ｸﾗｽの初期化
			$clsSwScene->clsSwSceneInit($mySqlConnObj,$fdtSceneId);
			//ﾌﾟﾛﾊﾟﾃｨGet
			fncSwSceneGetProperty($clsSwScene);
			break;
		case 'INSERT':
			//登録処理
			fncSwSceneDBInsert($mySqlConnObj,$clsSwScene);
			//選択状態にする
			$SubmitMode = 'SELECT';
			//ﾃﾞｰﾀ管理ｸﾗｽの初期化
			$clsSwScene->clsSwSceneInit($mySqlConnObj,$fdtSceneId);
			//ﾌﾟﾛﾊﾟﾃｨGet
			fncSwSceneGetProperty($clsSwScene);
			break;
		case 'UPDATE':
			//更新処理
			fncSwSceneDBUpdate($mySqlConnObj,$clsSwScene);
			//選択状態にする
			$SubmitMode = 'SELECT';
			//ﾃﾞｰﾀ管理ｸﾗｽの初期化
			$clsSwScene->clsSwSceneInit($mySqlConnObj,$fdtSceneId);
			//ﾌﾟﾛﾊﾟﾃｨGet
			fncSwSceneGetProperty($clsSwScene);
			break;
		case 'DELETE':
			//削除処理
			fncSwSceneDBDelete($mySqlConnObj,$clsSwScene);
			//ﾌﾟﾛﾊﾟﾃｨReset
			fncSwSceneResetProperty($clsSwScene);
			//初期状態にする
			$SubmitMode = 'RESET';
			break;
		case 'RESET':
			//ﾌﾟﾛﾊﾟﾃｨReset
			fncSwSceneResetProperty($clsSwScene);
			//初期状態にする
			$SubmitMode = 'RESET';
			break;
		default:
			break;
	}
	//ﾌｫｰﾑの表示
	fncMainForm($mySqlConnObj);
}//end function

// ------------------------------------------------------------------------------
//      MAIN FORM
//          fncMainForm($mySqlConnObj)
// ------------------------------------------------------------------------------
function fncMainForm($mySqlConnObj){
	global	$ThisPHP,$SubmitMode;
	global	$fdtSceneId;
	global	$fdtScenarioId;
	global	$fdtSceneOrderNo;
	global	$fdtSceneValidCd;
	global	$fdtSceneName;
	global	$fdtSceneDescription;
	global	$fdtSceneTimeMin;
	global	$fdtSceneTimeSec;
	global	$fdtCopySourceId;

	//css powerd by Bootstrap ver3
	print <<<END_OF_HTML

	<div class="wrap" >
	
		<div class="container-fluid">
		
		<!-- ﾒﾆｭｰ 制御ﾎﾞﾀﾝ 表示ROW -->
			<div class="row">
				<div class="col-sm-12">
				<legend class="form-inline submenu">
				<ul class="list-inline">
				  <li>
END_OF_HTML;

	//管理メニュー ﾄﾞﾛｯﾌﾟﾀﾞｳﾝﾒﾆｭｰ
	$TargetForm = 'frmSwScene';
	include_once("./include/swMgrDropDownMenu.php");

	
    //SubmitModeで表示するボタンを制御する
	switch($SubmitMode){
		case 'SELECT':
			print <<<END_OF_HTML
			 	</li>
			 	<li>
			        <input type="button" value="RESET"
			            id="btnReset"
			            onclick="fncSwSceneSubmit(frmSwScene,'RESET','FALSE','');"
			            class="btn btn-default btn-sm">
			        <span style="width: 120px;"></span>
			        <input type="button" value="INSERT"
			            id="btnInsert"
			            onclick="fncSwSceneSubmit(frmSwScene,'INSERT','TRUE','新規登録');"
			            class="btn btn-warning btn-sm">
			        <input type="button" value="UPDATE"
			            id="btnUpdate"
			            onclick="fncSwSceneSubmit(frmSwScene,'UPDATE','TRUE','更新');"
			            class="btn btn-warning btn-sm">
			        <input type="button" value="DELETE"
			            id="btnDelete"
			            onclick="fncSwSceneSubmit(frmSwScene,'DELETE','TRUE','削除');"
			            class="btn btn-danger btn-sm">
			
				</li>
END_OF_HTML;

	
			break;
        default:
			print <<<END_OF_HTML
			    </li>
			 	<li>
			        <input type="button" value="RESET"
			            id="btnReset"
			            onclick="fncSwSceneSubmit(frmSwScene,'RESET','FALSE','');"
			            class="btn btn-default btn-sm">
			        <input type="button" value="INSERT"
			            id="btnInsert"
			            onclick="fncSwSceneSubmit(frmSwScene,'INSERT','TRUE','新規登録');"
			            class="btn btn-warning btn-sm">
			
				</li>
END_OF_HTML;
            break;
    }
	print <<<END_OF_HTML

					</ul>
					</legend>
    			</div><!-- col-sm-12 end -->
    		</div><!-- row end -->
    		<!-- ﾒﾆｭｰ 制御ﾎﾞﾀﾝ 表示ROW ここまで-->

    		<!-- ﾌｫｰﾑ と ﾘｽﾄ -->
    		<div class="row row-0">
				<div class="col-sm-7 row-0">
					<form class="form-horizontal" role="form" name="frmSwScene" id="frmSwScene" method="POST" action="$ThisPHP">
						<input type="hidden" name="SubmitMode" id="SubmitMode" value="$SubmitMode">

END_OF_HTML;

	print <<<END_OF_HTML

							<div class="form-group row-0">
								<label for="fdtSceneId" class="control-label col-xs-3">SCENE_ID</label>
								<div class="col-xs-8">
									<input type="number"
										class="form-control input-sm text-right"
										id="fdtSceneId" name="fdtSceneId"
										value="$fdtSceneId"
										onkeyup="AjaxFunc_AddFigure(frmSwScene,'fdtSceneId');"
										placeholder="SCENE_ID">
								</div>
							</div>
END_OF_HTML;
	print <<<END_OF_HTML

							<div class="form-group row-0">
								<label for="fdtScenarioId" class="control-label col-xs-3">SCENARIO_ID</label>
								<div class="col-xs-8">
									<input type="number"
										class="form-control input-sm text-right"
										id="fdtScenarioId" name="fdtScenarioId"
										value="$fdtScenarioId"
										onkeyup="AjaxFunc_AddFigure(frmSwScene,'fdtScenarioId');"
										placeholder="SCENARIO_ID">
								</div>
							</div>
END_OF_HTML;
	print <<<END_OF_HTML

							<div class="form-group row-0">
								<label for="fdtSceneOrderNo" class="control-label col-xs-3">場面順番</label>
								<div class="col-xs-8">
									<input type="number"
										class="form-control input-sm text-right"
										id="fdtSceneOrderNo" name="fdtSceneOrderNo"
										value="$fdtSceneOrderNo"
										onkeyup="AjaxFunc_AddFigure(frmSwScene,'fdtSceneOrderNo');"
										placeholder="場面順番">
								</div>
							</div>
END_OF_HTML;
	print <<<END_OF_HTML

							<div class="form-group row-0">
								<label for="fdtSceneValidCd" class="control-label col-xs-3">有効</label>
								<div class="col-xs-8">
								
END_OF_HTML;

	//------------------------------------------------------------
	//CSVﾃﾞｰﾀからselect box を作成する
	$ObjName = "fdtSceneValidCd";		//select box の名称.ID
	//ここでfunctionからCSVﾃﾞｰﾀを取得
	$csvArray = swFunc_MakeSelectItemsCsvSample();	//CSVﾃﾞｰﾀ
	$default = "$fdtSceneValidCd";		//ﾃﾞﾌｫﾙﾄ値
	$onChange = '';					//onChange で起動する javascript or jQuery
	$ViewCode = FALSE;				//ｺｰﾄﾞを表示する場合はTRUE
	//SelectBoxHtml出力
	print swFunc_MakeSelectBox($ObjName,$csvArray,$default,$onChange,$ViewCode);
//------------------------------------------------------------

	print <<<END_OF_HTML

								</div>
							</div>
END_OF_HTML;
	print <<<END_OF_HTML

							<div class="form-group row-0">
								<label for="fdtSceneName" class="control-label col-xs-3">場面名称</label>
								<div class="col-xs-8">
									<input type="text" 
										class="form-control input-sm"
										id="fdtSceneName" name="fdtSceneName"
										value="$fdtSceneName"
										placeholder="場面名称">
								</div>
							</div>
END_OF_HTML;
	print <<<END_OF_HTML

							<div class="form-group row-0">
								<label for="fdtSceneDescription" class="control-label col-xs-3">場面説明</label>
								<div class="col-xs-8">
									<textarea placeholder="場面説明" 
										class="form-control input-sm"
										id="fdtSceneDescription" name="fdtSceneDescription"
										rows="3"
										id="InputTextarea">$fdtSceneDescription</textarea>
								</div>
							</div>
END_OF_HTML;
	print <<<END_OF_HTML

							<div class="form-group row-0">
								<label for="fdtSceneTimeMin" class="control-label col-xs-3">時間（分）</label>
								<div class="col-xs-8">
									<input type="number"
										class="form-control input-sm text-right"
										id="fdtSceneTimeMin" name="fdtSceneTimeMin"
										value="$fdtSceneTimeMin"
										onkeyup="AjaxFunc_AddFigure(frmSwScene,'fdtSceneTimeMin');"
										placeholder="時間（分）">
								</div>
							</div>
END_OF_HTML;
	print <<<END_OF_HTML

							<div class="form-group row-0">
								<label for="fdtSceneTimeSec" class="control-label col-xs-3">時間（秒）</label>
								<div class="col-xs-8">
									<input type="number"
										class="form-control input-sm text-right"
										id="fdtSceneTimeSec" name="fdtSceneTimeSec"
										value="$fdtSceneTimeSec"
										onkeyup="AjaxFunc_AddFigure(frmSwScene,'fdtSceneTimeSec');"
										placeholder="時間（秒）">
								</div>
							</div>
END_OF_HTML;
	print <<<END_OF_HTML

							<div class="form-group row-0">
								<label for="fdtCopySourceId" class="control-label col-xs-3">複写元</label>
								<div class="col-xs-8">
									<input type="number"
										class="form-control input-sm text-right"
										id="fdtCopySourceId" name="fdtCopySourceId"
										value="$fdtCopySourceId"
										onkeyup="AjaxFunc_AddFigure(frmSwScene,'fdtCopySourceId');"
										placeholder="複写元">
								</div>
							</div>
END_OF_HTML;
	print <<<END_OF_HTML

						</form>
					</div>
					<div class="col-sm-5" >
	                	<!-- リストの表示位置 -->
	                    <span id="side_list_area"></span>
					</div>
				</div>
			</div>
 
		</div><!--container-->
	</div><!--wrap-->


    <script type="text/javascript" src="./ajax/ajaxSwScene.js"></script>
    <script type="text/javascript">
        fncMakeSwSceneList(frmSwScene);
    </script>


	<!--footer表示位置--><span class="sw-footer"></span>
	<script language="JavaScript">
		AjaxFunc_FooterLoad();
	</script>
	
  </body>
</html>

END_OF_HTML;

}//end function

// ------------------------------------------------------------------------------
//      ﾌﾟﾛﾊﾟﾃｨReset
//          fncSwSceneResetProperty($clsSwScene)
// ------------------------------------------------------------------------------
function fncSwSceneResetProperty($clsSwScene){
	global	$fdtSceneId;
	global	$fdtScenarioId;
	global	$fdtSceneOrderNo;
	global	$fdtSceneValidCd;
	global	$fdtSceneName;
	global	$fdtSceneDescription;
	global	$fdtSceneTimeMin;
	global	$fdtSceneTimeSec;
	global	$fdtCopySourceId;

	//ﾌﾟﾛﾊﾟﾃｨReset
	$fdtSceneId = "";                         //SCENE_ID
	$fdtScenarioId = "";                      //SCENARIO_ID
	$fdtSceneOrderNo = "";                    //場面順番
	$fdtSceneValidCd = "";                    //有効
	$fdtSceneName = "";                       //場面名称
	$fdtSceneDescription = "";                //場面説明
	$fdtSceneTimeMin = "";                    //時間（分）
	$fdtSceneTimeSec = "";                    //時間（秒）
	$fdtCopySourceId = "";                    //複写元
}//end function

// ------------------------------------------------------------------------------
//      ﾌﾟﾛﾊﾟﾃｨGet
//          fncSwSceneGetProperty($clsSwScene)
// ------------------------------------------------------------------------------
function fncSwSceneGetProperty($clsSwScene){
	global	$fdtSceneId;
	global	$fdtScenarioId;
	global	$fdtSceneOrderNo;
	global	$fdtSceneValidCd;
	global	$fdtSceneName;
	global	$fdtSceneDescription;
	global	$fdtSceneTimeMin;
	global	$fdtSceneTimeSec;
	global	$fdtCopySourceId;

	//ﾌﾟﾛﾊﾟﾃｨGet
	$fdtSceneId = $clsSwScene->clsSwSceneGetSceneId();                  //SCENE_ID
	$fdtScenarioId = $clsSwScene->clsSwSceneGetScenarioId();            //SCENARIO_ID
	$fdtSceneOrderNo = $clsSwScene->clsSwSceneGetSceneOrderNo();        //場面順番
	$fdtSceneValidCd = $clsSwScene->clsSwSceneGetSceneValidCd();        //有効
	$fdtSceneName = $clsSwScene->clsSwSceneGetSceneName();              //場面名称

	//場面説明は<br>を\nに変換する
	$fdtSceneDescription = $clsSwScene->clsSwSceneGetSceneDescription();//場面説明
	$fdtSceneDescription = str_replace("<br>","\n",$fdtSceneDescription);

	$fdtSceneTimeMin = $clsSwScene->clsSwSceneGetSceneTimeMin();        //時間（分）
	$fdtSceneTimeSec = $clsSwScene->clsSwSceneGetSceneTimeSec();        //時間（秒）
	$fdtCopySourceId = $clsSwScene->clsSwSceneGetCopySourceId();        //複写元
}//end function

// ------------------------------------------------------------------------------
//      ﾌﾟﾛﾊﾟﾃｨSet
//          fncSwSceneSetProperty($clsSwScene)
// ------------------------------------------------------------------------------
function fncSwSceneSetProperty($clsSwScene){
	global	$fdtSceneId;
	global	$fdtScenarioId;
	global	$fdtSceneOrderNo;
	global	$fdtSceneValidCd;
	global	$fdtSceneName;
	global	$fdtSceneDescription;
	global	$fdtSceneTimeMin;
	global	$fdtSceneTimeSec;
	global	$fdtCopySourceId;

	//ﾌﾟﾛﾊﾟﾃｨSet

	//SCENE_IDはｶﾝﾏを取り除いてからSetする
	$fdtSceneId = str_replace(",","",$fdtSceneId);
	$clsSwScene->clsSwSceneSetSceneId($fdtSceneId);                  //SCENE_ID


	//SCENARIO_IDはｶﾝﾏを取り除いてからSetする
	$fdtScenarioId = str_replace(",","",$fdtScenarioId);
	$clsSwScene->clsSwSceneSetScenarioId($fdtScenarioId);            //SCENARIO_ID


	//場面順番はｶﾝﾏを取り除いてからSetする
	$fdtSceneOrderNo = str_replace(",","",$fdtSceneOrderNo);
	$clsSwScene->clsSwSceneSetSceneOrderNo($fdtSceneOrderNo);        //場面順番

	$clsSwScene->clsSwSceneSetSceneValidCd($fdtSceneValidCd);        //有効
	$clsSwScene->clsSwSceneSetSceneName($fdtSceneName);              //場面名称

	//場面説明は改行を<br>にしてからSetする
	$fdtSceneDescription = str_replace("\r\n","<br>",$fdtSceneDescription);
	$fdtSceneDescription = str_replace("\r","<br>",$fdtSceneDescription);
	$fdtSceneDescription = str_replace("\n","<br>",$fdtSceneDescription);
	$clsSwScene->clsSwSceneSetSceneDescription($fdtSceneDescription);//場面説明


	//時間（分）はｶﾝﾏを取り除いてからSetする
	$fdtSceneTimeMin = str_replace(",","",$fdtSceneTimeMin);
	$clsSwScene->clsSwSceneSetSceneTimeMin($fdtSceneTimeMin);        //時間（分）


	//時間（秒）はｶﾝﾏを取り除いてからSetする
	$fdtSceneTimeSec = str_replace(",","",$fdtSceneTimeSec);
	$clsSwScene->clsSwSceneSetSceneTimeSec($fdtSceneTimeSec);        //時間（秒）


	//複写元はｶﾝﾏを取り除いてからSetする
	$fdtCopySourceId = str_replace(",","",$fdtCopySourceId);
	$clsSwScene->clsSwSceneSetCopySourceId($fdtCopySourceId);        //複写元

}//end function

// ------------------------------------------------------------------------------
//      DB INSERT
//          fncSwSceneDBInsert($mySqlConnObj,$clsSwScene)
// ------------------------------------------------------------------------------
function fncSwSceneDBInsert($mySqlConnObj,$clsSwScene){
	global	$fdtSceneId;

	//ﾌﾟﾛﾊﾟﾃｨｾｯﾄ
	fncSwSceneSetProperty($clsSwScene);
	//ﾃﾞｰﾀ管理ｸﾗｽ DbInsert
	$fdtSceneId = $clsSwScene->clsSwSceneDbInsert($mySqlConnObj);
}//end function

// ------------------------------------------------------------------------------
//      DB UPDATE
//          fncSwSceneDBUpdate($mySqlConnObj,$clsSwScene)
// ------------------------------------------------------------------------------
function fncSwSceneDBUpdate($mySqlConnObj,$clsSwScene){
	//ﾌﾟﾛﾊﾟﾃｨｾｯﾄ
	fncSwSceneSetProperty($clsSwScene);
	//ﾃﾞｰﾀ管理ｸﾗｽ DbUpdate
	$clsSwScene->clsSwSceneDbUpdate($mySqlConnObj);
}//end function

// ------------------------------------------------------------------------------
//      DB DELETE
//          fncSwSceneDBDelete($mySqlConnObj,$clsSwScene)
// ------------------------------------------------------------------------------
function fncSwSceneDBDelete($mySqlConnObj,$clsSwScene){
	//ﾌﾟﾛﾊﾟﾃｨｾｯﾄ
	fncSwSceneSetProperty($clsSwScene);
	//ﾃﾞｰﾀ管理ｸﾗｽ DbDelete
	$clsSwScene->clsSwSceneDbDelete($mySqlConnObj);
}//end function


// -----------------------------------------------------------
?>