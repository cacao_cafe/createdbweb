<?php
// -----------------------------------------------------------
//
// Copyright (C) 2016 ScenarioWriterCafe All Rights Reserved.
// 
//     CREATE TABLE SW_SCENE
//
//     createTable_SwScene.php
// -----------------------------------------------------------
$Result = fncCreateTable();
echo $Result;
exit;


// -----------------------------------------------------------
//     fncCreateTable()
// -----------------------------------------------------------
function fncCreateTable(){
	//共通定数
	include_once("../../sw_config/swConstant.php");

	//MySQLに接続
	include_once("../include/ConnectMySQL.php");

	//DROP TABLE SQLを編集
	$strSQL = <<<END_OF_SQL

DROP TABLE IF EXISTS SW_SCENE;

END_OF_SQL;

	//DROP TABLE EXECUTE 
	$stmt = $mySqlConnObj->prepare($strSQL);
	$stmt->execute();

	//CREATE TABLE SQLを編集
	$strSQL = <<<END_OF_SQL

CREATE TABLE SW_SCENE(
	  SCENE_ID                         INT NOT NULL AUTO_INCREMENT COMMENT 'SCENE_ID'
	, SCENARIO_ID                      INT  COMMENT 'SCENARIO_ID'
	, SCENE_ORDER_NO                   DOUBLE  COMMENT '場面順番'
	, SCENE_VALID_CD                   INT  COMMENT '有効'
	, SCENE_NAME                       VARCHAR(256)  COMMENT '場面名称'
	, SCENE_DESCRIPTION                MEDIUMTEXT  COMMENT '場面説明'
	, SCENE_TIME_MIN                   INT  COMMENT '時間（分）'
	, SCENE_TIME_SEC                   INT  COMMENT '時間（秒）'
	, COPY_SOURCE_ID                   INT  COMMENT '複写元'
	, PRIMARY KEY (SCENE_ID)
	)DEFAULT CHARACTER SET 'UTF8'
	COMMENT '場面設定';

END_OF_SQL;

	//CREATE TABLE EXECUTE 
	$stmt = $mySqlConnObj->prepare($strSQL);
	$stmt->execute();

	//UNIQUE INDEX のSQLを編集
	$strSQL = <<<END_OF_SQL

	CREATE UNIQUE INDEX SW_SCENE_KEY_SCENE_ID ON SW_SCENE(SCENE_ID);
END_OF_SQL;

	//CREATE INDEX EXECUTE 
	$stmt = $mySqlConnObj->prepare($strSQL);
	$stmt->execute();


	//DB切断
	include_once("../include/DisConnectMySQL.php");
	
	
	return	'CREATE TABLE SW_SCENE Success...';
}//end function
// -----------------------------------------------------------
?>