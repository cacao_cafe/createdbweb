<?php
// -----------------------------------------------------------
//
// Copyright (C) 2015 ScenarioWriterCafe All Rights Reserved.
// 
//     CREATE TABLE SW_USER
//
//     createTable_SwUser.php
// -----------------------------------------------------------
$Result = fncCreateTable();
echo $Result;
exit;


// -----------------------------------------------------------
//     fncCreateTable()
// -----------------------------------------------------------
function fncCreateTable(){
	//共通定数
	include_once("../../sw_config/swConstant.php");

	//MySQLに接続
	include_once("../include/ConnectMySQL.php");

	//DROP TABLE SQLを編集
	$strSQL = <<<END_OF_SQL

DROP TABLE IF EXISTS SW_USER;

END_OF_SQL;

	//DROP TABLE EXECUTE 
	$myResult = $mySqlConnObj->query($strSQL) or die($this->fncAlert($mySqlConnObj->error));

	//CREATE TABLE SQLを編集
	$strSQL = <<<END_OF_SQL

CREATE TABLE SW_USER(
	  USER_ID                          INT NOT NULL AUTO_INCREMENT COMMENT 'ユーザーID'
	, USER_MAILAD                      VARCHAR(256)  COMMENT 'メールアドレス'
	, USER_PASSWD                      VARCHAR(256)  COMMENT 'パスワード'
	, USER_NAME                        VARCHAR(256)  COMMENT 'ユーザー名'
	, USER_EXPIRATION_DATE             DATE  COMMENT '利用期限'
	, USER_MAX_SCENARIO                INT  COMMENT '作成可能シナリオ数'
	, PRIMARY KEY (USER_ID)
	)DEFAULT CHARACTER SET 'UTF8'
	COMMENT 'USER_INFO';

END_OF_SQL;

	//CREATE TABLE EXECUTE 
	$myResult = $mySqlConnObj->query($strSQL) or die($this->fncAlert($mySqlConnObj->error));

	//UNIQUE INDEX のSQLを編集
	$strSQL = <<<END_OF_SQL

	CREATE UNIQUE INDEX SW_USER_KEY_USER_ID ON SW_USER(USER_ID);
END_OF_SQL;

	//CREATE INDEX EXECUTE 
	$myResult = $mySqlConnObj->query($strSQL) or die($this->fncAlert($mySqlConnObj->error));


	//DB切断
	include_once("../include/DisConnectMySQL.php");
	
	
	return	'CREATE TABLE SW_USER Success...';
}//end function
// -----------------------------------------------------------
?>