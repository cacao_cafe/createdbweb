<?php
// -----------------------------------------------------------
//
// Copyright (C) 2015 ScenarioWriterCafe All Rights Reserved.
// 
//     USER_INFO ﾃﾞｰﾀ管理ｸﾗｽ
//     clsSwUser
// -----------------------------------------------------------
class clsSwUser{
	var $UserId;                          //ユーザーID
	var $UserMailad;                      //メールアドレス
	var $UserPasswd;                      //パスワード
	var $UserName;                        //ユーザー名
	var $UserExpirationDate;              //利用期限
	var $UserMaxScenario;                 //作成可能シナリオ数
// ｺﾝｽﾄﾗｸﾀ
    function clsSwUser(){
    }
// ﾌﾟﾛﾊﾟﾃｨ
// --------------------------------------
// ﾌﾟﾛﾊﾟﾃｨ Get UserId
//    ユーザーID を返す
// --------------------------------------
	function clsSwUserGetUserId(){
		return $this->UserId;
	}
// --------------------------------------
// ﾌﾟﾛﾊﾟﾃｨ Set UserId
//    ユーザーID を設定する
// --------------------------------------
	function clsSwUserSetUserId($valUserId){
		$this->UserId = $valUserId;
	}
// --------------------------------------
// ﾌﾟﾛﾊﾟﾃｨ Get UserMailad
//    メールアドレス を返す
// --------------------------------------
	function clsSwUserGetUserMailad(){
		return $this->UserMailad;
	}
// --------------------------------------
// ﾌﾟﾛﾊﾟﾃｨ Set UserMailad
//    メールアドレス を設定する
// --------------------------------------
	function clsSwUserSetUserMailad($valUserMailad){
		$this->UserMailad = $valUserMailad;
	}
// --------------------------------------
// ﾌﾟﾛﾊﾟﾃｨ Get UserPasswd
//    パスワード を返す
// --------------------------------------
	function clsSwUserGetUserPasswd(){
		return $this->UserPasswd;
	}
// --------------------------------------
// ﾌﾟﾛﾊﾟﾃｨ Set UserPasswd
//    パスワード を設定する
// --------------------------------------
	function clsSwUserSetUserPasswd($valUserPasswd){
		$this->UserPasswd = $valUserPasswd;
	}
// --------------------------------------
// ﾌﾟﾛﾊﾟﾃｨ Get UserName
//    ユーザー名 を返す
// --------------------------------------
	function clsSwUserGetUserName(){
		return $this->UserName;
	}
// --------------------------------------
// ﾌﾟﾛﾊﾟﾃｨ Set UserName
//    ユーザー名 を設定する
// --------------------------------------
	function clsSwUserSetUserName($valUserName){
		$this->UserName = $valUserName;
	}
// --------------------------------------
// ﾌﾟﾛﾊﾟﾃｨ Get UserExpirationDate
//    利用期限 を返す
// --------------------------------------
	function clsSwUserGetUserExpirationDate(){
		return $this->UserExpirationDate;
	}
// --------------------------------------
// ﾌﾟﾛﾊﾟﾃｨ Set UserExpirationDate
//    利用期限 を設定する
// --------------------------------------
	function clsSwUserSetUserExpirationDate($valUserExpirationDate){
		$this->UserExpirationDate = $valUserExpirationDate;
	}
// --------------------------------------
// ﾌﾟﾛﾊﾟﾃｨ Get UserMaxScenario
//    作成可能シナリオ数 を返す
// --------------------------------------
	function clsSwUserGetUserMaxScenario(){
		return $this->UserMaxScenario;
	}
// --------------------------------------
// ﾌﾟﾛﾊﾟﾃｨ Set UserMaxScenario
//    作成可能シナリオ数 を設定する
// --------------------------------------
	function clsSwUserSetUserMaxScenario($valUserMaxScenario){
		if ($valUserMaxScenario == '' ){
			$valUserMaxScenario = '5';
		}
		$this->UserMaxScenario = $valUserMaxScenario;
	}
// -------------------------------------------------------------
//    ｸﾗｽ初期化
// -------------------------------------------------------------
	function clsSwUserInit($mySqlConnObj,$valUserId){
		//ﾌﾟﾛﾊﾟﾃｨの初期化
		$this->UserId = '';                          //ユーザーID
		$this->UserMailad = '';                      //メールアドレス
		$this->UserPasswd = '';                      //パスワード
		$this->UserName = '';                        //ユーザー名
		$this->UserExpirationDate = '';              //利用期限
		$this->UserMaxScenario = '5';                //作成可能シナリオ数
		//DB から ﾃﾞｰﾀを取得しﾌﾟﾛﾊﾟﾃｨにｾｯﾄする
		$strSQL = <<<END_OF_SQL

		SELECT * FROM SW_USER
				WHERE USER_ID = '$valUserId';
END_OF_SQL;

		$myResult = $mySqlConnObj->query($strSQL) or die($this->fncAlert($mySqlConnObj->error));
		while($myRow = $myResult->fetch_assoc( )){
			$this->UserId = $myRow['USER_ID'];
			$this->UserMailad = $myRow['USER_MAILAD'];
			$this->UserPasswd = $myRow['USER_PASSWD'];
			$this->UserName = $myRow['USER_NAME'];
			$this->UserExpirationDate = $myRow['USER_EXPIRATION_DATE'];
			$this->UserMaxScenario = $myRow['USER_MAX_SCENARIO'];
		}//end while
    }//end function

// -------------------------------------------------------------
//    DB更新　INSERT
// -------------------------------------------------------------
	function clsSwUserDbInsert($mySqlConnObj){
		//ﾌﾟﾛﾊﾟﾃｨ取得
		$valUserId = $this->UserId;
		$valUserMailad = $this->UserMailad;
		$valUserPasswd = $this->UserPasswd;
		$valUserName = $this->UserName;
		$valUserExpirationDate = $this->UserExpirationDate;
		$valUserMaxScenario = $this->UserMaxScenario;
			//INSERT SQL
			$strSQL = <<<END_OF_SQL

			INSERT INTO SW_USER(
				  USER_MAILAD
				, USER_PASSWD
				, USER_NAME
				, USER_EXPIRATION_DATE
				, USER_MAX_SCENARIO
			) values (
				  '$valUserMailad'
				, '$valUserPasswd'
				, '$valUserName'
				, '$valUserExpirationDate'
				, '$valUserMaxScenario'
			);
END_OF_SQL;

			//INSRET 
			$myResult = $mySqlConnObj->query($strSQL) or die($this->fncAlert($mySqlConnObj->error));

			//AUTO_INCREMENT値取得
			$LastInsertId = $mySqlConnObj->insert_id;
			//AUTO_INCREMENT値を返す
			return $LastInsertId;
	}//end function

// -------------------------------------------------------------
//    DB更新　UPDATE
// -------------------------------------------------------------
	function clsSwUserDbUpdate($mySqlConnObj){
		//ﾌﾟﾛﾊﾟﾃｨ取得
		$valUserId = $this->UserId;
		$valUserMailad = $this->UserMailad;
		$valUserPasswd = $this->UserPasswd;
		$valUserName = $this->UserName;
		$valUserExpirationDate = $this->UserExpirationDate;
		$valUserMaxScenario = $this->UserMaxScenario;
		//登録確認
		$strSQL = <<<END_OF_SQL

		SELECT * FROM SW_USER
				WHERE USER_ID = '$valUserId';
END_OF_SQL;

		//登録確認SQL Execute 
		$myResult = $mySqlConnObj->query($strSQL) or die($this->fncAlert($mySqlConnObj->error));

		//件数取得
		$myRowCnt = $myResult->num_rows;
		if($myRowCnt == 0 ){
			echo $this->fncAlert("clsSwUser ->> NOT ENTRY!");
		}else{
			//UPDATE SQL
			$strSQL = <<<END_OF_SQL

			UPDATE SW_USER SET 
				  USER_MAILAD = '$valUserMailad'
				, USER_PASSWD = '$valUserPasswd'
				, USER_NAME = '$valUserName'
				, USER_EXPIRATION_DATE = '$valUserExpirationDate'
				, USER_MAX_SCENARIO = '$valUserMaxScenario'
				WHERE USER_ID = '$valUserId';
END_OF_SQL;

			//UPDATESQL Execute 
			$myResult = $mySqlConnObj->query($strSQL) or die($this->fncAlert($mySqlConnObj->error));

		}//end if
	}//end function

// -------------------------------------------------------------
//    DB更新　DELETE
// -------------------------------------------------------------
	function clsSwUserDbDelete($mySqlConnObj){
		//ﾌﾟﾛﾊﾟﾃｨ取得
		$valUserId = $this->UserId;
		//登録確認
		$strSQL = <<<END_OF_SQL

		SELECT * FROM SW_USER
				WHERE USER_ID = '$valUserId';
END_OF_SQL;

		//登録確認SQL Execute 
		$myResult = $mySqlConnObj->query($strSQL) or die($this->fncAlert($mySqlConnObj->error));

		//件数取得
		$myRowCnt = $myResult->num_rows;
		if($myRowCnt == 0 ){
			echo $this->fncAlert("clsSwUser ->> NOT ENTRY!");
		}else{
			//DELETE SQL
			$strSQL = <<<END_OF_SQL

			DELETE FROM SW_USER
				WHERE USER_ID = '$valUserId';
END_OF_SQL;

			//DELETE SQL Execute 
			$myResult = $mySqlConnObj->query($strSQL) or die($this->fncAlert($mySqlConnObj->error));

		}//end if
	}//end function

	// ------------------------------------------------------------------------------
	// java script alert
	// ------------------------------------------------------------------------------
	function fncAlert($AlertMsg){
		$strAlert = <<<END_OF_TEXT

			<script language="JavaScript">
				bootbox.alert("$AlertMsg",function() {
			});
			</script>
END_OF_TEXT;

		return $strAlert;
	}
	
} // end class
// -----------------------------------------------------------
?>