<?php
// -----------------------------------------------------------
//
// Copyright (C) 2015 ScenarioWriterCafe All Rights Reserved.
// 
//     SW_USER I/O ｼｽﾃﾑ
//
//     ajaxSwUser.php
// -----------------------------------------------------------
// ------------------------------------------------------------------------------
	//定数読み込み
	include_once("../../sw_config/swConstant.php");
	//DB接続ｸﾗｽの初期化
	include_once("../include/ConnectMySQL.php");
// ------------------------------------------------------------------------------
	//共通関数をｲﾝｸﾙｰﾄﾞ
	include_once("../include/swFunc.php");
	//ﾌｫｰﾑのﾃﾞｰﾀを読み込む
	fncGetPostItems();

	//MainProcedure
	fncMainProc($mySqlConnObj);

	exit();

// ------------------------------------------------------------------------------
//      POSTされた要素を取得
//          fncGetPostItems()
// ------------------------------------------------------------------------------
function fncGetPostItems(){
	//共通global変数
	global	$SubMode;
	//ﾌｫｰﾑ name要素をglobal変数にする
	global	$fdtUserId;
	global	$fdtUserMailad;
	global	$fdtUserPasswd;
	global	$fdtUserName;
	global	$fdtUserExpirationDate;
	global	$fdtUserMaxScenario;

	//処理ﾓｰﾄが空白ならreturnするﾞ
	if(!isset($_POST['SubMode'])){return;}
	//処理ﾓｰﾄﾞ
	$SubMode = swFunc_GetPostData('SubMode');
	if($SubMode == ''){return;}
	//POSTされた要素を取得
	$fdtUserId = swFunc_GetPostData('fdtUserId');                    //ユーザーID
	$fdtUserMailad = swFunc_GetPostData('fdtUserMailad');            //メールアドレス
	$fdtUserPasswd = swFunc_GetPostData('fdtUserPasswd');            //パスワード
	$fdtUserName = swFunc_GetPostData('fdtUserName');                //ユーザー名
	$fdtUserExpirationDate = swFunc_GetPostData('fdtUserExpirationDate');//利用期限

	//ｶﾚﾝﾀﾞｰ日付要素
	$fdtUserExpirationDateYY = swFunc_GetPostData('fdtUserExpirationDateYY');//利用期限年
	$fdtUserExpirationDateMM = swFunc_GetPostData('fdtUserExpirationDateMM');//利用期限月
	$fdtUserExpirationDateDD = swFunc_GetPostData('fdtUserExpirationDateDD');//利用期限日

	$fdtUserMaxScenario = swFunc_GetPostData('fdtUserMaxScenario');  //作成可能シナリオ数
}//end function

// ------------------------------------------------------------------------------
//      MAIN PROCEDURE
//          fncMainProc($mySqlConnObj)
// ------------------------------------------------------------------------------
function fncMainProc($mySqlConnObj){
	global	$SubMode;
	//global 変数
	global	$fdtUserId;
	global	$fdtUserMailad;
	global	$fdtUserPasswd;
	global	$fdtUserName;
	global	$fdtUserExpirationDate;
	global	$fdtUserMaxScenario;

	//SubModeで処理を制御
	if($SubMode == 'SwUserList'){
		$resultHtml = fncMakeSwUserList($mySqlConnObj);
	}

	// 出力charsetをUTF-8に指定
	mb_http_output ( 'UTF-8' );
	// 出力
	echo($resultHtml);
}//end function

//--------------------------------------------------------------------------------
// SW_USER ALL LIST
//--------------------------------------------------------------------------------
function fncMakeSwUserList($mySqlConnObj){
	//ﾘｽﾄのﾍｯﾀﾞｰ
	$retHtml = <<<END_OF_HTML
	
		<div class="scroll_div">
		<table class="table" _fixedhead="rows:1;div-full-mode: no;">
			<tr>
				<th>メールアドレス</th>
				<th>ユーザー名</th>
				<th>利用期限</th>
				<th>作成可能シナリオ数</th>
			</tr>
END_OF_HTML;

	//DB から ﾃﾞｰﾀを取得しﾌﾟﾛﾊﾟﾃｨにｾｯﾄする
	$strSQL = <<<END_OF_SQL
		SELECT *,USER_ID as KEY_ITEM
			FROM SW_USER
					ORDER BY USER_ID;
END_OF_SQL;
	//SQLを実行
	$myResult = $mySqlConnObj->query($strSQL);
	while($myRow = $myResult->fetch_assoc()){
		$valUserMailad = $myRow['USER_MAILAD'];
		$valUserName = $myRow['USER_NAME'];
		$valUserExpirationDate = $myRow['USER_EXPIRATION_DATE'];
		$valUserMaxScenario = $myRow['USER_MAX_SCENARIO'];
		$valKeyItem = $myRow['KEY_ITEM'];
		//ﾘｽﾄ
		$retHtml .= <<<END_OF_HTML
		
			<tr onclick="fncSelectSwUser('$valKeyItem');">
				<td>$valUserMailad</td>
				<td>$valUserName</td>
				<td>$valUserExpirationDate</td>
				<td>$valUserMaxScenario</td>
			</tr>
END_OF_HTML;
		}//end while

		$retHtml .= <<<END_OF_HTML
		
		</table>
		</div>
END_OF_HTML;
	//結果セットを開放
	$myResult->free();
	//HTMLを返す
	return $retHtml;
}
