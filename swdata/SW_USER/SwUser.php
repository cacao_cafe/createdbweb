<?php
// -----------------------------------------------------------
//
// Copyright (C) 2015 ScenarioWriterCafe All Rights Reserved.
// 
//     SW_USER I/O ｼｽﾃﾑ
//
//     SwUser.php
// -----------------------------------------------------------
// ------------------------------------------------------------------------------
	//定数読み込み
	include_once("../sw_config/swConstant.php");
	//DB接続ｸﾗｽの初期化
	include_once("./include/ConnectMySQL.php");
// ------------------------------------------------------------------------------
	//ﾃﾞﾌｫﾙﾄｱｸｼｮﾝ
	$ThisPHP = 'SwUser.php';
	//ﾃﾞｰﾀ管理ｸﾗｽ
	include_once("./class/clsSwUser.php");
	$clsSwUser = new clsSwUser();
	//共通関数をｲﾝｸﾙｰﾄﾞ
	include_once("./include/swFunc.php");
	// ﾍｯﾀﾞ表示
	$HtmlTitle = "USER_INFO";
	include_once("./include/swHeader.php");

	//ﾌｫｰﾑのﾃﾞｰﾀを読み込む
	fncGetPostItems();

	//MainProcedure
	fncMainProc($mySqlConnObj);

	exit();

// ------------------------------------------------------------------------------
//      POSTされた要素を取得
//          fncGetPostItems()
// ------------------------------------------------------------------------------
function fncGetPostItems(){
	//共通global変数
	global	$SubmitMode;
	//ﾌｫｰﾑ name要素をglobal変数にする
	global	$fdtUserId;
	global	$fdtUserMailad;
	global	$fdtUserPasswd;
	global	$fdtUserName;
	global	$fdtUserExpirationDate;
	global	$fdtUserExpirationDateYY,$fdtUserExpirationDateMM,$fdtUserExpirationDateDD;
	global	$fdtUserMaxScenario;

	//処理ﾓｰﾄが空白ならreturnするﾞ
	if(!isset($_POST['SubmitMode'])){return;}
	//処理ﾓｰﾄﾞ
	$SubmitMode = swFunc_GetPostData('SubmitMode');
	if($SubmitMode == ''){return;}
	//POSTされた要素を取得
	$fdtUserId = swFunc_GetPostData('fdtUserId');                    //ユーザーID
	$fdtUserMailad = swFunc_GetPostData('fdtUserMailad');            //メールアドレス
	$fdtUserPasswd = swFunc_GetPostData('fdtUserPasswd');            //パスワード
	$fdtUserName = swFunc_GetPostData('fdtUserName');                //ユーザー名
	$fdtUserExpirationDate = swFunc_GetPostData('fdtUserExpirationDate');//利用期限

	//ｶﾚﾝﾀﾞｰ日付要素
	$fdtUserExpirationDateYY = swFunc_GetPostData('fdtUserExpirationDateYY');//利用期限年
	$fdtUserExpirationDateMM = swFunc_GetPostData('fdtUserExpirationDateMM');//利用期限月
	$fdtUserExpirationDateDD = swFunc_GetPostData('fdtUserExpirationDateDD');//利用期限日

	$fdtUserMaxScenario = swFunc_GetPostData('fdtUserMaxScenario');  //作成可能シナリオ数
}//end function

// ------------------------------------------------------------------------------
//      MAIN PROCEDURE
//          fncMainProc($mySqlConnObj)
// ------------------------------------------------------------------------------
function fncMainProc($mySqlConnObj){
	global	$SubmitMode;
	//class
	global	$clsSwUser;

	global	$fdtUserId;
	global	$fdtUserMailad;
	global	$fdtUserPasswd;
	global	$fdtUserName;
	global	$fdtUserExpirationDate;
	global	$fdtUserExpirationDateYY,$fdtUserExpirationDateMM,$fdtUserExpirationDateDD;
	global	$fdtUserMaxScenario;

	//SubmitModeで処理を分岐
	switch($SubmitMode ){
		case 'SELECT':
			//ﾃﾞｰﾀ管理ｸﾗｽの初期化
			$clsSwUser->clsSwUserInit($mySqlConnObj,$fdtUserId);
			//ﾌﾟﾛﾊﾟﾃｨGet
			fncSwUserGetProperty($clsSwUser);
			break;
		case 'INSERT':
			//登録処理
			fncSwUserDBInsert($mySqlConnObj,$clsSwUser);
			//選択状態にする
			$SubmitMode = 'SELECT';
			//ﾃﾞｰﾀ管理ｸﾗｽの初期化
			$clsSwUser->clsSwUserInit($mySqlConnObj,$fdtUserId);
			//ﾌﾟﾛﾊﾟﾃｨGet
			fncSwUserGetProperty($clsSwUser);
			break;
		case 'UPDATE':
			//更新処理
			fncSwUserDBUpdate($mySqlConnObj,$clsSwUser);
			//選択状態にする
			$SubmitMode = 'SELECT';
			//ﾃﾞｰﾀ管理ｸﾗｽの初期化
			$clsSwUser->clsSwUserInit($mySqlConnObj,$fdtUserId);
			//ﾌﾟﾛﾊﾟﾃｨGet
			fncSwUserGetProperty($clsSwUser);
			break;
		case 'DELETE':
			//削除処理
			fncSwUserDBDelete($mySqlConnObj,$clsSwUser);
			//ﾌﾟﾛﾊﾟﾃｨReset
			fncSwUserResetProperty($clsSwUser);
			//初期状態にする
			$SubmitMode = 'RESET';
			break;
		case 'RESET':
			//ﾌﾟﾛﾊﾟﾃｨReset
			fncSwUserResetProperty($clsSwUser);
			//初期状態にする
			$SubmitMode = 'RESET';
			break;
		default:
			break;
	}
	//ﾌｫｰﾑの表示
	fncMainForm($mySqlConnObj);
}//end function

// ------------------------------------------------------------------------------
//      MAIN FORM
//          fncMainForm($mySqlConnObj)
// ------------------------------------------------------------------------------
function fncMainForm($mySqlConnObj){
	global	$ThisPHP,$SubmitMode;
	global	$fdtUserId;
	global	$fdtUserMailad;
	global	$fdtUserPasswd;
	global	$fdtUserName;
	global	$fdtUserExpirationDate;
	global	$fdtUserExpirationDateYY,$fdtUserExpirationDateMM,$fdtUserExpirationDateDD;
	global	$fdtUserMaxScenario;

	//css powerd by Bootstrap ver3
	print <<<END_OF_HTML

	<div class="wrap" >
	
		<div class="container-fluid">
		
		<!-- ﾒﾆｭｰ 制御ﾎﾞﾀﾝ 表示ROW -->
			<div class="row">
				<div class="col-sm-12">
				<legend class="form-inline submenu">
				<ul class="list-inline">
				  <li>
END_OF_HTML;

	//管理メニュー ﾄﾞﾛｯﾌﾟﾀﾞｳﾝﾒﾆｭｰ
	$TargetForm = 'frmSwUser';
	include_once("./include/swMgrDropDownMenu.php");

	
    //SubmitModeで表示するボタンを制御する
	switch($SubmitMode){
		case 'SELECT':
			print <<<END_OF_HTML
			 	</li>
			 	<li>
			        <input type="button" value="RESET"
			            id="btnReset"
			            onclick="fncSwUserSubmit(frmSwUser,'RESET','FALSE','');"
			            class="btn btn-default btn-sm">
			        <span style="width: 120px;"></span>
			        <input type="button" value="INSERT"
			            id="btnInsert"
			            onclick="fncSwUserSubmit(frmSwUser,'INSERT','TRUE','新規登録');"
			            class="btn btn-warning btn-sm">
			        <input type="button" value="UPDATE"
			            id="btnUpdate"
			            onclick="fncSwUserSubmit(frmSwUser,'UPDATE','TRUE','更新');"
			            class="btn btn-warning btn-sm">
			        <input type="button" value="DELETE"
			            id="btnDelete"
			            onclick="fncSwUserSubmit(frmSwUser,'DELETE','TRUE','削除');"
			            class="btn btn-danger btn-sm">
			
				</li>
END_OF_HTML;

	
			break;
        default:
			print <<<END_OF_HTML
			    </li>
			 	<li>
			        <input type="button" value="RESET"
			            id="btnReset"
			            onclick="fncSwUserSubmit(frmSwUser,'RESET','FALSE','');"
			            class="btn btn-default btn-sm">
			        <input type="button" value="INSERT"
			            id="btnInsert"
			            onclick="fncSwUserSubmit(frmSwUser,'INSERT','TRUE','新規登録');"
			            class="btn btn-warning btn-sm">
			
				</li>
END_OF_HTML;
            break;
    }
	print <<<END_OF_HTML

					</ul>
					</legend>
    			</div><!-- col-sm-12 end -->
    		</div><!-- row end -->
    		<!-- ﾒﾆｭｰ 制御ﾎﾞﾀﾝ 表示ROW ここまで-->

    		<!-- ﾌｫｰﾑ と ﾘｽﾄ -->
    		<div class="row row-0">
				<div class="col-sm-7 row-0">
					<form class="form-horizontal" role="form" name="frmSwUser" id="frmSwUser" method="POST" action="$ThisPHP">
						<input type="hidden" name="SubmitMode" id="SubmitMode" value="$SubmitMode">

END_OF_HTML;

	print <<<END_OF_HTML

							<div class="form-group row-0">
								<label for="fdtUserId" class="control-label col-xs-3">ユーザーID</label>
								<div class="col-xs-8">
									<input type="number"
										class="form-control input-sm text-right"
										id="fdtUserId" name="fdtUserId"
										value="$fdtUserId"
										onkeyup="AjaxFunc_AddFigure(frmSwUser,'fdtUserId');"
										placeholder="ユーザーID">
								</div>
							</div>
END_OF_HTML;
	print <<<END_OF_HTML

							<div class="form-group row-0">
								<label for="fdtUserMailad" class="control-label col-xs-3">メールアドレス</label>
								<div class="col-xs-8">
									<input type="text" 
										class="form-control input-sm"
										id="fdtUserMailad" name="fdtUserMailad"
										value="$fdtUserMailad"
										placeholder="メールアドレス">
								</div>
							</div>
END_OF_HTML;
	print <<<END_OF_HTML

							<div class="form-group row-0">
								<label for="fdtUserPasswd" class="control-label col-xs-3">パスワード</label>
								<div class="col-xs-8">
									<input type="text" 
										class="form-control input-sm"
										id="fdtUserPasswd" name="fdtUserPasswd"
										value="$fdtUserPasswd"
										placeholder="パスワード">
								</div>
							</div>
END_OF_HTML;
	print <<<END_OF_HTML

							<div class="form-group row-0">
								<label for="fdtUserName" class="control-label col-xs-3">ユーザー名</label>
								<div class="col-xs-8">
									<input type="text" 
										class="form-control input-sm"
										id="fdtUserName" name="fdtUserName"
										value="$fdtUserName"
										placeholder="ユーザー名">
								</div>
							</div>
END_OF_HTML;
	print <<<END_OF_HTML

							<div class="form-group row-0">
								<label for="fdtUserExpirationDate" class="control-label col-xs-3">利用期限</label>
								<div class="form-inline col-xs-8"><!-- calendar size inlileにする-->
								
END_OF_HTML;

	//------------------------------------------------------------
	//利用期限を ｶﾚﾝﾀﾞｰ 付日付選択で作成
	$ObjName = "fdtUserExpirationDate";		//ObjName名 **YY,**MM,**DD で作成する
	$default = "$fdtUserExpirationDate";		//ﾃﾞﾌｫﾙﾄ値
	$OnChange = '';					//onChange で起動する javascript or jQuery
	print swFunc_MakeCalendarBox($mySqlConnObj,'frmSwUser',$ObjName,$default,TRUE,$OnChange);
//------------------------------------------------------------

	print <<<END_OF_HTML

								</div>
							</div>
END_OF_HTML;
	print <<<END_OF_HTML

							<div class="form-group row-0">
								<label for="fdtUserMaxScenario" class="control-label col-xs-3">作成可能シナリオ数</label>
								<div class="col-xs-8">
									<input type="number"
										class="form-control input-sm text-right"
										id="fdtUserMaxScenario" name="fdtUserMaxScenario"
										value="$fdtUserMaxScenario"
										onkeyup="AjaxFunc_AddFigure(frmSwUser,'fdtUserMaxScenario');"
										placeholder="作成可能シナリオ数">
								</div>
							</div>
END_OF_HTML;
	print <<<END_OF_HTML

						</form>
					</div>
					<div class="col-sm-5" >
	                	<!-- リストの表示位置 -->
	                    <span id="side_list_area"></span>
					</div>
				</div>
			</div>
 
		</div><!--container-->
	</div><!--wrap-->


    <script type="text/javascript" src="./ajax/ajaxSwUser.js"></script>
    <script type="text/javascript">
        fncMakeSwUserList(frmSwUser);
    </script>


	<!--footer表示位置--><span class="sw-footer"></span>
	<script language="JavaScript">
		AjaxFunc_FooterLoad();
	</script>
	
  </body>
</html>

END_OF_HTML;

}//end function

// ------------------------------------------------------------------------------
//      ﾌﾟﾛﾊﾟﾃｨReset
//          fncSwUserResetProperty($clsSwUser)
// ------------------------------------------------------------------------------
function fncSwUserResetProperty($clsSwUser){
	global	$fdtUserId;
	global	$fdtUserMailad;
	global	$fdtUserPasswd;
	global	$fdtUserName;
	global	$fdtUserExpirationDate;
	global	$fdtUserExpirationDateYY,$fdtUserExpirationDateMM,$fdtUserExpirationDateDD;
	global	$fdtUserMaxScenario;

	//ﾌﾟﾛﾊﾟﾃｨReset
	$fdtUserId = "";                          //ユーザーID
	$fdtUserMailad = "";                      //メールアドレス
	$fdtUserPasswd = "";                      //パスワード
	$fdtUserName = "";                        //ユーザー名
	$fdtUserExpirationDate = "";              //利用期限
	$fdtUserMaxScenario = "5";                 //作成可能シナリオ数
}//end function

// ------------------------------------------------------------------------------
//      ﾌﾟﾛﾊﾟﾃｨGet
//          fncSwUserGetProperty($clsSwUser)
// ------------------------------------------------------------------------------
function fncSwUserGetProperty($clsSwUser){
	global	$fdtUserId;
	global	$fdtUserMailad;
	global	$fdtUserPasswd;
	global	$fdtUserName;
	global	$fdtUserExpirationDate;
	global	$fdtUserExpirationDateYY,$fdtUserExpirationDateMM,$fdtUserExpirationDateDD;
	global	$fdtUserMaxScenario;

	//ﾌﾟﾛﾊﾟﾃｨGet
	$fdtUserId = $clsSwUser->clsSwUserGetUserId();                    //ユーザーID
	$fdtUserMailad = $clsSwUser->clsSwUserGetUserMailad();            //メールアドレス
	$fdtUserPasswd = $clsSwUser->clsSwUserGetUserPasswd();            //パスワード
	$fdtUserName = $clsSwUser->clsSwUserGetUserName();                //ユーザー名
	$fdtUserExpirationDate = $clsSwUser->clsSwUserGetUserExpirationDate();//利用期限
	$fdtUserMaxScenario = $clsSwUser->clsSwUserGetUserMaxScenario();  //作成可能シナリオ数
}//end function

// ------------------------------------------------------------------------------
//      ﾌﾟﾛﾊﾟﾃｨSet
//          fncSwUserSetProperty($clsSwUser)
// ------------------------------------------------------------------------------
function fncSwUserSetProperty($clsSwUser){
	global	$fdtUserId;
	global	$fdtUserMailad;
	global	$fdtUserPasswd;
	global	$fdtUserName;
	global	$fdtUserExpirationDate;
	global	$fdtUserExpirationDateYY,$fdtUserExpirationDateMM,$fdtUserExpirationDateDD;
	global	$fdtUserMaxScenario;

	//ﾌﾟﾛﾊﾟﾃｨSet

	//ユーザーIDはｶﾝﾏを取り除いてからSetする
	$fdtUserId = str_replace(",","",$fdtUserId);
	$clsSwUser->clsSwUserSetUserId($fdtUserId);                    //ユーザーID

	$clsSwUser->clsSwUserSetUserMailad($fdtUserMailad);            //メールアドレス
	$clsSwUser->clsSwUserSetUserPasswd($fdtUserPasswd);            //パスワード
	$clsSwUser->clsSwUserSetUserName($fdtUserName);                //ユーザー名

	//利用期限は日付要素を結合してからSetする
	$fdtUserExpirationDate = $fdtUserExpirationDateYY.'/'.$fdtUserExpirationDateMM.'/'.$fdtUserExpirationDateDD;
	if ($fdtUserExpirationDate == "//"){$fdtUserExpirationDate = "";}
	if ($fdtUserExpirationDate == "0000/00/00"){$fdtUserExpirationDate = "";}
	$clsSwUser->clsSwUserSetUserExpirationDate($fdtUserExpirationDate);//利用期限


	//作成可能シナリオ数はｶﾝﾏを取り除いてからSetする
	$fdtUserMaxScenario = str_replace(",","",$fdtUserMaxScenario);
	$clsSwUser->clsSwUserSetUserMaxScenario($fdtUserMaxScenario);  //作成可能シナリオ数

}//end function

// ------------------------------------------------------------------------------
//      DB INSERT
//          fncSwUserDBInsert($mySqlConnObj,$clsSwUser)
// ------------------------------------------------------------------------------
function fncSwUserDBInsert($mySqlConnObj,$clsSwUser){
	global	$fdtUserId;

	//ﾌﾟﾛﾊﾟﾃｨｾｯﾄ
	fncSwUserSetProperty($clsSwUser);
	//ﾃﾞｰﾀ管理ｸﾗｽ DbInsert
	$fdtUserId = $clsSwUser->clsSwUserDbInsert($mySqlConnObj);
}//end function

// ------------------------------------------------------------------------------
//      DB UPDATE
//          fncSwUserDBUpdate($mySqlConnObj,$clsSwUser)
// ------------------------------------------------------------------------------
function fncSwUserDBUpdate($mySqlConnObj,$clsSwUser){
	//ﾌﾟﾛﾊﾟﾃｨｾｯﾄ
	fncSwUserSetProperty($clsSwUser);
	//ﾃﾞｰﾀ管理ｸﾗｽ DbUpdate
	$clsSwUser->clsSwUserDbUpdate($mySqlConnObj);
}//end function

// ------------------------------------------------------------------------------
//      DB DELETE
//          fncSwUserDBDelete($mySqlConnObj,$clsSwUser)
// ------------------------------------------------------------------------------
function fncSwUserDBDelete($mySqlConnObj,$clsSwUser){
	//ﾌﾟﾛﾊﾟﾃｨｾｯﾄ
	fncSwUserSetProperty($clsSwUser);
	//ﾃﾞｰﾀ管理ｸﾗｽ DbDelete
	$clsSwUser->clsSwUserDbDelete($mySqlConnObj);
}//end function


// -----------------------------------------------------------
?>