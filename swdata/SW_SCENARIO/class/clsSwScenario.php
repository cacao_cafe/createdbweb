<?php
// -----------------------------------------------------------
//
// Copyright (C) 2015 ScenarioWriterCafe All Rights Reserved.
// 
//      ﾃﾞｰﾀ管理ｸﾗｽ
//     clsSwScenario
// -----------------------------------------------------------
class clsSwScenario{
	var $ScenarioId;                      //シナリオID
	var $UserId;                          //USER_ID
	var $ScenarioTitle;                   //タイトル
	var $ScenarioSubtitle;                //サブタイトル
	var $ScenarioWriterName;              //作者名
	var $ScenarioMemo;                    //メモ
	var $ScenarioDate;                    //執筆日
// ｺﾝｽﾄﾗｸﾀ
    function clsSwScenario(){
    }
// ﾌﾟﾛﾊﾟﾃｨ
// --------------------------------------
// ﾌﾟﾛﾊﾟﾃｨ Get ScenarioId
//    シナリオID を返す
// --------------------------------------
	function clsSwScenarioGetScenarioId(){
		return $this->ScenarioId;
	}
// --------------------------------------
// ﾌﾟﾛﾊﾟﾃｨ Set ScenarioId
//    シナリオID を設定する
// --------------------------------------
	function clsSwScenarioSetScenarioId($valScenarioId){
		$this->ScenarioId = $valScenarioId;
	}
// --------------------------------------
// ﾌﾟﾛﾊﾟﾃｨ Get UserId
//    USER_ID を返す
// --------------------------------------
	function clsSwScenarioGetUserId(){
		return $this->UserId;
	}
// --------------------------------------
// ﾌﾟﾛﾊﾟﾃｨ Set UserId
//    USER_ID を設定する
// --------------------------------------
	function clsSwScenarioSetUserId($valUserId){
		$this->UserId = $valUserId;
	}
// --------------------------------------
// ﾌﾟﾛﾊﾟﾃｨ Get ScenarioTitle
//    タイトル を返す
// --------------------------------------
	function clsSwScenarioGetScenarioTitle(){
		return $this->ScenarioTitle;
	}
// --------------------------------------
// ﾌﾟﾛﾊﾟﾃｨ Set ScenarioTitle
//    タイトル を設定する
// --------------------------------------
	function clsSwScenarioSetScenarioTitle($valScenarioTitle){
		$this->ScenarioTitle = $valScenarioTitle;
	}
// --------------------------------------
// ﾌﾟﾛﾊﾟﾃｨ Get ScenarioSubtitle
//    サブタイトル を返す
// --------------------------------------
	function clsSwScenarioGetScenarioSubtitle(){
		return $this->ScenarioSubtitle;
	}
// --------------------------------------
// ﾌﾟﾛﾊﾟﾃｨ Set ScenarioSubtitle
//    サブタイトル を設定する
// --------------------------------------
	function clsSwScenarioSetScenarioSubtitle($valScenarioSubtitle){
		$this->ScenarioSubtitle = $valScenarioSubtitle;
	}
// --------------------------------------
// ﾌﾟﾛﾊﾟﾃｨ Get ScenarioWriterName
//    作者名 を返す
// --------------------------------------
	function clsSwScenarioGetScenarioWriterName(){
		return $this->ScenarioWriterName;
	}
// --------------------------------------
// ﾌﾟﾛﾊﾟﾃｨ Set ScenarioWriterName
//    作者名 を設定する
// --------------------------------------
	function clsSwScenarioSetScenarioWriterName($valScenarioWriterName){
		$this->ScenarioWriterName = $valScenarioWriterName;
	}
// --------------------------------------
// ﾌﾟﾛﾊﾟﾃｨ Get ScenarioMemo
//    メモ を返す
// --------------------------------------
	function clsSwScenarioGetScenarioMemo(){
		return $this->ScenarioMemo;
	}
// --------------------------------------
// ﾌﾟﾛﾊﾟﾃｨ Set ScenarioMemo
//    メモ を設定する
// --------------------------------------
	function clsSwScenarioSetScenarioMemo($valScenarioMemo){
		$this->ScenarioMemo = $valScenarioMemo;
	}
// --------------------------------------
// ﾌﾟﾛﾊﾟﾃｨ Get ScenarioDate
//    執筆日 を返す
// --------------------------------------
	function clsSwScenarioGetScenarioDate(){
		return $this->ScenarioDate;
	}
// --------------------------------------
// ﾌﾟﾛﾊﾟﾃｨ Set ScenarioDate
//    執筆日 を設定する
// --------------------------------------
	function clsSwScenarioSetScenarioDate($valScenarioDate){
		$this->ScenarioDate = $valScenarioDate;
	}
// -------------------------------------------------------------
//    ｸﾗｽ初期化
// -------------------------------------------------------------
	function clsSwScenarioInit($mySqlConnObj,$valScenarioId){
		//ﾌﾟﾛﾊﾟﾃｨの初期化
		$this->ScenarioId = '';                      //シナリオID
		$this->UserId = '';                          //USER_ID
		$this->ScenarioTitle = '';                   //タイトル
		$this->ScenarioSubtitle = '';                //サブタイトル
		$this->ScenarioWriterName = '';              //作者名
		$this->ScenarioMemo = '';                    //メモ
		$this->ScenarioDate = '';                    //執筆日
		//DB から ﾃﾞｰﾀを取得しﾌﾟﾛﾊﾟﾃｨにｾｯﾄする
		$strSQL = <<<END_OF_SQL

		SELECT * FROM SW_SCENARIO
				WHERE SCENARIO_ID = '$valScenarioId';
END_OF_SQL;

		$myResult = $mySqlConnObj->query($strSQL) or die($this->fncAlert($mySqlConnObj->error));
		while($myRow = $myResult->fetch_assoc( )){
			$this->ScenarioId = $myRow['SCENARIO_ID'];
			$this->UserId = $myRow['USER_ID'];
			$this->ScenarioTitle = $myRow['SCENARIO_TITLE'];
			$this->ScenarioSubtitle = $myRow['SCENARIO_SUBTITLE'];
			$this->ScenarioWriterName = $myRow['SCENARIO_WRITER_NAME'];
			$this->ScenarioMemo = $myRow['SCENARIO_MEMO'];
			$this->ScenarioDate = $myRow['SCENARIO_DATE'];
		}//end while
    }//end function

// -------------------------------------------------------------
//    DB更新　INSERT
// -------------------------------------------------------------
	function clsSwScenarioDbInsert($mySqlConnObj){
		//ﾌﾟﾛﾊﾟﾃｨ取得
		$valScenarioId = $this->ScenarioId;
		$valUserId = $this->UserId;
		$valScenarioTitle = $this->ScenarioTitle;
		$valScenarioSubtitle = $this->ScenarioSubtitle;
		$valScenarioWriterName = $this->ScenarioWriterName;
		$valScenarioMemo = $this->ScenarioMemo;
		$valScenarioDate = $this->ScenarioDate;
			//INSERT SQL
			$strSQL = <<<END_OF_SQL

			INSERT INTO SW_SCENARIO(
				  USER_ID
				, SCENARIO_TITLE
				, SCENARIO_SUBTITLE
				, SCENARIO_WRITER_NAME
				, SCENARIO_MEMO
				, SCENARIO_DATE
			) values (
				  '$valUserId'
				, '$valScenarioTitle'
				, '$valScenarioSubtitle'
				, '$valScenarioWriterName'
				, '$valScenarioMemo'
				, current_timestamp
			);
END_OF_SQL;

			//INSRET 
			$myResult = $mySqlConnObj->query($strSQL) or die($this->fncAlert($mySqlConnObj->error));

			//AUTO_INCREMENT値取得
			$LastInsertId = $mySqlConnObj->insert_id;
			//AUTO_INCREMENT値を返す
			return $LastInsertId;
	}//end function

// -------------------------------------------------------------
//    DB更新　UPDATE
// -------------------------------------------------------------
	function clsSwScenarioDbUpdate($mySqlConnObj){
		//ﾌﾟﾛﾊﾟﾃｨ取得
		$valScenarioId = $this->ScenarioId;
		$valUserId = $this->UserId;
		$valScenarioTitle = $this->ScenarioTitle;
		$valScenarioSubtitle = $this->ScenarioSubtitle;
		$valScenarioWriterName = $this->ScenarioWriterName;
		$valScenarioMemo = $this->ScenarioMemo;
		$valScenarioDate = $this->ScenarioDate;
		//登録確認
		$strSQL = <<<END_OF_SQL

		SELECT * FROM SW_SCENARIO
				WHERE SCENARIO_ID = '$valScenarioId';
END_OF_SQL;

		//登録確認SQL Execute 
		$myResult = $mySqlConnObj->query($strSQL) or die($this->fncAlert($mySqlConnObj->error));

		//件数取得
		$myRowCnt = $myResult->num_rows;
		if($myRowCnt == 0 ){
			echo $this->fncAlert("clsSwScenario ->> NOT ENTRY!");
		}else{
			//UPDATE SQL
			$strSQL = <<<END_OF_SQL

			UPDATE SW_SCENARIO SET 
				  USER_ID = '$valUserId'
				, SCENARIO_TITLE = '$valScenarioTitle'
				, SCENARIO_SUBTITLE = '$valScenarioSubtitle'
				, SCENARIO_WRITER_NAME = '$valScenarioWriterName'
				, SCENARIO_MEMO = '$valScenarioMemo'
				, SCENARIO_DATE = current_timestamp
				WHERE SCENARIO_ID = '$valScenarioId';
END_OF_SQL;

			//UPDATESQL Execute 
			$myResult = $mySqlConnObj->query($strSQL) or die($this->fncAlert($mySqlConnObj->error));

		}//end if
	}//end function

// -------------------------------------------------------------
//    DB更新　DELETE
// -------------------------------------------------------------
	function clsSwScenarioDbDelete($mySqlConnObj){
		//ﾌﾟﾛﾊﾟﾃｨ取得
		$valScenarioId = $this->ScenarioId;
		//登録確認
		$strSQL = <<<END_OF_SQL

		SELECT * FROM SW_SCENARIO
				WHERE SCENARIO_ID = '$valScenarioId';
END_OF_SQL;

		//登録確認SQL Execute 
		$myResult = $mySqlConnObj->query($strSQL) or die($this->fncAlert($mySqlConnObj->error));

		//件数取得
		$myRowCnt = $myResult->num_rows;
		if($myRowCnt == 0 ){
			echo $this->fncAlert("clsSwScenario ->> NOT ENTRY!");
		}else{
			//DELETE SQL
			$strSQL = <<<END_OF_SQL

			DELETE FROM SW_SCENARIO
				WHERE SCENARIO_ID = '$valScenarioId';
END_OF_SQL;

			//DELETE SQL Execute 
			$myResult = $mySqlConnObj->query($strSQL) or die($this->fncAlert($mySqlConnObj->error));

		}//end if
	}//end function

	// ------------------------------------------------------------------------------
	// java script alert
	// ------------------------------------------------------------------------------
	function fncAlert($AlertMsg){
		$strAlert = <<<END_OF_TEXT

			<script language="JavaScript">
				bootbox.alert("$AlertMsg",function() {
			});
			</script>
END_OF_TEXT;

		return $strAlert;
	}
	
} // end class
// -----------------------------------------------------------
?>