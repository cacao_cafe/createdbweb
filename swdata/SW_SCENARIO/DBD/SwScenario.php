<?php
// -----------------------------------------------------------
//
// Copyright (C) 2015 ScenarioWriterCafe All Rights Reserved.
// 
//     CREATE TABLE SW_SCENARIO
//
//     createTable_SwScenario.php
// -----------------------------------------------------------
$Result = fncCreateTable();
echo $Result;
exit;


// -----------------------------------------------------------
//     fncCreateTable()
// -----------------------------------------------------------
function fncCreateTable(){
	//共通定数
	include_once("../../sw_config/swConstant.php");

	//MySQLに接続
	include_once("../include/ConnectMySQL.php");

	//DROP TABLE SQLを編集
	$strSQL = <<<END_OF_SQL

DROP TABLE IF EXISTS SW_SCENARIO;

END_OF_SQL;

	//DROP TABLE EXECUTE 
	$myResult = $mySqlConnObj->query($strSQL) or die($this->fncAlert($mySqlConnObj->error));

	//CREATE TABLE SQLを編集
	$strSQL = <<<END_OF_SQL

CREATE TABLE SW_SCENARIO(
	  SCENARIO_ID                      INT NOT NULL AUTO_INCREMENT COMMENT 'シナリオID'
	, USER_ID                          INT  COMMENT 'USER_ID'
	, SCENARIO_TITLE                   VARCHAR(256)  COMMENT 'タイトル'
	, SCENARIO_SUBTITLE                VARCHAR(256)  COMMENT 'サブタイトル'
	, SCENARIO_WRITER_NAME             VARCHAR(256)  COMMENT '作者名'
	, SCENARIO_MEMO                    MEDIUMTEXT  COMMENT 'メモ'
	, SCENARIO_DATE                    TIMESTAMP  COMMENT '執筆日'
	, PRIMARY KEY (SCENARIO_ID)
	)DEFAULT CHARACTER SET 'UTF8'
	COMMENT '';

END_OF_SQL;

	//CREATE TABLE EXECUTE 
	$myResult = $mySqlConnObj->query($strSQL) or die($this->fncAlert($mySqlConnObj->error));

	//UNIQUE INDEX のSQLを編集
	$strSQL = <<<END_OF_SQL

	CREATE UNIQUE INDEX SW_SCENARIO_KEY_SCENARIO_ID ON SW_SCENARIO(SCENARIO_ID);
END_OF_SQL;

	//CREATE INDEX EXECUTE 
	$myResult = $mySqlConnObj->query($strSQL) or die($this->fncAlert($mySqlConnObj->error));


	//DB切断
	include_once("../include/DisConnectMySQL.php");
	
	
	return	'CREATE TABLE SW_SCENARIO Success...';
}//end function
// -----------------------------------------------------------
?>