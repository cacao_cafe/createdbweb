<?php
// -----------------------------------------------------------
//
// Copyright (C) 2015 ScenarioWriterCafe All Rights Reserved.
// 
//     SW_SCENARIO I/O ｼｽﾃﾑ
//
//     SwScenario.php
// -----------------------------------------------------------
// ------------------------------------------------------------------------------
	//定数読み込み
	include_once("../sw_config/swConstant.php");
	//DB接続ｸﾗｽの初期化
	include_once("./include/ConnectMySQL.php");
// ------------------------------------------------------------------------------
	//ﾃﾞﾌｫﾙﾄｱｸｼｮﾝ
	$ThisPHP = 'SwScenario.php';
	//ﾃﾞｰﾀ管理ｸﾗｽ
	include_once("./class/clsSwScenario.php");
	$clsSwScenario = new clsSwScenario();
	//共通関数をｲﾝｸﾙｰﾄﾞ
	include_once("./include/swFunc.php");
	// ﾍｯﾀﾞ表示
	$HtmlTitle = "Create DB Web System";
	include_once("./include/swHeader.php");

	//ﾌｫｰﾑのﾃﾞｰﾀを読み込む
	fncGetPostItems();

	//MainProcedure
	fncMainProc($mySqlConnObj);

	exit();

// ------------------------------------------------------------------------------
//      POSTされた要素を取得
//          fncGetPostItems()
// ------------------------------------------------------------------------------
function fncGetPostItems(){
	//共通global変数
	global	$SubmitMode;
	//ﾌｫｰﾑ name要素をglobal変数にする
	global	$fdtScenarioId;
	global	$fdtUserId;
	global	$fdtScenarioTitle;
	global	$fdtScenarioSubtitle;
	global	$fdtScenarioWriterName;
	global	$fdtScenarioMemo;
	global	$fdtScenarioDate;

	//処理ﾓｰﾄが空白ならreturnするﾞ
	if(!isset($_POST['SubmitMode'])){return;}
	//処理ﾓｰﾄﾞ
	$SubmitMode = swFunc_GetPostData('SubmitMode');
	if($SubmitMode == ''){return;}
	//POSTされた要素を取得
	$fdtScenarioId = swFunc_GetPostData('fdtScenarioId');            //シナリオID
	$fdtUserId = swFunc_GetPostData('fdtUserId');                    //USER_ID
	$fdtScenarioTitle = swFunc_GetPostData('fdtScenarioTitle');      //タイトル
	$fdtScenarioSubtitle = swFunc_GetPostData('fdtScenarioSubtitle');//サブタイトル
	$fdtScenarioWriterName = swFunc_GetPostData('fdtScenarioWriterName');//作者名
	$fdtScenarioMemo = swFunc_GetPostData('fdtScenarioMemo');        //メモ
	$fdtScenarioDate = swFunc_GetPostData('fdtScenarioDate');        //執筆日
}//end function

// ------------------------------------------------------------------------------
//      MAIN PROCEDURE
//          fncMainProc($mySqlConnObj)
// ------------------------------------------------------------------------------
function fncMainProc($mySqlConnObj){
	global	$SubmitMode;
	//class
	global	$clsSwScenario;

	global	$fdtScenarioId;
	global	$fdtUserId;
	global	$fdtScenarioTitle;
	global	$fdtScenarioSubtitle;
	global	$fdtScenarioWriterName;
	global	$fdtScenarioMemo;
	global	$fdtScenarioDate;

	//SubmitModeで処理を分岐
	switch($SubmitMode ){
		case 'SELECT':
			//ﾃﾞｰﾀ管理ｸﾗｽの初期化
			$clsSwScenario->clsSwScenarioInit($mySqlConnObj,$fdtScenarioId);
			//ﾌﾟﾛﾊﾟﾃｨGet
			fncSwScenarioGetProperty($clsSwScenario);
			break;
		case 'INSERT':
			//登録処理
			fncSwScenarioDBInsert($mySqlConnObj,$clsSwScenario);
			//選択状態にする
			$SubmitMode = 'SELECT';
			//ﾃﾞｰﾀ管理ｸﾗｽの初期化
			$clsSwScenario->clsSwScenarioInit($mySqlConnObj,$fdtScenarioId);
			//ﾌﾟﾛﾊﾟﾃｨGet
			fncSwScenarioGetProperty($clsSwScenario);
			break;
		case 'UPDATE':
			//更新処理
			fncSwScenarioDBUpdate($mySqlConnObj,$clsSwScenario);
			//選択状態にする
			$SubmitMode = 'SELECT';
			//ﾃﾞｰﾀ管理ｸﾗｽの初期化
			$clsSwScenario->clsSwScenarioInit($mySqlConnObj,$fdtScenarioId);
			//ﾌﾟﾛﾊﾟﾃｨGet
			fncSwScenarioGetProperty($clsSwScenario);
			break;
		case 'DELETE':
			//削除処理
			fncSwScenarioDBDelete($mySqlConnObj,$clsSwScenario);
			//ﾌﾟﾛﾊﾟﾃｨReset
			fncSwScenarioResetProperty($clsSwScenario);
			//初期状態にする
			$SubmitMode = 'RESET';
			break;
		case 'RESET':
			//ﾌﾟﾛﾊﾟﾃｨReset
			fncSwScenarioResetProperty($clsSwScenario);
			//初期状態にする
			$SubmitMode = 'RESET';
			break;
		default:
			break;
	}
	//ﾌｫｰﾑの表示
	fncMainForm($mySqlConnObj);
}//end function

// ------------------------------------------------------------------------------
//      MAIN FORM
//          fncMainForm($mySqlConnObj)
// ------------------------------------------------------------------------------
function fncMainForm($mySqlConnObj){
	global	$ThisPHP,$SubmitMode;
	global	$fdtScenarioId;
	global	$fdtUserId;
	global	$fdtScenarioTitle;
	global	$fdtScenarioSubtitle;
	global	$fdtScenarioWriterName;
	global	$fdtScenarioMemo;
	global	$fdtScenarioDate;

	//css powerd by Bootstrap ver3
	print <<<END_OF_HTML

	<div class="wrap" >
	
		<div class="container-fluid">
		
		<!-- ﾒﾆｭｰ 制御ﾎﾞﾀﾝ 表示ROW -->
			<div class="row">
				<div class="col-sm-12">
				<legend class="form-inline submenu">
				<ul class="list-inline">
				  <li>
END_OF_HTML;

	//管理メニュー ﾄﾞﾛｯﾌﾟﾀﾞｳﾝﾒﾆｭｰ
	$TargetForm = 'frmSwScenario';
	include_once("./include/swMgrDropDownMenu.php");

	
    //SubmitModeで表示するボタンを制御する
	switch($SubmitMode){
		case 'SELECT':
			print <<<END_OF_HTML
			 	</li>
			 	<li>
			        <input type="button" value="RESET"
			            id="btnReset"
			            onclick="fncSwScenarioSubmit(frmSwScenario,'RESET','FALSE','');"
			            class="btn btn-default btn-sm">
			        <span style="width: 120px;"></span>
			        <input type="button" value="INSERT"
			            id="btnInsert"
			            onclick="fncSwScenarioSubmit(frmSwScenario,'INSERT','TRUE','新規登録');"
			            class="btn btn-warning btn-sm">
			        <input type="button" value="UPDATE"
			            id="btnUpdate"
			            onclick="fncSwScenarioSubmit(frmSwScenario,'UPDATE','TRUE','更新');"
			            class="btn btn-warning btn-sm">
			        <input type="button" value="DELETE"
			            id="btnDelete"
			            onclick="fncSwScenarioSubmit(frmSwScenario,'DELETE','TRUE','削除');"
			            class="btn btn-danger btn-sm">
			
				</li>
END_OF_HTML;

	
			break;
        default:
			print <<<END_OF_HTML
			    </li>
			 	<li>
			        <input type="button" value="RESET"
			            id="btnReset"
			            onclick="fncSwScenarioSubmit(frmSwScenario,'RESET','FALSE','');"
			            class="btn btn-default btn-sm">
			        <input type="button" value="INSERT"
			            id="btnInsert"
			            onclick="fncSwScenarioSubmit(frmSwScenario,'INSERT','TRUE','新規登録');"
			            class="btn btn-warning btn-sm">
			
				</li>
END_OF_HTML;
            break;
    }
	print <<<END_OF_HTML

					</ul>
					</legend>
    			</div><!-- col-sm-12 end -->
    		</div><!-- row end -->
    		<!-- ﾒﾆｭｰ 制御ﾎﾞﾀﾝ 表示ROW ここまで-->

    		<!-- ﾌｫｰﾑ と ﾘｽﾄ -->
    		<div class="row row-0">
				<div class="col-sm-7 row-0">
					<form class="form-horizontal" role="form" name="frmSwScenario" id="frmSwScenario" method="POST" action="$ThisPHP">
						<input type="hidden" name="SubmitMode" id="SubmitMode" value="$SubmitMode">

END_OF_HTML;

	print <<<END_OF_HTML

							<div class="form-group row-0">
								<label for="fdtScenarioId" class="control-label col-xs-3">シナリオID</label>
								<div class="col-xs-8">
									<input type="number"
										class="form-control input-sm text-right"
										id="fdtScenarioId" name="fdtScenarioId"
										value="$fdtScenarioId"
										onkeyup="AjaxFunc_AddFigure(frmSwScenario,'fdtScenarioId');"
										placeholder="シナリオID">
								</div>
							</div>
END_OF_HTML;
	print <<<END_OF_HTML

							<div class="form-group row-0">
								<label for="fdtUserId" class="control-label col-xs-3">USER_ID</label>
								<div class="col-xs-8">
									<input type="number"
										class="form-control input-sm text-right"
										id="fdtUserId" name="fdtUserId"
										value="$fdtUserId"
										onkeyup="AjaxFunc_AddFigure(frmSwScenario,'fdtUserId');"
										placeholder="USER_ID">
								</div>
							</div>
END_OF_HTML;
	print <<<END_OF_HTML

							<div class="form-group row-0">
								<label for="fdtScenarioTitle" class="control-label col-xs-3">タイトル</label>
								<div class="col-xs-8">
									<input type="text" 
										class="form-control input-sm"
										id="fdtScenarioTitle" name="fdtScenarioTitle"
										value="$fdtScenarioTitle"
										placeholder="タイトル">
								</div>
							</div>
END_OF_HTML;
	print <<<END_OF_HTML

							<div class="form-group row-0">
								<label for="fdtScenarioSubtitle" class="control-label col-xs-3">サブタイトル</label>
								<div class="col-xs-8">
									<input type="text" 
										class="form-control input-sm"
										id="fdtScenarioSubtitle" name="fdtScenarioSubtitle"
										value="$fdtScenarioSubtitle"
										placeholder="サブタイトル">
								</div>
							</div>
END_OF_HTML;
	print <<<END_OF_HTML

							<div class="form-group row-0">
								<label for="fdtScenarioWriterName" class="control-label col-xs-3">作者名</label>
								<div class="col-xs-8">
									<input type="text" 
										class="form-control input-sm"
										id="fdtScenarioWriterName" name="fdtScenarioWriterName"
										value="$fdtScenarioWriterName"
										placeholder="作者名">
								</div>
							</div>
END_OF_HTML;
	print <<<END_OF_HTML

							<div class="form-group row-0">
								<label for="fdtScenarioMemo" class="control-label col-xs-3">メモ</label>
								<div class="col-xs-8">
									<textarea placeholder="メモ" 
										class="form-control input-sm"
										id="fdtScenarioMemo" name="fdtScenarioMemo"
										rows="3"
										id="InputTextarea">$fdtScenarioMemo</textarea>
								</div>
							</div>
END_OF_HTML;
	print <<<END_OF_HTML

							<div class="form-group row-0">
								<label for="fdtScenarioDate" class="control-label col-xs-3">執筆日</label>
								<div class="col-xs-8">
									<input type="text" 
										class="form-control input-sm"
										id="fdtScenarioDate" name="fdtScenarioDate"
										value="$fdtScenarioDate"
										placeholder="執筆日">
								</div>
							</div>
END_OF_HTML;
	print <<<END_OF_HTML

						</form>
					</div>
					<div class="col-sm-5" >
	                	<!-- リストの表示位置 -->
	                    <span id="side_list_area"></span>
					</div>
				</div>
			</div>
 
		</div><!--container-->
	</div><!--wrap-->


    <script type="text/javascript" src="./ajax/ajaxSwScenario.js"></script>
    <script type="text/javascript">
        fncMakeSwScenarioList(frmSwScenario);
    </script>


	<!--footer表示位置--><span class="sw-footer"></span>
	<script language="JavaScript">
		AjaxFunc_FooterLoad();
	</script>
	
  </body>
</html>

END_OF_HTML;

}//end function

// ------------------------------------------------------------------------------
//      ﾌﾟﾛﾊﾟﾃｨReset
//          fncSwScenarioResetProperty($clsSwScenario)
// ------------------------------------------------------------------------------
function fncSwScenarioResetProperty($clsSwScenario){
	global	$fdtScenarioId;
	global	$fdtUserId;
	global	$fdtScenarioTitle;
	global	$fdtScenarioSubtitle;
	global	$fdtScenarioWriterName;
	global	$fdtScenarioMemo;
	global	$fdtScenarioDate;

	//ﾌﾟﾛﾊﾟﾃｨReset
	$fdtScenarioId = "";                      //シナリオID
	$fdtUserId = "";                          //USER_ID
	$fdtScenarioTitle = "";                   //タイトル
	$fdtScenarioSubtitle = "";                //サブタイトル
	$fdtScenarioWriterName = "";              //作者名
	$fdtScenarioMemo = "";                    //メモ
	$fdtScenarioDate = "";                    //執筆日
}//end function

// ------------------------------------------------------------------------------
//      ﾌﾟﾛﾊﾟﾃｨGet
//          fncSwScenarioGetProperty($clsSwScenario)
// ------------------------------------------------------------------------------
function fncSwScenarioGetProperty($clsSwScenario){
	global	$fdtScenarioId;
	global	$fdtUserId;
	global	$fdtScenarioTitle;
	global	$fdtScenarioSubtitle;
	global	$fdtScenarioWriterName;
	global	$fdtScenarioMemo;
	global	$fdtScenarioDate;

	//ﾌﾟﾛﾊﾟﾃｨGet
	$fdtScenarioId = $clsSwScenario->clsSwScenarioGetScenarioId();            //シナリオID
	$fdtUserId = $clsSwScenario->clsSwScenarioGetUserId();                    //USER_ID
	$fdtScenarioTitle = $clsSwScenario->clsSwScenarioGetScenarioTitle();      //タイトル
	$fdtScenarioSubtitle = $clsSwScenario->clsSwScenarioGetScenarioSubtitle();//サブタイトル
	$fdtScenarioWriterName = $clsSwScenario->clsSwScenarioGetScenarioWriterName();//作者名

	//メモは<br>を\nに変換する
	$fdtScenarioMemo = $clsSwScenario->clsSwScenarioGetScenarioMemo();        //メモ
	$fdtScenarioMemo = str_replace("<br>","\n",$fdtScenarioMemo);

	$fdtScenarioDate = $clsSwScenario->clsSwScenarioGetScenarioDate();        //執筆日
}//end function

// ------------------------------------------------------------------------------
//      ﾌﾟﾛﾊﾟﾃｨSet
//          fncSwScenarioSetProperty($clsSwScenario)
// ------------------------------------------------------------------------------
function fncSwScenarioSetProperty($clsSwScenario){
	global	$fdtScenarioId;
	global	$fdtUserId;
	global	$fdtScenarioTitle;
	global	$fdtScenarioSubtitle;
	global	$fdtScenarioWriterName;
	global	$fdtScenarioMemo;
	global	$fdtScenarioDate;

	//ﾌﾟﾛﾊﾟﾃｨSet

	//シナリオIDはｶﾝﾏを取り除いてからSetする
	$fdtScenarioId = str_replace(",","",$fdtScenarioId);
	$clsSwScenario->clsSwScenarioSetScenarioId($fdtScenarioId);            //シナリオID


	//USER_IDはｶﾝﾏを取り除いてからSetする
	$fdtUserId = str_replace(",","",$fdtUserId);
	$clsSwScenario->clsSwScenarioSetUserId($fdtUserId);                    //USER_ID

	$clsSwScenario->clsSwScenarioSetScenarioTitle($fdtScenarioTitle);      //タイトル
	$clsSwScenario->clsSwScenarioSetScenarioSubtitle($fdtScenarioSubtitle);//サブタイトル
	$clsSwScenario->clsSwScenarioSetScenarioWriterName($fdtScenarioWriterName);//作者名

	//メモは改行を<br>にしてからSetする
	$fdtScenarioMemo = str_replace("\r\n","<br>",$fdtScenarioMemo);
	$fdtScenarioMemo = str_replace("\r","<br>",$fdtScenarioMemo);
	$fdtScenarioMemo = str_replace("\n","<br>",$fdtScenarioMemo);
	$clsSwScenario->clsSwScenarioSetScenarioMemo($fdtScenarioMemo);        //メモ

	$clsSwScenario->clsSwScenarioSetScenarioDate($fdtScenarioDate);        //執筆日
}//end function

// ------------------------------------------------------------------------------
//      DB INSERT
//          fncSwScenarioDBInsert($mySqlConnObj,$clsSwScenario)
// ------------------------------------------------------------------------------
function fncSwScenarioDBInsert($mySqlConnObj,$clsSwScenario){
	global	$fdtScenarioId;

	//ﾌﾟﾛﾊﾟﾃｨｾｯﾄ
	fncSwScenarioSetProperty($clsSwScenario);
	//ﾃﾞｰﾀ管理ｸﾗｽ DbInsert
	$fdtScenarioId = $clsSwScenario->clsSwScenarioDbInsert($mySqlConnObj);
}//end function

// ------------------------------------------------------------------------------
//      DB UPDATE
//          fncSwScenarioDBUpdate($mySqlConnObj,$clsSwScenario)
// ------------------------------------------------------------------------------
function fncSwScenarioDBUpdate($mySqlConnObj,$clsSwScenario){
	//ﾌﾟﾛﾊﾟﾃｨｾｯﾄ
	fncSwScenarioSetProperty($clsSwScenario);
	//ﾃﾞｰﾀ管理ｸﾗｽ DbUpdate
	$clsSwScenario->clsSwScenarioDbUpdate($mySqlConnObj);
}//end function

// ------------------------------------------------------------------------------
//      DB DELETE
//          fncSwScenarioDBDelete($mySqlConnObj,$clsSwScenario)
// ------------------------------------------------------------------------------
function fncSwScenarioDBDelete($mySqlConnObj,$clsSwScenario){
	//ﾌﾟﾛﾊﾟﾃｨｾｯﾄ
	fncSwScenarioSetProperty($clsSwScenario);
	//ﾃﾞｰﾀ管理ｸﾗｽ DbDelete
	$clsSwScenario->clsSwScenarioDbDelete($mySqlConnObj);
}//end function


// -----------------------------------------------------------
?>