<?php
// -----------------------------------------------------------
//
// Copyright (C) 2015 ScenarioWriterCafe All Rights Reserved.
// 
//     SW_USER_OPTION I/O ｼｽﾃﾑ
//
//     ajaxSwUserOption.php
// -----------------------------------------------------------
// ------------------------------------------------------------------------------
	//定数読み込み
	include_once("../../sw_config/swConstant.php");
	//DB接続ｸﾗｽの初期化
	include_once("../include/ConnectMySQL.php");
// ------------------------------------------------------------------------------
	//共通関数をｲﾝｸﾙｰﾄﾞ
	include_once("../include/swFunc.php");
	//ﾌｫｰﾑのﾃﾞｰﾀを読み込む
	fncGetPostItems();

	//MainProcedure
	fncMainProc($mySqlConnObj);

	exit();

// ------------------------------------------------------------------------------
//      POSTされた要素を取得
//          fncGetPostItems()
// ------------------------------------------------------------------------------
function fncGetPostItems(){
	//共通global変数
	global	$SubMode;
	//ﾌｫｰﾑ name要素をglobal変数にする
	global	$fdtUserOptionId;
	global	$fdtUserId;
	global	$fdtUserOptionStyleId;
	global	$fdtUserOptionStyleOrderNo;
	global	$fdtUserOptionStyleName;
	global	$fdtUserOptionStyleFontSize;
	global	$fdtUserOptionStyleColor;
	global	$fdtUserOptionStyleIndent;
	global	$fdtUserOptionStyleStr;
	global	$fdtUserOptionStyleWord;

	//処理ﾓｰﾄが空白ならreturnするﾞ
	if(!isset($_POST['SubMode'])){return;}
	//処理ﾓｰﾄﾞ
	$SubMode = swFunc_GetPostData('SubMode');
	if($SubMode == ''){return;}
	//POSTされた要素を取得
	$fdtUserOptionId = swFunc_GetPostData('fdtUserOptionId');        //USER_OPTION_ID
	$fdtUserId = swFunc_GetPostData('fdtUserId');                    //USER_ID
	$fdtUserOptionStyleId = swFunc_GetPostData('fdtUserOptionStyleId');//スタイルID
	$fdtUserOptionStyleOrderNo = swFunc_GetPostData('fdtUserOptionStyleOrderNo');//並び順
	$fdtUserOptionStyleName = swFunc_GetPostData('fdtUserOptionStyleName');//スタイル名
	$fdtUserOptionStyleFontSize = swFunc_GetPostData('fdtUserOptionStyleFontSize');//フォントサイズ
	$fdtUserOptionStyleColor = swFunc_GetPostData('fdtUserOptionStyleColor');//文字色
	$fdtUserOptionStyleIndent = swFunc_GetPostData('fdtUserOptionStyleIndent');//字下げ数
	$fdtUserOptionStyleStr = swFunc_GetPostData('fdtUserOptionStyleStr');//省略文字
	$fdtUserOptionStyleWord = swFunc_GetPostData('fdtUserOptionStyleWord');//台詞
}//end function

// ------------------------------------------------------------------------------
//      MAIN PROCEDURE
//          fncMainProc($mySqlConnObj)
// ------------------------------------------------------------------------------
function fncMainProc($mySqlConnObj){
	global	$SubMode;
	//global 変数
	global	$fdtUserOptionId;
	global	$fdtUserId;
	global	$fdtUserOptionStyleId;
	global	$fdtUserOptionStyleOrderNo;
	global	$fdtUserOptionStyleName;
	global	$fdtUserOptionStyleFontSize;
	global	$fdtUserOptionStyleColor;
	global	$fdtUserOptionStyleIndent;
	global	$fdtUserOptionStyleStr;
	global	$fdtUserOptionStyleWord;

	//SubModeで処理を制御
	if($SubMode == 'SwUserOptionList'){
		$resultHtml = fncMakeSwUserOptionList($mySqlConnObj);
	}

	// 出力charsetをUTF-8に指定
	mb_http_output ( 'UTF-8' );
	// 出力
	echo($resultHtml);
}//end function

//--------------------------------------------------------------------------------
// SW_USER_OPTION ALL LIST
//--------------------------------------------------------------------------------
function fncMakeSwUserOptionList($mySqlConnObj){
	//ﾘｽﾄのﾍｯﾀﾞｰ
	$retHtml = <<<END_OF_HTML
	
		<div class="scroll_div">
		<table class="table" _fixedhead="rows:1;div-full-mode: no;">
			<tr>
			</tr>
END_OF_HTML;

	//DB から ﾃﾞｰﾀを取得しﾌﾟﾛﾊﾟﾃｨにｾｯﾄする
	$strSQL = <<<END_OF_SQL
		SELECT *,USER_OPTION_ID as KEY_ITEM
			FROM SW_USER_OPTION
					ORDER BY USER_OPTION_ID;
END_OF_SQL;
	//SQLを実行
	$myResult = $mySqlConnObj->query($strSQL);
	while($myRow = $myResult->fetch_assoc()){
		$valKeyItem = $myRow['KEY_ITEM'];
		//ﾘｽﾄ
		$retHtml .= <<<END_OF_HTML
		
			<tr onclick="fncSelectSwUserOption('$valKeyItem');">
			</tr>
END_OF_HTML;
		}//end while

		$retHtml .= <<<END_OF_HTML
		
		</table>
		</div>
END_OF_HTML;
	//結果セットを開放
	$myResult->free();
	//HTMLを返す
	return $retHtml;
}
