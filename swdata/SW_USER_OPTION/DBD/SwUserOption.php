<?php
// -----------------------------------------------------------
//
// Copyright (C) 2015 ScenarioWriterCafe All Rights Reserved.
// 
//     CREATE TABLE SW_USER_OPTION
//
//     createTable_SwUserOption.php
// -----------------------------------------------------------
$Result = fncCreateTable();
echo $Result;
exit;


// -----------------------------------------------------------
//     fncCreateTable()
// -----------------------------------------------------------
function fncCreateTable(){
	//共通定数
	include_once("../../sw_config/swConstant.php");

	//MySQLに接続
	include_once("../include/ConnectMySQL.php");

	//DROP TABLE SQLを編集
	$strSQL = <<<END_OF_SQL

DROP TABLE IF EXISTS SW_USER_OPTION;

END_OF_SQL;

	//DROP TABLE EXECUTE 
	$myResult = $mySqlConnObj->query($strSQL) or die($this->fncAlert($mySqlConnObj->error));

	//CREATE TABLE SQLを編集
	$strSQL = <<<END_OF_SQL

CREATE TABLE SW_USER_OPTION(
	  USER_OPTION_ID                   INT NOT NULL AUTO_INCREMENT COMMENT 'USER_OPTION_ID'
	, USER_ID                          INT  COMMENT 'USER_ID'
	, USER_OPTION_STYLE_ID             INT  COMMENT 'スタイルID'
	, USER_OPTION_STYLE_ORDER_NO       INT  COMMENT '並び順'
	, USER_OPTION_STYLE_NAME           VARCHAR(256)  COMMENT 'スタイル名'
	, USER_OPTION_STYLE_FONT_SIZE      INT  COMMENT 'フォントサイズ'
	, USER_OPTION_STYLE_COLOR          VARCHAR(24)  COMMENT '文字色'
	, USER_OPTION_STYLE_INDENT         INT  COMMENT '字下げ数'
	, USER_OPTION_STYLE_STR            VARCHAR(24)  COMMENT '省略文字'
	, USER_OPTION_STYLE_WORD           INT  COMMENT '台詞'
	, PRIMARY KEY (USER_OPTION_ID)
	)DEFAULT CHARACTER SET 'UTF8'
	COMMENT '個別設定';

END_OF_SQL;

	//CREATE TABLE EXECUTE 
	$myResult = $mySqlConnObj->query($strSQL) or die($this->fncAlert($mySqlConnObj->error));

	//UNIQUE INDEX のSQLを編集
	$strSQL = <<<END_OF_SQL

	CREATE UNIQUE INDEX SW_USER_OPTION_KEY_USER_OPTION_ID ON SW_USER_OPTION(USER_OPTION_ID);
END_OF_SQL;

	//CREATE INDEX EXECUTE 
	$myResult = $mySqlConnObj->query($strSQL) or die($this->fncAlert($mySqlConnObj->error));


	//INDEX のSQLを編集
	$strSQL = <<<END_OF_SQL

	CREATE INDEX SW_USER_OPTION_KEY_USER_ID ON SW_USER_OPTION(USER_ID);
END_OF_SQL;

	//CREATE INDEX 
	$myResult = $mySqlConnObj->query($strSQL) or die($this->fncAlert($mySqlConnObj->error));

	//DB切断
	include_once("../include/DisConnectMySQL.php");
	
	
	return	'CREATE TABLE SW_USER_OPTION Success...';
}//end function
// -----------------------------------------------------------
?>