CalendarClass
概要
カレンダー出力用のHTMLを出力するクラスです。 
日本の休日表示に対応しています。秋分の日や春分の日，振り替え休日や国民の休日にも対応しています。 



メンバ変数概要
string $style_table 
string $style_title 
string $style_weekname 
string $style_body 
string $style_day 
string $bgcolor_weekname 
string $bgcolor_weekday 
string $bgcolor_saturday 
string $bgcolor_holiday 
string $bgcolor_other 
array $weekname 

メソッド概要
string getCalendar(int $year, int $month, [int $week_start = 0], [int $disp_flg = 0]) 
void setData(int $year, int $month, int $day, string $data) 
void setLink(int $year, int $month, int $day, string $link) 
array getHoliday(int $year, int $month) 

//*************************************
//	メンバ変数
//*************************************
//-------------------------------------
//	string $style_table
//-------------------------------------
	var: カレンダー本体のスタイル 
	access: public 
	デフォルトの設定値
	var $style_table = 'style="width:210px; border-collapse:collapse; border:1px solid #cccccc; font-size:90%"'; 

//-------------------------------------
//	string $style_title
//-------------------------------------
	var: カレンダータイトルのスタイル 
	access: public 
	デフォルトの設定値
	var $style_title = 'style="text-align:center; background-color:#333333; color:#ffffff"'; 
	
//-------------------------------------
//	string $style_weekname
//-------------------------------------
	var: カレンダー曜日名のスタイル 
	access: public 
	デフォルトの設定値
	var $style_weekname = 'style="text-align:center; width:30px; border:1px solid #cccccc"'; 

//-------------------------------------
//	string $style_body
//-------------------------------------
	var: カレンダー日付枠のスタイル 
	access: public 
	デフォルトの設定値
	var $style_body = 'style="text-align:left; border:1px solid #cccccc;"'; 
	
//-------------------------------------	
//	string $style_day
//-------------------------------------
	var: カレンダー日付ブロックのスタイル 
	access: public 
	デフォルトの設定値
	var $style_day = 'style="margin:0px; padding:0px; text-align:right"'; 
	
//-------------------------------------	
//	string $bgcolor_weekname
//-------------------------------------
	var: 背景色(曜日名) 
	access: public 
	デフォルトの設定値
	var $bgcolor_weekname = "#eeeeee"; 

//-------------------------------------
//	string $bgcolor_weekday
//-------------------------------------
	var: 背景色(平日) 
	access: public 
	デフォルトの設定値
	var $bgcolor_weekday = "#ffffff"; 

//-------------------------------------
//	string $bgcolor_saturday
//-------------------------------------
	var: 背景色(土曜日) 
	access: public 
	デフォルトの設定値
	var $bgcolor_saturday = "#ddddff"; 

//-------------------------------------
//	string $bgcolor_holiday
//-------------------------------------
	var: 背景色(日祝日) 
	access: public 
	デフォルトの設定値
	var $bgcolor_holiday = "#ffdddd"; 

//-------------------------------------
//	string $bgcolor_other
//-------------------------------------
	var: 背景色(当月以外の日) 
	access: public 
	デフォルトの設定値
	var $bgcolor_other = "#eeeeee"; 

//-------------------------------------
//	array $weekname
//-------------------------------------
	var: 曜日名部分の文字列 
	access: public 
	デフォルトの設定値
	var $weekname = array("日", "月", "火", "水", "木", "金", "土"); 



//*************************************
//	メソッド
//*************************************
//-------------------------------------
//	getCalendar
//-------------------------------------
	カレンダー表示用に作成されたHTML結果を取得する

	return: 作成されたHTML 
	access: public 
	string getCalendar(int $year, int $month, [int $week_start = 0], [int $disp_flg = 0])
	
	int $year: 年 
	int $month: 月 
	int $week_start: カレンダー左端の曜日(0:日曜日～6:土曜日) 
	int $disp_flg: 当月以外の日を表示するか 0:表示しない 1:表示する 

//-------------------------------------
//	setData
//-------------------------------------
カレンダー内の日付下に出力するデータを設定する

	return: void 
	access: public 
	void setData(int $year, int $month, int $day, string $data)
	
	int $year: 年 
	int $month: 月 
	int $day: 日 
	int $data: 出力させたい内容 

//-------------------------------------
//	setLink
//-------------------------------------
カレンダー内の日付にリンクする内容を設定する

	return: void 
	access: public 
	void setLink(int $year, int $month, int $day, string $link)
	
	int $year: 年 
	int $month: 月 
	int $day: 日 
	int $link: リンク先URL 

//-------------------------------------
//	getHoliday
//-------------------------------------
指定された年月の休日一覧を取得する

	return: 休日情報 array('日' => 名称) 
	access: public 
	array getHoliday(int $year, int $month)
	
	int $year: 年 
	int $month: 月 
