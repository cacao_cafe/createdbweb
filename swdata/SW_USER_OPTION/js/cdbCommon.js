// ---------------------------------------------- 
//		cdbCommon.js
//
//    	charset=UTF-8
// Copyright (C) 2014 dbu@mac.com All Rights Reserved.
// ---------------------------------------------- 
// ---------------------------------------------- 
// ﾎﾞﾀﾝが押されたときの制御 
// ---------------------------------------------- 
function cdbCommon_frmSubmit(frm,valSubmitMode,valMode,valMsg){
    var nRet = true;
	if (valMode == true ){
	    if ( nRet == true ){
	        if (confirm(valMsg + '　実行しますか？')){
	            frm.SubmitMode.value = valSubmitMode;
	            frm.submit();
	            return true;
	        }
	    }
	    return false;
	}else{
        frm.SubmitMode.value = valSubmitMode;
        frm.submit();
        return true;
	}
    return false;
}

// ---------------------------------------------- 
// Enterが押されたときsubmitしない 
//	cdbCommonBlockEnter(evt)
// ---------------------------------------------- 
function cdbCommon_BlockEnter(evt){
	evt = (evt) ? evt : event;
	var charCode=(evt.charCode) ? evt.charCode :
		((evt.which) ? evt.which : evt.keyCode);
	if ( Number(charCode) == 13 || Number(charCode) == 3) {
		return false;
	} else {
		return true;
	}
}

// ---------------------------------------------- 
// 桁区切り
//	cdbCommonAddFigure(frm,valElements)
// ---------------------------------------------- 
function cdbCommon_AddFigure(frm,valElements) {
	var str = frm.elements[valElements].value;
	var num = new String(str).replace(/,/g, "");
	while(num != (num = num.replace(/^(-?\d+)(\d{3})/, "$1,$2")));
	frm.elements[valElements].value=num;
}

