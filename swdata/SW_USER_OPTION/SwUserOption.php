<?php
// -----------------------------------------------------------
//
// Copyright (C) 2015 ScenarioWriterCafe All Rights Reserved.
// 
//     SW_USER_OPTION I/O ｼｽﾃﾑ
//
//     SwUserOption.php
// -----------------------------------------------------------
// ------------------------------------------------------------------------------
	//定数読み込み
	include_once("../sw_config/swConstant.php");
	//DB接続ｸﾗｽの初期化
	include_once("./include/ConnectMySQL.php");
// ------------------------------------------------------------------------------
	//ﾃﾞﾌｫﾙﾄｱｸｼｮﾝ
	$ThisPHP = 'SwUserOption.php';
	//ﾃﾞｰﾀ管理ｸﾗｽ
	include_once("./class/clsSwUserOption.php");
	$clsSwUserOption = new clsSwUserOption();
	//共通関数をｲﾝｸﾙｰﾄﾞ
	include_once("./include/swFunc.php");
	// ﾍｯﾀﾞ表示
	$HtmlTitle = "個別設定";
	include_once("./include/swHeader.php");

	//ﾌｫｰﾑのﾃﾞｰﾀを読み込む
	fncGetPostItems();

	//MainProcedure
	fncMainProc($mySqlConnObj);

	exit();

// ------------------------------------------------------------------------------
//      POSTされた要素を取得
//          fncGetPostItems()
// ------------------------------------------------------------------------------
function fncGetPostItems(){
	//共通global変数
	global	$SubmitMode;
	//ﾌｫｰﾑ name要素をglobal変数にする
	global	$fdtUserOptionId;
	global	$fdtUserId;
	global	$fdtUserOptionStyleId;
	global	$fdtUserOptionStyleOrderNo;
	global	$fdtUserOptionStyleName;
	global	$fdtUserOptionStyleFontSize;
	global	$fdtUserOptionStyleColor;
	global	$fdtUserOptionStyleIndent;
	global	$fdtUserOptionStyleStr;
	global	$fdtUserOptionStyleWord;

	//処理ﾓｰﾄが空白ならreturnするﾞ
	if(!isset($_POST['SubmitMode'])){return;}
	//処理ﾓｰﾄﾞ
	$SubmitMode = swFunc_GetPostData('SubmitMode');
	if($SubmitMode == ''){return;}
	//POSTされた要素を取得
	$fdtUserOptionId = swFunc_GetPostData('fdtUserOptionId');        //USER_OPTION_ID
	$fdtUserId = swFunc_GetPostData('fdtUserId');                    //USER_ID
	$fdtUserOptionStyleId = swFunc_GetPostData('fdtUserOptionStyleId');//スタイルID
	$fdtUserOptionStyleOrderNo = swFunc_GetPostData('fdtUserOptionStyleOrderNo');//並び順
	$fdtUserOptionStyleName = swFunc_GetPostData('fdtUserOptionStyleName');//スタイル名
	$fdtUserOptionStyleFontSize = swFunc_GetPostData('fdtUserOptionStyleFontSize');//フォントサイズ
	$fdtUserOptionStyleColor = swFunc_GetPostData('fdtUserOptionStyleColor');//文字色
	$fdtUserOptionStyleIndent = swFunc_GetPostData('fdtUserOptionStyleIndent');//字下げ数
	$fdtUserOptionStyleStr = swFunc_GetPostData('fdtUserOptionStyleStr');//省略文字
	$fdtUserOptionStyleWord = swFunc_GetPostData('fdtUserOptionStyleWord');//台詞
}//end function

// ------------------------------------------------------------------------------
//      MAIN PROCEDURE
//          fncMainProc($mySqlConnObj)
// ------------------------------------------------------------------------------
function fncMainProc($mySqlConnObj){
	global	$SubmitMode;
	//class
	global	$clsSwUserOption;

	global	$fdtUserOptionId;
	global	$fdtUserId;
	global	$fdtUserOptionStyleId;
	global	$fdtUserOptionStyleOrderNo;
	global	$fdtUserOptionStyleName;
	global	$fdtUserOptionStyleFontSize;
	global	$fdtUserOptionStyleColor;
	global	$fdtUserOptionStyleIndent;
	global	$fdtUserOptionStyleStr;
	global	$fdtUserOptionStyleWord;

	//SubmitModeで処理を分岐
	switch($SubmitMode ){
		case 'SELECT':
			//ﾃﾞｰﾀ管理ｸﾗｽの初期化
			$clsSwUserOption->clsSwUserOptionInit($mySqlConnObj,$fdtUserOptionId);
			//ﾌﾟﾛﾊﾟﾃｨGet
			fncSwUserOptionGetProperty($clsSwUserOption);
			break;
		case 'INSERT':
			//登録処理
			fncSwUserOptionDBInsert($mySqlConnObj,$clsSwUserOption);
			//選択状態にする
			$SubmitMode = 'SELECT';
			//ﾃﾞｰﾀ管理ｸﾗｽの初期化
			$clsSwUserOption->clsSwUserOptionInit($mySqlConnObj,$fdtUserOptionId);
			//ﾌﾟﾛﾊﾟﾃｨGet
			fncSwUserOptionGetProperty($clsSwUserOption);
			break;
		case 'UPDATE':
			//更新処理
			fncSwUserOptionDBUpdate($mySqlConnObj,$clsSwUserOption);
			//選択状態にする
			$SubmitMode = 'SELECT';
			//ﾃﾞｰﾀ管理ｸﾗｽの初期化
			$clsSwUserOption->clsSwUserOptionInit($mySqlConnObj,$fdtUserOptionId);
			//ﾌﾟﾛﾊﾟﾃｨGet
			fncSwUserOptionGetProperty($clsSwUserOption);
			break;
		case 'DELETE':
			//削除処理
			fncSwUserOptionDBDelete($mySqlConnObj,$clsSwUserOption);
			//ﾌﾟﾛﾊﾟﾃｨReset
			fncSwUserOptionResetProperty($clsSwUserOption);
			//初期状態にする
			$SubmitMode = 'RESET';
			break;
		case 'RESET':
			//ﾌﾟﾛﾊﾟﾃｨReset
			fncSwUserOptionResetProperty($clsSwUserOption);
			//初期状態にする
			$SubmitMode = 'RESET';
			break;
		default:
			break;
	}
	//ﾌｫｰﾑの表示
	fncMainForm($mySqlConnObj);
}//end function

// ------------------------------------------------------------------------------
//      MAIN FORM
//          fncMainForm($mySqlConnObj)
// ------------------------------------------------------------------------------
function fncMainForm($mySqlConnObj){
	global	$ThisPHP,$SubmitMode;
	global	$fdtUserOptionId;
	global	$fdtUserId;
	global	$fdtUserOptionStyleId;
	global	$fdtUserOptionStyleOrderNo;
	global	$fdtUserOptionStyleName;
	global	$fdtUserOptionStyleFontSize;
	global	$fdtUserOptionStyleColor;
	global	$fdtUserOptionStyleIndent;
	global	$fdtUserOptionStyleStr;
	global	$fdtUserOptionStyleWord;

	//css powerd by Bootstrap ver3
	print <<<END_OF_HTML

	<div class="wrap" >
	
		<div class="container-fluid">
		
		<!-- ﾒﾆｭｰ 制御ﾎﾞﾀﾝ 表示ROW -->
			<div class="row">
				<div class="col-sm-12">
				<legend class="form-inline submenu">
				<ul class="list-inline">
				  <li>
END_OF_HTML;

	//管理メニュー ﾄﾞﾛｯﾌﾟﾀﾞｳﾝﾒﾆｭｰ
	$TargetForm = 'frmSwUserOption';
	include_once("./include/swMgrDropDownMenu.php");

	
    //SubmitModeで表示するボタンを制御する
	switch($SubmitMode){
		case 'SELECT':
			print <<<END_OF_HTML
			 	</li>
			 	<li>
			        <input type="button" value="RESET"
			            id="btnReset"
			            onclick="fncSwUserOptionSubmit(frmSwUserOption,'RESET','FALSE','');"
			            class="btn btn-default btn-sm">
			        <span style="width: 120px;"></span>
			        <input type="button" value="INSERT"
			            id="btnInsert"
			            onclick="fncSwUserOptionSubmit(frmSwUserOption,'INSERT','TRUE','新規登録');"
			            class="btn btn-warning btn-sm">
			        <input type="button" value="UPDATE"
			            id="btnUpdate"
			            onclick="fncSwUserOptionSubmit(frmSwUserOption,'UPDATE','TRUE','更新');"
			            class="btn btn-warning btn-sm">
			        <input type="button" value="DELETE"
			            id="btnDelete"
			            onclick="fncSwUserOptionSubmit(frmSwUserOption,'DELETE','TRUE','削除');"
			            class="btn btn-danger btn-sm">
			
				</li>
END_OF_HTML;

	
			break;
        default:
			print <<<END_OF_HTML
			    </li>
			 	<li>
			        <input type="button" value="RESET"
			            id="btnReset"
			            onclick="fncSwUserOptionSubmit(frmSwUserOption,'RESET','FALSE','');"
			            class="btn btn-default btn-sm">
			        <input type="button" value="INSERT"
			            id="btnInsert"
			            onclick="fncSwUserOptionSubmit(frmSwUserOption,'INSERT','TRUE','新規登録');"
			            class="btn btn-warning btn-sm">
			
				</li>
END_OF_HTML;
            break;
    }
	print <<<END_OF_HTML

					</ul>
					</legend>
    			</div><!-- col-sm-12 end -->
    		</div><!-- row end -->
    		<!-- ﾒﾆｭｰ 制御ﾎﾞﾀﾝ 表示ROW ここまで-->

    		<!-- ﾌｫｰﾑ と ﾘｽﾄ -->
    		<div class="row row-0">
				<div class="col-sm-7 row-0">
					<form class="form-horizontal" role="form" name="frmSwUserOption" id="frmSwUserOption" method="POST" action="$ThisPHP">
						<input type="hidden" name="SubmitMode" id="SubmitMode" value="$SubmitMode">

END_OF_HTML;

	print <<<END_OF_HTML

							<div class="form-group row-0">
								<label for="fdtUserOptionId" class="control-label col-xs-3">USER_OPTION_ID</label>
								<div class="col-xs-8">
									<input type="number"
										class="form-control input-sm text-right"
										id="fdtUserOptionId" name="fdtUserOptionId"
										value="$fdtUserOptionId"
										onkeyup="AjaxFunc_AddFigure(frmSwUserOption,'fdtUserOptionId');"
										placeholder="USER_OPTION_ID">
								</div>
							</div>
END_OF_HTML;
	print <<<END_OF_HTML

							<div class="form-group row-0">
								<label for="fdtUserId" class="control-label col-xs-3">USER_ID</label>
								<div class="col-xs-8">
									<input type="number"
										class="form-control input-sm text-right"
										id="fdtUserId" name="fdtUserId"
										value="$fdtUserId"
										onkeyup="AjaxFunc_AddFigure(frmSwUserOption,'fdtUserId');"
										placeholder="USER_ID">
								</div>
							</div>
END_OF_HTML;
	print <<<END_OF_HTML

							<div class="form-group row-0">
								<label for="fdtUserOptionStyleId" class="control-label col-xs-3">スタイルID</label>
								<div class="col-xs-8">
									<input type="number"
										class="form-control input-sm text-right"
										id="fdtUserOptionStyleId" name="fdtUserOptionStyleId"
										value="$fdtUserOptionStyleId"
										onkeyup="AjaxFunc_AddFigure(frmSwUserOption,'fdtUserOptionStyleId');"
										placeholder="スタイルID">
								</div>
							</div>
END_OF_HTML;
	print <<<END_OF_HTML

							<div class="form-group row-0">
								<label for="fdtUserOptionStyleOrderNo" class="control-label col-xs-3">並び順</label>
								<div class="col-xs-8">
									<input type="number"
										class="form-control input-sm text-right"
										id="fdtUserOptionStyleOrderNo" name="fdtUserOptionStyleOrderNo"
										value="$fdtUserOptionStyleOrderNo"
										onkeyup="AjaxFunc_AddFigure(frmSwUserOption,'fdtUserOptionStyleOrderNo');"
										placeholder="並び順">
								</div>
							</div>
END_OF_HTML;
	print <<<END_OF_HTML

							<div class="form-group row-0">
								<label for="fdtUserOptionStyleName" class="control-label col-xs-3">スタイル名</label>
								<div class="col-xs-8">
									<input type="text" 
										class="form-control input-sm"
										id="fdtUserOptionStyleName" name="fdtUserOptionStyleName"
										value="$fdtUserOptionStyleName"
										placeholder="スタイル名">
								</div>
							</div>
END_OF_HTML;
	print <<<END_OF_HTML

							<div class="form-group row-0">
								<label for="fdtUserOptionStyleFontSize" class="control-label col-xs-3">フォントサイズ</label>
								<div class="col-xs-8">
									<input type="number"
										class="form-control input-sm text-right"
										id="fdtUserOptionStyleFontSize" name="fdtUserOptionStyleFontSize"
										value="$fdtUserOptionStyleFontSize"
										onkeyup="AjaxFunc_AddFigure(frmSwUserOption,'fdtUserOptionStyleFontSize');"
										placeholder="フォントサイズ">
								</div>
							</div>
END_OF_HTML;
	print <<<END_OF_HTML

							<div class="form-group row-0">
								<label for="fdtUserOptionStyleColor" class="control-label col-xs-3">文字色</label>
								<div class="col-xs-8">
									<input type="text" 
										class="form-control input-sm"
										id="fdtUserOptionStyleColor" name="fdtUserOptionStyleColor"
										value="$fdtUserOptionStyleColor"
										placeholder="文字色">
								</div>
							</div>
END_OF_HTML;
	print <<<END_OF_HTML

							<div class="form-group row-0">
								<label for="fdtUserOptionStyleIndent" class="control-label col-xs-3">字下げ数</label>
								<div class="col-xs-8">
									<input type="number"
										class="form-control input-sm text-right"
										id="fdtUserOptionStyleIndent" name="fdtUserOptionStyleIndent"
										value="$fdtUserOptionStyleIndent"
										onkeyup="AjaxFunc_AddFigure(frmSwUserOption,'fdtUserOptionStyleIndent');"
										placeholder="字下げ数">
								</div>
							</div>
END_OF_HTML;
	print <<<END_OF_HTML

							<div class="form-group row-0">
								<label for="fdtUserOptionStyleStr" class="control-label col-xs-3">省略文字</label>
								<div class="col-xs-8">
									<input type="text" 
										class="form-control input-sm"
										id="fdtUserOptionStyleStr" name="fdtUserOptionStyleStr"
										value="$fdtUserOptionStyleStr"
										placeholder="省略文字">
								</div>
							</div>
END_OF_HTML;
	print <<<END_OF_HTML

							<div class="form-group row-0">
								<label for="fdtUserOptionStyleWord" class="control-label col-xs-3">台詞</label>
								<div class="col-xs-8">
									<input type="number"
										class="form-control input-sm text-right"
										id="fdtUserOptionStyleWord" name="fdtUserOptionStyleWord"
										value="$fdtUserOptionStyleWord"
										onkeyup="AjaxFunc_AddFigure(frmSwUserOption,'fdtUserOptionStyleWord');"
										placeholder="台詞">
								</div>
							</div>
END_OF_HTML;
	print <<<END_OF_HTML

						</form>
					</div>
					<div class="col-sm-5" >
	                	<!-- リストの表示位置 -->
	                    <span id="side_list_area"></span>
					</div>
				</div>
			</div>
 
		</div><!--container-->
	</div><!--wrap-->


    <script type="text/javascript" src="./ajax/ajaxSwUserOption.js"></script>
    <script type="text/javascript">
        fncMakeSwUserOptionList(frmSwUserOption);
    </script>


	<!--footer表示位置--><span class="sw-footer"></span>
	<script language="JavaScript">
		AjaxFunc_FooterLoad();
	</script>
	
  </body>
</html>

END_OF_HTML;

}//end function

// ------------------------------------------------------------------------------
//      ﾌﾟﾛﾊﾟﾃｨReset
//          fncSwUserOptionResetProperty($clsSwUserOption)
// ------------------------------------------------------------------------------
function fncSwUserOptionResetProperty($clsSwUserOption){
	global	$fdtUserOptionId;
	global	$fdtUserId;
	global	$fdtUserOptionStyleId;
	global	$fdtUserOptionStyleOrderNo;
	global	$fdtUserOptionStyleName;
	global	$fdtUserOptionStyleFontSize;
	global	$fdtUserOptionStyleColor;
	global	$fdtUserOptionStyleIndent;
	global	$fdtUserOptionStyleStr;
	global	$fdtUserOptionStyleWord;

	//ﾌﾟﾛﾊﾟﾃｨReset
	$fdtUserOptionId = "";                    //USER_OPTION_ID
	$fdtUserId = "";                          //USER_ID
	$fdtUserOptionStyleId = "";               //スタイルID
	$fdtUserOptionStyleOrderNo = "";          //並び順
	$fdtUserOptionStyleName = "";             //スタイル名
	$fdtUserOptionStyleFontSize = "";         //フォントサイズ
	$fdtUserOptionStyleColor = "";            //文字色
	$fdtUserOptionStyleIndent = "";           //字下げ数
	$fdtUserOptionStyleStr = "";              //省略文字
	$fdtUserOptionStyleWord = "";             //台詞
}//end function

// ------------------------------------------------------------------------------
//      ﾌﾟﾛﾊﾟﾃｨGet
//          fncSwUserOptionGetProperty($clsSwUserOption)
// ------------------------------------------------------------------------------
function fncSwUserOptionGetProperty($clsSwUserOption){
	global	$fdtUserOptionId;
	global	$fdtUserId;
	global	$fdtUserOptionStyleId;
	global	$fdtUserOptionStyleOrderNo;
	global	$fdtUserOptionStyleName;
	global	$fdtUserOptionStyleFontSize;
	global	$fdtUserOptionStyleColor;
	global	$fdtUserOptionStyleIndent;
	global	$fdtUserOptionStyleStr;
	global	$fdtUserOptionStyleWord;

	//ﾌﾟﾛﾊﾟﾃｨGet
	$fdtUserOptionId = $clsSwUserOption->clsSwUserOptionGetUserOptionId();        //USER_OPTION_ID
	$fdtUserId = $clsSwUserOption->clsSwUserOptionGetUserId();                    //USER_ID
	$fdtUserOptionStyleId = $clsSwUserOption->clsSwUserOptionGetUserOptionStyleId();//スタイルID
	$fdtUserOptionStyleOrderNo = $clsSwUserOption->clsSwUserOptionGetUserOptionStyleOrderNo();//並び順
	$fdtUserOptionStyleName = $clsSwUserOption->clsSwUserOptionGetUserOptionStyleName();//スタイル名
	$fdtUserOptionStyleFontSize = $clsSwUserOption->clsSwUserOptionGetUserOptionStyleFontSize();//フォントサイズ
	$fdtUserOptionStyleColor = $clsSwUserOption->clsSwUserOptionGetUserOptionStyleColor();//文字色
	$fdtUserOptionStyleIndent = $clsSwUserOption->clsSwUserOptionGetUserOptionStyleIndent();//字下げ数
	$fdtUserOptionStyleStr = $clsSwUserOption->clsSwUserOptionGetUserOptionStyleStr();//省略文字
	$fdtUserOptionStyleWord = $clsSwUserOption->clsSwUserOptionGetUserOptionStyleWord();//台詞
}//end function

// ------------------------------------------------------------------------------
//      ﾌﾟﾛﾊﾟﾃｨSet
//          fncSwUserOptionSetProperty($clsSwUserOption)
// ------------------------------------------------------------------------------
function fncSwUserOptionSetProperty($clsSwUserOption){
	global	$fdtUserOptionId;
	global	$fdtUserId;
	global	$fdtUserOptionStyleId;
	global	$fdtUserOptionStyleOrderNo;
	global	$fdtUserOptionStyleName;
	global	$fdtUserOptionStyleFontSize;
	global	$fdtUserOptionStyleColor;
	global	$fdtUserOptionStyleIndent;
	global	$fdtUserOptionStyleStr;
	global	$fdtUserOptionStyleWord;

	//ﾌﾟﾛﾊﾟﾃｨSet

	//USER_OPTION_IDはｶﾝﾏを取り除いてからSetする
	$fdtUserOptionId = str_replace(",","",$fdtUserOptionId);
	$clsSwUserOption->clsSwUserOptionSetUserOptionId($fdtUserOptionId);        //USER_OPTION_ID


	//USER_IDはｶﾝﾏを取り除いてからSetする
	$fdtUserId = str_replace(",","",$fdtUserId);
	$clsSwUserOption->clsSwUserOptionSetUserId($fdtUserId);                    //USER_ID


	//スタイルIDはｶﾝﾏを取り除いてからSetする
	$fdtUserOptionStyleId = str_replace(",","",$fdtUserOptionStyleId);
	$clsSwUserOption->clsSwUserOptionSetUserOptionStyleId($fdtUserOptionStyleId);//スタイルID


	//並び順はｶﾝﾏを取り除いてからSetする
	$fdtUserOptionStyleOrderNo = str_replace(",","",$fdtUserOptionStyleOrderNo);
	$clsSwUserOption->clsSwUserOptionSetUserOptionStyleOrderNo($fdtUserOptionStyleOrderNo);//並び順

	$clsSwUserOption->clsSwUserOptionSetUserOptionStyleName($fdtUserOptionStyleName);//スタイル名

	//フォントサイズはｶﾝﾏを取り除いてからSetする
	$fdtUserOptionStyleFontSize = str_replace(",","",$fdtUserOptionStyleFontSize);
	$clsSwUserOption->clsSwUserOptionSetUserOptionStyleFontSize($fdtUserOptionStyleFontSize);//フォントサイズ

	$clsSwUserOption->clsSwUserOptionSetUserOptionStyleColor($fdtUserOptionStyleColor);//文字色

	//字下げ数はｶﾝﾏを取り除いてからSetする
	$fdtUserOptionStyleIndent = str_replace(",","",$fdtUserOptionStyleIndent);
	$clsSwUserOption->clsSwUserOptionSetUserOptionStyleIndent($fdtUserOptionStyleIndent);//字下げ数

	$clsSwUserOption->clsSwUserOptionSetUserOptionStyleStr($fdtUserOptionStyleStr);//省略文字

	//台詞はｶﾝﾏを取り除いてからSetする
	$fdtUserOptionStyleWord = str_replace(",","",$fdtUserOptionStyleWord);
	$clsSwUserOption->clsSwUserOptionSetUserOptionStyleWord($fdtUserOptionStyleWord);//台詞

}//end function

// ------------------------------------------------------------------------------
//      DB INSERT
//          fncSwUserOptionDBInsert($mySqlConnObj,$clsSwUserOption)
// ------------------------------------------------------------------------------
function fncSwUserOptionDBInsert($mySqlConnObj,$clsSwUserOption){
	global	$fdtUserOptionId;

	//ﾌﾟﾛﾊﾟﾃｨｾｯﾄ
	fncSwUserOptionSetProperty($clsSwUserOption);
	//ﾃﾞｰﾀ管理ｸﾗｽ DbInsert
	$fdtUserOptionId = $clsSwUserOption->clsSwUserOptionDbInsert($mySqlConnObj);
}//end function

// ------------------------------------------------------------------------------
//      DB UPDATE
//          fncSwUserOptionDBUpdate($mySqlConnObj,$clsSwUserOption)
// ------------------------------------------------------------------------------
function fncSwUserOptionDBUpdate($mySqlConnObj,$clsSwUserOption){
	//ﾌﾟﾛﾊﾟﾃｨｾｯﾄ
	fncSwUserOptionSetProperty($clsSwUserOption);
	//ﾃﾞｰﾀ管理ｸﾗｽ DbUpdate
	$clsSwUserOption->clsSwUserOptionDbUpdate($mySqlConnObj);
}//end function

// ------------------------------------------------------------------------------
//      DB DELETE
//          fncSwUserOptionDBDelete($mySqlConnObj,$clsSwUserOption)
// ------------------------------------------------------------------------------
function fncSwUserOptionDBDelete($mySqlConnObj,$clsSwUserOption){
	//ﾌﾟﾛﾊﾟﾃｨｾｯﾄ
	fncSwUserOptionSetProperty($clsSwUserOption);
	//ﾃﾞｰﾀ管理ｸﾗｽ DbDelete
	$clsSwUserOption->clsSwUserOptionDbDelete($mySqlConnObj);
}//end function


// -----------------------------------------------------------
?>