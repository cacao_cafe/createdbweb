<?php
// -----------------------------------------------------------
//
// Copyright (C) 2015 ScenarioWriterCafe All Rights Reserved.
// 
//     個別設定 ﾃﾞｰﾀ管理ｸﾗｽ
//     clsSwUserOption
// -----------------------------------------------------------
class clsSwUserOption{
	var $UserOptionId;                    //USER_OPTION_ID
	var $UserId;                          //USER_ID
	var $UserOptionStyleId;               //スタイルID
	var $UserOptionStyleOrderNo;          //並び順
	var $UserOptionStyleName;             //スタイル名
	var $UserOptionStyleFontSize;         //フォントサイズ
	var $UserOptionStyleColor;            //文字色
	var $UserOptionStyleIndent;           //字下げ数
	var $UserOptionStyleStr;              //省略文字
	var $UserOptionStyleWord;             //台詞
// ｺﾝｽﾄﾗｸﾀ
    function clsSwUserOption(){
    }
// ﾌﾟﾛﾊﾟﾃｨ
// --------------------------------------
// ﾌﾟﾛﾊﾟﾃｨ Get UserOptionId
//    USER_OPTION_ID を返す
// --------------------------------------
	function clsSwUserOptionGetUserOptionId(){
		return $this->UserOptionId;
	}
// --------------------------------------
// ﾌﾟﾛﾊﾟﾃｨ Set UserOptionId
//    USER_OPTION_ID を設定する
// --------------------------------------
	function clsSwUserOptionSetUserOptionId($valUserOptionId){
		$this->UserOptionId = $valUserOptionId;
	}
// --------------------------------------
// ﾌﾟﾛﾊﾟﾃｨ Get UserId
//    USER_ID を返す
// --------------------------------------
	function clsSwUserOptionGetUserId(){
		return $this->UserId;
	}
// --------------------------------------
// ﾌﾟﾛﾊﾟﾃｨ Set UserId
//    USER_ID を設定する
// --------------------------------------
	function clsSwUserOptionSetUserId($valUserId){
		$this->UserId = $valUserId;
	}
// --------------------------------------
// ﾌﾟﾛﾊﾟﾃｨ Get UserOptionStyleId
//    スタイルID を返す
// --------------------------------------
	function clsSwUserOptionGetUserOptionStyleId(){
		return $this->UserOptionStyleId;
	}
// --------------------------------------
// ﾌﾟﾛﾊﾟﾃｨ Set UserOptionStyleId
//    スタイルID を設定する
// --------------------------------------
	function clsSwUserOptionSetUserOptionStyleId($valUserOptionStyleId){
		$this->UserOptionStyleId = $valUserOptionStyleId;
	}
// --------------------------------------
// ﾌﾟﾛﾊﾟﾃｨ Get UserOptionStyleOrderNo
//    並び順 を返す
// --------------------------------------
	function clsSwUserOptionGetUserOptionStyleOrderNo(){
		return $this->UserOptionStyleOrderNo;
	}
// --------------------------------------
// ﾌﾟﾛﾊﾟﾃｨ Set UserOptionStyleOrderNo
//    並び順 を設定する
// --------------------------------------
	function clsSwUserOptionSetUserOptionStyleOrderNo($valUserOptionStyleOrderNo){
		$this->UserOptionStyleOrderNo = $valUserOptionStyleOrderNo;
	}
// --------------------------------------
// ﾌﾟﾛﾊﾟﾃｨ Get UserOptionStyleName
//    スタイル名 を返す
// --------------------------------------
	function clsSwUserOptionGetUserOptionStyleName(){
		return $this->UserOptionStyleName;
	}
// --------------------------------------
// ﾌﾟﾛﾊﾟﾃｨ Set UserOptionStyleName
//    スタイル名 を設定する
// --------------------------------------
	function clsSwUserOptionSetUserOptionStyleName($valUserOptionStyleName){
		$this->UserOptionStyleName = $valUserOptionStyleName;
	}
// --------------------------------------
// ﾌﾟﾛﾊﾟﾃｨ Get UserOptionStyleFontSize
//    フォントサイズ を返す
// --------------------------------------
	function clsSwUserOptionGetUserOptionStyleFontSize(){
		return $this->UserOptionStyleFontSize;
	}
// --------------------------------------
// ﾌﾟﾛﾊﾟﾃｨ Set UserOptionStyleFontSize
//    フォントサイズ を設定する
// --------------------------------------
	function clsSwUserOptionSetUserOptionStyleFontSize($valUserOptionStyleFontSize){
		$this->UserOptionStyleFontSize = $valUserOptionStyleFontSize;
	}
// --------------------------------------
// ﾌﾟﾛﾊﾟﾃｨ Get UserOptionStyleColor
//    文字色 を返す
// --------------------------------------
	function clsSwUserOptionGetUserOptionStyleColor(){
		return $this->UserOptionStyleColor;
	}
// --------------------------------------
// ﾌﾟﾛﾊﾟﾃｨ Set UserOptionStyleColor
//    文字色 を設定する
// --------------------------------------
	function clsSwUserOptionSetUserOptionStyleColor($valUserOptionStyleColor){
		$this->UserOptionStyleColor = $valUserOptionStyleColor;
	}
// --------------------------------------
// ﾌﾟﾛﾊﾟﾃｨ Get UserOptionStyleIndent
//    字下げ数 を返す
// --------------------------------------
	function clsSwUserOptionGetUserOptionStyleIndent(){
		return $this->UserOptionStyleIndent;
	}
// --------------------------------------
// ﾌﾟﾛﾊﾟﾃｨ Set UserOptionStyleIndent
//    字下げ数 を設定する
// --------------------------------------
	function clsSwUserOptionSetUserOptionStyleIndent($valUserOptionStyleIndent){
		$this->UserOptionStyleIndent = $valUserOptionStyleIndent;
	}
// --------------------------------------
// ﾌﾟﾛﾊﾟﾃｨ Get UserOptionStyleStr
//    省略文字 を返す
// --------------------------------------
	function clsSwUserOptionGetUserOptionStyleStr(){
		return $this->UserOptionStyleStr;
	}
// --------------------------------------
// ﾌﾟﾛﾊﾟﾃｨ Set UserOptionStyleStr
//    省略文字 を設定する
// --------------------------------------
	function clsSwUserOptionSetUserOptionStyleStr($valUserOptionStyleStr){
		$this->UserOptionStyleStr = $valUserOptionStyleStr;
	}
// --------------------------------------
// ﾌﾟﾛﾊﾟﾃｨ Get UserOptionStyleWord
//    台詞 を返す
// --------------------------------------
	function clsSwUserOptionGetUserOptionStyleWord(){
		return $this->UserOptionStyleWord;
	}
// --------------------------------------
// ﾌﾟﾛﾊﾟﾃｨ Set UserOptionStyleWord
//    台詞 を設定する
// --------------------------------------
	function clsSwUserOptionSetUserOptionStyleWord($valUserOptionStyleWord){
		$this->UserOptionStyleWord = $valUserOptionStyleWord;
	}
// -------------------------------------------------------------
//    ｸﾗｽ初期化
// -------------------------------------------------------------
	function clsSwUserOptionInit($mySqlConnObj,$valUserOptionId){
		//ﾌﾟﾛﾊﾟﾃｨの初期化
		$this->UserOptionId = '';                    //USER_OPTION_ID
		$this->UserId = '';                          //USER_ID
		$this->UserOptionStyleId = '';               //スタイルID
		$this->UserOptionStyleOrderNo = '';          //並び順
		$this->UserOptionStyleName = '';             //スタイル名
		$this->UserOptionStyleFontSize = '';         //フォントサイズ
		$this->UserOptionStyleColor = '';            //文字色
		$this->UserOptionStyleIndent = '';           //字下げ数
		$this->UserOptionStyleStr = '';              //省略文字
		$this->UserOptionStyleWord = '';             //台詞
		//DB から ﾃﾞｰﾀを取得しﾌﾟﾛﾊﾟﾃｨにｾｯﾄする
		$strSQL = <<<END_OF_SQL

		SELECT * FROM SW_USER_OPTION
				WHERE USER_OPTION_ID = '$valUserOptionId';
END_OF_SQL;

		$myResult = $mySqlConnObj->query($strSQL) or die($this->fncAlert($mySqlConnObj->error));
		while($myRow = $myResult->fetch_assoc( )){
			$this->UserOptionId = $myRow['USER_OPTION_ID'];
			$this->UserId = $myRow['USER_ID'];
			$this->UserOptionStyleId = $myRow['USER_OPTION_STYLE_ID'];
			$this->UserOptionStyleOrderNo = $myRow['USER_OPTION_STYLE_ORDER_NO'];
			$this->UserOptionStyleName = $myRow['USER_OPTION_STYLE_NAME'];
			$this->UserOptionStyleFontSize = $myRow['USER_OPTION_STYLE_FONT_SIZE'];
			$this->UserOptionStyleColor = $myRow['USER_OPTION_STYLE_COLOR'];
			$this->UserOptionStyleIndent = $myRow['USER_OPTION_STYLE_INDENT'];
			$this->UserOptionStyleStr = $myRow['USER_OPTION_STYLE_STR'];
			$this->UserOptionStyleWord = $myRow['USER_OPTION_STYLE_WORD'];
		}//end while
    }//end function

// -------------------------------------------------------------
//    DB更新　INSERT
// -------------------------------------------------------------
	function clsSwUserOptionDbInsert($mySqlConnObj){
		//ﾌﾟﾛﾊﾟﾃｨ取得
		$valUserOptionId = $this->UserOptionId;
		$valUserId = $this->UserId;
		$valUserOptionStyleId = $this->UserOptionStyleId;
		$valUserOptionStyleOrderNo = $this->UserOptionStyleOrderNo;
		$valUserOptionStyleName = $this->UserOptionStyleName;
		$valUserOptionStyleFontSize = $this->UserOptionStyleFontSize;
		$valUserOptionStyleColor = $this->UserOptionStyleColor;
		$valUserOptionStyleIndent = $this->UserOptionStyleIndent;
		$valUserOptionStyleStr = $this->UserOptionStyleStr;
		$valUserOptionStyleWord = $this->UserOptionStyleWord;
			//INSERT SQL
			$strSQL = <<<END_OF_SQL

			INSERT INTO SW_USER_OPTION(
				  USER_ID
				, USER_OPTION_STYLE_ID
				, USER_OPTION_STYLE_ORDER_NO
				, USER_OPTION_STYLE_NAME
				, USER_OPTION_STYLE_FONT_SIZE
				, USER_OPTION_STYLE_COLOR
				, USER_OPTION_STYLE_INDENT
				, USER_OPTION_STYLE_STR
				, USER_OPTION_STYLE_WORD
			) values (
				  '$valUserId'
				, '$valUserOptionStyleId'
				, '$valUserOptionStyleOrderNo'
				, '$valUserOptionStyleName'
				, '$valUserOptionStyleFontSize'
				, '$valUserOptionStyleColor'
				, '$valUserOptionStyleIndent'
				, '$valUserOptionStyleStr'
				, '$valUserOptionStyleWord'
			);
END_OF_SQL;

			//INSRET 
			$myResult = $mySqlConnObj->query($strSQL) or die($this->fncAlert($mySqlConnObj->error));

			//AUTO_INCREMENT値取得
			$LastInsertId = $mySqlConnObj->insert_id;
			//AUTO_INCREMENT値を返す
			return $LastInsertId;
	}//end function

// -------------------------------------------------------------
//    DB更新　UPDATE
// -------------------------------------------------------------
	function clsSwUserOptionDbUpdate($mySqlConnObj){
		//ﾌﾟﾛﾊﾟﾃｨ取得
		$valUserOptionId = $this->UserOptionId;
		$valUserId = $this->UserId;
		$valUserOptionStyleId = $this->UserOptionStyleId;
		$valUserOptionStyleOrderNo = $this->UserOptionStyleOrderNo;
		$valUserOptionStyleName = $this->UserOptionStyleName;
		$valUserOptionStyleFontSize = $this->UserOptionStyleFontSize;
		$valUserOptionStyleColor = $this->UserOptionStyleColor;
		$valUserOptionStyleIndent = $this->UserOptionStyleIndent;
		$valUserOptionStyleStr = $this->UserOptionStyleStr;
		$valUserOptionStyleWord = $this->UserOptionStyleWord;
		//登録確認
		$strSQL = <<<END_OF_SQL

		SELECT * FROM SW_USER_OPTION
				WHERE USER_OPTION_ID = '$valUserOptionId';
END_OF_SQL;

		//登録確認SQL Execute 
		$myResult = $mySqlConnObj->query($strSQL) or die($this->fncAlert($mySqlConnObj->error));

		//件数取得
		$myRowCnt = $myResult->num_rows;
		if($myRowCnt == 0 ){
			echo $this->fncAlert("clsSwUserOption ->> NOT ENTRY!");
		}else{
			//UPDATE SQL
			$strSQL = <<<END_OF_SQL

			UPDATE SW_USER_OPTION SET 
				  USER_ID = '$valUserId'
				, USER_OPTION_STYLE_ID = '$valUserOptionStyleId'
				, USER_OPTION_STYLE_ORDER_NO = '$valUserOptionStyleOrderNo'
				, USER_OPTION_STYLE_NAME = '$valUserOptionStyleName'
				, USER_OPTION_STYLE_FONT_SIZE = '$valUserOptionStyleFontSize'
				, USER_OPTION_STYLE_COLOR = '$valUserOptionStyleColor'
				, USER_OPTION_STYLE_INDENT = '$valUserOptionStyleIndent'
				, USER_OPTION_STYLE_STR = '$valUserOptionStyleStr'
				, USER_OPTION_STYLE_WORD = '$valUserOptionStyleWord'
				WHERE USER_OPTION_ID = '$valUserOptionId';
END_OF_SQL;

			//UPDATESQL Execute 
			$myResult = $mySqlConnObj->query($strSQL) or die($this->fncAlert($mySqlConnObj->error));

		}//end if
	}//end function

// -------------------------------------------------------------
//    DB更新　DELETE
// -------------------------------------------------------------
	function clsSwUserOptionDbDelete($mySqlConnObj){
		//ﾌﾟﾛﾊﾟﾃｨ取得
		$valUserOptionId = $this->UserOptionId;
		//登録確認
		$strSQL = <<<END_OF_SQL

		SELECT * FROM SW_USER_OPTION
				WHERE USER_OPTION_ID = '$valUserOptionId';
END_OF_SQL;

		//登録確認SQL Execute 
		$myResult = $mySqlConnObj->query($strSQL) or die($this->fncAlert($mySqlConnObj->error));

		//件数取得
		$myRowCnt = $myResult->num_rows;
		if($myRowCnt == 0 ){
			echo $this->fncAlert("clsSwUserOption ->> NOT ENTRY!");
		}else{
			//DELETE SQL
			$strSQL = <<<END_OF_SQL

			DELETE FROM SW_USER_OPTION
				WHERE USER_OPTION_ID = '$valUserOptionId';
END_OF_SQL;

			//DELETE SQL Execute 
			$myResult = $mySqlConnObj->query($strSQL) or die($this->fncAlert($mySqlConnObj->error));

		}//end if
	}//end function

	// ------------------------------------------------------------------------------
	// java script alert
	// ------------------------------------------------------------------------------
	function fncAlert($AlertMsg){
		$strAlert = <<<END_OF_TEXT

			<script language="JavaScript">
				bootbox.alert("$AlertMsg",function() {
			});
			</script>
END_OF_TEXT;

		return $strAlert;
	}
	
} // end class
// -----------------------------------------------------------
?>