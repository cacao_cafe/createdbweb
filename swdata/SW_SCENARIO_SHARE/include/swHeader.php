<?php
// ------------------------------------------------------------------------------
//
//		ScenarioWriterCafe共通
//				swHeader.php
//
//		Copyright(C) 2015 ScenarioWriterCafe All Rights Reserved.
//
// ------------------------------------------------------------------------------
	$AddStyleSheet = '';
	if(isset($myStyle)){
		if($myStyle == 'html'){
			$AddStyleSheet = '<link href="css/acctStyle.css" rel="stylesheet">';
		}
	}

print <<<END_OF_HTML

<!DOCTYPE html>
<html lang="ja">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>$HtmlTitle</title>
	<link rel="shortcut icon" href="./favicon.ico" type="image/vnd.microsoft.icon">

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <!-- site style -->
    <link href="css/acctAppStyle.css" rel="stylesheet">
	$AddStyleSheet
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!-- jQuery -->
	<script type="text/javascript" src="./js/jquery-2.1.4.min.js"></script>
	<script type="text/javascript" src="./js/bootstrap.min.js"></script>
	<script type="text/javascript" src="./js/bootbox.min.js"></script>
	<script type="text/javascript" src="./js/fixed_midashi.js"></script>
	<script type="text/javascript" src="./ajax/ajaxFunc.js"></script>
  </head>
  <body onLoad="FixedMidashi.create();">
<!--
	【special thanks】
		テーブルヘッダ固定 fixed_midashi.js 
			http://hp.vector.co.jp/authors/VA056612/fixed_midashi/manual/index.html
			Copyright (C) 2012-2015 K.Koiso
		
		CSS3を使って美しく装飾されたテーブルの作り方
			http://weboook.blog22.fc2.com/blog-entry-329.html
			Copyright (C) 2015 Webpark All rights reserved.
-->


END_OF_HTML;


?>
