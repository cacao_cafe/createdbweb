<?php
// -----------------------------------------------------------
//
// Copyright (C) 2015 ScenarioWriterCafe All Rights Reserved.
// 
//     SW_SCENARIO_SHARE I/O ｼｽﾃﾑ
//
//     SwScenarioShare.php
// -----------------------------------------------------------
// ------------------------------------------------------------------------------
	//定数読み込み
	include_once("../sw_config/swConstant.php");
	//DB接続ｸﾗｽの初期化
	include_once("./include/ConnectMySQL.php");
// ------------------------------------------------------------------------------
	//ﾃﾞﾌｫﾙﾄｱｸｼｮﾝ
	$ThisPHP = 'SwScenarioShare.php';
	//ﾃﾞｰﾀ管理ｸﾗｽ
	include_once("./class/clsSwScenarioShare.php");
	$clsSwScenarioShare = new clsSwScenarioShare();
	//共通関数をｲﾝｸﾙｰﾄﾞ
	include_once("./include/swFunc.php");
	// ﾍｯﾀﾞ表示
	$HtmlTitle = "共有ID管理";
	include_once("./include/swHeader.php");

	//ﾌｫｰﾑのﾃﾞｰﾀを読み込む
	fncGetPostItems();

	//MainProcedure
	fncMainProc($mySqlConnObj);

	exit();

// ------------------------------------------------------------------------------
//      POSTされた要素を取得
//          fncGetPostItems()
// ------------------------------------------------------------------------------
function fncGetPostItems(){
	//共通global変数
	global	$SubmitMode;
	//ﾌｫｰﾑ name要素をglobal変数にする
	global	$fdtScenarioId;
	global	$fdtShareId;
	global	$fdtSharePassword;
	global	$fdtShareType;
	global	$fdtShareDate;

	//処理ﾓｰﾄが空白ならreturnするﾞ
	if(!isset($_POST['SubmitMode'])){return;}
	//処理ﾓｰﾄﾞ
	$SubmitMode = swFunc_GetPostData('SubmitMode');
	if($SubmitMode == ''){return;}
	//POSTされた要素を取得
	$fdtScenarioId = swFunc_GetPostData('fdtScenarioId');            //シナリオID
	$fdtShareId = swFunc_GetPostData('fdtShareId');                  //共有ID
	$fdtSharePassword = swFunc_GetPostData('fdtSharePassword');      //パスワード
	$fdtShareType = swFunc_GetPostData('fdtShareType');              //公開・非公開
	$fdtShareDate = swFunc_GetPostData('fdtShareDate');              //共有日時
}//end function

// ------------------------------------------------------------------------------
//      MAIN PROCEDURE
//          fncMainProc($mySqlConnObj)
// ------------------------------------------------------------------------------
function fncMainProc($mySqlConnObj){
	global	$SubmitMode;
	//class
	global	$clsSwScenarioShare;

	global	$fdtScenarioId;
	global	$fdtShareId;
	global	$fdtSharePassword;
	global	$fdtShareType;
	global	$fdtShareDate;

	//SubmitModeで処理を分岐
	switch($SubmitMode ){
		case 'SELECT':
			//ﾃﾞｰﾀ管理ｸﾗｽの初期化
			$clsSwScenarioShare->clsSwScenarioShareInit($mySqlConnObj,$fdtScenarioId);
			//ﾌﾟﾛﾊﾟﾃｨGet
			fncSwScenarioShareGetProperty($clsSwScenarioShare);
			break;
		case 'INSERT':
			//登録処理
			fncSwScenarioShareDBInsert($mySqlConnObj,$clsSwScenarioShare);
			//選択状態にする
			$SubmitMode = 'SELECT';
			//ﾃﾞｰﾀ管理ｸﾗｽの初期化
			$clsSwScenarioShare->clsSwScenarioShareInit($mySqlConnObj,$fdtScenarioId);
			//ﾌﾟﾛﾊﾟﾃｨGet
			fncSwScenarioShareGetProperty($clsSwScenarioShare);
			break;
		case 'UPDATE':
			//更新処理
			fncSwScenarioShareDBUpdate($mySqlConnObj,$clsSwScenarioShare);
			//選択状態にする
			$SubmitMode = 'SELECT';
			//ﾃﾞｰﾀ管理ｸﾗｽの初期化
			$clsSwScenarioShare->clsSwScenarioShareInit($mySqlConnObj,$fdtScenarioId);
			//ﾌﾟﾛﾊﾟﾃｨGet
			fncSwScenarioShareGetProperty($clsSwScenarioShare);
			break;
		case 'DELETE':
			//削除処理
			fncSwScenarioShareDBDelete($mySqlConnObj,$clsSwScenarioShare);
			//ﾌﾟﾛﾊﾟﾃｨReset
			fncSwScenarioShareResetProperty($clsSwScenarioShare);
			//初期状態にする
			$SubmitMode = 'RESET';
			break;
		case 'RESET':
			//ﾌﾟﾛﾊﾟﾃｨReset
			fncSwScenarioShareResetProperty($clsSwScenarioShare);
			//初期状態にする
			$SubmitMode = 'RESET';
			break;
		default:
			break;
	}
	//ﾌｫｰﾑの表示
	fncMainForm($mySqlConnObj);
}//end function

// ------------------------------------------------------------------------------
//      MAIN FORM
//          fncMainForm($mySqlConnObj)
// ------------------------------------------------------------------------------
function fncMainForm($mySqlConnObj){
	global	$ThisPHP,$SubmitMode;
	global	$fdtScenarioId;
	global	$fdtShareId;
	global	$fdtSharePassword;
	global	$fdtShareType;
	global	$fdtShareDate;

	//css powerd by Bootstrap ver3
	print <<<END_OF_HTML

	<div class="wrap" >
	
		<div class="container-fluid">
		
		<!-- ﾒﾆｭｰ 制御ﾎﾞﾀﾝ 表示ROW -->
			<div class="row">
				<div class="col-sm-12">
				<legend class="form-inline submenu">
				<ul class="list-inline">
				  <li>
END_OF_HTML;

	//管理メニュー ﾄﾞﾛｯﾌﾟﾀﾞｳﾝﾒﾆｭｰ
	$TargetForm = 'frmSwScenarioShare';
	include_once("./include/swMgrDropDownMenu.php");

	
    //SubmitModeで表示するボタンを制御する
	switch($SubmitMode){
		case 'SELECT':
			print <<<END_OF_HTML
			 	</li>
			 	<li>
			        <input type="button" value="RESET"
			            id="btnReset"
			            onclick="fncSwScenarioShareSubmit(frmSwScenarioShare,'RESET','FALSE','');"
			            class="btn btn-default btn-sm">
			        <span style="width: 120px;"></span>
			        <input type="button" value="INSERT"
			            id="btnInsert"
			            onclick="fncSwScenarioShareSubmit(frmSwScenarioShare,'INSERT','TRUE','新規登録');"
			            class="btn btn-warning btn-sm">
			        <input type="button" value="UPDATE"
			            id="btnUpdate"
			            onclick="fncSwScenarioShareSubmit(frmSwScenarioShare,'UPDATE','TRUE','更新');"
			            class="btn btn-warning btn-sm">
			        <input type="button" value="DELETE"
			            id="btnDelete"
			            onclick="fncSwScenarioShareSubmit(frmSwScenarioShare,'DELETE','TRUE','削除');"
			            class="btn btn-danger btn-sm">
			
				</li>
END_OF_HTML;

	
			break;
        default:
			print <<<END_OF_HTML
			    </li>
			 	<li>
			        <input type="button" value="RESET"
			            id="btnReset"
			            onclick="fncSwScenarioShareSubmit(frmSwScenarioShare,'RESET','FALSE','');"
			            class="btn btn-default btn-sm">
			        <input type="button" value="INSERT"
			            id="btnInsert"
			            onclick="fncSwScenarioShareSubmit(frmSwScenarioShare,'INSERT','TRUE','新規登録');"
			            class="btn btn-warning btn-sm">
			
				</li>
END_OF_HTML;
            break;
    }
	print <<<END_OF_HTML

					</ul>
					</legend>
    			</div><!-- col-sm-12 end -->
    		</div><!-- row end -->
    		<!-- ﾒﾆｭｰ 制御ﾎﾞﾀﾝ 表示ROW ここまで-->

    		<!-- ﾌｫｰﾑ と ﾘｽﾄ -->
    		<div class="row row-0">
				<div class="col-sm-7 row-0">
					<form class="form-horizontal" role="form" name="frmSwScenarioShare" id="frmSwScenarioShare" method="POST" action="$ThisPHP">
						<input type="hidden" name="SubmitMode" id="SubmitMode" value="$SubmitMode">

END_OF_HTML;

	print <<<END_OF_HTML

							<div class="form-group row-0">
								<label for="fdtScenarioId" class="control-label col-xs-3">シナリオID</label>
								<div class="col-xs-8">
									<input type="number"
										class="form-control input-sm text-right"
										id="fdtScenarioId" name="fdtScenarioId"
										value="$fdtScenarioId"
										onkeyup="AjaxFunc_AddFigure(frmSwScenarioShare,'fdtScenarioId');"
										placeholder="シナリオID">
								</div>
							</div>
END_OF_HTML;
	print <<<END_OF_HTML

							<div class="form-group row-0">
								<label for="fdtShareId" class="control-label col-xs-3">共有ID</label>
								<div class="col-xs-8">
									<input type="text" 
										class="form-control input-sm"
										id="fdtShareId" name="fdtShareId"
										value="$fdtShareId"
										placeholder="共有ID">
								</div>
							</div>
END_OF_HTML;
	print <<<END_OF_HTML

							<div class="form-group row-0">
								<label for="fdtSharePassword" class="control-label col-xs-3">パスワード</label>
								<div class="col-xs-8">
									<input type="text" 
										class="form-control input-sm"
										id="fdtSharePassword" name="fdtSharePassword"
										value="$fdtSharePassword"
										placeholder="パスワード">
								</div>
							</div>
END_OF_HTML;
	print <<<END_OF_HTML

							<div class="form-group row-0">
								<label for="fdtShareType" class="control-label col-xs-3">公開・非公開</label>
								<div class="col-xs-8">
								
END_OF_HTML;

	//------------------------------------------------------------
	//CSVﾃﾞｰﾀからselect box を作成する
	$ObjName = "fdtShareType";		//select box の名称.ID
	//ここでfunctionからCSVﾃﾞｰﾀを取得
	$csvArray = swFunc_MakeSelectItemsCsvSample();	//CSVﾃﾞｰﾀ
	$default = "$fdtShareType";		//ﾃﾞﾌｫﾙﾄ値
	$onChange = '';					//onChange で起動する javascript or jQuery
	$ViewCode = FALSE;				//ｺｰﾄﾞを表示する場合はTRUE
	//SelectBoxHtml出力
	print swFunc_MakeSelectBox($ObjName,$csvArray,$default,$onChange,$ViewCode);
//------------------------------------------------------------

	print <<<END_OF_HTML

								</div>
							</div>
END_OF_HTML;
	print <<<END_OF_HTML

							<div class="form-group row-0">
								<label for="fdtShareDate" class="control-label col-xs-3">共有日時</label>
								<div class="col-xs-8">
									<input type="text" 
										class="form-control input-sm"
										id="fdtShareDate" name="fdtShareDate"
										value="$fdtShareDate"
										placeholder="共有日時">
								</div>
							</div>
END_OF_HTML;
	print <<<END_OF_HTML

						</form>
					</div>
					<div class="col-sm-5" >
	                	<!-- リストの表示位置 -->
	                    <span id="side_list_area"></span>
					</div>
				</div>
			</div>
 
		</div><!--container-->
	</div><!--wrap-->


    <script type="text/javascript" src="./ajax/ajaxSwScenarioShare.js"></script>
    <script type="text/javascript">
        fncMakeSwScenarioShareList(frmSwScenarioShare);
    </script>


	<!--footer表示位置--><span class="sw-footer"></span>
	<script language="JavaScript">
		AjaxFunc_FooterLoad();
	</script>
	
  </body>
</html>

END_OF_HTML;

}//end function

// ------------------------------------------------------------------------------
//      ﾌﾟﾛﾊﾟﾃｨReset
//          fncSwScenarioShareResetProperty($clsSwScenarioShare)
// ------------------------------------------------------------------------------
function fncSwScenarioShareResetProperty($clsSwScenarioShare){
	global	$fdtScenarioId;
	global	$fdtShareId;
	global	$fdtSharePassword;
	global	$fdtShareType;
	global	$fdtShareDate;

	//ﾌﾟﾛﾊﾟﾃｨReset
	$fdtScenarioId = "";                      //シナリオID
	$fdtShareId = "";                         //共有ID
	$fdtSharePassword = "";                   //パスワード
	$fdtShareType = "";                       //公開・非公開
	$fdtShareDate = "";                       //共有日時
}//end function

// ------------------------------------------------------------------------------
//      ﾌﾟﾛﾊﾟﾃｨGet
//          fncSwScenarioShareGetProperty($clsSwScenarioShare)
// ------------------------------------------------------------------------------
function fncSwScenarioShareGetProperty($clsSwScenarioShare){
	global	$fdtScenarioId;
	global	$fdtShareId;
	global	$fdtSharePassword;
	global	$fdtShareType;
	global	$fdtShareDate;

	//ﾌﾟﾛﾊﾟﾃｨGet
	$fdtScenarioId = $clsSwScenarioShare->clsSwScenarioShareGetScenarioId();            //シナリオID
	$fdtShareId = $clsSwScenarioShare->clsSwScenarioShareGetShareId();                  //共有ID
	$fdtSharePassword = $clsSwScenarioShare->clsSwScenarioShareGetSharePassword();      //パスワード
	$fdtShareType = $clsSwScenarioShare->clsSwScenarioShareGetShareType();              //公開・非公開
	$fdtShareDate = $clsSwScenarioShare->clsSwScenarioShareGetShareDate();              //共有日時
}//end function

// ------------------------------------------------------------------------------
//      ﾌﾟﾛﾊﾟﾃｨSet
//          fncSwScenarioShareSetProperty($clsSwScenarioShare)
// ------------------------------------------------------------------------------
function fncSwScenarioShareSetProperty($clsSwScenarioShare){
	global	$fdtScenarioId;
	global	$fdtShareId;
	global	$fdtSharePassword;
	global	$fdtShareType;
	global	$fdtShareDate;

	//ﾌﾟﾛﾊﾟﾃｨSet

	//シナリオIDはｶﾝﾏを取り除いてからSetする
	$fdtScenarioId = str_replace(",","",$fdtScenarioId);
	$clsSwScenarioShare->clsSwScenarioShareSetScenarioId($fdtScenarioId);            //シナリオID

	$clsSwScenarioShare->clsSwScenarioShareSetShareId($fdtShareId);                  //共有ID
	$clsSwScenarioShare->clsSwScenarioShareSetSharePassword($fdtSharePassword);      //パスワード
	$clsSwScenarioShare->clsSwScenarioShareSetShareType($fdtShareType);              //公開・非公開
	$clsSwScenarioShare->clsSwScenarioShareSetShareDate($fdtShareDate);              //共有日時
}//end function

// ------------------------------------------------------------------------------
//      DB INSERT
//          fncSwScenarioShareDBInsert($mySqlConnObj,$clsSwScenarioShare)
// ------------------------------------------------------------------------------
function fncSwScenarioShareDBInsert($mySqlConnObj,$clsSwScenarioShare){

	//ﾌﾟﾛﾊﾟﾃｨｾｯﾄ
	fncSwScenarioShareSetProperty($clsSwScenarioShare);
	//ﾃﾞｰﾀ管理ｸﾗｽ DbInsert
	$clsSwScenarioShare->clsSwScenarioShareDbInsert($mySqlConnObj);
}//end function

// ------------------------------------------------------------------------------
//      DB UPDATE
//          fncSwScenarioShareDBUpdate($mySqlConnObj,$clsSwScenarioShare)
// ------------------------------------------------------------------------------
function fncSwScenarioShareDBUpdate($mySqlConnObj,$clsSwScenarioShare){
	//ﾌﾟﾛﾊﾟﾃｨｾｯﾄ
	fncSwScenarioShareSetProperty($clsSwScenarioShare);
	//ﾃﾞｰﾀ管理ｸﾗｽ DbUpdate
	$clsSwScenarioShare->clsSwScenarioShareDbUpdate($mySqlConnObj);
}//end function

// ------------------------------------------------------------------------------
//      DB DELETE
//          fncSwScenarioShareDBDelete($mySqlConnObj,$clsSwScenarioShare)
// ------------------------------------------------------------------------------
function fncSwScenarioShareDBDelete($mySqlConnObj,$clsSwScenarioShare){
	//ﾌﾟﾛﾊﾟﾃｨｾｯﾄ
	fncSwScenarioShareSetProperty($clsSwScenarioShare);
	//ﾃﾞｰﾀ管理ｸﾗｽ DbDelete
	$clsSwScenarioShare->clsSwScenarioShareDbDelete($mySqlConnObj);
}//end function


// -----------------------------------------------------------
?>