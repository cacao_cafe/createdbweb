<?php
// -----------------------------------------------------------
//
// Copyright (C) 2015 ScenarioWriterCafe All Rights Reserved.
// 
//     共有ID管理 ﾃﾞｰﾀ管理ｸﾗｽ
//     clsSwScenarioShare
// -----------------------------------------------------------
class clsSwScenarioShare{
	var $ScenarioId;                      //シナリオID
	var $ShareId;                         //共有ID
	var $SharePassword;                   //パスワード
	var $ShareType;                       //公開・非公開
	var $ShareDate;                       //共有日時
// ｺﾝｽﾄﾗｸﾀ
    function clsSwScenarioShare(){
    }
// ﾌﾟﾛﾊﾟﾃｨ
// --------------------------------------
// ﾌﾟﾛﾊﾟﾃｨ Get ScenarioId
//    シナリオID を返す
// --------------------------------------
	function clsSwScenarioShareGetScenarioId(){
		return $this->ScenarioId;
	}
// --------------------------------------
// ﾌﾟﾛﾊﾟﾃｨ Set ScenarioId
//    シナリオID を設定する
// --------------------------------------
	function clsSwScenarioShareSetScenarioId($valScenarioId){
		$this->ScenarioId = $valScenarioId;
	}
// --------------------------------------
// ﾌﾟﾛﾊﾟﾃｨ Get ShareId
//    共有ID を返す
// --------------------------------------
	function clsSwScenarioShareGetShareId(){
		return $this->ShareId;
	}
// --------------------------------------
// ﾌﾟﾛﾊﾟﾃｨ Set ShareId
//    共有ID を設定する
// --------------------------------------
	function clsSwScenarioShareSetShareId($valShareId){
		$this->ShareId = $valShareId;
	}
// --------------------------------------
// ﾌﾟﾛﾊﾟﾃｨ Get SharePassword
//    パスワード を返す
// --------------------------------------
	function clsSwScenarioShareGetSharePassword(){
		return $this->SharePassword;
	}
// --------------------------------------
// ﾌﾟﾛﾊﾟﾃｨ Set SharePassword
//    パスワード を設定する
// --------------------------------------
	function clsSwScenarioShareSetSharePassword($valSharePassword){
		$this->SharePassword = $valSharePassword;
	}
// --------------------------------------
// ﾌﾟﾛﾊﾟﾃｨ Get ShareType
//    公開・非公開 を返す
// --------------------------------------
	function clsSwScenarioShareGetShareType(){
		return $this->ShareType;
	}
// --------------------------------------
// ﾌﾟﾛﾊﾟﾃｨ Set ShareType
//    公開・非公開 を設定する
// --------------------------------------
	function clsSwScenarioShareSetShareType($valShareType){
		$this->ShareType = $valShareType;
	}
// --------------------------------------
// ﾌﾟﾛﾊﾟﾃｨ Get ShareDate
//    共有日時 を返す
// --------------------------------------
	function clsSwScenarioShareGetShareDate(){
		return $this->ShareDate;
	}
// --------------------------------------
// ﾌﾟﾛﾊﾟﾃｨ Set ShareDate
//    共有日時 を設定する
// --------------------------------------
	function clsSwScenarioShareSetShareDate($valShareDate){
		$this->ShareDate = $valShareDate;
	}
// -------------------------------------------------------------
//    ｸﾗｽ初期化
// -------------------------------------------------------------
	function clsSwScenarioShareInit($mySqlConnObj,$valScenarioId){
		//ﾌﾟﾛﾊﾟﾃｨの初期化
		$this->ScenarioId = '';                      //シナリオID
		$this->ShareId = '';                         //共有ID
		$this->SharePassword = '';                   //パスワード
		$this->ShareType = '';                       //公開・非公開
		$this->ShareDate = '';                       //共有日時
		//DB から ﾃﾞｰﾀを取得しﾌﾟﾛﾊﾟﾃｨにｾｯﾄする
		$strSQL = <<<END_OF_SQL

		SELECT * FROM SW_SCENARIO_SHARE
				WHERE SCENARIO_ID = '$valScenarioId';
END_OF_SQL;

		$myResult = $mySqlConnObj->query($strSQL) or die($this->fncAlert($mySqlConnObj->error));
		while($myRow = $myResult->fetch_assoc( )){
			$this->ScenarioId = $myRow['SCENARIO_ID'];
			$this->ShareId = $myRow['SHARE_ID'];
			$this->SharePassword = $myRow['SHARE_PASSWORD'];
			$this->ShareType = $myRow['SHARE_TYPE'];
			$this->ShareDate = $myRow['SHARE_DATE'];
		}//end while
    }//end function

// -------------------------------------------------------------
//    DB更新　INSERT
// -------------------------------------------------------------
	function clsSwScenarioShareDbInsert($mySqlConnObj){
		//ﾌﾟﾛﾊﾟﾃｨ取得
		$valScenarioId = $this->ScenarioId;
		$valShareId = $this->ShareId;
		$valSharePassword = $this->SharePassword;
		$valShareType = $this->ShareType;
		$valShareDate = $this->ShareDate;
			//INSERT SQL
			$strSQL = <<<END_OF_SQL

			INSERT INTO SW_SCENARIO_SHARE(
				  SCENARIO_ID
				, SHARE_ID
				, SHARE_PASSWORD
				, SHARE_TYPE
				, SHARE_DATE
			) values (
				  '$valScenarioId'
				, '$valShareId'
				, '$valSharePassword'
				, '$valShareType'
				, current_timestamp
			);
END_OF_SQL;

			//INSRET 
			$myResult = $mySqlConnObj->prepare($strSQL);
			$stmt->execute();

	}//end function

// -------------------------------------------------------------
//    DB更新　UPDATE
// -------------------------------------------------------------
	function clsSwScenarioShareDbUpdate($mySqlConnObj){
		//ﾌﾟﾛﾊﾟﾃｨ取得
		$valScenarioId = $this->ScenarioId;
		$valShareId = $this->ShareId;
		$valSharePassword = $this->SharePassword;
		$valShareType = $this->ShareType;
		$valShareDate = $this->ShareDate;
		//登録確認
		$strSQL = <<<END_OF_SQL

		SELECT * FROM SW_SCENARIO_SHARE
				WHERE SCENARIO_ID = '$valScenarioId';
END_OF_SQL;

		//登録確認SQL Execute 
		$myResult = $mySqlConnObj->prepare($strSQL);
		$stmt->execute();

		//件数取得
		$myRowCnt = $myResult->num_rows;
		if($myRowCnt == 0 ){
			echo $this->fncAlert("clsSwScenarioShare ->> NOT ENTRY!");
		}else{
			//UPDATE SQL
			$strSQL = <<<END_OF_SQL

			UPDATE SW_SCENARIO_SHARE SET 
				  SHARE_ID = '$valShareId'
				, SHARE_PASSWORD = '$valSharePassword'
				, SHARE_TYPE = '$valShareType'
				, SHARE_DATE = current_timestamp
				WHERE SCENARIO_ID = '$valScenarioId';
END_OF_SQL;

			//UPDATESQL Execute 
			$myResult = $mySqlConnObj->prepare($strSQL);
			$stmt->execute();

		}//end if
	}//end function

// -------------------------------------------------------------
//    DB更新　DELETE
// -------------------------------------------------------------
	function clsSwScenarioShareDbDelete($mySqlConnObj){
		//ﾌﾟﾛﾊﾟﾃｨ取得
		$valScenarioId = $this->ScenarioId;
		//登録確認
		$strSQL = <<<END_OF_SQL

		SELECT * FROM SW_SCENARIO_SHARE
				WHERE SCENARIO_ID = '$valScenarioId';
END_OF_SQL;

		//登録確認SQL Execute 
		$myResult = $mySqlConnObj->prepare($strSQL);
		$stmt->execute();

		//件数取得
		$myRowCnt = $myResult->num_rows;
		if($myRowCnt == 0 ){
			echo $this->fncAlert("clsSwScenarioShare ->> NOT ENTRY!");
		}else{
			//DELETE SQL
			$strSQL = <<<END_OF_SQL

			DELETE FROM SW_SCENARIO_SHARE
				WHERE SCENARIO_ID = '$valScenarioId';
END_OF_SQL;

			//DELETE SQL Execute 
			$myResult = $mySqlConnObj->prepare($strSQL);
			$stmt->execute();

		}//end if
	}//end function

	// ------------------------------------------------------------------------------
	// java script alert
	// ------------------------------------------------------------------------------
	function fncAlert($AlertMsg){
		$strAlert = <<<END_OF_TEXT

			<script language="JavaScript">
				bootbox.alert("$AlertMsg",function() {
			});
			</script>
END_OF_TEXT;

		return $strAlert;
	}
	
} // end class
// -----------------------------------------------------------
?>