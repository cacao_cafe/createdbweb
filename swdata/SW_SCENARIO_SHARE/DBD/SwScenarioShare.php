<?php
// -----------------------------------------------------------
//
// Copyright (C) 2015 ScenarioWriterCafe All Rights Reserved.
// 
//     CREATE TABLE SW_SCENARIO_SHARE
//
//     createTable_SwScenarioShare.php
// -----------------------------------------------------------
$Result = fncCreateTable();
echo $Result;
exit;


// -----------------------------------------------------------
//     fncCreateTable()
// -----------------------------------------------------------
function fncCreateTable(){
	//共通定数
	include_once("../../sw_config/swConstant.php");

	//MySQLに接続
	include_once("../include/ConnectMySQL.php");

	//DROP TABLE SQLを編集
	$strSQL = <<<END_OF_SQL

DROP TABLE IF EXISTS SW_SCENARIO_SHARE;

END_OF_SQL;

	//DROP TABLE EXECUTE 
	$stmt = $mySqlConnObj->prepare($strSQL);
	$stmt->execute();

	//CREATE TABLE SQLを編集
	$strSQL = <<<END_OF_SQL

CREATE TABLE SW_SCENARIO_SHARE(
	  SCENARIO_ID                      INT NOT NULL COMMENT 'シナリオID'
	, SHARE_ID                         VARCHAR(256)  COMMENT '共有ID'
	, SHARE_PASSWORD                   VARCHAR(256)  COMMENT 'パスワード'
	, SHARE_TYPE                       INT  COMMENT '公開・非公開'
	, SHARE_DATE                       TIMESTAMP  COMMENT '共有日時'
	, PRIMARY KEY (SCENARIO_ID)
	)DEFAULT CHARACTER SET 'UTF8'
	COMMENT '共有ID管理';

END_OF_SQL;

	//CREATE TABLE EXECUTE 
	$stmt = $mySqlConnObj->prepare($strSQL);
	$stmt->execute();

	//UNIQUE INDEX のSQLを編集
	$strSQL = <<<END_OF_SQL

	CREATE UNIQUE INDEX SW_SCENARIO_SHARE_KEY_SCENARIO_ID ON SW_SCENARIO_SHARE(SCENARIO_ID);
END_OF_SQL;

	//CREATE INDEX EXECUTE 
	$stmt = $mySqlConnObj->prepare($strSQL);
	$stmt->execute();


	//DB切断
	include_once("../include/DisConnectMySQL.php");
	
	
	return	'CREATE TABLE SW_SCENARIO_SHARE Success...';
}//end function
// -----------------------------------------------------------
?>