<?php
// -----------------------------------------------------------
//
// Copyright (C) 2015 ScenarioWriterCafe All Rights Reserved.
// 
//     CREATE TABLE SW_USER_OPTION_SETTING
//
//     createTable_SwUserOptionSetting.php
// -----------------------------------------------------------
$Result = fncCreateTable();
echo $Result;
exit;


// -----------------------------------------------------------
//     fncCreateTable()
// -----------------------------------------------------------
function fncCreateTable(){
	//共通定数
	include_once("../../sw_config/swConstant.php");

	//MySQLに接続
	include_once("../include/ConnectMySQL.php");

	//DROP TABLE SQLを編集
	$strSQL = <<<END_OF_SQL

DROP TABLE IF EXISTS SW_USER_OPTION_SETTING;

END_OF_SQL;

	//DROP TABLE EXECUTE 
	$stmt = $mySqlConnObj->prepare($strSQL);
	$stmt->execute();

	//CREATE TABLE SQLを編集
	$strSQL = <<<END_OF_SQL

CREATE TABLE SW_USER_OPTION_SETTING(
	  USER_ID                          INT NOT NULL COMMENT 'USER_ID'
	, CHARACTER_LENGTH                 INT  COMMENT 'キャラクタ部の文字数'
	, BODY_LENGTH                      INT  COMMENT 'シナリオ本文の文字数'
	, PRIMARY KEY (USER_ID)
	)DEFAULT CHARACTER SET 'UTF8'
	COMMENT 'ユーザーオプション設定';

END_OF_SQL;

	//CREATE TABLE EXECUTE 
	$stmt = $mySqlConnObj->prepare($strSQL);
	$stmt->execute();

	//UNIQUE INDEX のSQLを編集
	$strSQL = <<<END_OF_SQL

	CREATE UNIQUE INDEX SW_USER_OPTION_SETTING_KEY_USER_ID ON SW_USER_OPTION_SETTING(USER_ID);
END_OF_SQL;

	//CREATE INDEX EXECUTE 
	$stmt = $mySqlConnObj->prepare($strSQL);
	$stmt->execute();


	//DB切断
	include_once("../include/DisConnectMySQL.php");
	
	
	return	'CREATE TABLE SW_USER_OPTION_SETTING Success...';
}//end function
// -----------------------------------------------------------
?>