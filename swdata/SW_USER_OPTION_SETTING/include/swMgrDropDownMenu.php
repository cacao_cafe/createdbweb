<?php
// -----------------------------------------------------------
//
// Copyright (C) 2015 dbu@mac.com All Rights Reserved.
// 
//     管理画面サブメニュー
//
//     acctMgrDropDownMenu.php
// -----------------------------------------------------------
print <<<END_OF_HTML


  <div class="dropdown">
    <button class="btn btn-default btn-sm dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false">
    	管理メニュー<span class="caret"></span>
    </button>
    <ul class="dropdown-menu" role="menu">
      <li><a href="#">Home</a></li>
      <li class="divider"></li>
      <li><a href="#">SAMPLE</a></li>
      <li><a href="#">SAMPLE</a></li>
      <li><a href="#">SAMPLE</a></li>
      <li class="divider"></li>
      <li><a href="#">SAMPLE</a></li>
      <li><a href="#">SAMPLE</a></li>
      <li><a href="#">SAMPLE</a></li>
      <li class="divider"></li>
      <li><a href="#">SAMPLE</a></li>
      <li><a href="#">SAMPLE</a></li>
      <li><a href="#">SAMPLE</a></li>
      <li><a href="#">SAMPLE</a></li>
    </ul>
  </div>

END_OF_HTML;

// -----------------------------------------------------------
?>
