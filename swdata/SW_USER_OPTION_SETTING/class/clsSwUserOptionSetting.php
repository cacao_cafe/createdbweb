<?php
// -----------------------------------------------------------
//
// Copyright (C) 2015 ScenarioWriterCafe All Rights Reserved.
// 
//     ユーザーオプション設定 ﾃﾞｰﾀ管理ｸﾗｽ
//     clsSwUserOptionSetting
// -----------------------------------------------------------
class clsSwUserOptionSetting{
	var $UserId;                          //USER_ID
	var $CharacterLength;                 //キャラクタ部の文字数
	var $BodyLength;                      //シナリオ本文の文字数
// ｺﾝｽﾄﾗｸﾀ
    function clsSwUserOptionSetting(){
    }
// ﾌﾟﾛﾊﾟﾃｨ
// --------------------------------------
// ﾌﾟﾛﾊﾟﾃｨ Get UserId
//    USER_ID を返す
// --------------------------------------
	function clsSwUserOptionSettingGetUserId(){
		return $this->UserId;
	}
// --------------------------------------
// ﾌﾟﾛﾊﾟﾃｨ Set UserId
//    USER_ID を設定する
// --------------------------------------
	function clsSwUserOptionSettingSetUserId($valUserId){
		$this->UserId = $valUserId;
	}
// --------------------------------------
// ﾌﾟﾛﾊﾟﾃｨ Get CharacterLength
//    キャラクタ部の文字数 を返す
// --------------------------------------
	function clsSwUserOptionSettingGetCharacterLength(){
		return $this->CharacterLength;
	}
// --------------------------------------
// ﾌﾟﾛﾊﾟﾃｨ Set CharacterLength
//    キャラクタ部の文字数 を設定する
// --------------------------------------
	function clsSwUserOptionSettingSetCharacterLength($valCharacterLength){
		$this->CharacterLength = $valCharacterLength;
	}
// --------------------------------------
// ﾌﾟﾛﾊﾟﾃｨ Get BodyLength
//    シナリオ本文の文字数 を返す
// --------------------------------------
	function clsSwUserOptionSettingGetBodyLength(){
		return $this->BodyLength;
	}
// --------------------------------------
// ﾌﾟﾛﾊﾟﾃｨ Set BodyLength
//    シナリオ本文の文字数 を設定する
// --------------------------------------
	function clsSwUserOptionSettingSetBodyLength($valBodyLength){
		$this->BodyLength = $valBodyLength;
	}
// -------------------------------------------------------------
//    ｸﾗｽ初期化
// -------------------------------------------------------------
	function clsSwUserOptionSettingInit($mySqlConnObj,$valUserId){
		//ﾌﾟﾛﾊﾟﾃｨの初期化
		$this->UserId = '';                          //USER_ID
		$this->CharacterLength = '';                 //キャラクタ部の文字数
		$this->BodyLength = '';                      //シナリオ本文の文字数
		//DB から ﾃﾞｰﾀを取得しﾌﾟﾛﾊﾟﾃｨにｾｯﾄする
		$strSQL = <<<END_OF_SQL

		SELECT * FROM SW_USER_OPTION_SETTING
				WHERE USER_ID = '$valUserId';
END_OF_SQL;

		$myResult = $mySqlConnObj->query($strSQL) or die($this->fncAlert($mySqlConnObj->error));
		while($myRow = $myResult->fetch_assoc( )){
			$this->UserId = $myRow['USER_ID'];
			$this->CharacterLength = $myRow['CHARACTER_LENGTH'];
			$this->BodyLength = $myRow['BODY_LENGTH'];
		}//end while
    }//end function

// -------------------------------------------------------------
//    DB更新　INSERT
// -------------------------------------------------------------
	function clsSwUserOptionSettingDbInsert($mySqlConnObj){
		//ﾌﾟﾛﾊﾟﾃｨ取得
		$valUserId = $this->UserId;
		$valCharacterLength = $this->CharacterLength;
		$valBodyLength = $this->BodyLength;
			//INSERT SQL
			$strSQL = <<<END_OF_SQL

			INSERT INTO SW_USER_OPTION_SETTING(
				  USER_ID
				, CHARACTER_LENGTH
				, BODY_LENGTH
			) values (
				  '$valUserId'
				, '$valCharacterLength'
				, '$valBodyLength'
			);
END_OF_SQL;

			//INSRET 
			$myResult = $mySqlConnObj->prepare($strSQL);
			$stmt->execute();

	}//end function

// -------------------------------------------------------------
//    DB更新　UPDATE
// -------------------------------------------------------------
	function clsSwUserOptionSettingDbUpdate($mySqlConnObj){
		//ﾌﾟﾛﾊﾟﾃｨ取得
		$valUserId = $this->UserId;
		$valCharacterLength = $this->CharacterLength;
		$valBodyLength = $this->BodyLength;
		//登録確認
		$strSQL = <<<END_OF_SQL

		SELECT * FROM SW_USER_OPTION_SETTING
				WHERE USER_ID = '$valUserId';
END_OF_SQL;

		//登録確認SQL Execute 
		$myResult = $mySqlConnObj->prepare($strSQL);
		$stmt->execute();

		//件数取得
		$myRowCnt = $myResult->num_rows;
		if($myRowCnt == 0 ){
			echo $this->fncAlert("clsSwUserOptionSetting ->> NOT ENTRY!");
		}else{
			//UPDATE SQL
			$strSQL = <<<END_OF_SQL

			UPDATE SW_USER_OPTION_SETTING SET 
				  CHARACTER_LENGTH = '$valCharacterLength'
				, BODY_LENGTH = '$valBodyLength'
				WHERE USER_ID = '$valUserId';
END_OF_SQL;

			//UPDATESQL Execute 
			$myResult = $mySqlConnObj->prepare($strSQL);
			$stmt->execute();

		}//end if
	}//end function

// -------------------------------------------------------------
//    DB更新　DELETE
// -------------------------------------------------------------
	function clsSwUserOptionSettingDbDelete($mySqlConnObj){
		//ﾌﾟﾛﾊﾟﾃｨ取得
		$valUserId = $this->UserId;
		//登録確認
		$strSQL = <<<END_OF_SQL

		SELECT * FROM SW_USER_OPTION_SETTING
				WHERE USER_ID = '$valUserId';
END_OF_SQL;

		//登録確認SQL Execute 
		$myResult = $mySqlConnObj->prepare($strSQL);
		$stmt->execute();

		//件数取得
		$myRowCnt = $myResult->num_rows;
		if($myRowCnt == 0 ){
			echo $this->fncAlert("clsSwUserOptionSetting ->> NOT ENTRY!");
		}else{
			//DELETE SQL
			$strSQL = <<<END_OF_SQL

			DELETE FROM SW_USER_OPTION_SETTING
				WHERE USER_ID = '$valUserId';
END_OF_SQL;

			//DELETE SQL Execute 
			$myResult = $mySqlConnObj->prepare($strSQL);
			$stmt->execute();

		}//end if
	}//end function

	// ------------------------------------------------------------------------------
	// java script alert
	// ------------------------------------------------------------------------------
	function fncAlert($AlertMsg){
		$strAlert = <<<END_OF_TEXT

			<script language="JavaScript">
				bootbox.alert("$AlertMsg",function() {
			});
			</script>
END_OF_TEXT;

		return $strAlert;
	}
	
} // end class
// -----------------------------------------------------------
?>