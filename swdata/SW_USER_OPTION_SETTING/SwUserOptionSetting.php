<?php
// -----------------------------------------------------------
//
// Copyright (C) 2015 ScenarioWriterCafe All Rights Reserved.
// 
//     SW_USER_OPTION_SETTING I/O ｼｽﾃﾑ
//
//     SwUserOptionSetting.php
// -----------------------------------------------------------
// ------------------------------------------------------------------------------
	//定数読み込み
	include_once("../sw_config/swConstant.php");
	//DB接続ｸﾗｽの初期化
	include_once("./include/ConnectMySQL.php");
// ------------------------------------------------------------------------------
	//ﾃﾞﾌｫﾙﾄｱｸｼｮﾝ
	$ThisPHP = 'SwUserOptionSetting.php';
	//ﾃﾞｰﾀ管理ｸﾗｽ
	include_once("./class/clsSwUserOptionSetting.php");
	$clsSwUserOptionSetting = new clsSwUserOptionSetting();
	//共通関数をｲﾝｸﾙｰﾄﾞ
	include_once("./include/swFunc.php");
	// ﾍｯﾀﾞ表示
	$HtmlTitle = "ユーザーオプション設定";
	include_once("./include/swHeader.php");

	//ﾌｫｰﾑのﾃﾞｰﾀを読み込む
	fncGetPostItems();

	//MainProcedure
	fncMainProc($mySqlConnObj);

	exit();

// ------------------------------------------------------------------------------
//      POSTされた要素を取得
//          fncGetPostItems()
// ------------------------------------------------------------------------------
function fncGetPostItems(){
	//共通global変数
	global	$SubmitMode;
	//ﾌｫｰﾑ name要素をglobal変数にする
	global	$fdtUserId;
	global	$fdtCharacterLength;
	global	$fdtBodyLength;

	//処理ﾓｰﾄが空白ならreturnするﾞ
	if(!isset($_POST['SubmitMode'])){return;}
	//処理ﾓｰﾄﾞ
	$SubmitMode = swFunc_GetPostData('SubmitMode');
	if($SubmitMode == ''){return;}
	//POSTされた要素を取得
	$fdtUserId = swFunc_GetPostData('fdtUserId');                    //USER_ID
	$fdtCharacterLength = swFunc_GetPostData('fdtCharacterLength');  //キャラクタ部の文字数
	$fdtBodyLength = swFunc_GetPostData('fdtBodyLength');            //シナリオ本文の文字数
}//end function

// ------------------------------------------------------------------------------
//      MAIN PROCEDURE
//          fncMainProc($mySqlConnObj)
// ------------------------------------------------------------------------------
function fncMainProc($mySqlConnObj){
	global	$SubmitMode;
	//class
	global	$clsSwUserOptionSetting;

	global	$fdtUserId;
	global	$fdtCharacterLength;
	global	$fdtBodyLength;

	//SubmitModeで処理を分岐
	switch($SubmitMode ){
		case 'SELECT':
			//ﾃﾞｰﾀ管理ｸﾗｽの初期化
			$clsSwUserOptionSetting->clsSwUserOptionSettingInit($mySqlConnObj,$fdtUserId);
			//ﾌﾟﾛﾊﾟﾃｨGet
			fncSwUserOptionSettingGetProperty($clsSwUserOptionSetting);
			break;
		case 'INSERT':
			//登録処理
			fncSwUserOptionSettingDBInsert($mySqlConnObj,$clsSwUserOptionSetting);
			//選択状態にする
			$SubmitMode = 'SELECT';
			//ﾃﾞｰﾀ管理ｸﾗｽの初期化
			$clsSwUserOptionSetting->clsSwUserOptionSettingInit($mySqlConnObj,$fdtUserId);
			//ﾌﾟﾛﾊﾟﾃｨGet
			fncSwUserOptionSettingGetProperty($clsSwUserOptionSetting);
			break;
		case 'UPDATE':
			//更新処理
			fncSwUserOptionSettingDBUpdate($mySqlConnObj,$clsSwUserOptionSetting);
			//選択状態にする
			$SubmitMode = 'SELECT';
			//ﾃﾞｰﾀ管理ｸﾗｽの初期化
			$clsSwUserOptionSetting->clsSwUserOptionSettingInit($mySqlConnObj,$fdtUserId);
			//ﾌﾟﾛﾊﾟﾃｨGet
			fncSwUserOptionSettingGetProperty($clsSwUserOptionSetting);
			break;
		case 'DELETE':
			//削除処理
			fncSwUserOptionSettingDBDelete($mySqlConnObj,$clsSwUserOptionSetting);
			//ﾌﾟﾛﾊﾟﾃｨReset
			fncSwUserOptionSettingResetProperty($clsSwUserOptionSetting);
			//初期状態にする
			$SubmitMode = 'RESET';
			break;
		case 'RESET':
			//ﾌﾟﾛﾊﾟﾃｨReset
			fncSwUserOptionSettingResetProperty($clsSwUserOptionSetting);
			//初期状態にする
			$SubmitMode = 'RESET';
			break;
		default:
			break;
	}
	//ﾌｫｰﾑの表示
	fncMainForm($mySqlConnObj);
}//end function

// ------------------------------------------------------------------------------
//      MAIN FORM
//          fncMainForm($mySqlConnObj)
// ------------------------------------------------------------------------------
function fncMainForm($mySqlConnObj){
	global	$ThisPHP,$SubmitMode;
	global	$fdtUserId;
	global	$fdtCharacterLength;
	global	$fdtBodyLength;

	//css powerd by Bootstrap ver3
	print <<<END_OF_HTML

	<div class="wrap" >
	
		<div class="container-fluid">
		
		<!-- ﾒﾆｭｰ 制御ﾎﾞﾀﾝ 表示ROW -->
			<div class="row">
				<div class="col-sm-12">
				<legend class="form-inline submenu">
				<ul class="list-inline">
				  <li>
END_OF_HTML;

	//管理メニュー ﾄﾞﾛｯﾌﾟﾀﾞｳﾝﾒﾆｭｰ
	$TargetForm = 'frmSwUserOptionSetting';
	include_once("./include/swMgrDropDownMenu.php");

	
    //SubmitModeで表示するボタンを制御する
	switch($SubmitMode){
		case 'SELECT':
			print <<<END_OF_HTML
			 	</li>
			 	<li>
			        <input type="button" value="RESET"
			            id="btnReset"
			            onclick="fncSwUserOptionSettingSubmit(frmSwUserOptionSetting,'RESET','FALSE','');"
			            class="btn btn-default btn-sm">
			        <span style="width: 120px;"></span>
			        <input type="button" value="INSERT"
			            id="btnInsert"
			            onclick="fncSwUserOptionSettingSubmit(frmSwUserOptionSetting,'INSERT','TRUE','新規登録');"
			            class="btn btn-warning btn-sm">
			        <input type="button" value="UPDATE"
			            id="btnUpdate"
			            onclick="fncSwUserOptionSettingSubmit(frmSwUserOptionSetting,'UPDATE','TRUE','更新');"
			            class="btn btn-warning btn-sm">
			        <input type="button" value="DELETE"
			            id="btnDelete"
			            onclick="fncSwUserOptionSettingSubmit(frmSwUserOptionSetting,'DELETE','TRUE','削除');"
			            class="btn btn-danger btn-sm">
			
				</li>
END_OF_HTML;

	
			break;
        default:
			print <<<END_OF_HTML
			    </li>
			 	<li>
			        <input type="button" value="RESET"
			            id="btnReset"
			            onclick="fncSwUserOptionSettingSubmit(frmSwUserOptionSetting,'RESET','FALSE','');"
			            class="btn btn-default btn-sm">
			        <input type="button" value="INSERT"
			            id="btnInsert"
			            onclick="fncSwUserOptionSettingSubmit(frmSwUserOptionSetting,'INSERT','TRUE','新規登録');"
			            class="btn btn-warning btn-sm">
			
				</li>
END_OF_HTML;
            break;
    }
	print <<<END_OF_HTML

					</ul>
					</legend>
    			</div><!-- col-sm-12 end -->
    		</div><!-- row end -->
    		<!-- ﾒﾆｭｰ 制御ﾎﾞﾀﾝ 表示ROW ここまで-->

    		<!-- ﾌｫｰﾑ と ﾘｽﾄ -->
    		<div class="row row-0">
				<div class="col-sm-7 row-0">
					<form class="form-horizontal" role="form" name="frmSwUserOptionSetting" id="frmSwUserOptionSetting" method="POST" action="$ThisPHP">
						<input type="hidden" name="SubmitMode" id="SubmitMode" value="$SubmitMode">

END_OF_HTML;

	print <<<END_OF_HTML

							<div class="form-group row-0">
								<label for="fdtUserId" class="control-label col-xs-3">USER_ID</label>
								<div class="col-xs-8">
									<input type="number"
										class="form-control input-sm text-right"
										id="fdtUserId" name="fdtUserId"
										value="$fdtUserId"
										onkeyup="AjaxFunc_AddFigure(frmSwUserOptionSetting,'fdtUserId');"
										placeholder="USER_ID">
								</div>
							</div>
END_OF_HTML;
	print <<<END_OF_HTML

							<div class="form-group row-0">
								<label for="fdtCharacterLength" class="control-label col-xs-3">キャラクタ部の文字数</label>
								<div class="col-xs-8">
									<input type="number"
										class="form-control input-sm text-right"
										id="fdtCharacterLength" name="fdtCharacterLength"
										value="$fdtCharacterLength"
										onkeyup="AjaxFunc_AddFigure(frmSwUserOptionSetting,'fdtCharacterLength');"
										placeholder="キャラクタ部の文字数">
								</div>
							</div>
END_OF_HTML;
	print <<<END_OF_HTML

							<div class="form-group row-0">
								<label for="fdtBodyLength" class="control-label col-xs-3">シナリオ本文の文字数</label>
								<div class="col-xs-8">
									<input type="number"
										class="form-control input-sm text-right"
										id="fdtBodyLength" name="fdtBodyLength"
										value="$fdtBodyLength"
										onkeyup="AjaxFunc_AddFigure(frmSwUserOptionSetting,'fdtBodyLength');"
										placeholder="シナリオ本文の文字数">
								</div>
							</div>
END_OF_HTML;
	print <<<END_OF_HTML

						</form>
					</div>
					<div class="col-sm-5" >
	                	<!-- リストの表示位置 -->
	                    <span id="side_list_area"></span>
					</div>
				</div>
			</div>
 
		</div><!--container-->
	</div><!--wrap-->


    <script type="text/javascript" src="./ajax/ajaxSwUserOptionSetting.js"></script>
    <script type="text/javascript">
        fncMakeSwUserOptionSettingList(frmSwUserOptionSetting);
    </script>


	<!--footer表示位置--><span class="sw-footer"></span>
	<script language="JavaScript">
		AjaxFunc_FooterLoad();
	</script>
	
  </body>
</html>

END_OF_HTML;

}//end function

// ------------------------------------------------------------------------------
//      ﾌﾟﾛﾊﾟﾃｨReset
//          fncSwUserOptionSettingResetProperty($clsSwUserOptionSetting)
// ------------------------------------------------------------------------------
function fncSwUserOptionSettingResetProperty($clsSwUserOptionSetting){
	global	$fdtUserId;
	global	$fdtCharacterLength;
	global	$fdtBodyLength;

	//ﾌﾟﾛﾊﾟﾃｨReset
	$fdtUserId = "";                          //USER_ID
	$fdtCharacterLength = "";                 //キャラクタ部の文字数
	$fdtBodyLength = "";                      //シナリオ本文の文字数
}//end function

// ------------------------------------------------------------------------------
//      ﾌﾟﾛﾊﾟﾃｨGet
//          fncSwUserOptionSettingGetProperty($clsSwUserOptionSetting)
// ------------------------------------------------------------------------------
function fncSwUserOptionSettingGetProperty($clsSwUserOptionSetting){
	global	$fdtUserId;
	global	$fdtCharacterLength;
	global	$fdtBodyLength;

	//ﾌﾟﾛﾊﾟﾃｨGet
	$fdtUserId = $clsSwUserOptionSetting->clsSwUserOptionSettingGetUserId();                    //USER_ID
	$fdtCharacterLength = $clsSwUserOptionSetting->clsSwUserOptionSettingGetCharacterLength();  //キャラクタ部の文字数
	$fdtBodyLength = $clsSwUserOptionSetting->clsSwUserOptionSettingGetBodyLength();            //シナリオ本文の文字数
}//end function

// ------------------------------------------------------------------------------
//      ﾌﾟﾛﾊﾟﾃｨSet
//          fncSwUserOptionSettingSetProperty($clsSwUserOptionSetting)
// ------------------------------------------------------------------------------
function fncSwUserOptionSettingSetProperty($clsSwUserOptionSetting){
	global	$fdtUserId;
	global	$fdtCharacterLength;
	global	$fdtBodyLength;

	//ﾌﾟﾛﾊﾟﾃｨSet

	//USER_IDはｶﾝﾏを取り除いてからSetする
	$fdtUserId = str_replace(",","",$fdtUserId);
	$clsSwUserOptionSetting->clsSwUserOptionSettingSetUserId($fdtUserId);                    //USER_ID


	//キャラクタ部の文字数はｶﾝﾏを取り除いてからSetする
	$fdtCharacterLength = str_replace(",","",$fdtCharacterLength);
	$clsSwUserOptionSetting->clsSwUserOptionSettingSetCharacterLength($fdtCharacterLength);  //キャラクタ部の文字数


	//シナリオ本文の文字数はｶﾝﾏを取り除いてからSetする
	$fdtBodyLength = str_replace(",","",$fdtBodyLength);
	$clsSwUserOptionSetting->clsSwUserOptionSettingSetBodyLength($fdtBodyLength);            //シナリオ本文の文字数

}//end function

// ------------------------------------------------------------------------------
//      DB INSERT
//          fncSwUserOptionSettingDBInsert($mySqlConnObj,$clsSwUserOptionSetting)
// ------------------------------------------------------------------------------
function fncSwUserOptionSettingDBInsert($mySqlConnObj,$clsSwUserOptionSetting){

	//ﾌﾟﾛﾊﾟﾃｨｾｯﾄ
	fncSwUserOptionSettingSetProperty($clsSwUserOptionSetting);
	//ﾃﾞｰﾀ管理ｸﾗｽ DbInsert
	$clsSwUserOptionSetting->clsSwUserOptionSettingDbInsert($mySqlConnObj);
}//end function

// ------------------------------------------------------------------------------
//      DB UPDATE
//          fncSwUserOptionSettingDBUpdate($mySqlConnObj,$clsSwUserOptionSetting)
// ------------------------------------------------------------------------------
function fncSwUserOptionSettingDBUpdate($mySqlConnObj,$clsSwUserOptionSetting){
	//ﾌﾟﾛﾊﾟﾃｨｾｯﾄ
	fncSwUserOptionSettingSetProperty($clsSwUserOptionSetting);
	//ﾃﾞｰﾀ管理ｸﾗｽ DbUpdate
	$clsSwUserOptionSetting->clsSwUserOptionSettingDbUpdate($mySqlConnObj);
}//end function

// ------------------------------------------------------------------------------
//      DB DELETE
//          fncSwUserOptionSettingDBDelete($mySqlConnObj,$clsSwUserOptionSetting)
// ------------------------------------------------------------------------------
function fncSwUserOptionSettingDBDelete($mySqlConnObj,$clsSwUserOptionSetting){
	//ﾌﾟﾛﾊﾟﾃｨｾｯﾄ
	fncSwUserOptionSettingSetProperty($clsSwUserOptionSetting);
	//ﾃﾞｰﾀ管理ｸﾗｽ DbDelete
	$clsSwUserOptionSetting->clsSwUserOptionSettingDbDelete($mySqlConnObj);
}//end function


// -----------------------------------------------------------
?>