<?php
// -----------------------------------------------------------
//
// Copyright (C) 2015 ScenarioWriterCafe All Rights Reserved.
// 
//     SW_SYNOPSIS I/O ｼｽﾃﾑ
//
//     SwSynopsis.php
// -----------------------------------------------------------
// ------------------------------------------------------------------------------
	//定数読み込み
	include_once("../sw_config/swConstant.php");
	//DB接続ｸﾗｽの初期化
	include_once("./include/ConnectMySQL.php");
// ------------------------------------------------------------------------------
	//ﾃﾞﾌｫﾙﾄｱｸｼｮﾝ
	$ThisPHP = 'SwSynopsis.php';
	//ﾃﾞｰﾀ管理ｸﾗｽ
	include_once("./class/clsSwSynopsis.php");
	$clsSwSynopsis = new clsSwSynopsis();
	//共通関数をｲﾝｸﾙｰﾄﾞ
	include_once("./include/swFunc.php");
	// ﾍｯﾀﾞ表示
	$HtmlTitle = "シノプシス";
	include_once("./include/swHeader.php");

	//ﾌｫｰﾑのﾃﾞｰﾀを読み込む
	fncGetPostItems();

	//MainProcedure
	fncMainProc($mySqlConnObj);

	exit();

// ------------------------------------------------------------------------------
//      POSTされた要素を取得
//          fncGetPostItems()
// ------------------------------------------------------------------------------
function fncGetPostItems(){
	//共通global変数
	global	$SubmitMode;
	//ﾌｫｰﾑ name要素をglobal変数にする
	global	$fdtSynopsisId;
	global	$fdtScenarioId;
	global	$fdtSynopsis;
	global	$fdtUpdateDate;

	//処理ﾓｰﾄが空白ならreturnするﾞ
	if(!isset($_POST['SubmitMode'])){return;}
	//処理ﾓｰﾄﾞ
	$SubmitMode = swFunc_GetPostData('SubmitMode');
	if($SubmitMode == ''){return;}
	//POSTされた要素を取得
	$fdtSynopsisId = swFunc_GetPostData('fdtSynopsisId');            //SYNOPSIS_ID
	$fdtScenarioId = swFunc_GetPostData('fdtScenarioId');            //SCENARIO_ID
	$fdtSynopsis = swFunc_GetPostData('fdtSynopsis');                //SYNOPSIS
	$fdtUpdateDate = swFunc_GetPostData('fdtUpdateDate');            //更新日時
}//end function

// ------------------------------------------------------------------------------
//      MAIN PROCEDURE
//          fncMainProc($mySqlConnObj)
// ------------------------------------------------------------------------------
function fncMainProc($mySqlConnObj){
	global	$SubmitMode;
	//class
	global	$clsSwSynopsis;

	global	$fdtSynopsisId;
	global	$fdtScenarioId;
	global	$fdtSynopsis;
	global	$fdtUpdateDate;

	//SubmitModeで処理を分岐
	switch($SubmitMode ){
		case 'SELECT':
			//ﾃﾞｰﾀ管理ｸﾗｽの初期化
			$clsSwSynopsis->clsSwSynopsisInit($mySqlConnObj,$fdtSynopsisId);
			//ﾌﾟﾛﾊﾟﾃｨGet
			fncSwSynopsisGetProperty($clsSwSynopsis);
			break;
		case 'INSERT':
			//登録処理
			fncSwSynopsisDBInsert($mySqlConnObj,$clsSwSynopsis);
			//選択状態にする
			$SubmitMode = 'SELECT';
			//ﾃﾞｰﾀ管理ｸﾗｽの初期化
			$clsSwSynopsis->clsSwSynopsisInit($mySqlConnObj,$fdtSynopsisId);
			//ﾌﾟﾛﾊﾟﾃｨGet
			fncSwSynopsisGetProperty($clsSwSynopsis);
			break;
		case 'UPDATE':
			//更新処理
			fncSwSynopsisDBUpdate($mySqlConnObj,$clsSwSynopsis);
			//選択状態にする
			$SubmitMode = 'SELECT';
			//ﾃﾞｰﾀ管理ｸﾗｽの初期化
			$clsSwSynopsis->clsSwSynopsisInit($mySqlConnObj,$fdtSynopsisId);
			//ﾌﾟﾛﾊﾟﾃｨGet
			fncSwSynopsisGetProperty($clsSwSynopsis);
			break;
		case 'DELETE':
			//削除処理
			fncSwSynopsisDBDelete($mySqlConnObj,$clsSwSynopsis);
			//ﾌﾟﾛﾊﾟﾃｨReset
			fncSwSynopsisResetProperty($clsSwSynopsis);
			//初期状態にする
			$SubmitMode = 'RESET';
			break;
		case 'RESET':
			//ﾌﾟﾛﾊﾟﾃｨReset
			fncSwSynopsisResetProperty($clsSwSynopsis);
			//初期状態にする
			$SubmitMode = 'RESET';
			break;
		default:
			break;
	}
	//ﾌｫｰﾑの表示
	fncMainForm($mySqlConnObj);
}//end function

// ------------------------------------------------------------------------------
//      MAIN FORM
//          fncMainForm($mySqlConnObj)
// ------------------------------------------------------------------------------
function fncMainForm($mySqlConnObj){
	global	$ThisPHP,$SubmitMode;
	global	$fdtSynopsisId;
	global	$fdtScenarioId;
	global	$fdtSynopsis;
	global	$fdtUpdateDate;

	//css powerd by Bootstrap ver3
	print <<<END_OF_HTML

	<div class="wrap" >
	
		<div class="container-fluid">
		
		<!-- ﾒﾆｭｰ 制御ﾎﾞﾀﾝ 表示ROW -->
			<div class="row">
				<div class="col-sm-12">
				<legend class="form-inline submenu">
				<ul class="list-inline">
				  <li>
END_OF_HTML;

	//管理メニュー ﾄﾞﾛｯﾌﾟﾀﾞｳﾝﾒﾆｭｰ
	$TargetForm = 'frmSwSynopsis';
	include_once("./include/swMgrDropDownMenu.php");

	
    //SubmitModeで表示するボタンを制御する
	switch($SubmitMode){
		case 'SELECT':
			print <<<END_OF_HTML
			 	</li>
			 	<li>
			        <input type="button" value="RESET"
			            id="btnReset"
			            onclick="fncSwSynopsisSubmit(frmSwSynopsis,'RESET','FALSE','');"
			            class="btn btn-default btn-sm">
			        <span style="width: 120px;"></span>
			        <input type="button" value="INSERT"
			            id="btnInsert"
			            onclick="fncSwSynopsisSubmit(frmSwSynopsis,'INSERT','TRUE','新規登録');"
			            class="btn btn-warning btn-sm">
			        <input type="button" value="UPDATE"
			            id="btnUpdate"
			            onclick="fncSwSynopsisSubmit(frmSwSynopsis,'UPDATE','TRUE','更新');"
			            class="btn btn-warning btn-sm">
			        <input type="button" value="DELETE"
			            id="btnDelete"
			            onclick="fncSwSynopsisSubmit(frmSwSynopsis,'DELETE','TRUE','削除');"
			            class="btn btn-danger btn-sm">
			
				</li>
END_OF_HTML;

	
			break;
        default:
			print <<<END_OF_HTML
			    </li>
			 	<li>
			        <input type="button" value="RESET"
			            id="btnReset"
			            onclick="fncSwSynopsisSubmit(frmSwSynopsis,'RESET','FALSE','');"
			            class="btn btn-default btn-sm">
			        <input type="button" value="INSERT"
			            id="btnInsert"
			            onclick="fncSwSynopsisSubmit(frmSwSynopsis,'INSERT','TRUE','新規登録');"
			            class="btn btn-warning btn-sm">
			
				</li>
END_OF_HTML;
            break;
    }
	print <<<END_OF_HTML

					</ul>
					</legend>
    			</div><!-- col-sm-12 end -->
    		</div><!-- row end -->
    		<!-- ﾒﾆｭｰ 制御ﾎﾞﾀﾝ 表示ROW ここまで-->

    		<!-- ﾌｫｰﾑ と ﾘｽﾄ -->
    		<div class="row row-0">
				<div class="col-sm-7 row-0">
					<form class="form-horizontal" role="form" name="frmSwSynopsis" id="frmSwSynopsis" method="POST" action="$ThisPHP">
						<input type="hidden" name="SubmitMode" id="SubmitMode" value="$SubmitMode">

END_OF_HTML;

	print <<<END_OF_HTML

							<div class="form-group row-0">
								<label for="fdtSynopsisId" class="control-label col-xs-3">SYNOPSIS_ID</label>
								<div class="col-xs-8">
									<input type="number"
										class="form-control input-sm text-right"
										id="fdtSynopsisId" name="fdtSynopsisId"
										value="$fdtSynopsisId"
										onkeyup="AjaxFunc_AddFigure(frmSwSynopsis,'fdtSynopsisId');"
										placeholder="SYNOPSIS_ID">
								</div>
							</div>
END_OF_HTML;
	print <<<END_OF_HTML

							<div class="form-group row-0">
								<label for="fdtScenarioId" class="control-label col-xs-3">SCENARIO_ID</label>
								<div class="col-xs-8">
									<input type="number"
										class="form-control input-sm text-right"
										id="fdtScenarioId" name="fdtScenarioId"
										value="$fdtScenarioId"
										onkeyup="AjaxFunc_AddFigure(frmSwSynopsis,'fdtScenarioId');"
										placeholder="SCENARIO_ID">
								</div>
							</div>
END_OF_HTML;
	print <<<END_OF_HTML

							<div class="form-group row-0">
								<label for="fdtSynopsis" class="control-label col-xs-3">SYNOPSIS</label>
								<div class="col-xs-8">
									<textarea placeholder="SYNOPSIS" 
										class="form-control input-sm"
										id="fdtSynopsis" name="fdtSynopsis"
										rows="3"
										id="InputTextarea">$fdtSynopsis</textarea>
								</div>
							</div>
END_OF_HTML;
	print <<<END_OF_HTML

							<div class="form-group row-0">
								<label for="fdtUpdateDate" class="control-label col-xs-3">更新日時</label>
								<div class="col-xs-8">
									<input type="text" 
										class="form-control input-sm"
										id="fdtUpdateDate" name="fdtUpdateDate"
										value="$fdtUpdateDate"
										placeholder="更新日時">
								</div>
							</div>
END_OF_HTML;
	print <<<END_OF_HTML

						</form>
					</div>
					<div class="col-sm-5" >
	                	<!-- リストの表示位置 -->
	                    <span id="side_list_area"></span>
					</div>
				</div>
			</div>
 
		</div><!--container-->
	</div><!--wrap-->


    <script type="text/javascript" src="./ajax/ajaxSwSynopsis.js"></script>
    <script type="text/javascript">
        fncMakeSwSynopsisList(frmSwSynopsis);
    </script>


	<!--footer表示位置--><span class="sw-footer"></span>
	<script language="JavaScript">
		AjaxFunc_FooterLoad();
	</script>
	
  </body>
</html>

END_OF_HTML;

}//end function

// ------------------------------------------------------------------------------
//      ﾌﾟﾛﾊﾟﾃｨReset
//          fncSwSynopsisResetProperty($clsSwSynopsis)
// ------------------------------------------------------------------------------
function fncSwSynopsisResetProperty($clsSwSynopsis){
	global	$fdtSynopsisId;
	global	$fdtScenarioId;
	global	$fdtSynopsis;
	global	$fdtUpdateDate;

	//ﾌﾟﾛﾊﾟﾃｨReset
	$fdtSynopsisId = "";                      //SYNOPSIS_ID
	$fdtScenarioId = "";                      //SCENARIO_ID
	$fdtSynopsis = "";                        //SYNOPSIS
	$fdtUpdateDate = "";                      //更新日時
}//end function

// ------------------------------------------------------------------------------
//      ﾌﾟﾛﾊﾟﾃｨGet
//          fncSwSynopsisGetProperty($clsSwSynopsis)
// ------------------------------------------------------------------------------
function fncSwSynopsisGetProperty($clsSwSynopsis){
	global	$fdtSynopsisId;
	global	$fdtScenarioId;
	global	$fdtSynopsis;
	global	$fdtUpdateDate;

	//ﾌﾟﾛﾊﾟﾃｨGet
	$fdtSynopsisId = $clsSwSynopsis->clsSwSynopsisGetSynopsisId();            //SYNOPSIS_ID
	$fdtScenarioId = $clsSwSynopsis->clsSwSynopsisGetScenarioId();            //SCENARIO_ID

	//SYNOPSISは<br>を\nに変換する
	$fdtSynopsis = $clsSwSynopsis->clsSwSynopsisGetSynopsis();                //SYNOPSIS
	$fdtSynopsis = str_replace("<br>","\n",$fdtSynopsis);

	$fdtUpdateDate = $clsSwSynopsis->clsSwSynopsisGetUpdateDate();            //更新日時
}//end function

// ------------------------------------------------------------------------------
//      ﾌﾟﾛﾊﾟﾃｨSet
//          fncSwSynopsisSetProperty($clsSwSynopsis)
// ------------------------------------------------------------------------------
function fncSwSynopsisSetProperty($clsSwSynopsis){
	global	$fdtSynopsisId;
	global	$fdtScenarioId;
	global	$fdtSynopsis;
	global	$fdtUpdateDate;

	//ﾌﾟﾛﾊﾟﾃｨSet

	//SYNOPSIS_IDはｶﾝﾏを取り除いてからSetする
	$fdtSynopsisId = str_replace(",","",$fdtSynopsisId);
	$clsSwSynopsis->clsSwSynopsisSetSynopsisId($fdtSynopsisId);            //SYNOPSIS_ID


	//SCENARIO_IDはｶﾝﾏを取り除いてからSetする
	$fdtScenarioId = str_replace(",","",$fdtScenarioId);
	$clsSwSynopsis->clsSwSynopsisSetScenarioId($fdtScenarioId);            //SCENARIO_ID


	//SYNOPSISは改行を<br>にしてからSetする
	$fdtSynopsis = str_replace("\r\n","<br>",$fdtSynopsis);
	$fdtSynopsis = str_replace("\r","<br>",$fdtSynopsis);
	$fdtSynopsis = str_replace("\n","<br>",$fdtSynopsis);
	$clsSwSynopsis->clsSwSynopsisSetSynopsis($fdtSynopsis);                //SYNOPSIS

	$clsSwSynopsis->clsSwSynopsisSetUpdateDate($fdtUpdateDate);            //更新日時
}//end function

// ------------------------------------------------------------------------------
//      DB INSERT
//          fncSwSynopsisDBInsert($mySqlConnObj,$clsSwSynopsis)
// ------------------------------------------------------------------------------
function fncSwSynopsisDBInsert($mySqlConnObj,$clsSwSynopsis){
	global	$fdtSynopsisId;

	//ﾌﾟﾛﾊﾟﾃｨｾｯﾄ
	fncSwSynopsisSetProperty($clsSwSynopsis);
	//ﾃﾞｰﾀ管理ｸﾗｽ DbInsert
	$fdtSynopsisId = $clsSwSynopsis->clsSwSynopsisDbInsert($mySqlConnObj);
}//end function

// ------------------------------------------------------------------------------
//      DB UPDATE
//          fncSwSynopsisDBUpdate($mySqlConnObj,$clsSwSynopsis)
// ------------------------------------------------------------------------------
function fncSwSynopsisDBUpdate($mySqlConnObj,$clsSwSynopsis){
	//ﾌﾟﾛﾊﾟﾃｨｾｯﾄ
	fncSwSynopsisSetProperty($clsSwSynopsis);
	//ﾃﾞｰﾀ管理ｸﾗｽ DbUpdate
	$clsSwSynopsis->clsSwSynopsisDbUpdate($mySqlConnObj);
}//end function

// ------------------------------------------------------------------------------
//      DB DELETE
//          fncSwSynopsisDBDelete($mySqlConnObj,$clsSwSynopsis)
// ------------------------------------------------------------------------------
function fncSwSynopsisDBDelete($mySqlConnObj,$clsSwSynopsis){
	//ﾌﾟﾛﾊﾟﾃｨｾｯﾄ
	fncSwSynopsisSetProperty($clsSwSynopsis);
	//ﾃﾞｰﾀ管理ｸﾗｽ DbDelete
	$clsSwSynopsis->clsSwSynopsisDbDelete($mySqlConnObj);
}//end function


// -----------------------------------------------------------
?>