<?php
// ------------------------------------------------------------------------------
//
//		small-accounting.net 共通 footer
//				acctFooter.php
//
//		Copyright(C) 2015 dbu@mac.com All Rights Reserved.
//
// ------------------------------------------------------------------------------

// ------------------------------------------------------------------------------
//
//	共通フッター
//
// ------------------------------------------------------------------------------
	//現在の日付を取得
	$today = getdate();
	$THIS_YEAR = sprintf( "%04d",$today['year']);

	if ( $THIS_YEAR == '2015'){
		$cp_year = '2015';
	}else{
		$cp_year = '2015-'.$THIS_YEAR;
	}

print <<<END_OF_HTML

	
	<div class="footer">
		<div class="container">
      		<div class="text-center">Copyright &copy; $cp_year small-accounting.net. All Rights Reserved.</div>
		</div>
    </div>

END_OF_HTML;

?>
