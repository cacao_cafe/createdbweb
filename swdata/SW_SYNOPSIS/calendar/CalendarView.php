<?php

//	--------------------------------------------------------------------------------

//	Copyright (C) 2009 Minamikyushu City Office All Rights Reserved.

//

//			カレンダー表示

//				CalendarView.php

//	--------------------------------------------------------------------------------

//		カレンダークラスを使ってカレンダーを表示する

//		パラメータ	

//			viewMonth	カレンダー表示モード（必須）

//						1 : 1ヶ月表示

//						2 : 3ヶ月分表示（前月 当月 翌月）

//						3 : 3ヶ月分表示（当月 翌月 翌々月）

//			frmName		リターン先フォーム名（必須）

//			yearName	リターン先フォームの年のOption名（必須）

//			monthName	リターン先フォームの月のOption名（必須）

//			dayName		リターン先フォームの日のOption名（必須）

//			year		デフォルト表示年

//			month		デフォルト表示月

//			day			デフォルト表示日

//

//		使用例(sampleForm.php）

//			暦ボタンを表示し，window.open() でカレンダー表示する

//			<input type="button" value="暦" onClick="window.open('./calendar/calendarView.php?viewMonth=2&frmName=empConfigForm2&yearName=begin_year&monthName=begin_month&dayName=begin_day&year=$begin_year&month=$begin_month&day=$begin_day', 'CAL', 'width=660,height=240')">

//	--------------------------------------------------------------------------------

// このPHP名

$ThisPHP = 'CalendarView.php';



// 日付情報クラス

require_once("./dateinfo.php");

$dateInfo = new DateInfo();



// カレンダークラス

require_once("./CalendarClass.php");

$calendar = new CalendarClass;



// パラメータを取得

$viewMonth = $_GET["viewMonth"];

$frmName = $_GET["frmName"];

$yearName = $_GET["yearName"];

$monthName = $_GET["monthName"];

$dayName = $_GET["dayName"];

$year = $_GET["year"];

$month = $_GET["month"];

$day = $_GET["day"];

$xday = 1;



// 日付情報のチェック

if ($year != ""){

	// 不正なら今日にする

	if (!checkdate($month, $day, $year)){

		//echo "date err!$year $month $day<br>";

		$year = date("Y");		//年 4桁の数字。

		$month = date("n");		//月 数字 先頭にゼロをつけない。

		$day = date("j");		//日 先頭にゼロをつけない。

	}

}else{

	// デフォルト年月日がないときは今日にする

	$year = date("Y");

	$month = date("n");

	$day = date("j");

}



//echo "$year<br>$month<br><br>";

//

//日付計算

$PrevDate = date("Y/m/d",strtotime("-1 month" ,strtotime("$year/$month/$xday")));

$NextDate = date("Y/m/d",strtotime("+1 month" ,strtotime("$year/$month/$xday")));

$Next2Date = date("Y/m/d",strtotime("+2 month" ,strtotime("$year/$month/$xday")));

$PrevYear= date("Y",strtotime("-1 month" ,strtotime("$year/$month/$xday")));

$PrevMonth= date("m",strtotime("-1 month" ,strtotime("$year/$month/$xday")));

$NextYear= date("Y",strtotime("+1 month" ,strtotime("$year/$month/$xday")));

$NextMonth= date("m",strtotime("+1 month" ,strtotime("$year/$month/$xday")));

$Next2Year= date("Y",strtotime("+2 month" ,strtotime("$year/$month/$xday")));

$Next2Month= date("m",strtotime("+2 month" ,strtotime("$year/$month/$xday")));



//echo "$PrevYear<br>$PrevMonth<br><br>";

//echo "$NextYear<br>$NextMonth<br><br>";



//

//画像HTML配列

$monthImgSrcArry = array(

					"<img src=\"" . "images/m01th.gif\" width=\"55\" height=\"38\" alt=\"1月\">",

					"<img src=\"" . "images/m02th.gif\" width=\"59\" height=\"38\" alt=\"2月\">",

					"<img src=\"" . "images/m03th.gif\" width=\"44\" height=\"38\" alt=\"3月\">",

					"<img src=\"" . "images/m04th.gif\" width=\"45\" height=\"38\" alt=\"4月\">",

					"<img src=\"" . "images/m05th.gif\" width=\"44\" height=\"38\" alt=\"5月\">",

					"<img src=\"" . "images/m06th.gif\" width=\"44\" height=\"38\" alt=\"6月\">",

					"<img src=\"" . "images/m07th.gif\" width=\"45\" height=\"38\" alt=\"7月\">",

					"<img src=\"" . "images/m08th.gif\" width=\"48\" height=\"38\" alt=\"8月\">",

					"<img src=\"" . "images/m09th.gif\" width=\"63\" height=\"38\" alt=\"9月\">",

					"<img src=\"" . "images/m10th.gif\" width=\"61\" height=\"38\" alt=\"10月\">",

					"<img src=\"" . "images/m11th.gif\" width=\"61\" height=\"38\" alt=\"11月\">",

					"<img src=\"" . "images/m12th.gif\" width=\"61\" height=\"38\" alt=\"12月\">"

					);

//画像HTMLを取得

$PrevMonthImg = $monthImgSrcArry[$PrevMonth - 1];

$ThisMonthImg = $monthImgSrcArry[$month - 1];

$NextMonthImg = $monthImgSrcArry[$NextMonth - 1];

$Next2MonthImg = $monthImgSrcArry[$Next2Month - 1];



// 4ヶ月分のカレンダーHTMLを取得

$PrevMonthCalendar = $calendar->getCalendar($PrevYear, $PrevMonth);

$ThisMonthCalendar = $calendar->getCalendar($year, $month);

$NextMonthCalendar = $calendar->getCalendar($NextYear, $NextMonth);

$Next2MonthCalendar = $calendar->getCalendar($Next2Year, $Next2Month);



//

//	ｶﾚﾝﾀﾞｰを表示する

//

print <<<END_OF_HTML

<html>

	<head>

		<META HTTP-EQUIV="Pragma" CONTENT="no-cache">

		<META HTTP-EQUIV="Content-Script-Type" CONTENT="text/javascript">

		<meta http-equiv="Content-type" content="text/html; charset=utf-8">

		<title>日付入力カレンダー</title>

		

		<style type="text/css">

		<!--

		a{text-decoration:none;}

		a.usually:link { color: #0000ff}

		a.usually:visited { color: #0000ff}

		a.usually:active { color: #ff0000}

		a.usually:hover { color: #ff0000}

		-->

		</style>

		

		<script language="JavaScript">

		<!--

		function WrtDay(Day, Month, Year){

		  if(typeof(window.opener.document.$frmName) != "undefined")

		  {

		    if(typeof(window.opener.document.$frmName.$yearName) != "undefined")

		    {

		      window.opener.document.$frmName.$yearName.selectedIndex = SetSelectItem(window.opener.document.$frmName.$yearName, Year);

		    }

		    if(typeof(window.opener.document.$frmName.$monthName) != "undefined")

		    {

		      window.opener.document.$frmName.$monthName.selectedIndex = SetSelectItem(window.opener.document.$frmName.$monthName, Month);

		    }

		    if(typeof(window.opener.document.$frmName.$dayName) != "undefined")

		    {

		      window.opener.document.$frmName.$dayName.selectedIndex = SetSelectItem(window.opener.document.$frmName.$dayName, Day);

		    }

		  }

		  window.close();

		}

		

		function SetSelectItem(objSelect, item)

		{

		  for(i=0; i<objSelect.length; i++)

		  {

		      if(objSelect.value = item)

		      {

			  return objSelect.selectedIndex;

		      }

		  }

		  return -1;

		}

		-->

		</script>

	</head>

	<body>

END_OF_HTML;

//

//	カレンダー表示 

//

	if ( $viewMonth == 3 ){

		//	3ヶ月分表示（当月 次月 次々月）

		print <<<END_OF_HTML

		<table>

			<tr>

				<td align="center">

						<a href="$ThisPHP?viewMonth=$viewMonth&frmName=$frmName&yearName=$yearName&monthName=$monthName&dayName=$dayName&year=$PrevYear&month=$PrevMonth&day=$xday"><img src="./images/triangle_l.gif" width="20" height="20" border="0" alt="前の月へ"></a>

						&nbsp;

						$ThisMonthImg

				</td>

				<td align="center">

						$NextMonthImg

				</td>

				<td align="center">

						$Next2MonthImg

						&nbsp;

						<a href="$ThisPHP?viewMonth=$viewMonth&frmName=$frmName&yearName=$yearName&monthName=$monthName&dayName=$dayName&year=$NextYear&month=$NextMonth&day=$xday"><img src="./images/triangle_r.gif" width="20" height="20" border="0" alt="次の月へ"></a>

				</td>

			</tr>

			<tr>

				<td valign="top">

					$ThisMonthCalendar

				</td>

				<td valign="top">

					$NextMonthCalendar

				</td>

				<td valign="top">

					$Next2MonthCalendar

				</td>

			</tr>

END_OF_HTML;

	}elseif ( $viewMonth == 2 ){

		//	3ヶ月分表示（前月 当月 次月）

		print <<<END_OF_HTML

		<table>

			<tr>

				<td align="center">

						<a href="$ThisPHP?viewMonth=$viewMonth&frmName=$frmName&yearName=$yearName&monthName=$monthName&dayName=$dayName&year=$PrevYear&month=$PrevMonth&day=$xday"><img src="./images/triangle_l.gif" width="20" height="20" border="0" alt="前の月へ"></a>

						&nbsp;

						$PrevMonthImg

				</td>

				<td align="center">

						$ThisMonthImg

				</td>

				<td align="center">

						$NextMonthImg

						&nbsp;

						<a href="$ThisPHP?viewMonth=$viewMonth&frmName=$frmName&yearName=$yearName&monthName=$monthName&dayName=$dayName&year=$NextYear&month=$NextMonth&day=$xday"><img src="./images/triangle_r.gif" width="20" height="20" border="0" alt="次の月へ"></a>

				</td>

			</tr>

			<tr>

				<td valign="top">

					$PrevMonthCalendar

				</td>

				<td valign="top">

					$ThisMonthCalendar

				</td>

				<td valign="top">

					$NextMonthCalendar

				</td>

			</tr>

END_OF_HTML;

	}else{

		//	1ヶ月分表示

		print <<<END_OF_HTML

		<table>

			<tr>

				<td align="center">

						<a href="$ThisPHP?viewMonth=$viewMonth&frmName=$frmName&yearName=$yearName&monthName=$monthName&dayName=$dayName&year=$PrevYear&month=$PrevMonth&day=$day"><img src="./images/triangle_l.gif" width="20" height="20" border="0" alt="前の月へ"></a>

						&nbsp;

						$ThisMonthImg

						&nbsp;

						<a href="$ThisPHP?viewMonth=$viewMonth&frmName=$frmName&yearName=$yearName&monthName=$monthName&dayName=$dayName&year=$NextYear&month=$NextMonth&day=$day"><img src="./images/triangle_r.gif" width="20" height="20" border="0" alt="次の月へ"></a>

				</td>

			</tr>

			<tr>

				<td valign="top">

					$ThisMonthCalendar

				</td>

			</tr>

END_OF_HTML;

	}



//

//	閉じるボタンとフッタ

if ( $viewMonth == 3 ){

	//	3ヶ月分表示（当月 次月 次々月）

	print <<<END_OF_HTML

		<tr>

			<td colspan="3" align="center">

				<input type="button" value="CLOSE" onClick="window.close()">

			</td>

		</tr>

		</table>

	</body>

</html>

END_OF_HTML;

}elseif ( $viewMonth == 2 ){

	//	3ヶ月分表示（前月 当月 次月）

	print <<<END_OF_HTML

		<tr>

			<td colspan="3" align="center">

				<input type="button" value="CLOSE" onClick="window.close()">

			</td>

		</tr>

		</table>

	</body>

</html>

END_OF_HTML;

}else{

	//	1ヶ月分表示

	print <<<END_OF_HTML

		<tr>

			<td align="center">

				<input type="button" value="CLOSE" onClick="window.close()">

			</td>

		</tr>

		</table>

	</body>

</html>

END_OF_HTML;

}

?>