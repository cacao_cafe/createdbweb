<?php
// -----------------------------------------------------------
//
// Copyright (C) 2015 ScenarioWriterCafe All Rights Reserved.
// 
//     CREATE TABLE SW_SYNOPSIS
//
//     createTable_SwSynopsis.php
// -----------------------------------------------------------
$Result = fncCreateTable();
echo $Result;
exit;


// -----------------------------------------------------------
//     fncCreateTable()
// -----------------------------------------------------------
function fncCreateTable(){
	//共通定数
	include_once("../../sw_config/swConstant.php");

	//MySQLに接続
	include_once("../include/ConnectMySQL.php");

	//DROP TABLE SQLを編集
	$strSQL = <<<END_OF_SQL

DROP TABLE IF EXISTS SW_SYNOPSIS;

END_OF_SQL;

	//DROP TABLE EXECUTE 
	$myResult = $mySqlConnObj->query($strSQL) or die($this->fncAlert($mySqlConnObj->error));

	//CREATE TABLE SQLを編集
	$strSQL = <<<END_OF_SQL

CREATE TABLE SW_SYNOPSIS(
	  SYNOPSIS_ID                      INT NOT NULL AUTO_INCREMENT COMMENT 'SYNOPSIS_ID'
	, SCENARIO_ID                      INT  COMMENT 'SCENARIO_ID'
	, SYNOPSIS                         MEDIUMTEXT  COMMENT 'SYNOPSIS'
	, UPDATE_DATE                      TIMESTAMP  COMMENT '更新日時'
	, PRIMARY KEY (SYNOPSIS_ID)
	)DEFAULT CHARACTER SET 'UTF8'
	COMMENT 'シノプシス';

END_OF_SQL;

	//CREATE TABLE EXECUTE 
	$myResult = $mySqlConnObj->query($strSQL) or die($this->fncAlert($mySqlConnObj->error));

	//UNIQUE INDEX のSQLを編集
	$strSQL = <<<END_OF_SQL

	CREATE UNIQUE INDEX SW_SYNOPSIS_KEY_SYNOPSIS_ID ON SW_SYNOPSIS(SYNOPSIS_ID);
END_OF_SQL;

	//CREATE INDEX EXECUTE 
	$myResult = $mySqlConnObj->query($strSQL) or die($this->fncAlert($mySqlConnObj->error));


	//DB切断
	include_once("../include/DisConnectMySQL.php");
	
	
	return	'CREATE TABLE SW_SYNOPSIS Success...';
}//end function
// -----------------------------------------------------------
?>