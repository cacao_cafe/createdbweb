<?php
// -----------------------------------------------------------
//
// Copyright (C) 2015 ScenarioWriterCafe All Rights Reserved.
// 
//     シノプシス ﾃﾞｰﾀ管理ｸﾗｽ
//     clsSwSynopsis
// -----------------------------------------------------------
class clsSwSynopsis{
	var $SynopsisId;                      //SYNOPSIS_ID
	var $ScenarioId;                      //SCENARIO_ID
	var $Synopsis;                        //SYNOPSIS
	var $UpdateDate;                      //更新日時
// ｺﾝｽﾄﾗｸﾀ
    function clsSwSynopsis(){
    }
// ﾌﾟﾛﾊﾟﾃｨ
// --------------------------------------
// ﾌﾟﾛﾊﾟﾃｨ Get SynopsisId
//    SYNOPSIS_ID を返す
// --------------------------------------
	function clsSwSynopsisGetSynopsisId(){
		return $this->SynopsisId;
	}
// --------------------------------------
// ﾌﾟﾛﾊﾟﾃｨ Set SynopsisId
//    SYNOPSIS_ID を設定する
// --------------------------------------
	function clsSwSynopsisSetSynopsisId($valSynopsisId){
		$this->SynopsisId = $valSynopsisId;
	}
// --------------------------------------
// ﾌﾟﾛﾊﾟﾃｨ Get ScenarioId
//    SCENARIO_ID を返す
// --------------------------------------
	function clsSwSynopsisGetScenarioId(){
		return $this->ScenarioId;
	}
// --------------------------------------
// ﾌﾟﾛﾊﾟﾃｨ Set ScenarioId
//    SCENARIO_ID を設定する
// --------------------------------------
	function clsSwSynopsisSetScenarioId($valScenarioId){
		$this->ScenarioId = $valScenarioId;
	}
// --------------------------------------
// ﾌﾟﾛﾊﾟﾃｨ Get Synopsis
//    SYNOPSIS を返す
// --------------------------------------
	function clsSwSynopsisGetSynopsis(){
		return $this->Synopsis;
	}
// --------------------------------------
// ﾌﾟﾛﾊﾟﾃｨ Set Synopsis
//    SYNOPSIS を設定する
// --------------------------------------
	function clsSwSynopsisSetSynopsis($valSynopsis){
		$this->Synopsis = $valSynopsis;
	}
// --------------------------------------
// ﾌﾟﾛﾊﾟﾃｨ Get UpdateDate
//    更新日時 を返す
// --------------------------------------
	function clsSwSynopsisGetUpdateDate(){
		return $this->UpdateDate;
	}
// --------------------------------------
// ﾌﾟﾛﾊﾟﾃｨ Set UpdateDate
//    更新日時 を設定する
// --------------------------------------
	function clsSwSynopsisSetUpdateDate($valUpdateDate){
		$this->UpdateDate = $valUpdateDate;
	}
// -------------------------------------------------------------
//    ｸﾗｽ初期化
// -------------------------------------------------------------
	function clsSwSynopsisInit($mySqlConnObj,$valSynopsisId){
		//ﾌﾟﾛﾊﾟﾃｨの初期化
		$this->SynopsisId = '';                      //SYNOPSIS_ID
		$this->ScenarioId = '';                      //SCENARIO_ID
		$this->Synopsis = '';                        //SYNOPSIS
		$this->UpdateDate = '';                      //更新日時
		//DB から ﾃﾞｰﾀを取得しﾌﾟﾛﾊﾟﾃｨにｾｯﾄする
		$strSQL = <<<END_OF_SQL

		SELECT * FROM SW_SYNOPSIS
				WHERE SYNOPSIS_ID = '$valSynopsisId';
END_OF_SQL;

		$myResult = $mySqlConnObj->query($strSQL) or die($this->fncAlert($mySqlConnObj->error));
		while($myRow = $myResult->fetch_assoc( )){
			$this->SynopsisId = $myRow['SYNOPSIS_ID'];
			$this->ScenarioId = $myRow['SCENARIO_ID'];
			$this->Synopsis = $myRow['SYNOPSIS'];
			$this->UpdateDate = $myRow['UPDATE_DATE'];
		}//end while
    }//end function

// -------------------------------------------------------------
//    DB更新　INSERT
// -------------------------------------------------------------
	function clsSwSynopsisDbInsert($mySqlConnObj){
		//ﾌﾟﾛﾊﾟﾃｨ取得
		$valSynopsisId = $this->SynopsisId;
		$valScenarioId = $this->ScenarioId;
		$valSynopsis = $this->Synopsis;
		$valUpdateDate = $this->UpdateDate;
			//INSERT SQL
			$strSQL = <<<END_OF_SQL

			INSERT INTO SW_SYNOPSIS(
				  SCENARIO_ID
				, SYNOPSIS
				, UPDATE_DATE
			) values (
				  '$valScenarioId'
				, '$valSynopsis'
				, current_timestamp
			);
END_OF_SQL;

			//INSRET 
			$myResult = $mySqlConnObj->query($strSQL) or die($this->fncAlert($mySqlConnObj->error));

			//AUTO_INCREMENT値取得
			$LastInsertId = $mySqlConnObj->insert_id;
			//AUTO_INCREMENT値を返す
			return $LastInsertId;
	}//end function

// -------------------------------------------------------------
//    DB更新　UPDATE
// -------------------------------------------------------------
	function clsSwSynopsisDbUpdate($mySqlConnObj){
		//ﾌﾟﾛﾊﾟﾃｨ取得
		$valSynopsisId = $this->SynopsisId;
		$valScenarioId = $this->ScenarioId;
		$valSynopsis = $this->Synopsis;
		$valUpdateDate = $this->UpdateDate;
		//登録確認
		$strSQL = <<<END_OF_SQL

		SELECT * FROM SW_SYNOPSIS
				WHERE SYNOPSIS_ID = '$valSynopsisId';
END_OF_SQL;

		//登録確認SQL Execute 
		$myResult = $mySqlConnObj->query($strSQL) or die($this->fncAlert($mySqlConnObj->error));

		//件数取得
		$myRowCnt = $myResult->num_rows;
		if($myRowCnt == 0 ){
			echo $this->fncAlert("clsSwSynopsis ->> NOT ENTRY!");
		}else{
			//UPDATE SQL
			$strSQL = <<<END_OF_SQL

			UPDATE SW_SYNOPSIS SET 
				  SCENARIO_ID = '$valScenarioId'
				, SYNOPSIS = '$valSynopsis'
				, UPDATE_DATE = current_timestamp
				WHERE SYNOPSIS_ID = '$valSynopsisId';
END_OF_SQL;

			//UPDATESQL Execute 
			$myResult = $mySqlConnObj->query($strSQL) or die($this->fncAlert($mySqlConnObj->error));

		}//end if
	}//end function

// -------------------------------------------------------------
//    DB更新　DELETE
// -------------------------------------------------------------
	function clsSwSynopsisDbDelete($mySqlConnObj){
		//ﾌﾟﾛﾊﾟﾃｨ取得
		$valSynopsisId = $this->SynopsisId;
		//登録確認
		$strSQL = <<<END_OF_SQL

		SELECT * FROM SW_SYNOPSIS
				WHERE SYNOPSIS_ID = '$valSynopsisId';
END_OF_SQL;

		//登録確認SQL Execute 
		$myResult = $mySqlConnObj->query($strSQL) or die($this->fncAlert($mySqlConnObj->error));

		//件数取得
		$myRowCnt = $myResult->num_rows;
		if($myRowCnt == 0 ){
			echo $this->fncAlert("clsSwSynopsis ->> NOT ENTRY!");
		}else{
			//DELETE SQL
			$strSQL = <<<END_OF_SQL

			DELETE FROM SW_SYNOPSIS
				WHERE SYNOPSIS_ID = '$valSynopsisId';
END_OF_SQL;

			//DELETE SQL Execute 
			$myResult = $mySqlConnObj->query($strSQL) or die($this->fncAlert($mySqlConnObj->error));

		}//end if
	}//end function

	// ------------------------------------------------------------------------------
	// java script alert
	// ------------------------------------------------------------------------------
	function fncAlert($AlertMsg){
		$strAlert = <<<END_OF_TEXT

			<script language="JavaScript">
				bootbox.alert("$AlertMsg",function() {
			});
			</script>
END_OF_TEXT;

		return $strAlert;
	}
	
} // end class
// -----------------------------------------------------------
?>