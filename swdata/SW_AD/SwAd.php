<?php
// -----------------------------------------------------------
//
// Copyright (C) 2016 ScenarioWriterCafe All Rights Reserved.
// 
//     SW_AD I/O ｼｽﾃﾑ
//
//     SwAd.php
// -----------------------------------------------------------
// ------------------------------------------------------------------------------
	//定数読み込み
	include_once("../sw_config/swConstant.php");
	//DB接続ｸﾗｽの初期化
	include_once("./include/ConnectMySQL.php");
// ------------------------------------------------------------------------------
	//ﾃﾞﾌｫﾙﾄｱｸｼｮﾝ
	$ThisPHP = 'SwAd.php';
	//ﾃﾞｰﾀ管理ｸﾗｽ
	include_once("./class/clsSwAd.php");
	$clsSwAd = new clsSwAd();
	//共通関数をｲﾝｸﾙｰﾄﾞ
	include_once("./include/swFunc.php");
	// ﾍｯﾀﾞ表示
	$HtmlTitle = "広告データ";
	include_once("./include/swHeader.php");

	//ﾌｫｰﾑのﾃﾞｰﾀを読み込む
	fncGetPostItems();

	//MainProcedure
	fncMainProc($mySqlConnObj);

	exit();

// ------------------------------------------------------------------------------
//      POSTされた要素を取得
//          fncGetPostItems()
// ------------------------------------------------------------------------------
function fncGetPostItems(){
	//共通global変数
	global	$SubmitMode;
	//ﾌｫｰﾑ name要素をglobal変数にする
	global	$fdtAdId;
	global	$fdtAdName;
	global	$fdtAdHtml;

	//処理ﾓｰﾄが空白ならreturnするﾞ
	if(!isset($_POST['SubmitMode'])){return;}
	//処理ﾓｰﾄﾞ
	$SubmitMode = swFunc_GetPostData('SubmitMode');
	if($SubmitMode == ''){return;}
	//POSTされた要素を取得
	$fdtAdId = swFunc_GetPostData('fdtAdId');                        //広告データID
	$fdtAdName = swFunc_GetPostData('fdtAdName');                    //広告名
	$fdtAdHtml = swFunc_GetPostData('fdtAdHtml');                    //広告HTML
}//end function

// ------------------------------------------------------------------------------
//      MAIN PROCEDURE
//          fncMainProc($mySqlConnObj)
// ------------------------------------------------------------------------------
function fncMainProc($mySqlConnObj){
	global	$SubmitMode;
	//class
	global	$clsSwAd;

	global	$fdtAdId;
	global	$fdtAdName;
	global	$fdtAdHtml;

	//SubmitModeで処理を分岐
	switch($SubmitMode ){
		case 'SELECT':
			//ﾃﾞｰﾀ管理ｸﾗｽの初期化
			$clsSwAd->clsSwAdInit($mySqlConnObj,$fdtAdId);
			//ﾌﾟﾛﾊﾟﾃｨGet
			fncSwAdGetProperty($clsSwAd);
			break;
		case 'INSERT':
			//登録処理
			fncSwAdDBInsert($mySqlConnObj,$clsSwAd);
			//選択状態にする
			$SubmitMode = 'SELECT';
			//ﾃﾞｰﾀ管理ｸﾗｽの初期化
			$clsSwAd->clsSwAdInit($mySqlConnObj,$fdtAdId);
			//ﾌﾟﾛﾊﾟﾃｨGet
			fncSwAdGetProperty($clsSwAd);
			break;
		case 'UPDATE':
			//更新処理
			fncSwAdDBUpdate($mySqlConnObj,$clsSwAd);
			//選択状態にする
			$SubmitMode = 'SELECT';
			//ﾃﾞｰﾀ管理ｸﾗｽの初期化
			$clsSwAd->clsSwAdInit($mySqlConnObj,$fdtAdId);
			//ﾌﾟﾛﾊﾟﾃｨGet
			fncSwAdGetProperty($clsSwAd);
			break;
		case 'DELETE':
			//削除処理
			fncSwAdDBDelete($mySqlConnObj,$clsSwAd);
			//ﾌﾟﾛﾊﾟﾃｨReset
			fncSwAdResetProperty($clsSwAd);
			//初期状態にする
			$SubmitMode = 'RESET';
			break;
		case 'RESET':
			//ﾌﾟﾛﾊﾟﾃｨReset
			fncSwAdResetProperty($clsSwAd);
			//初期状態にする
			$SubmitMode = 'RESET';
			break;
		default:
			break;
	}
	//ﾌｫｰﾑの表示
	fncMainForm($mySqlConnObj);
}//end function

// ------------------------------------------------------------------------------
//      MAIN FORM
//          fncMainForm($mySqlConnObj)
// ------------------------------------------------------------------------------
function fncMainForm($mySqlConnObj){
	global	$ThisPHP,$SubmitMode;
	global	$fdtAdId;
	global	$fdtAdName;
	global	$fdtAdHtml;

	//css powerd by Bootstrap ver3
	print <<<END_OF_HTML

	<div class="wrap" >
	
		<div class="container-fluid">
		
		<!-- ﾒﾆｭｰ 制御ﾎﾞﾀﾝ 表示ROW -->
			<div class="row">
				<div class="col-sm-12">
				<legend class="form-inline submenu">
				<ul class="list-inline">
				  <li>
END_OF_HTML;

	//管理メニュー ﾄﾞﾛｯﾌﾟﾀﾞｳﾝﾒﾆｭｰ
	$TargetForm = 'frmSwAd';
	include_once("./include/swMgrDropDownMenu.php");

	
    //SubmitModeで表示するボタンを制御する
	switch($SubmitMode){
		case 'SELECT':
			print <<<END_OF_HTML
			 	</li>
			 	<li>
			        <input type="button" value="RESET"
			            id="btnReset"
			            onclick="fncSwAdSubmit(frmSwAd,'RESET','FALSE','');"
			            class="btn btn-default btn-sm">
			        <span style="width: 120px;"></span>
			        <input type="button" value="INSERT"
			            id="btnInsert"
			            onclick="fncSwAdSubmit(frmSwAd,'INSERT','TRUE','新規登録');"
			            class="btn btn-warning btn-sm">
			        <input type="button" value="UPDATE"
			            id="btnUpdate"
			            onclick="fncSwAdSubmit(frmSwAd,'UPDATE','TRUE','更新');"
			            class="btn btn-warning btn-sm">
			        <input type="button" value="DELETE"
			            id="btnDelete"
			            onclick="fncSwAdSubmit(frmSwAd,'DELETE','TRUE','削除');"
			            class="btn btn-danger btn-sm">
			
				</li>
END_OF_HTML;

	
			break;
        default:
			print <<<END_OF_HTML
			    </li>
			 	<li>
			        <input type="button" value="RESET"
			            id="btnReset"
			            onclick="fncSwAdSubmit(frmSwAd,'RESET','FALSE','');"
			            class="btn btn-default btn-sm">
			        <input type="button" value="INSERT"
			            id="btnInsert"
			            onclick="fncSwAdSubmit(frmSwAd,'INSERT','TRUE','新規登録');"
			            class="btn btn-warning btn-sm">
			
				</li>
END_OF_HTML;
            break;
    }
	print <<<END_OF_HTML

					</ul>
					</legend>
    			</div><!-- col-sm-12 end -->
    		</div><!-- row end -->
    		<!-- ﾒﾆｭｰ 制御ﾎﾞﾀﾝ 表示ROW ここまで-->

    		<!-- ﾌｫｰﾑ と ﾘｽﾄ -->
    		<div class="row row-0">
				<div class="col-sm-7 row-0">
					<form class="form-horizontal" role="form" name="frmSwAd" id="frmSwAd" method="POST" action="$ThisPHP">
						<input type="hidden" name="SubmitMode" id="SubmitMode" value="$SubmitMode">

END_OF_HTML;

	print <<<END_OF_HTML

							<div class="form-group row-0">
								<label for="fdtAdId" class="control-label col-xs-3">広告データID</label>
								<div class="col-xs-8">
									<input type="number"
										class="form-control input-sm text-right"
										id="fdtAdId" name="fdtAdId"
										value="$fdtAdId"
										onkeyup="AjaxFunc_AddFigure(frmSwAd,'fdtAdId');"
										placeholder="広告データID">
								</div>
							</div>
END_OF_HTML;
	print <<<END_OF_HTML

							<div class="form-group row-0">
								<label for="fdtAdName" class="control-label col-xs-3">広告名</label>
								<div class="col-xs-8">
									<input type="text" 
										class="form-control input-sm"
										id="fdtAdName" name="fdtAdName"
										value="$fdtAdName"
										placeholder="広告名">
								</div>
							</div>
END_OF_HTML;
	print <<<END_OF_HTML

							<div class="form-group row-0">
								<label for="fdtAdHtml" class="control-label col-xs-3">広告HTML</label>
								<div class="col-xs-8">
									<textarea placeholder="広告HTML" 
										class="form-control input-sm"
										id="fdtAdHtml" name="fdtAdHtml"
										rows="3"
										id="InputTextarea">$fdtAdHtml</textarea>
								</div>
							</div>
END_OF_HTML;
	print <<<END_OF_HTML

						</form>
					</div>
					<div class="col-sm-5" >
	                	<!-- リストの表示位置 -->
	                    <span id="side_list_area"></span>
					</div>
				</div>
			</div>
 
		</div><!--container-->
	</div><!--wrap-->


    <script type="text/javascript" src="./ajax/ajaxSwAd.js"></script>
    <script type="text/javascript">
        fncMakeSwAdList(frmSwAd);
    </script>


	<!--footer表示位置--><span class="sw-footer"></span>
	<script language="JavaScript">
		AjaxFunc_FooterLoad();
	</script>
	
  </body>
</html>

END_OF_HTML;

}//end function

// ------------------------------------------------------------------------------
//      ﾌﾟﾛﾊﾟﾃｨReset
//          fncSwAdResetProperty($clsSwAd)
// ------------------------------------------------------------------------------
function fncSwAdResetProperty($clsSwAd){
	global	$fdtAdId;
	global	$fdtAdName;
	global	$fdtAdHtml;

	//ﾌﾟﾛﾊﾟﾃｨReset
	$fdtAdId = "";                            //広告データID
	$fdtAdName = "";                          //広告名
	$fdtAdHtml = "";                          //広告HTML
}//end function

// ------------------------------------------------------------------------------
//      ﾌﾟﾛﾊﾟﾃｨGet
//          fncSwAdGetProperty($clsSwAd)
// ------------------------------------------------------------------------------
function fncSwAdGetProperty($clsSwAd){
	global	$fdtAdId;
	global	$fdtAdName;
	global	$fdtAdHtml;

	//ﾌﾟﾛﾊﾟﾃｨGet
	$fdtAdId = $clsSwAd->clsSwAdGetAdId();                        //広告データID
	$fdtAdName = $clsSwAd->clsSwAdGetAdName();                    //広告名

	//広告HTMLは<br>を\nに変換する
	$fdtAdHtml = $clsSwAd->clsSwAdGetAdHtml();                    //広告HTML
	$fdtAdHtml = str_replace("<br>","\n",$fdtAdHtml);

}//end function

// ------------------------------------------------------------------------------
//      ﾌﾟﾛﾊﾟﾃｨSet
//          fncSwAdSetProperty($clsSwAd)
// ------------------------------------------------------------------------------
function fncSwAdSetProperty($clsSwAd){
	global	$fdtAdId;
	global	$fdtAdName;
	global	$fdtAdHtml;

	//ﾌﾟﾛﾊﾟﾃｨSet

	//広告データIDはｶﾝﾏを取り除いてからSetする
	$fdtAdId = str_replace(",","",$fdtAdId);
	$clsSwAd->clsSwAdSetAdId($fdtAdId);                        //広告データID

	$clsSwAd->clsSwAdSetAdName($fdtAdName);                    //広告名

	//広告HTMLは改行を<br>にしてからSetする
	$fdtAdHtml = str_replace("\r\n","<br>",$fdtAdHtml);
	$fdtAdHtml = str_replace("\r","<br>",$fdtAdHtml);
	$fdtAdHtml = str_replace("\n","<br>",$fdtAdHtml);
	$clsSwAd->clsSwAdSetAdHtml($fdtAdHtml);                    //広告HTML

}//end function

// ------------------------------------------------------------------------------
//      DB INSERT
//          fncSwAdDBInsert($mySqlConnObj,$clsSwAd)
// ------------------------------------------------------------------------------
function fncSwAdDBInsert($mySqlConnObj,$clsSwAd){
	global	$fdtAdId;

	//ﾌﾟﾛﾊﾟﾃｨｾｯﾄ
	fncSwAdSetProperty($clsSwAd);
	//ﾃﾞｰﾀ管理ｸﾗｽ DbInsert
	$fdtAdId = $clsSwAd->clsSwAdDbInsert($mySqlConnObj);
}//end function

// ------------------------------------------------------------------------------
//      DB UPDATE
//          fncSwAdDBUpdate($mySqlConnObj,$clsSwAd)
// ------------------------------------------------------------------------------
function fncSwAdDBUpdate($mySqlConnObj,$clsSwAd){
	//ﾌﾟﾛﾊﾟﾃｨｾｯﾄ
	fncSwAdSetProperty($clsSwAd);
	//ﾃﾞｰﾀ管理ｸﾗｽ DbUpdate
	$clsSwAd->clsSwAdDbUpdate($mySqlConnObj);
}//end function

// ------------------------------------------------------------------------------
//      DB DELETE
//          fncSwAdDBDelete($mySqlConnObj,$clsSwAd)
// ------------------------------------------------------------------------------
function fncSwAdDBDelete($mySqlConnObj,$clsSwAd){
	//ﾌﾟﾛﾊﾟﾃｨｾｯﾄ
	fncSwAdSetProperty($clsSwAd);
	//ﾃﾞｰﾀ管理ｸﾗｽ DbDelete
	$clsSwAd->clsSwAdDbDelete($mySqlConnObj);
}//end function


// -----------------------------------------------------------
?>