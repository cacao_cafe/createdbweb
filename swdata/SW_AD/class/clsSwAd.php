<?php
// -----------------------------------------------------------
//
// Copyright (C) 2016 ScenarioWriterCafe All Rights Reserved.
// 
//     広告データ ﾃﾞｰﾀ管理ｸﾗｽ
//     clsSwAd
// -----------------------------------------------------------
class clsSwAd{
	var $AdId;                            //広告データID
	var $AdName;                          //広告名
	var $AdHtml;                          //広告HTML
// ｺﾝｽﾄﾗｸﾀ
    function clsfncSwAd(){
    }
// ﾌﾟﾛﾊﾟﾃｨ
// --------------------------------------
// ﾌﾟﾛﾊﾟﾃｨGet AdId
//    広告データID を返す
// --------------------------------------
	function clsSwAdGetAdId()
		return $this->AdId;
	}
// --------------------------------------
// ﾌﾟﾛﾊﾟﾃｨSet AdId
//    広告データID を設定する
// --------------------------------------
	function clsSwAdSetAdId($valAdId){
		$this->AdId = valAdId;
	}
// --------------------------------------
// ﾌﾟﾛﾊﾟﾃｨGet AdName
//    広告名 を返す
// --------------------------------------
	function clsSwAdGetAdName()
		return $this->AdName;
	}
// --------------------------------------
// ﾌﾟﾛﾊﾟﾃｨSet AdName
//    広告名 を設定する
// --------------------------------------
	function clsSwAdSetAdName($valAdName){
		$this->AdName = valAdName;
	}
// --------------------------------------
// ﾌﾟﾛﾊﾟﾃｨGet AdHtml
//    広告HTML を返す
// --------------------------------------
	function clsSwAdGetAdHtml()
		return $this->AdHtml;
	}
// --------------------------------------
// ﾌﾟﾛﾊﾟﾃｨSet AdHtml
//    広告HTML を設定する
// --------------------------------------
	function clsSwAdSetAdHtml($valAdHtml){
		$this->AdHtml = valAdHtml;
	}

// -------------------------------------------------------------
//    ｸﾗｽ初期化
// -------------------------------------------------------------
	function clsSwAdInit($mySqlConnObj,$valAdId){
		//ﾌﾟﾛﾊﾟﾃｨの初期化
		$this->AdId = '';                            //広告データID
		$this->AdName = '';                          //広告名
		$this->AdHtml = '';                          //広告HTML
		//DB から ﾃﾞｰﾀを取得しﾌﾟﾛﾊﾟﾃｨにｾｯﾄする
		$strSQL = <<<END_OF_SQL

		SELECT * FROM SW_AD
		WHERE AD_ID = :AdId;
END_OF_SQL;

		$stmt = $mySqlConnObj->prepare($strSQL);
		$stmt->setFetchMode(PDO::FETCH_ASSOC);
		//パラメータのセット
		$stmt->bindParam(':AdId', $valAdId, PDO::PARAM_INT);	
		$stmt->execute();
		while($myRow = $stmt -> fetch(PDO::FETCH_ASSOC)) {
			$this->AdId = $myRow['AD_ID];
			$this->AdName = $myRow['AD_NAME];
			$this->AdHtml = $myRow['AD_HTML];
		}//end while
    }//end function

// -------------------------------------------------------------
//    DB更新　INSERT
// -------------------------------------------------------------
	function clsSwAdDbInsert($mySqlConnObj){
		//ﾌﾟﾛﾊﾟﾃｨ取得
		$valAdId = $this->AdId;
		$valAdName = $this->AdName;
		$valAdHtml = $this->AdHtml;
		//INSERT SQL
		$strSQL = <<<END_OF_SQL

		INSERT INTO SW_AD(
			  AD_NAME
			, AD_HTML
			) values (
			  :AdName
			, :AdHtml
			);
END_OF_SQL;


		$stmt = $mySqlConnObj->prepare($strSQL);
		$stmt->bindParam(':AdName', $valAdName, PDO::PARAM_INT);
		$stmt->bindParam(':AdHtml', $valAdHtml, PDO::PARAM_INT);	
		$stmt->execute();
		//AUTO_INCREMENT値取得
		$LastInsertId = $mySqlConnObj->lastInsertId();
		//AUTO_INCREMENT値を返す
		return $LastInsertId;
	}//end function

// -------------------------------------------------------------
//    DB更新　UPDATE
// -------------------------------------------------------------
	function clsSwAdDbUpdate($mySqlConnObj){
		//プロパティ取得
		$valAdId = $this->AdId;
		$valAdName = $this->AdName;
		$valAdHtml = $this->AdHtml;
		//登録確認
		$strSQL = <<<END_OF_SQL

		SELECT * FROM SW_AD
		WHERE AD_ID = :AdId;
END_OF_SQL;


		$stmt = $mySqlConnObj->prepare($strSQL);
		//パラメータのセット
		$stmt->bindParam(':AdId', $valAdId, PDO::PARAM_INT);	
		$stmt->execute();
		//件数取得
		$myRowCnt = $stmt->rowCount();
		if($myRowCnt == 0 ){
            echo $this->fncAlert("clsSwAd ->> NOT ENTRY!");
        }else{
			//UPDATE SQL
			$strSQL = <<<END_OF_SQL

			UPDATE SW_AD SET 
				  AD_NAME = '$valAdName'
				, AD_HTML = '$valAdHtml'
		WHERE AD_ID = :AdId;
END_OF_SQL;


			$stmt = $mySqlConnObj->prepare($strSQL);	
		//パラメータのセット
		$stmt->bindParam(':AdId', $valAdId, PDO::PARAM_INT);	
			$stmt->execute();
		}//end if
	}//end function

// -------------------------------------------------------------
//    DB更新　DELETE
// -------------------------------------------------------------
	function $classNameDbDelete($mySqlConnObj){
		//ﾌﾟﾛﾊﾟﾃｨ取得
		$valAdId = $this->AdId;
		//登録確認
		$strSQL = <<<END_OF_SQL

		SELECT * FROM SW_AD
			WHERE AD_ID = :AdId;
END_OF_SQL;

		$stmt = $mySqlConnObj->prepare($strSQL);
		//パラメータのセット
		$stmt->bindParam(':AdId', $valAdId, PDO::PARAM_INT);	
		$stmt->execute();
		//件数取得
		$myRowCnt = $myResult->num_rows;
		if($myRowCnt == 0 ){
            echo $this->fncAlert("clsSwAd ->> NOT ENTRY!");
        }else{
			//DELETE SQL
			$strSQL = <<<END_OF_SQL

			DELETE FROM SW_AD
			WHERE AD_ID = :AdId;
END_OF_SQL;


			$stmt = $mySqlConnObj->prepare($strSQL);	
		//パラメータのセット
		$stmt->bindParam(':AdId', $valAdId, PDO::PARAM_INT);	
			$stmt->execute();
		}//end if
	}//end function
	// ------------------------------------------------------------------------------
	// java script alert
	// ------------------------------------------------------------------------------
	function fncAlert($AlertMsg){
		$strAlert = <<<END_OF_TEXT

			<script language="JavaScript">
				bootbox.alert("$AlertMsg",function() {
			});
			</script>
END_OF_TEXT;

		return $strAlert;
	}
	
} // end class
// -----------------------------------------------------------
?>