<?php
//
//	日付関連フォームサンプル
//
//

//日付関連部品クラス
require_once("./CalendarOptionClass.php");
$CalendarOptionClass = new CalendarOptionClass;

print <<<END_OF_HTML
<html>
	<head>
		<META HTTP-EQUIV="Pragma" CONTENT="no-cache">
		<META HTTP-EQUIV="Content-Script-Type" CONTENT="text/javascript">
		<meta http-equiv="Content-type" content="text/html; charset=utf-8">
		<title>CalendarSampleForm</title>
	</head>
	<body>
        <a href="#" onClick="window.open('calendarView.php?viewMonth=1&frmName=sampleForm&yearName=sample_year&monthName=sample_month&dayName=sample_day&year=2008&month=02&day=22', 'CAL', 'width=240,height=240')">
			<img src="./images/calender_ico.gif" alt="カレンダーから指定" width="19" height="19" border="0" align="middle">
		</a>
        <a href="#" onClick="window.open('calendarView.php?viewMonth=2&frmName=sampleForm&yearName=sample_year&monthName=sample_month&dayName=sample_day&year=2008&month=02&day=22', 'CAL', 'width=660,height=240')">
			<img src="./images/calender_ico.gif" alt="カレンダーから指定" width="19" height="19" border="0" align="middle">
		</a>
		<a href="#" onClick="window.open('calendarView.php?viewMonth=3&frmName=sampleForm&yearName=sample_year&monthName=sample_month&dayName=sample_day&year=2008&month=02&day=22', 'CAL', 'width=660,height=240')">
			<img src="./images/calender_ico.gif" alt="カレンダーから指定" width="19" height="19" border="0" align="middle">
		</a>

END_OF_HTML;
	//日付関連部品クラス
	require_once("./CalendarOptionClass.php");
	$CalendarOptionClass = new CalendarOptionClass;
	//--------------------------------------------------------------------------------
	//	年選択のOptionHTMLを生成する
	//	fncMakeYearOption
	//		$FormSelectName		object名
	//		$DefaultYear		ﾃﾞﾌｫﾙﾄ年 selected
	//		$NumberOfPastYear	過去の個数
	//		$NumberOfSelect		selectの個数
	//--------------------------------------------------------------------------------
	$YearOptionHTML = $CalendarOptionClass->fncMakeYearOption("sample_year",2008,0,5);
	//--------------------------------------------------------------------------------
	//	月選択のOptionHTMLを生成する
	//	fncMakeMonthOption
	//		$FormSelectName		object名
	//		$DefaultMonth		ﾃﾞﾌｫﾙﾄ月 selected
	//--------------------------------------------------------------------------------
	$MonthOptionHTML = $CalendarOptionClass->fncMakeMonthOption("sample_month",2);
	//--------------------------------------------------------------------------------
	//	日選択のOptionHTMLを生成する
	//	fncMakeDayOption
	//		$FormSelectName		object名
	//		$DefaultDay			ﾃﾞﾌｫﾙﾄ日 selected
	//--------------------------------------------------------------------------------
	$DayOptionHTML = $CalendarOptionClass->fncMakeDayOption("sample_day",22);
	
print <<<END_OF_HTML
			$YearOptionHTML
			<span class="fBlack12100">年</span>
			$MonthOptionHTML
			<span class="fBlack12100">月</span>
			$DayOptionHTML
			<span class="fBlack12100">日</span>

	<HR>
END_OF_HTML;

	//日付関連部品クラス
	require_once("./CalendarOptionClass.php");
	$CalendarOptionClass = new CalendarOptionClass;
	//--------------------------------------------------------------------------------
	//	年選択のOptionHTMLを生成する
	//	fncMakeYearOption
	//		$FormSelectName		object名
	//		$DefaultYear		ﾃﾞﾌｫﾙﾄ年 selected
	//		$NumberOfPastYear	過去の個数
	//		$NumberOfSelect		selectの個数
	//--------------------------------------------------------------------------------
	$FormInLimitYmd = getdate();
	$year = $date["year"];
	$month = $date["mon"];
	$day = $date["mday"];

	$YearOptionHTML = $CalendarOptionClass->fncMakeYearOption("FormInLimitYY",$year,1,5);
	//--------------------------------------------------------------------------------
	//	月選択のOptionHTMLを生成する
	//	fncMakeMonthOption
	//		$FormSelectName		object名
	//		$DefaultMonth		ﾃﾞﾌｫﾙﾄ月 selected
	//--------------------------------------------------------------------------------
	$MonthOptionHTML = $CalendarOptionClass->fncMakeMonthOption("FormInLimitMM",$month);
	//--------------------------------------------------------------------------------
	//	日選択のOptionHTMLを生成する
	//	fncMakeDayOption
	//		$FormSelectName		object名
	//		$DefaultDay			ﾃﾞﾌｫﾙﾄ日 selected
	//--------------------------------------------------------------------------------
	$DayOptionHTML = $CalendarOptionClass->fncMakeDayOption("FormInLimitDD",$day);
	
print <<<END_OF_HTML
			$YearOptionHTML
			<span class="fBlack10100">年</span>
			$MonthOptionHTML
			<span class="fBlack10100">月</span>
			$DayOptionHTML
			<span class="fBlack10100">日</span>

	        <a href="#" onClick="window.open('./calendarView.php?viewMonth=3&frmName=frmNews&yearName=FormInLimitYY&monthName=FormInLimitMM&dayName=FormInLimitDD&year=$cDate[0]&month=$cDate[1]&day=$cDate[2]', 'CAL', 'width=660,height=240')">
				<img src="./images/calender_ico.gif" alt="カレンダーから指定" width="19" height="19" border="0" align="middle">
			</a>
	</body>
</html>
END_OF_HTML;

?>
