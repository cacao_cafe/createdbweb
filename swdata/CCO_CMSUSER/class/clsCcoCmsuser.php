<?php
// -----------------------------------------------------------
//
// Copyright (C) 2016 ScenarioWriterCafe All Rights Reserved.
// 
//     CMS_USER ﾃﾞｰﾀ管理ｸﾗｽ
//     clsCcoCmsuser
// -----------------------------------------------------------
class clsCcoCmsuser{
	var $CmsuserId;                       //ユーザーID
	var $CmsuserLoginId;                  //ログインID
	var $CmsuserName;                     //ユーザー名
	var $CmsuserPassword;                 //パスワード
	var $CmsuserRequestMonth;             //請求月
	var $CmsuserPostalCode;               //郵便番号
	var $CmsuserAddress;                  //住所
	var $CmsuserCompany;                  //会社名
	var $CmsuserRequestName;              //請求者名
	var $CmsuserPhoneNo;                  //電話番号
	var $CmsuserMobilePhoneNo;            //電話番号（携帯）
	var $CmsuserMemo;                     //メモ
// ｺﾝｽﾄﾗｸﾀ
    function clsfncCcoCmsuser(){
    }
// ﾌﾟﾛﾊﾟﾃｨ
// --------------------------------------
// ﾌﾟﾛﾊﾟﾃｨGet CmsuserId
//    ユーザーID を返す
// --------------------------------------
	function clsCcoCmsuserGetCmsuserId()
		return $this->CmsuserId;
	}
// --------------------------------------
// ﾌﾟﾛﾊﾟﾃｨSet CmsuserId
//    ユーザーID を設定する
// --------------------------------------
	function clsCcoCmsuserSetCmsuserId($valCmsuserId){
		$this->CmsuserId = valCmsuserId;
	}
// --------------------------------------
// ﾌﾟﾛﾊﾟﾃｨGet CmsuserLoginId
//    ログインID を返す
// --------------------------------------
	function clsCcoCmsuserGetCmsuserLoginId()
		return $this->CmsuserLoginId;
	}
// --------------------------------------
// ﾌﾟﾛﾊﾟﾃｨSet CmsuserLoginId
//    ログインID を設定する
// --------------------------------------
	function clsCcoCmsuserSetCmsuserLoginId($valCmsuserLoginId){
		$this->CmsuserLoginId = valCmsuserLoginId;
	}
// --------------------------------------
// ﾌﾟﾛﾊﾟﾃｨGet CmsuserName
//    ユーザー名 を返す
// --------------------------------------
	function clsCcoCmsuserGetCmsuserName()
		return $this->CmsuserName;
	}
// --------------------------------------
// ﾌﾟﾛﾊﾟﾃｨSet CmsuserName
//    ユーザー名 を設定する
// --------------------------------------
	function clsCcoCmsuserSetCmsuserName($valCmsuserName){
		$this->CmsuserName = valCmsuserName;
	}
// --------------------------------------
// ﾌﾟﾛﾊﾟﾃｨGet CmsuserPassword
//    パスワード を返す
// --------------------------------------
	function clsCcoCmsuserGetCmsuserPassword()
		return $this->CmsuserPassword;
	}
// --------------------------------------
// ﾌﾟﾛﾊﾟﾃｨSet CmsuserPassword
//    パスワード を設定する
// --------------------------------------
	function clsCcoCmsuserSetCmsuserPassword($valCmsuserPassword){
		$this->CmsuserPassword = valCmsuserPassword;
	}
// --------------------------------------
// ﾌﾟﾛﾊﾟﾃｨGet CmsuserRequestMonth
//    請求月 を返す
// --------------------------------------
	function clsCcoCmsuserGetCmsuserRequestMonth()
		return $this->CmsuserRequestMonth;
	}
// --------------------------------------
// ﾌﾟﾛﾊﾟﾃｨSet CmsuserRequestMonth
//    請求月 を設定する
// --------------------------------------
	function clsCcoCmsuserSetCmsuserRequestMonth($valCmsuserRequestMonth){
		$this->CmsuserRequestMonth = valCmsuserRequestMonth;
	}
// --------------------------------------
// ﾌﾟﾛﾊﾟﾃｨGet CmsuserPostalCode
//    郵便番号 を返す
// --------------------------------------
	function clsCcoCmsuserGetCmsuserPostalCode()
		return $this->CmsuserPostalCode;
	}
// --------------------------------------
// ﾌﾟﾛﾊﾟﾃｨSet CmsuserPostalCode
//    郵便番号 を設定する
// --------------------------------------
	function clsCcoCmsuserSetCmsuserPostalCode($valCmsuserPostalCode){
		$this->CmsuserPostalCode = valCmsuserPostalCode;
	}
// --------------------------------------
// ﾌﾟﾛﾊﾟﾃｨGet CmsuserAddress
//    住所 を返す
// --------------------------------------
	function clsCcoCmsuserGetCmsuserAddress()
		return $this->CmsuserAddress;
	}
// --------------------------------------
// ﾌﾟﾛﾊﾟﾃｨSet CmsuserAddress
//    住所 を設定する
// --------------------------------------
	function clsCcoCmsuserSetCmsuserAddress($valCmsuserAddress){
		$this->CmsuserAddress = valCmsuserAddress;
	}
// --------------------------------------
// ﾌﾟﾛﾊﾟﾃｨGet CmsuserCompany
//    会社名 を返す
// --------------------------------------
	function clsCcoCmsuserGetCmsuserCompany()
		return $this->CmsuserCompany;
	}
// --------------------------------------
// ﾌﾟﾛﾊﾟﾃｨSet CmsuserCompany
//    会社名 を設定する
// --------------------------------------
	function clsCcoCmsuserSetCmsuserCompany($valCmsuserCompany){
		$this->CmsuserCompany = valCmsuserCompany;
	}
// --------------------------------------
// ﾌﾟﾛﾊﾟﾃｨGet CmsuserRequestName
//    請求者名 を返す
// --------------------------------------
	function clsCcoCmsuserGetCmsuserRequestName()
		return $this->CmsuserRequestName;
	}
// --------------------------------------
// ﾌﾟﾛﾊﾟﾃｨSet CmsuserRequestName
//    請求者名 を設定する
// --------------------------------------
	function clsCcoCmsuserSetCmsuserRequestName($valCmsuserRequestName){
		$this->CmsuserRequestName = valCmsuserRequestName;
	}
// --------------------------------------
// ﾌﾟﾛﾊﾟﾃｨGet CmsuserPhoneNo
//    電話番号 を返す
// --------------------------------------
	function clsCcoCmsuserGetCmsuserPhoneNo()
		return $this->CmsuserPhoneNo;
	}
// --------------------------------------
// ﾌﾟﾛﾊﾟﾃｨSet CmsuserPhoneNo
//    電話番号 を設定する
// --------------------------------------
	function clsCcoCmsuserSetCmsuserPhoneNo($valCmsuserPhoneNo){
		$this->CmsuserPhoneNo = valCmsuserPhoneNo;
	}
// --------------------------------------
// ﾌﾟﾛﾊﾟﾃｨGet CmsuserMobilePhoneNo
//    電話番号（携帯） を返す
// --------------------------------------
	function clsCcoCmsuserGetCmsuserMobilePhoneNo()
		return $this->CmsuserMobilePhoneNo;
	}
// --------------------------------------
// ﾌﾟﾛﾊﾟﾃｨSet CmsuserMobilePhoneNo
//    電話番号（携帯） を設定する
// --------------------------------------
	function clsCcoCmsuserSetCmsuserMobilePhoneNo($valCmsuserMobilePhoneNo){
		$this->CmsuserMobilePhoneNo = valCmsuserMobilePhoneNo;
	}
// --------------------------------------
// ﾌﾟﾛﾊﾟﾃｨGet CmsuserMemo
//    メモ を返す
// --------------------------------------
	function clsCcoCmsuserGetCmsuserMemo()
		return $this->CmsuserMemo;
	}
// --------------------------------------
// ﾌﾟﾛﾊﾟﾃｨSet CmsuserMemo
//    メモ を設定する
// --------------------------------------
	function clsCcoCmsuserSetCmsuserMemo($valCmsuserMemo){
		$this->CmsuserMemo = valCmsuserMemo;
	}

// -------------------------------------------------------------
//    ｸﾗｽ初期化
// -------------------------------------------------------------
	function clsCcoCmsuserInit($mySqlConnObj,$valCmsuserId){
		//ﾌﾟﾛﾊﾟﾃｨの初期化
		$this->CmsuserId = '';                       //ユーザーID
		$this->CmsuserLoginId = '';                  //ログインID
		$this->CmsuserName = '';                     //ユーザー名
		$this->CmsuserPassword = '';                 //パスワード
		$this->CmsuserRequestMonth = '';             //請求月
		$this->CmsuserPostalCode = '';               //郵便番号
		$this->CmsuserAddress = '';                  //住所
		$this->CmsuserCompany = '';                  //会社名
		$this->CmsuserRequestName = '';              //請求者名
		$this->CmsuserPhoneNo = '';                  //電話番号
		$this->CmsuserMobilePhoneNo = '';            //電話番号（携帯）
		$this->CmsuserMemo = '';                     //メモ
		//DB から ﾃﾞｰﾀを取得しﾌﾟﾛﾊﾟﾃｨにｾｯﾄする
		$strSQL = <<<END_OF_SQL

		SELECT * FROM CCO_CMSUSER
			WHERE CMSUSER_ID = :CmsuserId;
END_OF_SQL;

		$stmt = $mySqlConnObj->prepare($strSQL);
		$stmt->setFetchMode(PDO::FETCH_ASSOC);
		//パラメータのセット
		$stmt->bindParam(':CmsuserId', $valCmsuserId, PDO::PARAM_INT);	
		$stmt->execute();
		while($myRow = $stmt -> fetch(PDO::FETCH_ASSOC)) {
			$this->CmsuserId = $myRow['CMSUSER_ID'];
			$this->CmsuserLoginId = $myRow['CMSUSER_LOGIN_ID'];
			$this->CmsuserName = $myRow['CMSUSER_NAME'];
			$this->CmsuserPassword = $myRow['CMSUSER_PASSWORD'];
			$this->CmsuserRequestMonth = $myRow['CMSUSER_REQUEST_MONTH'];
			$this->CmsuserPostalCode = $myRow['CMSUSER_POSTAL_CODE'];
			$this->CmsuserAddress = $myRow['CMSUSER_ADDRESS'];
			$this->CmsuserCompany = $myRow['CMSUSER_COMPANY'];
			$this->CmsuserRequestName = $myRow['CMSUSER_REQUEST_NAME'];
			$this->CmsuserPhoneNo = $myRow['CMSUSER_PHONE_NO'];
			$this->CmsuserMobilePhoneNo = $myRow['CMSUSER_MOBILE_PHONE_NO'];
			$this->CmsuserMemo = $myRow['CMSUSER_MEMO'];
		}//end while
    }//end function

// -------------------------------------------------------------
//    DB更新　INSERT
// -------------------------------------------------------------
	function clsCcoCmsuserDbInsert($mySqlConnObj){
		//ﾌﾟﾛﾊﾟﾃｨ取得
		$valCmsuserId = $this->CmsuserId;
		$valCmsuserLoginId = $this->CmsuserLoginId;
		$valCmsuserName = $this->CmsuserName;
		$valCmsuserPassword = $this->CmsuserPassword;
		$valCmsuserRequestMonth = $this->CmsuserRequestMonth;
		$valCmsuserPostalCode = $this->CmsuserPostalCode;
		$valCmsuserAddress = $this->CmsuserAddress;
		$valCmsuserCompany = $this->CmsuserCompany;
		$valCmsuserRequestName = $this->CmsuserRequestName;
		$valCmsuserPhoneNo = $this->CmsuserPhoneNo;
		$valCmsuserMobilePhoneNo = $this->CmsuserMobilePhoneNo;
		$valCmsuserMemo = $this->CmsuserMemo;
		//INSERT SQL
		$strSQL = <<<END_OF_SQL

		INSERT INTO CCO_CMSUSER(
			  CMSUSER_LOGIN_ID
			, CMSUSER_NAME
			, CMSUSER_PASSWORD
			, CMSUSER_REQUEST_MONTH
			, CMSUSER_POSTAL_CODE
			, CMSUSER_ADDRESS
			, CMSUSER_COMPANY
			, CMSUSER_REQUEST_NAME
			, CMSUSER_PHONE_NO
			, CMSUSER_MOBILE_PHONE_NO
			, CMSUSER_MEMO
			) values (
			  :CmsuserLoginId
			, :CmsuserName
			, :CmsuserPassword
			, :CmsuserRequestMonth
			, :CmsuserPostalCode
			, :CmsuserAddress
			, :CmsuserCompany
			, :CmsuserRequestName
			, :CmsuserPhoneNo
			, :CmsuserMobilePhoneNo
			, :CmsuserMemo
			);
END_OF_SQL;


		$stmt = $mySqlConnObj->prepare($strSQL);
		$stmt->bindParam(':CmsuserLoginId', $valCmsuserLoginId, PDO::PARAM_INT);
		$stmt->bindParam(':CmsuserName', $valCmsuserName, PDO::PARAM_INT);
		$stmt->bindParam(':CmsuserPassword', $valCmsuserPassword, PDO::PARAM_INT);
		$stmt->bindParam(':CmsuserRequestMonth', $valCmsuserRequestMonth, PDO::PARAM_INT);
		$stmt->bindParam(':CmsuserPostalCode', $valCmsuserPostalCode, PDO::PARAM_INT);
		$stmt->bindParam(':CmsuserAddress', $valCmsuserAddress, PDO::PARAM_INT);
		$stmt->bindParam(':CmsuserCompany', $valCmsuserCompany, PDO::PARAM_INT);
		$stmt->bindParam(':CmsuserRequestName', $valCmsuserRequestName, PDO::PARAM_INT);
		$stmt->bindParam(':CmsuserPhoneNo', $valCmsuserPhoneNo, PDO::PARAM_INT);
		$stmt->bindParam(':CmsuserMobilePhoneNo', $valCmsuserMobilePhoneNo, PDO::PARAM_INT);
		$stmt->bindParam(':CmsuserMemo', $valCmsuserMemo, PDO::PARAM_INT);	
		$stmt->execute();
		//AUTO_INCREMENT値取得
		$LastInsertId = $mySqlConnObj->lastInsertId();
		//AUTO_INCREMENT値を返す
		return $LastInsertId;
	}//end function

// -------------------------------------------------------------
//    DB更新　UPDATE
// -------------------------------------------------------------
	function clsCcoCmsuserDbUpdate($mySqlConnObj){
		//プロパティ取得
		$valCmsuserId = $this->CmsuserId;
		$valCmsuserLoginId = $this->CmsuserLoginId;
		$valCmsuserName = $this->CmsuserName;
		$valCmsuserPassword = $this->CmsuserPassword;
		$valCmsuserRequestMonth = $this->CmsuserRequestMonth;
		$valCmsuserPostalCode = $this->CmsuserPostalCode;
		$valCmsuserAddress = $this->CmsuserAddress;
		$valCmsuserCompany = $this->CmsuserCompany;
		$valCmsuserRequestName = $this->CmsuserRequestName;
		$valCmsuserPhoneNo = $this->CmsuserPhoneNo;
		$valCmsuserMobilePhoneNo = $this->CmsuserMobilePhoneNo;
		$valCmsuserMemo = $this->CmsuserMemo;
		//登録確認
		$strSQL = <<<END_OF_SQL

		SELECT * FROM CCO_CMSUSER
		WHERE CMSUSER_ID = :CmsuserId;
END_OF_SQL;


		$stmt = $mySqlConnObj->prepare($strSQL);
		//パラメータのセット
		$stmt->bindParam(':CmsuserId', $valCmsuserId, PDO::PARAM_INT);	
		$stmt->execute();
		//件数取得
		$myRowCnt = $stmt->rowCount();
		if($myRowCnt == 0 ){
            echo $this->fncAlert("clsCcoCmsuser ->> NOT ENTRY!");
        }else{
			//UPDATE SQL
			$strSQL = <<<END_OF_SQL

			UPDATE CCO_CMSUSER SET 
				  CMSUSER_LOGIN_ID = '$valCmsuserLoginId'
				, CMSUSER_NAME = '$valCmsuserName'
				, CMSUSER_PASSWORD = '$valCmsuserPassword'
				, CMSUSER_REQUEST_MONTH = '$valCmsuserRequestMonth'
				, CMSUSER_POSTAL_CODE = '$valCmsuserPostalCode'
				, CMSUSER_ADDRESS = '$valCmsuserAddress'
				, CMSUSER_COMPANY = '$valCmsuserCompany'
				, CMSUSER_REQUEST_NAME = '$valCmsuserRequestName'
				, CMSUSER_PHONE_NO = '$valCmsuserPhoneNo'
				, CMSUSER_MOBILE_PHONE_NO = '$valCmsuserMobilePhoneNo'
				, CMSUSER_MEMO = '$valCmsuserMemo'
		WHERE CMSUSER_ID = :CmsuserId;
END_OF_SQL;


			$stmt = $mySqlConnObj->prepare($strSQL);	
		//パラメータのセット
		$stmt->bindParam(':CmsuserId', $valCmsuserId, PDO::PARAM_INT);	
			$stmt->execute();
		}//end if
	}//end function

// -------------------------------------------------------------
//    DB更新　DELETE
// -------------------------------------------------------------
	function $classNameDbDelete($mySqlConnObj){
		//ﾌﾟﾛﾊﾟﾃｨ取得
		$valCmsuserId = $this->CmsuserId;
		//登録確認
		$strSQL = <<<END_OF_SQL

		SELECT * FROM CCO_CMSUSER
			WHERE CMSUSER_ID = :CmsuserId;
END_OF_SQL;

		$stmt = $mySqlConnObj->prepare($strSQL);
		//パラメータのセット
		$stmt->bindParam(':CmsuserId', $valCmsuserId, PDO::PARAM_INT);	
		$stmt->execute();
		//件数取得
		$myRowCnt = $myResult->num_rows;
		if($myRowCnt == 0 ){
            echo $this->fncAlert("clsCcoCmsuser ->> NOT ENTRY!");
        }else{
			//DELETE SQL
			$strSQL = <<<END_OF_SQL

			DELETE FROM CCO_CMSUSER
			WHERE CMSUSER_ID = :CmsuserId;
END_OF_SQL;


			$stmt = $mySqlConnObj->prepare($strSQL);	
		//パラメータのセット
		$stmt->bindParam(':CmsuserId', $valCmsuserId, PDO::PARAM_INT);	
			$stmt->execute();
		}//end if
	}//end function
	// ------------------------------------------------------------------------------
	// java script alert
	// ------------------------------------------------------------------------------
	function fncAlert($AlertMsg){
		$strAlert = <<<END_OF_TEXT

			<script language="JavaScript">
				bootbox.alert("$AlertMsg",function() {
			});
			</script>
END_OF_TEXT;

		return $strAlert;
	}
	
} // end class
// -----------------------------------------------------------
?>