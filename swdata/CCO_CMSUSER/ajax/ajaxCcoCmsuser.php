<?php
// -----------------------------------------------------------
//
// Copyright (C) 2016 ScenarioWriterCafe All Rights Reserved.
// 
//     CCO_CMSUSER I/O ｼｽﾃﾑ
//
//     ajaxCcoCmsuser.php
// -----------------------------------------------------------
// ------------------------------------------------------------------------------
	//定数読み込み
	include_once("../../sw_config/swConstant.php");
	//DB接続ｸﾗｽの初期化
	include_once("../include/ConnectMySQL.php");
// ------------------------------------------------------------------------------
	//共通関数をｲﾝｸﾙｰﾄﾞ
	include_once("../include/swFunc.php");
	//ﾌｫｰﾑのﾃﾞｰﾀを読み込む
	fncGetPostItems();

	//MainProcedure
	fncMainProc($mySqlConnObj);

	exit();

// ------------------------------------------------------------------------------
//      POSTされた要素を取得
//          fncGetPostItems()
// ------------------------------------------------------------------------------
function fncGetPostItems(){
	//共通global変数
	global	$SubMode;
	//ﾌｫｰﾑ name要素をglobal変数にする
	global	$fdtCmsuserId;
	global	$fdtCmsuserLoginId;
	global	$fdtCmsuserName;
	global	$fdtCmsuserPassword;
	global	$fdtCmsuserRequestMonth;
	global	$fdtCmsuserPostalCode;
	global	$fdtCmsuserAddress;
	global	$fdtCmsuserCompany;
	global	$fdtCmsuserRequestName;
	global	$fdtCmsuserPhoneNo;
	global	$fdtCmsuserMobilePhoneNo;
	global	$fdtCmsuserMemo;

	//処理ﾓｰﾄが空白ならreturnするﾞ
	if(!isset($_POST['SubMode'])){return;}
	//処理ﾓｰﾄﾞ
	$SubMode = swFunc_GetPostData('SubMode');
	if($SubMode == ''){return;}
	//POSTされた要素を取得
	$fdtCmsuserId = swFunc_GetPostData('fdtCmsuserId');              //ユーザーID
	$fdtCmsuserLoginId = swFunc_GetPostData('fdtCmsuserLoginId');    //ログインID
	$fdtCmsuserName = swFunc_GetPostData('fdtCmsuserName');          //ユーザー名
	$fdtCmsuserPassword = swFunc_GetPostData('fdtCmsuserPassword');  //パスワード
	$fdtCmsuserRequestMonth = swFunc_GetPostData('fdtCmsuserRequestMonth');//請求月
	$fdtCmsuserPostalCode = swFunc_GetPostData('fdtCmsuserPostalCode');//郵便番号
	$fdtCmsuserAddress = swFunc_GetPostData('fdtCmsuserAddress');    //住所
	$fdtCmsuserCompany = swFunc_GetPostData('fdtCmsuserCompany');    //会社名
	$fdtCmsuserRequestName = swFunc_GetPostData('fdtCmsuserRequestName');//請求者名
	$fdtCmsuserPhoneNo = swFunc_GetPostData('fdtCmsuserPhoneNo');    //電話番号
	$fdtCmsuserMobilePhoneNo = swFunc_GetPostData('fdtCmsuserMobilePhoneNo');//電話番号（携帯）
	$fdtCmsuserMemo = swFunc_GetPostData('fdtCmsuserMemo');          //メモ
}//end function

// ------------------------------------------------------------------------------
//      MAIN PROCEDURE
//          fncMainProc($mySqlConnObj)
// ------------------------------------------------------------------------------
function fncMainProc($mySqlConnObj){
	global	$SubMode;
	//global 変数
	global	$fdtCmsuserId;
	global	$fdtCmsuserLoginId;
	global	$fdtCmsuserName;
	global	$fdtCmsuserPassword;
	global	$fdtCmsuserRequestMonth;
	global	$fdtCmsuserPostalCode;
	global	$fdtCmsuserAddress;
	global	$fdtCmsuserCompany;
	global	$fdtCmsuserRequestName;
	global	$fdtCmsuserPhoneNo;
	global	$fdtCmsuserMobilePhoneNo;
	global	$fdtCmsuserMemo;

	//SubModeで処理を制御
	if($SubMode == 'CcoCmsuserList'){
		$resultHtml = fncMakeCcoCmsuserList($mySqlConnObj);
	}

	// 出力charsetをUTF-8に指定
	mb_http_output ( 'UTF-8' );
	// 出力
	echo($resultHtml);
}//end function

//--------------------------------------------------------------------------------
// CCO_CMSUSER ALL LIST
//--------------------------------------------------------------------------------
function fncMakeCcoCmsuserList($mySqlConnObj){
	//ﾘｽﾄのﾍｯﾀﾞｰ
	$retHtml = <<<END_OF_HTML
	
		<div class="scroll_div">
		<table class="table" _fixedhead="rows:1;div-full-mode: no;">
			<tr>
				<th>ユーザーID</th>
				<th>ユーザー名</th>
				<th>会社名</th>
			</tr>
END_OF_HTML;

	//DB から ﾃﾞｰﾀを取得しﾌﾟﾛﾊﾟﾃｨにｾｯﾄする
	$strSQL = <<<END_OF_SQL
		SELECT *,CMSUSER_ID as KEY_ITEM
			FROM CCO_CMSUSER
					ORDER BY CMSUSER_ID;
END_OF_SQL;
	//SQLを実行
	$myResult = $mySqlConnObj->query($strSQL);
	while($myRow = $myResult->fetch_assoc()){
		$valCmsuserId = $myRow['CMSUSER_ID'];
		$valCmsuserName = $myRow['CMSUSER_NAME'];
		$valCmsuserCompany = $myRow['CMSUSER_COMPANY'];
		$valKeyItem = $myRow['KEY_ITEM'];
		//ﾘｽﾄ
		$retHtml .= <<<END_OF_HTML
		
			<tr onclick="fncSelectCcoCmsuser('$valKeyItem');">
				<td>$valCmsuserId</td>
				<td>$valCmsuserName</td>
				<td>$valCmsuserCompany</td>
			</tr>
END_OF_HTML;
		}//end while

		$retHtml .= <<<END_OF_HTML
		
		</table>
		</div>
END_OF_HTML;
	//結果セットを開放
	$myResult->free();
	//HTMLを返す
	return $retHtml;
}
