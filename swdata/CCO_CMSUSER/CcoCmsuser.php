<?php
// -----------------------------------------------------------
//
// Copyright (C) 2016 ScenarioWriterCafe All Rights Reserved.
// 
//     CCO_CMSUSER I/O ｼｽﾃﾑ
//
//     CcoCmsuser.php
// -----------------------------------------------------------
// ------------------------------------------------------------------------------
	//定数読み込み
	include_once("../sw_config/swConstant.php");
	//DB接続ｸﾗｽの初期化
	include_once("./include/ConnectMySQL.php");
// ------------------------------------------------------------------------------
	//ﾃﾞﾌｫﾙﾄｱｸｼｮﾝ
	$ThisPHP = 'CcoCmsuser.php';
	//ﾃﾞｰﾀ管理ｸﾗｽ
	include_once("./class/clsCcoCmsuser.php");
	$clsCcoCmsuser = new clsCcoCmsuser();
	//共通関数をｲﾝｸﾙｰﾄﾞ
	include_once("./include/swFunc.php");
	// ﾍｯﾀﾞ表示
	$HtmlTitle = "CMS_USER";
	include_once("./include/swHeader.php");

	//ﾌｫｰﾑのﾃﾞｰﾀを読み込む
	fncGetPostItems();

	//MainProcedure
	fncMainProc($mySqlConnObj);

	exit();

// ------------------------------------------------------------------------------
//      POSTされた要素を取得
//          fncGetPostItems()
// ------------------------------------------------------------------------------
function fncGetPostItems(){
	//共通global変数
	global	$SubmitMode;
	//ﾌｫｰﾑ name要素をglobal変数にする
	global	$fdtCmsuserId;
	global	$fdtCmsuserLoginId;
	global	$fdtCmsuserName;
	global	$fdtCmsuserPassword;
	global	$fdtCmsuserRequestMonth;
	global	$fdtCmsuserPostalCode;
	global	$fdtCmsuserAddress;
	global	$fdtCmsuserCompany;
	global	$fdtCmsuserRequestName;
	global	$fdtCmsuserPhoneNo;
	global	$fdtCmsuserMobilePhoneNo;
	global	$fdtCmsuserMemo;

	//処理ﾓｰﾄが空白ならreturnするﾞ
	if(!isset($_POST['SubmitMode'])){return;}
	//処理ﾓｰﾄﾞ
	$SubmitMode = swFunc_GetPostData('SubmitMode');
	if($SubmitMode == ''){return;}
	//POSTされた要素を取得
	$fdtCmsuserId = swFunc_GetPostData('fdtCmsuserId');              //ユーザーID
	$fdtCmsuserLoginId = swFunc_GetPostData('fdtCmsuserLoginId');    //ログインID
	$fdtCmsuserName = swFunc_GetPostData('fdtCmsuserName');          //ユーザー名
	$fdtCmsuserPassword = swFunc_GetPostData('fdtCmsuserPassword');  //パスワード
	$fdtCmsuserRequestMonth = swFunc_GetPostData('fdtCmsuserRequestMonth');//請求月
	$fdtCmsuserPostalCode = swFunc_GetPostData('fdtCmsuserPostalCode');//郵便番号
	$fdtCmsuserAddress = swFunc_GetPostData('fdtCmsuserAddress');    //住所
	$fdtCmsuserCompany = swFunc_GetPostData('fdtCmsuserCompany');    //会社名
	$fdtCmsuserRequestName = swFunc_GetPostData('fdtCmsuserRequestName');//請求者名
	$fdtCmsuserPhoneNo = swFunc_GetPostData('fdtCmsuserPhoneNo');    //電話番号
	$fdtCmsuserMobilePhoneNo = swFunc_GetPostData('fdtCmsuserMobilePhoneNo');//電話番号（携帯）
	$fdtCmsuserMemo = swFunc_GetPostData('fdtCmsuserMemo');          //メモ
}//end function

// ------------------------------------------------------------------------------
//      MAIN PROCEDURE
//          fncMainProc($mySqlConnObj)
// ------------------------------------------------------------------------------
function fncMainProc($mySqlConnObj){
	global	$SubmitMode;
	//class
	global	$clsCcoCmsuser;

	global	$fdtCmsuserId;
	global	$fdtCmsuserLoginId;
	global	$fdtCmsuserName;
	global	$fdtCmsuserPassword;
	global	$fdtCmsuserRequestMonth;
	global	$fdtCmsuserPostalCode;
	global	$fdtCmsuserAddress;
	global	$fdtCmsuserCompany;
	global	$fdtCmsuserRequestName;
	global	$fdtCmsuserPhoneNo;
	global	$fdtCmsuserMobilePhoneNo;
	global	$fdtCmsuserMemo;

	//SubmitModeで処理を分岐
	switch($SubmitMode ){
		case 'SELECT':
			//ﾃﾞｰﾀ管理ｸﾗｽの初期化
			$clsCcoCmsuser->clsCcoCmsuserInit($mySqlConnObj,$fdtCmsuserId);
			//ﾌﾟﾛﾊﾟﾃｨGet
			fncCcoCmsuserGetProperty($clsCcoCmsuser);
			break;
		case 'INSERT':
			//登録処理
			fncCcoCmsuserDBInsert($mySqlConnObj,$clsCcoCmsuser);
			//選択状態にする
			$SubmitMode = 'SELECT';
			//ﾃﾞｰﾀ管理ｸﾗｽの初期化
			$clsCcoCmsuser->clsCcoCmsuserInit($mySqlConnObj,$fdtCmsuserId);
			//ﾌﾟﾛﾊﾟﾃｨGet
			fncCcoCmsuserGetProperty($clsCcoCmsuser);
			break;
		case 'UPDATE':
			//更新処理
			fncCcoCmsuserDBUpdate($mySqlConnObj,$clsCcoCmsuser);
			//選択状態にする
			$SubmitMode = 'SELECT';
			//ﾃﾞｰﾀ管理ｸﾗｽの初期化
			$clsCcoCmsuser->clsCcoCmsuserInit($mySqlConnObj,$fdtCmsuserId);
			//ﾌﾟﾛﾊﾟﾃｨGet
			fncCcoCmsuserGetProperty($clsCcoCmsuser);
			break;
		case 'DELETE':
			//削除処理
			fncCcoCmsuserDBDelete($mySqlConnObj,$clsCcoCmsuser);
			//ﾌﾟﾛﾊﾟﾃｨReset
			fncCcoCmsuserResetProperty($clsCcoCmsuser);
			//初期状態にする
			$SubmitMode = 'RESET';
			break;
		case 'RESET':
			//ﾌﾟﾛﾊﾟﾃｨReset
			fncCcoCmsuserResetProperty($clsCcoCmsuser);
			//初期状態にする
			$SubmitMode = 'RESET';
			break;
		default:
			break;
	}
	//ﾌｫｰﾑの表示
	fncMainForm($mySqlConnObj);
}//end function

// ------------------------------------------------------------------------------
//      MAIN FORM
//          fncMainForm($mySqlConnObj)
// ------------------------------------------------------------------------------
function fncMainForm($mySqlConnObj){
	global	$ThisPHP,$SubmitMode;
	global	$fdtCmsuserId;
	global	$fdtCmsuserLoginId;
	global	$fdtCmsuserName;
	global	$fdtCmsuserPassword;
	global	$fdtCmsuserRequestMonth;
	global	$fdtCmsuserPostalCode;
	global	$fdtCmsuserAddress;
	global	$fdtCmsuserCompany;
	global	$fdtCmsuserRequestName;
	global	$fdtCmsuserPhoneNo;
	global	$fdtCmsuserMobilePhoneNo;
	global	$fdtCmsuserMemo;

	//css powerd by Bootstrap ver3
	print <<<END_OF_HTML

	<div class="wrap" >
	
		<div class="container-fluid">
		
		<!-- ﾒﾆｭｰ 制御ﾎﾞﾀﾝ 表示ROW -->
			<div class="row">
				<div class="col-sm-12">
				<legend class="form-inline submenu">
				<ul class="list-inline">
				  <li>
END_OF_HTML;

	//管理メニュー ﾄﾞﾛｯﾌﾟﾀﾞｳﾝﾒﾆｭｰ
	$TargetForm = 'frmCcoCmsuser';
	include_once("./include/swMgrDropDownMenu.php");

	
    //SubmitModeで表示するボタンを制御する
	switch($SubmitMode){
		case 'SELECT':
			print <<<END_OF_HTML
			 	</li>
			 	<li>
			        <input type="button" value="RESET"
			            id="btnReset"
			            onclick="fncCcoCmsuserSubmit(frmCcoCmsuser,'RESET','FALSE','');"
			            class="btn btn-default btn-sm">
			        <span style="width: 120px;"></span>
			        <input type="button" value="INSERT"
			            id="btnInsert"
			            onclick="fncCcoCmsuserSubmit(frmCcoCmsuser,'INSERT','TRUE','新規登録');"
			            class="btn btn-warning btn-sm">
			        <input type="button" value="UPDATE"
			            id="btnUpdate"
			            onclick="fncCcoCmsuserSubmit(frmCcoCmsuser,'UPDATE','TRUE','更新');"
			            class="btn btn-warning btn-sm">
			        <input type="button" value="DELETE"
			            id="btnDelete"
			            onclick="fncCcoCmsuserSubmit(frmCcoCmsuser,'DELETE','TRUE','削除');"
			            class="btn btn-danger btn-sm">
			
				</li>
END_OF_HTML;

	
			break;
        default:
			print <<<END_OF_HTML
			    </li>
			 	<li>
			        <input type="button" value="RESET"
			            id="btnReset"
			            onclick="fncCcoCmsuserSubmit(frmCcoCmsuser,'RESET','FALSE','');"
			            class="btn btn-default btn-sm">
			        <input type="button" value="INSERT"
			            id="btnInsert"
			            onclick="fncCcoCmsuserSubmit(frmCcoCmsuser,'INSERT','TRUE','新規登録');"
			            class="btn btn-warning btn-sm">
			
				</li>
END_OF_HTML;
            break;
    }
	print <<<END_OF_HTML

					</ul>
					</legend>
    			</div><!-- col-sm-12 end -->
    		</div><!-- row end -->
    		<!-- ﾒﾆｭｰ 制御ﾎﾞﾀﾝ 表示ROW ここまで-->

    		<!-- ﾌｫｰﾑ と ﾘｽﾄ -->
    		<div class="row row-0">
				<div class="col-sm-7 row-0">
					<form class="form-horizontal" role="form" name="frmCcoCmsuser" id="frmCcoCmsuser" method="POST" action="$ThisPHP">
						<input type="hidden" name="SubmitMode" id="SubmitMode" value="$SubmitMode">

END_OF_HTML;

	print <<<END_OF_HTML

							<div class="form-group row-0">
								<label for="fdtCmsuserId" class="control-label col-xs-3">ユーザーID</label>
								<div class="col-xs-8">
									<input type="number"
										class="form-control input-sm text-right"
										id="fdtCmsuserId" name="fdtCmsuserId"
										value="$fdtCmsuserId"
										onkeyup="AjaxFunc_AddFigure(frmCcoCmsuser,'fdtCmsuserId');"
										placeholder="ユーザーID">
								</div>
							</div>
END_OF_HTML;
	print <<<END_OF_HTML

							<div class="form-group row-0">
								<label for="fdtCmsuserLoginId" class="control-label col-xs-3">ログインID</label>
								<div class="col-xs-8">
									<input type="text" 
										class="form-control input-sm"
										id="fdtCmsuserLoginId" name="fdtCmsuserLoginId"
										value="$fdtCmsuserLoginId"
										placeholder="ログインID">
								</div>
							</div>
END_OF_HTML;
	print <<<END_OF_HTML

							<div class="form-group row-0">
								<label for="fdtCmsuserName" class="control-label col-xs-3">ユーザー名</label>
								<div class="col-xs-8">
									<input type="text" 
										class="form-control input-sm"
										id="fdtCmsuserName" name="fdtCmsuserName"
										value="$fdtCmsuserName"
										placeholder="ユーザー名">
								</div>
							</div>
END_OF_HTML;
	print <<<END_OF_HTML

							<div class="form-group row-0">
								<label for="fdtCmsuserPassword" class="control-label col-xs-3">パスワード</label>
								<div class="col-xs-8">
									<input type="text" 
										class="form-control input-sm"
										id="fdtCmsuserPassword" name="fdtCmsuserPassword"
										value="$fdtCmsuserPassword"
										placeholder="パスワード">
								</div>
							</div>
END_OF_HTML;
	print <<<END_OF_HTML

							<div class="form-group row-0">
								<label for="fdtCmsuserRequestMonth" class="control-label col-xs-3">請求月</label>
								<div class="col-xs-8">
								
END_OF_HTML;

	//------------------------------------------------------------
	//CSVﾃﾞｰﾀからselect box を作成する
	$ObjName = "fdtCmsuserRequestMonth";		//select box の名称.ID
	//ここでfunctionからCSVﾃﾞｰﾀを取得
	$csvArray = swFunc_MakeSelectItemsCsvSample();	//CSVﾃﾞｰﾀ
	$default = "$fdtCmsuserRequestMonth";		//ﾃﾞﾌｫﾙﾄ値
	$onChange = '';					//onChange で起動する javascript or jQuery
	$ViewCode = FALSE;				//ｺｰﾄﾞを表示する場合はTRUE
	//SelectBoxHtml出力
	print swFunc_MakeSelectBox($ObjName,$csvArray,$default,$onChange,$ViewCode);
//------------------------------------------------------------

	print <<<END_OF_HTML

								</div>
							</div>
END_OF_HTML;
	print <<<END_OF_HTML

							<div class="form-group row-0">
								<label for="fdtCmsuserPostalCode" class="control-label col-xs-3">郵便番号</label>
								<div class="col-xs-8">
									<input type="text" 
										class="form-control input-sm"
										id="fdtCmsuserPostalCode" name="fdtCmsuserPostalCode"
										value="$fdtCmsuserPostalCode"
										placeholder="郵便番号">
								</div>
							</div>
END_OF_HTML;
	print <<<END_OF_HTML

							<div class="form-group row-0">
								<label for="fdtCmsuserAddress" class="control-label col-xs-3">住所</label>
								<div class="col-xs-8">
									<input type="text" 
										class="form-control input-sm"
										id="fdtCmsuserAddress" name="fdtCmsuserAddress"
										value="$fdtCmsuserAddress"
										placeholder="住所">
								</div>
							</div>
END_OF_HTML;
	print <<<END_OF_HTML

							<div class="form-group row-0">
								<label for="fdtCmsuserCompany" class="control-label col-xs-3">会社名</label>
								<div class="col-xs-8">
									<input type="text" 
										class="form-control input-sm"
										id="fdtCmsuserCompany" name="fdtCmsuserCompany"
										value="$fdtCmsuserCompany"
										placeholder="会社名">
								</div>
							</div>
END_OF_HTML;
	print <<<END_OF_HTML

							<div class="form-group row-0">
								<label for="fdtCmsuserRequestName" class="control-label col-xs-3">請求者名</label>
								<div class="col-xs-8">
									<input type="text" 
										class="form-control input-sm"
										id="fdtCmsuserRequestName" name="fdtCmsuserRequestName"
										value="$fdtCmsuserRequestName"
										placeholder="請求者名">
								</div>
							</div>
END_OF_HTML;
	print <<<END_OF_HTML

							<div class="form-group row-0">
								<label for="fdtCmsuserPhoneNo" class="control-label col-xs-3">電話番号</label>
								<div class="col-xs-8">
									<input type="text" 
										class="form-control input-sm"
										id="fdtCmsuserPhoneNo" name="fdtCmsuserPhoneNo"
										value="$fdtCmsuserPhoneNo"
										placeholder="電話番号">
								</div>
							</div>
END_OF_HTML;
	print <<<END_OF_HTML

							<div class="form-group row-0">
								<label for="fdtCmsuserMobilePhoneNo" class="control-label col-xs-3">電話番号（携帯）</label>
								<div class="col-xs-8">
									<input type="text" 
										class="form-control input-sm"
										id="fdtCmsuserMobilePhoneNo" name="fdtCmsuserMobilePhoneNo"
										value="$fdtCmsuserMobilePhoneNo"
										placeholder="電話番号（携帯）">
								</div>
							</div>
END_OF_HTML;
	print <<<END_OF_HTML

							<div class="form-group row-0">
								<label for="fdtCmsuserMemo" class="control-label col-xs-3">メモ</label>
								<div class="col-xs-8">
									<textarea placeholder="メモ" 
										class="form-control input-sm"
										id="fdtCmsuserMemo" name="fdtCmsuserMemo"
										rows="3"
										id="InputTextarea">$fdtCmsuserMemo</textarea>
								</div>
							</div>
END_OF_HTML;
	print <<<END_OF_HTML

						</form>
					</div>
					<div class="col-sm-5" >
	                	<!-- リストの表示位置 -->
	                    <span id="side_list_area"></span>
					</div>
				</div>
			</div>
 
		</div><!--container-->
	</div><!--wrap-->


    <script type="text/javascript" src="./ajax/ajaxCcoCmsuser.js"></script>
    <script type="text/javascript">
        fncMakeCcoCmsuserList(frmCcoCmsuser);
    </script>


	<!--footer表示位置--><span class="sw-footer"></span>
	<script language="JavaScript">
		AjaxFunc_FooterLoad();
	</script>
	
  </body>
</html>

END_OF_HTML;

}//end function

// ------------------------------------------------------------------------------
//      ﾌﾟﾛﾊﾟﾃｨReset
//          fncCcoCmsuserResetProperty($clsCcoCmsuser)
// ------------------------------------------------------------------------------
function fncCcoCmsuserResetProperty($clsCcoCmsuser){
	global	$fdtCmsuserId;
	global	$fdtCmsuserLoginId;
	global	$fdtCmsuserName;
	global	$fdtCmsuserPassword;
	global	$fdtCmsuserRequestMonth;
	global	$fdtCmsuserPostalCode;
	global	$fdtCmsuserAddress;
	global	$fdtCmsuserCompany;
	global	$fdtCmsuserRequestName;
	global	$fdtCmsuserPhoneNo;
	global	$fdtCmsuserMobilePhoneNo;
	global	$fdtCmsuserMemo;

	//ﾌﾟﾛﾊﾟﾃｨReset
	$fdtCmsuserId = "";                       //ユーザーID
	$fdtCmsuserLoginId = "";                  //ログインID
	$fdtCmsuserName = "";                     //ユーザー名
	$fdtCmsuserPassword = "";                 //パスワード
	$fdtCmsuserRequestMonth = "";             //請求月
	$fdtCmsuserPostalCode = "";               //郵便番号
	$fdtCmsuserAddress = "";                  //住所
	$fdtCmsuserCompany = "";                  //会社名
	$fdtCmsuserRequestName = "";              //請求者名
	$fdtCmsuserPhoneNo = "";                  //電話番号
	$fdtCmsuserMobilePhoneNo = "";            //電話番号（携帯）
	$fdtCmsuserMemo = "";                     //メモ
}//end function

// ------------------------------------------------------------------------------
//      ﾌﾟﾛﾊﾟﾃｨGet
//          fncCcoCmsuserGetProperty($clsCcoCmsuser)
// ------------------------------------------------------------------------------
function fncCcoCmsuserGetProperty($clsCcoCmsuser){
	global	$fdtCmsuserId;
	global	$fdtCmsuserLoginId;
	global	$fdtCmsuserName;
	global	$fdtCmsuserPassword;
	global	$fdtCmsuserRequestMonth;
	global	$fdtCmsuserPostalCode;
	global	$fdtCmsuserAddress;
	global	$fdtCmsuserCompany;
	global	$fdtCmsuserRequestName;
	global	$fdtCmsuserPhoneNo;
	global	$fdtCmsuserMobilePhoneNo;
	global	$fdtCmsuserMemo;

	//ﾌﾟﾛﾊﾟﾃｨGet
	$fdtCmsuserId = $clsCcoCmsuser->clsCcoCmsuserGetCmsuserId();              //ユーザーID
	$fdtCmsuserLoginId = $clsCcoCmsuser->clsCcoCmsuserGetCmsuserLoginId();    //ログインID
	$fdtCmsuserName = $clsCcoCmsuser->clsCcoCmsuserGetCmsuserName();          //ユーザー名
	$fdtCmsuserPassword = $clsCcoCmsuser->clsCcoCmsuserGetCmsuserPassword();  //パスワード
	$fdtCmsuserRequestMonth = $clsCcoCmsuser->clsCcoCmsuserGetCmsuserRequestMonth();//請求月
	$fdtCmsuserPostalCode = $clsCcoCmsuser->clsCcoCmsuserGetCmsuserPostalCode();//郵便番号
	$fdtCmsuserAddress = $clsCcoCmsuser->clsCcoCmsuserGetCmsuserAddress();    //住所
	$fdtCmsuserCompany = $clsCcoCmsuser->clsCcoCmsuserGetCmsuserCompany();    //会社名
	$fdtCmsuserRequestName = $clsCcoCmsuser->clsCcoCmsuserGetCmsuserRequestName();//請求者名
	$fdtCmsuserPhoneNo = $clsCcoCmsuser->clsCcoCmsuserGetCmsuserPhoneNo();    //電話番号
	$fdtCmsuserMobilePhoneNo = $clsCcoCmsuser->clsCcoCmsuserGetCmsuserMobilePhoneNo();//電話番号（携帯）

	//メモは<br>を\nに変換する
	$fdtCmsuserMemo = $clsCcoCmsuser->clsCcoCmsuserGetCmsuserMemo();          //メモ
	$fdtCmsuserMemo = str_replace("<br>","\n",$fdtCmsuserMemo);

}//end function

// ------------------------------------------------------------------------------
//      ﾌﾟﾛﾊﾟﾃｨSet
//          fncCcoCmsuserSetProperty($clsCcoCmsuser)
// ------------------------------------------------------------------------------
function fncCcoCmsuserSetProperty($clsCcoCmsuser){
	global	$fdtCmsuserId;
	global	$fdtCmsuserLoginId;
	global	$fdtCmsuserName;
	global	$fdtCmsuserPassword;
	global	$fdtCmsuserRequestMonth;
	global	$fdtCmsuserPostalCode;
	global	$fdtCmsuserAddress;
	global	$fdtCmsuserCompany;
	global	$fdtCmsuserRequestName;
	global	$fdtCmsuserPhoneNo;
	global	$fdtCmsuserMobilePhoneNo;
	global	$fdtCmsuserMemo;

	//ﾌﾟﾛﾊﾟﾃｨSet

	//ユーザーIDはｶﾝﾏを取り除いてからSetする
	$fdtCmsuserId = str_replace(",","",$fdtCmsuserId);
	$clsCcoCmsuser->clsCcoCmsuserSetCmsuserId($fdtCmsuserId);              //ユーザーID

	$clsCcoCmsuser->clsCcoCmsuserSetCmsuserLoginId($fdtCmsuserLoginId);    //ログインID
	$clsCcoCmsuser->clsCcoCmsuserSetCmsuserName($fdtCmsuserName);          //ユーザー名
	$clsCcoCmsuser->clsCcoCmsuserSetCmsuserPassword($fdtCmsuserPassword);  //パスワード
	$clsCcoCmsuser->clsCcoCmsuserSetCmsuserRequestMonth($fdtCmsuserRequestMonth);//請求月
	$clsCcoCmsuser->clsCcoCmsuserSetCmsuserPostalCode($fdtCmsuserPostalCode);//郵便番号
	$clsCcoCmsuser->clsCcoCmsuserSetCmsuserAddress($fdtCmsuserAddress);    //住所
	$clsCcoCmsuser->clsCcoCmsuserSetCmsuserCompany($fdtCmsuserCompany);    //会社名
	$clsCcoCmsuser->clsCcoCmsuserSetCmsuserRequestName($fdtCmsuserRequestName);//請求者名
	$clsCcoCmsuser->clsCcoCmsuserSetCmsuserPhoneNo($fdtCmsuserPhoneNo);    //電話番号
	$clsCcoCmsuser->clsCcoCmsuserSetCmsuserMobilePhoneNo($fdtCmsuserMobilePhoneNo);//電話番号（携帯）

	//メモは改行を<br>にしてからSetする
	$fdtCmsuserMemo = str_replace("\r\n","<br>",$fdtCmsuserMemo);
	$fdtCmsuserMemo = str_replace("\r","<br>",$fdtCmsuserMemo);
	$fdtCmsuserMemo = str_replace("\n","<br>",$fdtCmsuserMemo);
	$clsCcoCmsuser->clsCcoCmsuserSetCmsuserMemo($fdtCmsuserMemo);          //メモ

}//end function

// ------------------------------------------------------------------------------
//      DB INSERT
//          fncCcoCmsuserDBInsert($mySqlConnObj,$clsCcoCmsuser)
// ------------------------------------------------------------------------------
function fncCcoCmsuserDBInsert($mySqlConnObj,$clsCcoCmsuser){
	global	$fdtCmsuserId;

	//ﾌﾟﾛﾊﾟﾃｨｾｯﾄ
	fncCcoCmsuserSetProperty($clsCcoCmsuser);
	//ﾃﾞｰﾀ管理ｸﾗｽ DbInsert
	$fdtCmsuserId = $clsCcoCmsuser->clsCcoCmsuserDbInsert($mySqlConnObj);
}//end function

// ------------------------------------------------------------------------------
//      DB UPDATE
//          fncCcoCmsuserDBUpdate($mySqlConnObj,$clsCcoCmsuser)
// ------------------------------------------------------------------------------
function fncCcoCmsuserDBUpdate($mySqlConnObj,$clsCcoCmsuser){
	//ﾌﾟﾛﾊﾟﾃｨｾｯﾄ
	fncCcoCmsuserSetProperty($clsCcoCmsuser);
	//ﾃﾞｰﾀ管理ｸﾗｽ DbUpdate
	$clsCcoCmsuser->clsCcoCmsuserDbUpdate($mySqlConnObj);
}//end function

// ------------------------------------------------------------------------------
//      DB DELETE
//          fncCcoCmsuserDBDelete($mySqlConnObj,$clsCcoCmsuser)
// ------------------------------------------------------------------------------
function fncCcoCmsuserDBDelete($mySqlConnObj,$clsCcoCmsuser){
	//ﾌﾟﾛﾊﾟﾃｨｾｯﾄ
	fncCcoCmsuserSetProperty($clsCcoCmsuser);
	//ﾃﾞｰﾀ管理ｸﾗｽ DbDelete
	$clsCcoCmsuser->clsCcoCmsuserDbDelete($mySqlConnObj);
}//end function


// -----------------------------------------------------------
?>