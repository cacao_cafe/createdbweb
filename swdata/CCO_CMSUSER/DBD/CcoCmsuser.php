<?php
// -----------------------------------------------------------
//
// Copyright (C) 2016 ScenarioWriterCafe All Rights Reserved.
// 
//     CREATE TABLE CCO_CMSUSER
//
//     createTable_CcoCmsuser.php
// -----------------------------------------------------------
$Result = fncCreateTable();
echo $Result;
exit;


// -----------------------------------------------------------
//     fncCreateTable()
// -----------------------------------------------------------
function fncCreateTable(){
	//共通定数
	include_once("../../sw_config/swConstant.php");

	//MySQLに接続
	include_once("../include/ConnectMySQL.php");

	//DROP TABLE SQLを編集
	$strSQL = <<<END_OF_SQL

DROP TABLE IF EXISTS CCO_CMSUSER;

END_OF_SQL;

	//DROP TABLE EXECUTE 
	$stmt = $mySqlConnObj->prepare($strSQL);
	$stmt->execute();

	//CREATE TABLE SQLを編集
	$strSQL = <<<END_OF_SQL

CREATE TABLE CCO_CMSUSER(
	  CMSUSER_ID                       INT NOT NULL AUTO_INCREMENT COMMENT 'ユーザーID'
	, CMSUSER_LOGIN_ID                 VARCHAR(256)  COMMENT 'ログインID'
	, CMSUSER_NAME                     VARCHAR(256)  COMMENT 'ユーザー名'
	, CMSUSER_PASSWORD                 VARCHAR(256)  COMMENT 'パスワード'
	, CMSUSER_REQUEST_MONTH            INT  COMMENT '請求月'
	, CMSUSER_POSTAL_CODE              VARCHAR(8)  COMMENT '郵便番号'
	, CMSUSER_ADDRESS                  VARCHAR(256)  COMMENT '住所'
	, CMSUSER_COMPANY                  VARCHAR(256)  COMMENT '会社名'
	, CMSUSER_REQUEST_NAME             VARCHAR(256)  COMMENT '請求者名'
	, CMSUSER_PHONE_NO                 VARCHAR(13)  COMMENT '電話番号'
	, CMSUSER_MOBILE_PHONE_NO          VARCHAR(13)  COMMENT '電話番号（携帯）'
	, CMSUSER_MEMO                     TEXT(50)  COMMENT 'メモ'
	, PRIMARY KEY (CMSUSER_ID)
	)DEFAULT CHARACTER SET 'UTF8'
	COMMENT 'CMS_USER';

END_OF_SQL;

	//CREATE TABLE EXECUTE 
	$stmt = $mySqlConnObj->prepare($strSQL);
	$stmt->execute();

	//UNIQUE INDEX のSQLを編集
	$strSQL = <<<END_OF_SQL

	CREATE UNIQUE INDEX CCO_CMSUSER_KEY_CMSUSER_ID ON CCO_CMSUSER(CMSUSER_ID);
END_OF_SQL;

	//CREATE INDEX EXECUTE 
	$stmt = $mySqlConnObj->prepare($strSQL);
	$stmt->execute();


	//DB切断
	include_once("../include/DisConnectMySQL.php");
	
	
	return	'CREATE TABLE CCO_CMSUSER Success...';
}//end function
// -----------------------------------------------------------
?>